# Milestones

[[_TOC_]]

## Undecided

* Platforms
    * iOS
    * Web
    * Tablet support
    * Voice actions
* Features
    * Plant library
    * Weather
    * Action frequency learning

## v0.2

PlantyPlants v0.2 is the Android Fishfood app.

### Features

* (flutter) Android
    * Watering My Plant action
        * Schedule
        * Notifications
        * Pending
        * History
* Website
    * Landing page to present the app

### Infrastructure

TODO

### Assumptions

* Potential users want a mobile app to organize their plants
    * Metric: Feedback from fishfooders
* Potential users are using a smartphone
* Potential users are using Android
* Potential users have a Google account and want to use it for PlantyPlants

### Market

Specialized task management app

### Cost

* Human: ?
* Infrastructure: CHF0
    * Firebase: CHF0
* Marketing: CHF0
* Other: CHF0


* Avg revenue per user: CHF0
* Avg expense per user: CHF0

### Other risks

* Infrastructure costs

## v0.1

PlantyPlants v0.1 is an internal unreleased version with stacks POCs.

### Features

* Android
    * My Plants list (per user)
        * Show, add, edit, remove
        * Upload image
* POC website
    * landing page with material design
    * Plant library
* POC Background tasks
    * My Plant actions

### Other risks

* Infrastructure costs

### Market

No market
*  Basic notes app is deemed not enough
