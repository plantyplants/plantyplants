import 'dart:async';
import 'package:file/local.dart';
import 'package:dotenv/dotenv.dart' show load, env;

import 'package:dartbase_admin/dartbase.dart';
import 'package:angel_framework/angel_framework.dart';
import 'package:angel_jael/angel_jael.dart';
import 'package:angel_hot/angel_hot.dart';
import 'package:angel_firestore/angel_firestore.dart';

main() async {
  var hot = HotReloader(createServer, [
    'bin/main.dart',
    Uri.parse('package:angel_hot/angel_hot.dart')
  ]);
  await hot.startServer('localhost', 3000);
}

Future<Angel> createServer() async {
  var app = Angel();
  var fileSystem = const LocalFileSystem();

  await app.configure(
    jael(fileSystem.directory('views')),
  );

  load();
  var firebase = await Firebase.initialize(env['FIREBASE_PROJECT_ID'],
      await ServiceAccount.fromFile(env['FIREBASE_SERVICE_ACCOUNT_PATH']));
  var firestore = Firestore(firebase: firebase);
  var collection = firestore.collection('public_plants');
  var service = FirestoreClientService(collection);

  app.get('/', (req, res) async {
    return await res.render('welcome', {
      'title': 'Welcome',
    });
  });

  app.get('/plant-library', (req, res) async {
    var plants = await service.index();
    plants.forEach((plant) {
      plant['detailsUrl'] = '/plant-library/${plant['name']}';
    });
    return await res.render('plant_library_list', {
      'title': 'Plant Library',
      'plants': plants,
    });
  });

  app.get('/plant-library/:name', (req, res) async {
    var name = Uri.decodeQueryComponent(req.params['name']);
    var plant = await service.findOne({'query': {'name': name}});
    return await res.render('plant_library_details', {
      'title': name,
      'plant': plant,
    });
  });

  return app;
}
