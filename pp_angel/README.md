# pp_angel

Server-side website for plantyplants using angeldart.

## Usage

* Launch server: `dart --observe bin/main.dart`
  * Note: `--observe` is needed because hot reload depends on the Dart VM service.

## Configuration

In ``.env``:

```
FIREBASE_PROJECT_ID = 'Firebase Project ID'
FIREBASE_SERVICE_ACCOUNT_PATH = 'service-account.json'
```
