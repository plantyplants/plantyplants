import 'package:test/test.dart';

class AppContext {
  dynamic router;
  dynamic po;
  dynamic fixture;

  AppContext(this.router, this.po, this.fixture);
}

class AppCommonTests {
  AppContext appContext;

  AppCommonTests(this.appContext);

  void welcomeTests() {
    test('page title', () {
      expect(appContext.po.pageBrand, 'Planty Plants');
    });

    test('route', () {
      expect(appContext.router.current.path, '/welcome');
    });

    test('tab is showing', () {
      expect(appContext.po.welcomeTabIsActive, isTrue);
    });

    // Doesn't finish
    //test('clicking on logo goes to welcome', () async {
    //  await appContext.po.selectBrandTitle();
    //  await appContext.fixture.update();
    //  expect(appContext.router.current.path, '/welcome');
    //  expect(appContext.po.welcomeTabIsActive, isTrue);
    //});
  }
}
