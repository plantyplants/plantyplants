@TestOn('browser')
library plants_test;

import 'package:test/test.dart';

import 'app_authenticated.dart' as app_authenticated;
import 'app_unauthenticated.dart' as app_unauthenticated;
import 'login_authenticated.dart' as login_authenticated;
import 'login_unauthenticated.dart' as login_unauthenticated;
import 'myplant_authenticated.dart' as myplant_authenticated;
import 'myplants_list_authenticated.dart' as myplants_list_authenticated;
import 'navbar_authenticated.dart' as navbar_authenticated;
import 'navbar_unauthenticated.dart' as navbar_unauthenticated;

void main() {
  group('app authenticated:', app_authenticated.main);
  group('app unauthenticated:', app_unauthenticated.main);
  group('login authenticated:', login_authenticated.main);
  group('login unauthenticated:', login_unauthenticated.main);
  group('myplant authenticated:', myplant_authenticated.main);
  group('myplants_list authenticated:', myplants_list_authenticated.main);
  group('navbar authenticated:', navbar_authenticated.main);
  group('navbar unauthenticated:', navbar_unauthenticated.main);
}
