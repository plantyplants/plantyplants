import 'package:angular_router/angular_router.dart';
import 'package:collection/collection.dart';
import 'package:test/test.dart';

IsNavParams isNavParams([dynamic expected]) => IsNavParams(expected);

class IsNavParams extends Matcher {
  NavigationParams _expected;
  IsNavParams([NavigationParams expected]) {
    _expected = expected == null ? NavigationParams() : expected;
  }

  @override
  bool matches(dynamic item, Map<dynamic, dynamic> matchState) =>
      item is NavigationParams &&
      _expected.fragment == item.fragment &&
      // ignore: always_specify_types
      const MapEquality()
          .equals(_expected.queryParameters, item.queryParameters) &&
      _expected.updateUrl == item.updateUrl;

  @override
  Description describe(Description description) =>
      description.add('NavigationParams(${_expected.queryParameters}, '
          '${_expected.fragment}, ${_expected.updateUrl})');
}
