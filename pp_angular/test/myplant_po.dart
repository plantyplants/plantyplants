import 'dart:async';

import 'package:pageloader/pageloader.dart';
import 'package:pageloader/webdriver.dart';
import 'utils.dart';

part 'myplant_po.g.dart';

@PageObject()
abstract class MyPlantDetailPO {
  MyPlantDetailPO();
  factory MyPlantDetailPO.create(PageLoaderElement context) =
      $MyPlantDetailPO.create;

  @ByTagName('h2')
  PageLoaderElement get title;

  @ById('inputName')
  PageLoaderElement get name;

  @ById('srName')
  PageLoaderElement get srName;

  @ById('inputDescription')
  PageLoaderElement get _description;

  @ById('srDescription')
  PageLoaderElement get srDescription;

  @ByTagName('button')
  @WithVisibleText('Back')
  PageLoaderElement get _back;

  @ByTagName('button')
  @WithVisibleText('Save')
  PageLoaderElement get saveButton;

  @ByTagName('button')
  @WithVisibleText('Edit')
  PageLoaderElement get editButton;

  @ByTagName('button')
  @WithVisibleText('Delete')
  PageLoaderElement get deleteButton;

  @ByClass('plant-title-link')
  @WithVisibleText('Rose')
  PageLoaderElement get plantRose;

  Map<String, String> plantFromDetails(String id) {
    if (!srName.exists) {
      return null;
    }

    return plantData(id, srName.visibleText, srDescription.visibleText);
  }

  Future<void> clearName() => name.clear();
  Future<void> typeName(String s) => name.type(s);
  Future<void> clearDescription() => _description.clear();
  Future<void> typeDescription(String s) => _description.type(s);

  Future<void> back() => _back.click();
  Future<void> save() => saveButton.click();
  Future<void> edit() => editButton.click();
  Future<void> delete() => deleteButton.click();
}
