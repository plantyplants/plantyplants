import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:mockito/mockito.dart';

const List<dynamic> routerProvidersForTesting = <dynamic>[
  ValueProvider.forToken(appBaseHref, '/'),
  routerProviders,
  // Mock platform location even with real router, otherwise sometimes tests
  // hang.
  ClassProvider(PlatformLocation, useClass: MockPlatformLocation),
];

//-----------------------------------------------------------------------------

class InjectorProbe {
  final InjectorFactory _parent;
  Injector _injector;

  InjectorProbe(this._parent);

  InjectorFactory get factory => _factory;
  Injector get injector => _injector ??= _factory();

  Injector _factory([Injector parent]) => _injector = _parent(parent);
  T get<T>(dynamic token) => injector?.get(token);
}

//-----------------------------------------------------------------------------

class MockRouter extends Mock implements Router {}

class MockPlatformLocation extends Mock implements PlatformLocation {
  String _url;

  @override
  String get hash => '';

  @override
  String get pathname => _url ?? '';

  @override
  String get search => '';

  @override
  void pushState(dynamic state, String title, String url) => _url = url;
}

class MockLocation extends Mock implements Location {}

class MockRouterState extends Mock implements RouterState {}

//-----------------------------------------------------------------------------
// Misc helper functions

Map<String, dynamic> plantData(
        String idAsString, String name, String description) =>
    <String, String>{
      'id': idAsString,
      'name': name,
      'description': description
    };

Stream<T> inIndexOrder<T>(Iterable<Future<T>> futures) async* {
  for (dynamic x in futures) {
    yield await x;
  }
}
