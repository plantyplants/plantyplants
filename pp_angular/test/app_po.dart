import 'dart:async';

import 'package:pageloader/pageloader.dart';

part 'app_po.g.dart';

@PageObject()
abstract class AppPO {
  AppPO();
  factory AppPO.create(PageLoaderElement context) = $AppPO.create;

  @ById('brandTitle')
  PageLoaderElement get _brand;

  @ByTagName('welcome')
  PageLoaderElement get _welcome;

  String get pageBrand => _brand.visibleText;

  Future<void> selectBrandTitle() => _brand.click();

  bool get welcomeTabIsActive => _welcome.exists;
}
