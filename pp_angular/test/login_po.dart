import 'package:pageloader/pageloader.dart';

part 'login_po.g.dart';

@PageObject()
abstract class LoginPO {
  LoginPO();
  factory LoginPO.create(PageLoaderElement context) = $LoginPO.create;

  @ById('buttonLogin')
  PageLoaderElement get buttonLogin;

  @ById('welcome-user')
  PageLoaderElement get welcomeUser;

  @ById('buttonSignout')
  PageLoaderElement get buttonSignOut;
}
