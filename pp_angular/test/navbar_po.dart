import 'dart:async';

import 'package:pageloader/pageloader.dart';

part 'navbar_po.g.dart';

@PageObject()
abstract class NavbarPO {
  NavbarPO();
  factory NavbarPO.create(PageLoaderElement context) = $NavbarPO.create;

  @ByCss('li.nav-item a')
  List<PageLoaderElement> get _tabLinks;

  @ByTagName('my-plants')
  PageLoaderElement get _myPlants;

  Iterable<String> get tabTitles => _tabLinks.map((el) => el.visibleText);

  Future<void> selectTab(int index) => _tabLinks[index].click();

  bool get plantsTabIsActive => _myPlants.exists;
}
