@TestOn('browser')

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angular_test/angular_test.dart';
import 'package:pp_common/users/blocs/authentication/authentication.dart';
import 'package:pp_angular/app_component.dart';
import 'package:pp_angular/app_component.template.dart' as ng;
import 'package:pp_angular/injectors.dart';
import 'package:pp_angular/firebase_service.dart';
import 'package:pageloader/html.dart';
import 'package:test/test.dart';

import 'app_common.dart';
import 'app_po.dart';
import 'app_unauthenticated.template.dart' as self;
import 'utils.dart';

NgTestFixture<AppComponent> fixture;
AppPO po;
Router router;

@GenerateInjector(<dynamic>[
  // Routing
  routerProvidersForTesting,

  // Firebase
  ClassProvider(FirebaseClient),

  // Users
  userRepositoryProvider,
  authenticationBlocProvider,
  loginBlocProvider,
  ClassProvider(AuthenticationState),

  // MyPlants
  myPlantsRepositoryMockProvider,
  myPlantsBlocProvider,
])
final InjectorFactory rootInjector = self.rootInjector$Injector;

void main() {
  final injector = InjectorProbe(rootInjector);
  final testBed = NgTestBed.forComponent<AppComponent>(ng.AppComponentNgFactory,
      rootInjector: injector.factory);
  final appContext = AppContext(router, po, fixture);
  final common = AppCommonTests(appContext);

  setUp(() async {
    appContext.fixture = await testBed.create();
    appContext.router = injector.get<Router>(Router);
    await appContext.router?.navigate('/');
    await appContext.fixture.update();
    final context =
        HtmlPageLoaderElement.createFromElement(appContext.fixture.rootElement);
    appContext.po = AppPO.create(context);
  });

  tearDown(disposeAnyRunningTest);

  group('Welcome:', common.welcomeTests);
}
