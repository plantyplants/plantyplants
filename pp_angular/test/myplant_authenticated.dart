@TestOn('browser')

import 'package:angular/angular.dart';
import 'package:angular_test/angular_test.dart';
import 'package:pp_common/my_plants/repository/entities.dart';
import 'package:pp_common/my_plants/repository/mock.dart';
import 'package:pp_common/users/blocs/authentication/authentication.dart';
import 'package:pp_common/my_plants/repository/interface.dart';
import 'package:pp_common/my_plants/repository/model.dart';
import 'package:pp_angular/firebase_service.dart';
import 'package:pp_angular/injectors.dart';
import 'package:pageloader/html.dart';
import 'package:pp_common/users/model.dart';
import 'package:test/test.dart';
import 'package:pp_angular/src/myplant_component.dart';
import 'package:pp_angular/src/myplant_component.template.dart' as ng;

import 'myplant_authenticated.template.dart' as self;
import 'myplant_po.dart';
import 'utils.dart';

NgTestFixture<MyPlantComponent> fixture;
MyPlantDetailPO po;

@GenerateInjector(<dynamic>[
  // Testing
  routerProvidersForTesting,

  // Firebase
  ClassProvider(FirebaseClient),

  // Users
  userRepositoryProvider,
  authenticationBlocProvider,
  loginBlocProvider,
  ClassProvider(AuthenticationState),

  // MyPlants
  myPlantsRepositoryMockProvider,
  myPlantsBlocProvider,
])
final InjectorFactory rootInjector = self.rootInjector$Injector;

final MockAuthenticationBloc mockAuthenticationBloc = MockAuthenticationBloc();

void main() {
  final injector = InjectorProbe(rootInjector);
  final testBed = NgTestBed.forComponent<MyPlantComponent>(
      ng.MyPlantComponentNgFactory,
      rootInjector: injector.factory);

  setUp(() async {
    // Recreating MyPlantComponent.onActivate
    fixture = await testBed.create(beforeChangeDetection: ((c) {
      c.authenticationBloc = mockAuthenticationBloc;
      c.authenticationState =
          Authenticated(User(authorIdFake, 'test@test.com', 'test'));
    }));
  });

  tearDown(disposeAnyRunningTest);

  const newPlant = <String, String>{
    'name': 'Olive Tree',
    'description': 'Olive Tree description',
  };

  group('Add new plant ${newPlant['name']}', () {
    setUp(() async {
      final context =
          HtmlPageLoaderElement.createFromElement(fixture.rootElement);
      po = MyPlantDetailPO.create(context);
    });

    test('Add plant with only name', () async {
      // Recreating MyPlantComponent.onActivate
      await fixture.update((c) {
        c.plantId = null;
        c.action = 'new';
        c.plantMutable = MyPlantMutable('', authorIdFake,
            id: null, photoIconUrl: '', description: '');
      });
      await po.typeName(newPlant['name']);
      await po.save();
      await fixture.update();

      final myPlantsRepository =
          injector.get<MyPlantsRepository>(MyPlantsRepository);
      final myPlants = await myPlantsRepository.myPlants(authorIdFake).single;
      final myPlant = myPlants
          .firstWhere((plant) => plant.name == newPlant['name'], orElse: null);
      expect(myPlant.name, newPlant['name']);
    });

    test('Add plant with name and description', () async {
      // Recreating MyPlantComponent.onActivate
      await fixture.update((c) {
        c.plantId = null;
        c.action = 'new';
        c.plantMutable = MyPlantMutable('', authorIdFake,
            id: null, photoIconUrl: '', description: '');
      });
      await po.typeName("${newPlant['name']}2");
      await po.typeDescription(newPlant['description']);
      await po.save();
      await fixture.update();

      final myPlantsRepository =
          injector.get<MyPlantsRepository>(MyPlantsRepository);
      final myPlants = await myPlantsRepository.myPlants(authorIdFake).single;
      final myPlant = myPlants.firstWhere(
          (plant) => plant.name == "${newPlant['name']}2",
          orElse: null);
      expect(myPlant.name, "${newPlant['name']}2");
      expect(myPlant.description, newPlant['description']);
    });
  });

  final targetPlant = myPlantsData[2].toEntity().toMap();

  group('${targetPlant['name']} initial plant:', () {
    const nameSuffix = 'X';
    final updatedPlant = <String, String>{
      'id': targetPlant['id'],
      'name': "${targetPlant['name']}$nameSuffix",
      'description': "${targetPlant['description']}$nameSuffix",
    };

    setUp(() async {
      final context =
          HtmlPageLoaderElement.createFromElement(fixture.rootElement);
      po = MyPlantDetailPO.create(context);
    });

    test('show plant details', () async {
      // Recreating MyPlantComponent.onActivate
      await fixture.update((c) {
        c.plantId = targetPlant['id'];
        c.action = 'view';
        c.plantMutable = MyPlantMutable.fromMyPlant(
            MyPlant.fromEntity(MyPlantEntity.fromMap(targetPlant)));
      });
      final plantMapGot = po.plantFromDetails(targetPlant['id']);
      expect(plantMapGot['id'], targetPlant['id']);
      expect(plantMapGot['name'], targetPlant['name']);
      expect(plantMapGot['description'], targetPlant['description']);
    });

    test('Update plant', () async {
      // Recreating MyPlantComponent.onActivate
      await fixture.update((c) {
        c.plantId = targetPlant['id'];
        c.action = 'edit';
        c.plantMutable = MyPlantMutable.fromMyPlant(
            MyPlant.fromEntity(MyPlantEntity.fromMap(targetPlant)));
      });
      await po.clearName();
      await po.typeName(updatedPlant['name']);
      await po.clearDescription();
      await po.typeDescription(updatedPlant['description']);
      expect(po.plantFromDetails(targetPlant['id']), updatedPlant);
      await po.save();
      await fixture.update();
      final myPlantsRepository =
          injector.get<MyPlantsRepository>(MyPlantsRepository);
      final myPlant =
          await myPlantsRepository.myPlant(targetPlant['id'], authorIdFake);
      expect(myPlant.name, updatedPlant['name']);
      expect(myPlant.description, updatedPlant['description']);
    });

    test('delete plant', () async {
      // Recreating MyPlantComponent.onActivate
      await fixture.update((c) {
        c.plantId = targetPlant['id'];
        c.action = 'edit';
        c.plantMutable = MyPlantMutable.fromMyPlant(
            MyPlant.fromEntity(MyPlantEntity.fromMap(targetPlant)));
      });
      await po.delete();
      await fixture.update();
      final myPlantsRepository =
          injector.get<MyPlantsRepository>(MyPlantsRepository);
      final myPlants = await myPlantsRepository.myPlants(authorIdFake).single;
      expect(myPlants.indexWhere((p) => p.id == targetPlant['id']), -1);
    });
  });
}
