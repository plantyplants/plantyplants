@TestOn('browser')

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angular_test/angular_test.dart';
import 'package:pp_common/users/blocs/authentication/authentication.dart';
import 'package:pp_angular/firebase_service.dart';
import 'package:pp_angular/injectors.dart';
import 'package:pp_angular/src/login_component.dart';
import 'package:pp_angular/src/login_component.template.dart' as ng;
import 'package:pageloader/html.dart';
import 'package:test/test.dart';

import 'login_po.dart';
import 'login_unauthenticated.template.dart' as self;
import 'utils.dart';

NgTestFixture<LoginComponent> fixture;
LoginPO po;

@GenerateInjector(<dynamic>[
  ClassProvider(Router, useClass: MockRouter),
  ClassProvider(FirebaseClient),
  userRepositoryProvider,
  authenticationBlocProvider,
  loginBlocProvider,
  ClassProvider(AuthenticationState),
])
final InjectorFactory rootInjector = self.rootInjector$Injector;

void main() {
  final injector = InjectorProbe(rootInjector);
  final testBed = NgTestBed.forComponent<LoginComponent>(
      ng.LoginComponentNgFactory,
      rootInjector: injector.factory);

  setUp(() async {
    fixture = await testBed.create();
    final context =
        HtmlPageLoaderElement.createFromElement(fixture.rootElement);
    po = LoginPO.create(context);
  });

  tearDown(disposeAnyRunningTest);

  test('login button present', () async {
    expect(po.buttonLogin.exists, isTrue);
    expect(po.buttonLogin.visibleText, 'Login with Google');
  });

  test('no Sign out button', () async {
    expect(po.buttonSignOut.exists, isFalse);
  });

  test('no welcome user', () async {
    expect(po.welcomeUser.exists, isFalse);
  });
}
