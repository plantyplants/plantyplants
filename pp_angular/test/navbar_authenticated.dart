@TestOn('browser')

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angular_test/angular_test.dart';

import 'package:pageloader/html.dart';
import 'package:pp_angular/src/navbar_component.dart';
import 'package:pp_common/users/blocs/login/bloc.dart';
import 'package:test/test.dart';

import 'package:pp_common/users/blocs/authentication/authentication.dart';
import 'package:pp_common/users/blocs/login/login.dart';
import 'package:pp_common/users/model.dart';
import 'package:pp_angular/src/navbar_component.template.dart' as ng;
import 'package:pp_angular/firebase_service.dart';
import 'package:pp_angular/injectors.dart';

import 'navbar_authenticated.template.dart' as self;
import 'navbar_po.dart';
import 'utils.dart';

NgTestFixture<NavbarComponent> fixture;
NavbarPO po;
Router router;
FirebaseService firebase;
AuthenticationState authenticationState;

@GenerateInjector(<dynamic>[
  // Firebase
  ClassProvider(FirebaseClient),
  routerProvidersForTesting,

  // Users
  userRepositoryProvider,
  authenticationBlocProvider,
  loginBlocProvider,
  ClassProvider(AuthenticationState),

  // MyPlants
  myPlantsRepositoryMockProvider,
  myPlantsBlocProvider,
])
final InjectorFactory rootInjector = self.rootInjector$Injector;

void main() {
  final injector = InjectorProbe(rootInjector);
  final testBed = NgTestBed.forComponent<NavbarComponent>(
      ng.NavbarComponentNgFactory,
      rootInjector: injector.factory);

  setUp(() async {
    fixture = await testBed.create(
        beforeChangeDetection: (c) => c.localAuthenticationState =
            Authenticated(
                User('000-000-000', 'example@example.com', 'Example User')));
    router = injector.get<Router>(Router);
    router.navigate('/');
    fixture.update();

    final loginBloc = injector.get<LoginBloc>(LoginBloc);
    loginBloc.add(LoginWithGooglePressed());
    final authBloc = injector.get<AuthenticationBloc>(AuthenticationBloc);
    authBloc.add(LoggedIn());
    final context =
        HtmlPageLoaderElement.createFromElement(fixture.rootElement);
    po = NavbarPO.create(context);
    await fixture.update();
  });

  tearDown(disposeAnyRunningTest);

  test('Basics: tab titles', () {
    final expectTitles = <String>['My Plants'];
    expect(po.tabTitles, expectTitles);
  });

  group('Select Plants:', () {
    setUp(() async {
      await po.selectTab(0);
      await fixture.update();
    });

    test('route', () async {
      await po.selectTab(0);
      await fixture.update();
      expect(router.current.path, '/my_plants');
    });

    test('tab is showing', () {
      expect(po.plantsTabIsActive, isTrue);
    });
  });
}
