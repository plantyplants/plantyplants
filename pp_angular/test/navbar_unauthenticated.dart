@TestOn('browser')

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angular_test/angular_test.dart';
import 'package:pp_common/users/blocs/authentication/authentication.dart';
import 'package:pp_angular/src/navbar_component.dart';
import 'package:pp_angular/src/navbar_component.template.dart' as ng;
import 'package:pp_angular/injectors.dart';
import 'package:pp_angular/firebase_service.dart';
import 'package:pageloader/html.dart';
import 'package:test/test.dart';

import 'navbar_po.dart';
import 'navbar_unauthenticated.template.dart' as self;
import 'utils.dart';

NgTestFixture<NavbarComponent> fixture;
NavbarPO po;
Router router;

@GenerateInjector(<dynamic>[
  ClassProvider(FirebaseClient),
  routerProvidersForTesting,
  userRepositoryProvider,
  authenticationBlocProvider,
  loginBlocProvider,
  ClassProvider(AuthenticationState),
])
final InjectorFactory rootInjector = self.rootInjector$Injector;

void main() {
  final injector = InjectorProbe(rootInjector);
  final testBed = NgTestBed.forComponent<NavbarComponent>(
      ng.NavbarComponentNgFactory,
      rootInjector: injector.factory);

  setUp(() async {
    fixture = await testBed.create();
    final context =
        HtmlPageLoaderElement.createFromElement(fixture.rootElement);
    po = NavbarPO.create(context);
  });

  tearDown(disposeAnyRunningTest);

  group('Basics:', () {
    test('tab titles', () {
      final expectTitles = <String>[];
      expect(po.tabTitles, expectTitles);
    });
  });
}
