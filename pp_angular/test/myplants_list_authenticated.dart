@TestOn('browser')

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angular_test/angular_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pp_common/users/blocs/authentication/authentication.dart';
import 'package:pp_angular/injectors.dart';
import 'package:pp_angular/firebase_service.dart';
import 'package:pp_angular/src/routes.dart';
import 'package:pp_angular/src/myplants_list_component.dart';
import 'package:pp_angular/src/myplants_list_component.template.dart'
    as ng;
import 'package:pp_common/users/model.dart';
import 'package:pp_common/my_plants/blocs/my_plants/my_plants.dart';
import 'package:pp_common/my_plants/repository/mock.dart';
import 'package:pageloader/html.dart';
import 'package:test/test.dart';

import 'matchers.dart';
import 'myplants_list_authenticated.template.dart' as self;
import 'myplants_list_po.dart';
import 'utils.dart';

NgTestFixture<MyPlantsListComponent> fixture;
MyPlantsListPO po;

@GenerateInjector(<dynamic>[
  // Routing
  ValueProvider.forToken(appBaseHref, '/'),
  ClassProvider(Routes),
  routerProviders,
  ClassProvider(Router, useClass: MockRouter),

  // Firebase
  ClassProvider(FirebaseClient),

  // Users
  userRepositoryProvider,
  authenticationBlocProvider,
  loginBlocProvider,
  ClassProvider(AuthenticationState),

  // MyPlants
  myPlantsRepositoryProvider,
  myPlantsBlocProvider,
])
final InjectorFactory rootInjector = self.rootInjector$Injector;

void main() {
  final injector = InjectorProbe(rootInjector);
  final testBed = NgTestBed.forComponent<MyPlantsListComponent>(
      ng.MyPlantsListComponentNgFactory,
      rootInjector: injector.factory);

  setUp(() async {
    // Recreating MyPlantComponent.onActivate
    fixture = await testBed.create(beforeChangeDetection: ((c) {
      c.authenticationBloc = mockAuthenticationBloc;
      c.authenticationState =
          Authenticated(User(authorIdFake, 'test@test.com', 'test'));
    }));
    final context =
        HtmlPageLoaderElement.createFromElement(fixture.rootElement);
    po = MyPlantsListPO.create(context);
    await fixture.update((c) => c.state = MyPlantsLoaded(myPlantsData));
  });

  tearDown(disposeAnyRunningTest);

  test('show plants', () {
    final expectedNames = <String>[
      'Tulip',
      'Magnolia',
      'Rose',
      'Apple tree',
      'Olive Tree',
      'Olive Tree2',
    ];
    expect(po.plantNames, expectedNames);
  });

  test('show Magnolia description', () {
    final description = List<String>.from(po.plantDescriptions)[1];
    expect(description, 'Magnificient Magnolia tree');
  });

  test('select plant and navigate to detail', () async {
    final Router mockRouter = injector.get<MockRouter>(Router);
    clearInteractions(mockRouter);
    await po.selectPlant(3);
    final c = verify(mockRouter.navigate(captureAny, captureAny));
    expect(c.captured[0], '/my_plants/000-000-004/view');
    expect(c.captured[1], isNavParams()); // empty params
    expect(c.captured.length, 2);
  });
}
