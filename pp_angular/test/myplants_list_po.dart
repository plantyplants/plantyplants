import 'dart:async';

import 'package:pageloader/pageloader.dart';

part 'myplants_list_po.g.dart';

@PageObject()
abstract class MyPlantsListPO {
  MyPlantsListPO();
  factory MyPlantsListPO.create(PageLoaderElement context) =
      $MyPlantsListPO.create;

  @First(ByCss('h1'))
  PageLoaderElement get _title;

  @ByTagName('a.plant-title-link')
  List<PageLoaderElement> get _plants;

  String get title => _title.visibleText;

  @ByTagName('p.plant-description')
  List<PageLoaderElement> get _plantDescriptions;

  @ById('AddNewPlantButton')
  PageLoaderElement get _newPlantButton;

  Iterable<String> get plantNames => _plants.map((el) => el.visibleText);

  Iterable<String> get plantDescriptions =>
      _plantDescriptions.map((el) => el.visibleText);

  Future<void> selectPlant(int index) => _plants[index].click();
  Future<void> addNewPlant() => _newPlantButton.click();
}
