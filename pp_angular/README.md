# PlantyPlants Web

Planty Plants Angulardart web

## Getting Started

## Running in dev

```
$ webdev serve -- --delete-conflicting-outputs
```

To serve with production compiler:

* Replace ``dartdevc`` by ``dart2js`` in ``build.yaml``

```
$ webdev serve --release
```

## Run tests

```
$ pub run build_runner test --fail-on-severe -- -p chrome
```

## Clean

```
$ pub run build_runner clean
```

## Deploying

```
$ webdev build --output web:build
$ firebase deploy
```
