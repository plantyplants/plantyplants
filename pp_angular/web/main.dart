import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:pp_common/users/blocs/authentication/authentication.dart';
import 'package:pp_angular/app_component.template.dart' as ng;
import 'package:pp_angular/firebase_service.dart';
import 'package:pp_angular/injectors.dart';

import 'main.template.dart' as self;

class FirebaseClientInitialized extends FirebaseClient {
  FirebaseClientInitialized() : super() {
    init();
  }
}

@GenerateInjector(<dynamic>[
  routerProvidersHash, // You can use routerProviders in production
  // Using a real back end?
  // Import 'package:http/browser_client.dart' and change the above to:
  //   ClassProvider(Client, useClass: BrowserClient),

  // Firebase
  ClassProvider(FirebaseClient, useClass: FirebaseClientInitialized),

  // Users
  userRepositoryProvider,
  authenticationBlocProvider,
  loginBlocProvider,
  ClassProvider(AuthenticationState),

  // MyPlants
  myPlantsRepositoryProvider,
  myPlantsBlocProvider,
])
final InjectorFactory injector = self.injector$Injector;

void main() {
  runApp(ng.AppComponentNgFactory, createInjector: injector);
}
