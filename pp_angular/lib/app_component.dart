import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angular_bloc/angular_bloc.dart';
import 'package:pp_common/my_plants/blocs/my_plants/my_plants.dart';
import 'package:pp_common/my_plants/repository/interface.dart';
import 'package:pp_common/users/blocs/authentication/authentication.dart';
import 'package:pp_common/users/repository/interface.dart';

import 'src/base_firebase_component.dart';
import 'src/navbar_component.dart';
import 'src/routes.dart';
import 'src/welcome_component.dart';

@Component(
  selector: 'my-app',
  templateUrl: 'app_component.html',
  directives: <dynamic>[
    coreDirectives,
    routerDirectives,
    NavbarComponent,
    WelcomeComponent,
  ],
  pipes: <dynamic>[
    BlocPipe,
  ],
  exports: <dynamic>[RoutePaths, Routes],
)

/// AppComponent is the root component of the website.
class AppComponent extends BaseFirebaseBlocComponent
    implements OnInit, OnDestroy {
  @override
  final AuthenticationBloc authenticationBloc;
  // ignore: unused_field
  final UserRepository _userRepository;
  // ignore: unused_field
  final MyPlantsBloc _myPlantsBloc;
  // ignore: unused_field
  final MyPlantsRepository _myPlantsRepository;
  @override
  AuthenticationState authenticationState;

  /// Initialize with authentication and my plants.
  AppComponent(this.authenticationBloc, this._userRepository,
      this._myPlantsBloc, this._myPlantsRepository, this.authenticationState)
      : super(authenticationBloc, authenticationState);

  /// Title of the page
  static const String title = 'Planty Plants';

  @override
  void ngOnInit() {
    authenticationBloc.add(AppStarted());
    authenticationBloc.listen((authenticationState) {
      authenticationState = authenticationState;
    }, onError: (dynamic e) => throw e);
  }

  @override
  void ngOnDestroy() {
    authenticationBloc.close();
  }
}
