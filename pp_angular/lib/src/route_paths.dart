import 'package:angular_router/angular_router.dart';

/// ID param identifier.
const String idParam = 'id';

/// Action param identifier. Can be: edit, view, add.
const String actionParam = 'action';

// ignore: avoid_classes_with_only_static_members
/// Route paths of the website pages
class RoutePaths {
  /// Route path for my plants overview.
  static final RoutePath myPlants = RoutePath(path: 'my_plants');

  /// Route path for my plant details with specified action.
  static final RoutePath myPlant =
      RoutePath(path: '${myPlants.path}/:$idParam/:$actionParam');

  /// Route path for my plants overview, can be add action.
  static final RoutePath newPlant =
      RoutePath(path: '${myPlants.path}/:$actionParam');

  /// Route path for landing page.
  static final RoutePath welcome = RoutePath(path: 'welcome');
}

/// Extracts ID string from parameters.
String getId(Map<String, String> parameters) {
  final id = parameters[idParam];
  return id == null ? null : id;
}

/// Extracts action string from parameters
String getAction(Map<String, String> parameters) {
  final action = parameters[actionParam];
  return action == null ? null : action;
}
