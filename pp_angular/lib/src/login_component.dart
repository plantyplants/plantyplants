import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:pp_common/users/blocs/authentication/authentication.dart';
import 'package:pp_common/users/blocs/login/login.dart';

import 'base_firebase_component.dart';

@Component(
  selector: 'login',
  templateUrl: 'login_component.html',
  directives: <dynamic>[coreDirectives],
)

/// [LoginComponent] displays a Sigin with Googlebutton (user unauthenticated)
/// or greeting message and sign-out button (user authenticated).
class LoginComponent extends BaseFirebaseBlocComponent {
  @override
  final AuthenticationBloc authenticationBloc;
  final LoginBloc _loginBloc;
  final Router _router;
  @override
  AuthenticationState authenticationState;

  /// Initialize with authentication, login and router.
  LoginComponent(this.authenticationBloc, this._loginBloc, this._router,
      this.authenticationState)
      : super(authenticationBloc, authenticationState);

  /// User clicked the sign in with Google button
  void signInWithGoogle() {
    _loginBloc.add(LoginWithGooglePressed());
    _loginBloc.listen((loginState) {
      // TODO: Add error if loginFails
      if (loginState == LoginState.success()) {
        authenticationBloc.add(LoggedIn());
      }
    }, onError: (dynamic e) => throw e);
    authenticationBloc.listen((authenticationState) {
      // TODO: Display error if auth failed
      this.authenticationState = authenticationState;
    }, onError: (dynamic e) => throw e);
  }

  /// User clicked the sign out button
  void signOut() async {
    authenticationBloc.add(LoggedOut());
    authenticationBloc.listen((authenticationState) {
      // TODO: Display error if auth failed
      this.authenticationState = authenticationState;
    }, onError: (dynamic e) => throw e);
    _router.navigateByUrl('/');
  }

  /// Returns the display name of the signed in user for greetings.
  String get displayName {
    final user = this.user();
    return user == null ? '' : user.displayName;
  }
}
