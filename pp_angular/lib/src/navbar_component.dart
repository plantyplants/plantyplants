import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angular_bloc/angular_bloc.dart';
import 'package:pp_common/users/blocs/authentication/authentication.dart';

import 'base_firebase_component.dart';
import 'login_component.dart';
import 'routes.dart';

@Component(
  selector: 'navbar',
  styleUrls: <String>['navbar_component.css'],
  templateUrl: 'navbar_component.html',
  directives: <dynamic>[
    coreDirectives,
    routerDirectives,
    LoginComponent,
  ],
  pipes: <dynamic>[
    BlocPipe,
  ],
  exports: <dynamic>[RoutePaths, Routes],
)

/// [Navbar] is the main top navigation bar.
class NavbarComponent extends BaseFirebaseBlocComponent {
  @Input()

  /// State of the authentication.
  AuthenticationState localAuthenticationState;

  @override
  final AuthenticationBloc authenticationBloc;
  @override
  AuthenticationState authenticationState;

  /// Initialize with authentication.
  NavbarComponent(this.authenticationBloc, this.authenticationState)
      : super(authenticationBloc, authenticationState);

  /// True if user is authenticated based on local state.
  bool get isAuthenticated => localAuthenticationState is Authenticated;

  /// True if user is unauthenticated based on local state.
  bool get isUnauthenticated => localAuthenticationState is Unauthenticated;
}
