import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

import 'package:pp_common/users/blocs/authentication/authentication.dart';
import 'package:pp_common/my_plants/blocs/my_plants/my_plants.dart';
import 'package:pp_common/my_plants/repository/model.dart';

import 'base_firebase_component.dart';

import 'route_paths.dart';
import 'routes.dart';

@Component(
  selector: 'my-plants-list',
  templateUrl: 'myplants_list_component.html',
  styleUrls: <String>['myplants_list_component.css'],
  directives: <dynamic>[coreDirectives, routerDirectives],
)

/// [MyPlantsListComponent] displays the list of my plants for which the
/// signed-in user is the author.
class MyPlantsListComponent extends BaseFirebaseBlocComponent
    implements OnActivate {
  final Router _router;
  @override
  final AuthenticationBloc authenticationBloc;
  @override
  AuthenticationState authenticationState;
  @Input()

  /// state of my plants, generated via bloc, contains list of my plants to
  /// display (already filtered for user == author)
  MyPlantsState state;

  /// Initialize with authentication and router.
  MyPlantsListComponent(
      this.authenticationBloc, this.authenticationState, this._router)
      : super(authenticationBloc, authenticationState);

  /// True if the plants are still loading.
  bool get isLoading => state is MyPlantsLoading;

  /// True if the plants are ready.
  bool get isLoaded => state is MyPlantsLoaded;

  /// True is there was a problem loading the plants.
  bool get isNotLoaded => state is MyPlantsNotLoaded;

  /// List of my plants to display (from state).
  List<MyPlant> get myPlants =>
      isLoaded ? (state as MyPlantsLoaded).myPlants : <MyPlant>[];

  @override
  void onActivate(_, __) async =>
      super.authValidator(_router, ((authState) async {}));

  /// Generates URL for detailed view of a my plant
  String myplantUrl(String id) => RoutePaths.myPlant
      .toUrl(parameters: <String, String>{idParam: '$id', actionParam: 'view'});
}
