import 'package:angular_router/angular_router.dart';

import 'myplant_component.template.dart' as myplant_template;
import 'myplants_component.template.dart' as myplants_template;
import 'route_paths.dart';
import 'welcome_component.template.dart' as welcome_template;

export 'route_paths.dart';

// ignore: avoid_classes_with_only_static_members
/// Route definitions for the page of the website.
class Routes {
  /// Route definition for my plants overview.
  static final RouteDefinition myPlants = RouteDefinition(
    routePath: RoutePaths.myPlants,
    component: myplants_template.MyPlantsComponentNgFactory,
  );

  /// Route definition for my plant details.
  static final RouteDefinition myPlant = RouteDefinition(
    routePath: RoutePaths.myPlant,
    component: myplant_template.MyPlantComponentNgFactory,
  );

  /// Route definition for new plant form.
  static final RouteDefinition newPlant = RouteDefinition(
    routePath: RoutePaths.newPlant,
    component: myplant_template.MyPlantComponentNgFactory,
  );

  /// Route definition for landing page.
  static final RouteDefinition welcome = RouteDefinition(
    routePath: RoutePaths.welcome,
    component: welcome_template.WelcomeComponentNgFactory,
  );

  /// Set of all route definitions.
  static final List<RouteDefinition> all = <RouteDefinition>[
    myPlant,
    myPlants,
    newPlant,
    welcome,
    RouteDefinition.redirect(
      path: '',
      redirectTo: RoutePaths.welcome.toUrl(),
    ),
  ];
}
