import 'package:angular_router/angular_router.dart';
import 'package:pp_common/users/blocs/authentication/authentication.dart';
import 'package:pp_common/users/model.dart';

/// Component extended by others to get authentication information.
class BaseFirebaseBlocComponent {
  /// authentication Bloc
  AuthenticationBloc authenticationBloc;

  /// authentication State
  AuthenticationState authenticationState;

  /// Initialize with authentication bloc and state.
  BaseFirebaseBlocComponent(this.authenticationBloc, this.authenticationState);

  /// Checks if the user is authenticated and call the callback if they are.
  /// Is usually called from onActivate method.
  void authValidator(Router router, Function callback) async {
    authenticationBloc.listen((authState) {
      authenticationState = authState;
      // TODO: Display error if auth failed
      // TODO: Add logging
      if (authState is Authenticated) {
        callback(authState);
      } else {
        // TODO: Add logging
        router.navigateByUrl('/');
      }
    }, onError: (dynamic e) => throw e);
  }

  /// Returns true if the user is logged in based on the authentication state.
  bool loggedIn() {
    if (authenticationBloc == null) {
      return false;
    }
    return authenticationState is Authenticated;
  }

  /// Returns [User] for the user if they are logged in.
  User user() {
    if (authenticationState is Authenticated) {
      return (authenticationState as Authenticated).user;
    }
    return null;
  }
}
