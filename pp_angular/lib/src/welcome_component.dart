import 'package:angular/angular.dart';

@Component(
  selector: 'welcome',
  templateUrl: 'welcome_component.html',
)

/// [WelcomeComponent] is the landing page.
class WelcomeComponent {}
