import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angular_bloc/angular_bloc.dart';

import 'package:pp_common/users/blocs/authentication/authentication.dart';
import 'package:pp_common/my_plants/blocs/my_plants/my_plants.dart';

import 'base_firebase_component.dart';
import 'myplants_list_component.dart';
import 'route_paths.dart';
import 'routes.dart';

@Component(
  selector: 'my-plants',
  templateUrl: 'myplants_component.html',
  styleUrls: <String>['myplants_component.css'],
  directives: <dynamic>[
    coreDirectives,
    routerDirectives,
    MyPlantsListComponent
  ],
  exports: <dynamic>[RoutePaths, Routes],
  pipes: <dynamic>[BlocPipe],
)

/// [MyPlantsComponent] is a screen that shows an add plant button and then
/// calls <my-plant-list> | bloc.
class MyPlantsComponent extends BaseFirebaseBlocComponent
    implements OnActivate {
  final Router _router;
  @override
  final AuthenticationBloc authenticationBloc;
  @override
  AuthenticationState authenticationState;

  /// My plants bloc.
  MyPlantsBloc myPlantsBloc;

  /// Initialize with authentication, my plants and router.
  MyPlantsComponent(this.authenticationBloc, this.authenticationState,
      this.myPlantsBloc, this._router)
      : super(authenticationBloc, authenticationState);

  @override
  void onActivate(_, __) async =>
      super.authValidator(_router, ((authState) async {
        myPlantsBloc.authorId = (authState as Authenticated).user.uid;
        myPlantsBloc.add(LoadMyPlants());
      }));

  /// Creates the URL for the new plant button.
  String newPlantUrl() => RoutePaths.newPlant
      .toUrl(parameters: <String, String>{actionParam: 'new'});
}
