import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:angular_router/angular_router.dart';

import 'package:pp_common/my_plants/blocs/my_plants/my_plants.dart';
import 'package:pp_common/my_plants/repository/interface.dart';
import 'package:pp_common/my_plants/repository/model.dart';
import 'package:pp_common/users/blocs/authentication/authentication.dart';

import 'base_firebase_component.dart';
import 'route_paths.dart';
import 'routes.dart';

@Component(
  selector: 'my-plant',
  templateUrl: 'myplant_component.html',
  styleUrls: <String>['myplant_component.css'],
  directives: <dynamic>[
    RouterOutlet,
    coreDirectives,
    formDirectives,
    routerDirectives
  ],
  exports: <dynamic>[RoutePaths, Routes],
)

/// [MyPlantComponent] displays a form for a my plant, to view details, add,
/// edit or delete one.
class MyPlantComponent extends BaseFirebaseBlocComponent implements OnActivate {
  /// Populated in onActivate, double binding with template
  MyPlantMutable plantMutable;

  /// ID of the plant to view or edit (if any).
  String plantId;
  @override
  final AuthenticationBloc authenticationBloc;
  @override
  AuthenticationState authenticationState;
  final Location _location;

  /// Angular [Router] instance.
  Router router;

  /// My plants repository instance.
  MyPlantsRepository myPlantsRepository;
  final MyPlantsBloc _myPlantsBloc;

  /// Action to perform: view, edit, new
  String action = 'view';

  /// Initialize with authentication, router and plants
  MyPlantComponent(
      this.authenticationBloc,
      this._location,
      this.authenticationState,
      this.router,
      this._myPlantsBloc,
      this.myPlantsRepository)
      : super(authenticationBloc, authenticationState);

  @override
  void onActivate(_, RouterState current) async {
    super.authValidator(router, ((authState) async {
      _myPlantsBloc.authorId = (authState as Authenticated).user.uid;
      final id = getId(current.parameters);
      plantId = id;
      if (id != null) {
        final myPlant =
            await myPlantsRepository.myPlant(id, _myPlantsBloc.authorId);
        plantMutable = MyPlantMutable.fromMyPlant(myPlant);
      }
      action = getAction(current.parameters);
      if (id == null) {
        action = 'new';
      }
      if (id != null && action == null) {
        action = 'view';
      }
      if (action == 'new') {
        final user = (authState as Authenticated).user;
        plantMutable = MyPlantMutable('', user.uid,
            id: null, photoIconUrl: '', description: '');
      }
    }));
  }

  /// User clicked to save the plant (edit or new).
  Future<void> save() async {
    if (action == 'edit') {
      _myPlantsBloc.add(UpdateMyPlant(plantMutable.toMyPlant()));
    }
    if (action == 'new') {
      _myPlantsBloc.add(AddMyPlant(plantMutable.toMyPlant()));
    }
    goBack();
  }

  /// User clicked to delete the plant.
  Future<void> delete() async {
    _myPlantsBloc.add(DeleteMyPlant(plantMutable.toMyPlant()));
    goBack();
  }

  /// Creates the URL for the detail page for a given plant, edit mode
  String myplantEditUrl(String id) => RoutePaths.myPlant
      .toUrl(parameters: <String, String>{idParam: '$id', actionParam: 'edit'});

  /// Go to previous page
  void goBack() => _location.back();
}
