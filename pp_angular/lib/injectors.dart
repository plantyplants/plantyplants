import 'dart:async';

import 'package:angular/angular.dart';

import 'package:bloc_test/bloc_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pp_common/my_plants/blocs/my_plants/my_plants.dart';
import 'package:pp_common/my_plants/repository/firebase.dart';
import 'package:pp_common/my_plants/repository/interface.dart';
import 'package:pp_common/my_plants/repository/mock.dart';
import 'package:pp_common/users/blocs/authentication/authentication.dart';
import 'package:pp_common/users/blocs/login/login.dart';
import 'package:pp_common/users/model.dart';
import 'package:pp_common/users/repository/interface.dart';
import 'package:pp_common/users/repository/firebase.dart';
import 'firebase_service.dart';

/*
 * User injectors
 */

/// Function to create a [UserRepository] initialized with a firebase
/// connection.
UserRepository userRepositoryFactory(FirebaseClient fbc) =>
    FirebaseUserRepository(firebase: fbc.fbb);

/// Provider factory for the [UserRepository].
const FactoryProvider userRepositoryProvider =
    FactoryProvider(UserRepository, userRepositoryFactory);

/// Function to create a [AuthenticationBloc] initialized with a user
/// repository.
AuthenticationBloc authenticationBlocFactory(UserRepository userRepository) =>
    AuthenticationBloc(userRepository: userRepository);

/// Provider factory for the [authenticationBloc].
const FactoryProvider authenticationBlocProvider =
    FactoryProvider(AuthenticationBloc, authenticationBlocFactory);

/// Mock for the [authenticationBloc] for authenticated user, used in tests.
class MockAuthenticationBloc extends Mock implements AuthenticationBloc {}

/// Instance of [MockAuthenticationBloc], used in tests.
final MockAuthenticationBloc mockAuthenticationBloc = MockAuthenticationBloc();

/// Function to create mock AuthenticationBloc for with a sample user.
AuthenticationBloc authenticationBlocLoggedInMockFactory(
    UserRepository userRepository) {
  whenListen(
      mockAuthenticationBloc,
      Stream<AuthenticationState>.fromIterable(<AuthenticationState>[
        Authenticated(User(authorIdFake, 'example@example.com', 'Test User'))
      ]));
  return mockAuthenticationBloc;
}

/// Provider factory for the [AuthenticationBloc] with an in memory persistence,
/// used for tests.
const FactoryProvider authenticationBlocLoggedInMockProvider =
    FactoryProvider(AuthenticationBloc, authenticationBlocLoggedInMockFactory);

/// Function to create a [loginBloc] with an initialized user repository.
LoginBloc loginBlocFactory(UserRepository userRepository) =>
    LoginBloc(userRepository: userRepository);

/// Provider factory for [LoginBloc].
const FactoryProvider loginBlocProvider =
    FactoryProvider(LoginBloc, loginBlocFactory);

/// Function to create mock Authenticated stated with a sample user.
AuthenticationState authenticatedFactory() =>
    Authenticated(User('000-000-000', 'example@example.com', 'Test User'));

/// Provider factory for [AuthenticationState].
const FactoryProvider authenticatedProvider =
    FactoryProvider(AuthenticationState, authenticatedFactory);

/*
 * MyPlants injectors
 */

/// Function to create a [FirebaseMyPlantsRepository] initialized with a
/// Firebase connection.
MyPlantsRepository myPlantsRepositoryFactory(FirebaseClient fbc) =>
    FirebaseMyPlantsRepository(fbc.fbb);

/// Provider factory for [MyPlantsRepository].
const FactoryProvider myPlantsRepositoryProvider =
    FactoryProvider(MyPlantsRepository, myPlantsRepositoryFactory);

/// Mock [MyPlantsRepository] which doesn't need a Firebase connection.
MyPlantsRepository myPlantsRepositoryMockFactory(FirebaseClient _fbc) =>
    MockMyPlantsRepository();

/// Provider factory for [MyPlantsRepository].
const FactoryProvider myPlantsRepositoryMockProvider =
    FactoryProvider(MyPlantsRepository, myPlantsRepositoryMockFactory);

/// Function to create a [MyPlantsBloc] initialized with a my plants
/// repository and an author.
MyPlantsBloc myPlantsBlocFactory(MyPlantsRepository myPlantsRepository) =>
    MyPlantsBloc(
        myPlantsRepository: myPlantsRepository, authorId: authorIdFake);

/// Provider factory for [MyPlantsBloc].
const FactoryProvider myPlantsBlocProvider =
    FactoryProvider(MyPlantsBloc, myPlantsBlocFactory);
