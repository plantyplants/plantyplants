import 'dart:async';

import 'package:common_firebase_browser/common_firebase_browser.dart';
import 'package:pp_common/firebase_setup.dart';

/// Service to interact with Firebase
abstract class FirebaseService {}

/// Implementation of the Firebase service, allows to bootstrap the connection.
class FirebaseClient extends FirebaseService {
  /// Firebase connection.
  FirebaseBrowser fbb;

  /// Bootstraps the Firebase connection
  Future<void> init() async {
    fbb = FirebaseBrowser();
    await fbb.init(setupConfig());
    fbb.setPersistence('local');
  }
}
