# My Plants User Acceptance Tests

Scope:
- Platform: Android
- Device type: smartphone
- Choosing a picture from camera or gallery for front picture
- Image compression of front picture to different formats
- Storage of images in Firebase storage
- Correct display of the images


Out of scope (covered by driver tests):
- Platform: non-Android
- Device type: non-smartphone
- Name and description fields verification

## UAT Entry Criteria

- Modification of a feature touching the front picture logic
- (or) before shipping new release
- (or) dependencies update

## UAT Requirements-based Test Cases

### 1. Front pictures in my plants

#### 1.1 Add from camera

1. On the home screen, tap `+` to add a new plant
2. Fill `Plant name` field with `Plant with photo`
3. Fill `Description` with `Nice picture`
4. Tap on the `camera` floating app icon
5. Take and validate a picture
  - Expectation: A version of the picture should appear between the app bar and the name. It should be horizontally centered and take about 1/3 of the vertical space.
6. Tap the blue save button in the action button item.
  - Expectation: A loading dialog appears and says "Saving plant..."
7. Wait 15 seconds
  - Expectation: The app is at the grid plants list screen.
  - Expectation: The plant `Plant with photo` appears with a small version of the picture taken previously.
8. Tap on `Plant with photo`
  - Expectation: The app should show the detail view. The app title should be `Plant with photo`.
  - Expectation: The picture taken previously is shown. It should be horizontally centered and take about 1/3 of the vertical space.
9. Tap on the picture
  - Expectation: The picture is in full screen (it may take a few seconds to load).
10. Expand with finger gesture, or on the Android emulator `Windows` key and drag the mouse
  - Expectation: Zoom on the picture
11. Drag the finger, or on the Android emulator the mouse
  - Expectation: Move on the picture
12. Tap the `system return` button at the bottom of the screen
  - Expectation: The app should show the detail view. The app title should be `Plant with photo`.
13. Tap the `back` button in the app bar at the top screen, next to `Plant with photo` 
  - Expectation: The app should show the grid plants list screen.

#### 1.2 Add from gallery

1. Download [this picture](http://via.placeholder.com/3872x2592) on the phone
1. Tap `+` to add a new plant
2. Fill `Plant name` field with `Plant with photo 2`
3. Fill `Description` with `Nice picture 2`
4. Tap on the `gallery` floating app icon
5. Select the picture previously downloaded from the gallery
  - Expectation: A version of the picture should appear between the app bar and the name. It should be horizontally centered and take about 1/3 of the vertical space.
6. Tap the save button
  - Expectation: A loading dialog appears and says "Saving plant..."
7. Wait 15 seconds
  - Expectation: The app is at the grid plants list screen.
  - Expectation: The plant `Plant with photo 2` appears with a small version of the picture taken previously.
8. Tap on `Plant with photo 2`
  - Expectation: The app should show the detail view. The app title should be `Plant with photo 2`.
  - Expectation: The picture taken previously is shown. It should be horizontally centered and take about 1/3 of the vertical space.
9. Tap on the picture
  - Expectation: The picture is in full screen.
10. Expand with finger gesture, or on the Android emulator `Windows` key and drag the mouse
  - Expectation: Zoom on the picture
11. Drag the finger, or on the Android emulator the mouse
  - Expectation: Move on the picture
12. Tap the `system return` button at the bottom of the screen
  - Expectation: The app should show the detail view. The app title should be `Plant with photo 2`.
13. Tap the `back` button in the app bar at the top screen, next to `Plant with photo 2`
  - Expectation: The app should show the grid plants list screen.

#### 1.3 Edit plant with photo

1. On the list screen, tap on `Plant with photo 2`
  - Expectation: App bar title should be `Plant with photo 2`, a picture should be visible, a text `Nice picture 2` should be visible under that picture.
2. Tap the `edit` floating icon
3. Take a photo with the `camera` floating action button, validate the picture
  - Expectation: the new picture should be visible.
4. Tap the `validate` floating action button
  - Expectation: A loading dialog appears and says "Updating plant..."
7. Wait 15 seconds
  - Expectation: The app is at the grid plants list screen.
  - Expectation: The new plant picture, taken with the camera, is visible.

#### 1.4 Edit plant without photo

1. On the list screen, tap `+` to add a new plant
2. Fill `Plant name` field with `Plant without photo`
3. Fill `Description` with `No photo`
4. Save the picture by tapping the floating action button
  - Expectation: no picture should be visible.
5. Take a photo with the `camera` floating action button, validate the picture
  - Expectation: the new picture should be visible.
4. Tap the `validate` floating action button
  - Expectation: A loading dialog appears and says "Updating plant..."
7. Wait 15 seconds
  - Expectation: The app is at the grid plants list screen.
  - Expectation: The new plant picture, taken with the camera, is visible.

#### 1.5 Delete plant with photo

1. Tap on the delete button on the top right of the screen.
2. Wait 5 seconds
  - Expectation: No `Plant without photo` should be in the grid list anymore.

### 2. Watering tasks

* TODO: Repeating watering tasks UI
* End-to-end with backend job, notification, acknowlegment
