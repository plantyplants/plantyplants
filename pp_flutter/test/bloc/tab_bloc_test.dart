import 'package:pp_flutter/bloc/tab/bloc.dart';
import 'package:test/test.dart';

void main() {
  group('MyPlantsBloc', () {
    TabBloc tabBloc;

    setUp(() {
      tabBloc = TabBloc();
    });

    test('should start with myPlants homepage', () {
      expect(
        tabBloc,
        emitsInOrder(<AppTab>[
          AppTab.myPlants,
        ]),
      );
    });

    test('should be able to navigate to notifications screen', () {
      expect(
        tabBloc,
        emitsInOrder(<AppTab>[
          AppTab.myPlants,
          AppTab.notifications,
        ]),
      );

      tabBloc.add(TabEvent.updated(tab: AppTab.notifications));
    });
  });
}
