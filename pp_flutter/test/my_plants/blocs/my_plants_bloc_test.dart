import 'package:mockito/mockito.dart';
import 'package:pp_flutter/my_plants/blocs/my_plants/bloc.dart';
import 'package:pp_flutter/my_plants/repository/interface.dart';
import 'package:pp_flutter/my_plants/repository/mock.dart';
import 'package:pp_flutter/my_plants/repository/model.dart';
import 'package:test/test.dart';

class MockErrorMyPlantsRepository extends Mock implements MyPlantsRepository {}

void main() {
  group('MyPlantsBloc', () {
    MockMyPlantsRepository myPlantsRepository;
    MyPlantsBloc myPlantsBloc;

    setUp(() {
      myPlantsRepository = MockMyPlantsRepository();
      myPlantsBloc = MyPlantsBloc(
          myPlantsRepository: myPlantsRepository, authorId: authorIdFake);
    });

    test('should listen to plants when loading', () {
      expectLater(
        myPlantsBloc,
        emitsInOrder(<MyPlantsState>[
          MyPlantsLoading(),
          MyPlantsLoaded(myPlantsData
              .where((plant) => plant.authorId == authorIdFake)
              .toList()),
        ]),
      );

      myPlantsBloc.add(LoadMyPlants());
    });

    test('should add new plants successfully', () {
      final myPlantToAdd =
          MyPlant(name: 'New Plant', authorId: authorIdFake, id: '100-000-000');
      var loadedPlants = myPlantsData
          .where((plant) => plant.authorId == authorIdFake)
          .toList();
      loadedPlants.add(myPlantToAdd);

      expectLater(
        myPlantsBloc,
        emitsInOrder(<MyPlantsState>[
          MyPlantsLoading(),
          MyPlantSavedSuccess(myPlantToAdd),
          MyPlantsLoaded(loadedPlants),
        ]),
      );

      myPlantsBloc.add(AddMyPlant(myPlantToAdd));
      myPlantsBloc.add(LoadMyPlants());
    });

    test('should update a plant', () {
      final initialPlant = MyPlant(
          name: 'Initial Plant', authorId: authorIdFake, id: '010-000-001');
      final myPlantToUpdate = MyPlant(
          name: 'Updated Plant', authorId: authorIdFake, id: '010-000-001');
      var initialList = myPlantsData
          .where((plant) => plant.authorId == authorIdFake)
          .toList();
      initialList.add(initialPlant);
      var updatedList = myPlantsData
          .where((plant) => plant.authorId == authorIdFake)
          .toList();
      updatedList.add(myPlantToUpdate);

      expectLater(
        myPlantsBloc,
        emitsInOrder(<MyPlantsState>[
          MyPlantsLoading(),
          MyPlantSavedSuccess(initialPlant),
          MyPlantSavedSuccess(myPlantToUpdate),
          MyPlantsLoaded(initialList),
          MyPlantsLoaded(updatedList),
        ]),
      );

      myPlantsBloc.add(AddMyPlant(initialPlant));
      myPlantsBloc.add(LoadMyPlants());
      myPlantsBloc.add(UpdateMyPlant(myPlantToUpdate));
      myPlantsBloc.add(LoadMyPlants());
    });

    test('should delete a plant', () {
      final myPlantToDelete = myPlantsData[0];
      var initialList = myPlantsData
          .where((plant) => plant.authorId == authorIdFake)
          .toList();
      var deletedList = myPlantsData
          .where((plant) => plant.authorId == authorIdFake)
          .toList();
      deletedList.remove(myPlantToDelete);

      expectLater(
        myPlantsBloc,
        emitsInOrder(<MyPlantsState>[
          MyPlantsLoading(),
          MyPlantsLoaded(initialList),
          MyPlantsLoaded(deletedList),
        ]),
      );

      myPlantsBloc.add(LoadMyPlants());
      myPlantsBloc.add(DeleteMyPlant(myPlantToDelete));
      myPlantsBloc.add(LoadMyPlants());
    });

    test('should update plant state', () {
      final updatedList = myPlantsData.sublist(1);

      expectLater(
        myPlantsBloc,
        emitsInOrder(<MyPlantsState>[
          MyPlantsLoading(),
          MyPlantsLoaded(updatedList),
        ]),
      );

      myPlantsBloc.add(MyPlantsUpdated(updatedList));
    });
  });

  group('MyPlantsBloc fail gracefully', () {
    MyPlantsRepository myPlantsRepository;
    MyPlantsBloc myPlantsBloc;

    test('should fail gracefully when adding new plants', () {
      myPlantsRepository = MockErrorMyPlantsRepository();
      when(myPlantsRepository.addNewMyPlant(any,
              frontPictureFormats: anyNamed('frontPictureFormats')))
          .thenAnswer((_) => throw Error());
      myPlantsBloc = MyPlantsBloc(
          myPlantsRepository: myPlantsRepository, authorId: authorIdFake);

      final myPlantToAdd =
          MyPlant(name: 'New Plant', authorId: authorIdFake, id: '100-000-000');

      expectLater(
        myPlantsBloc,
        emitsInOrder(<MyPlantsState>[
          MyPlantsLoading(),
          MyPlantSavedFailed(myPlantToAdd),
        ]),
      );

      myPlantsBloc.add(AddMyPlant(myPlantToAdd));
    });

    test('should fail gracefully when updating a plant', () {
      myPlantsRepository = MockErrorMyPlantsRepository();
      when(myPlantsRepository.updateMyPlant(any,
              frontPictureFormats: anyNamed('frontPictureFormats')))
          .thenAnswer((_) => throw Error());
      myPlantsBloc = MyPlantsBloc(
          myPlantsRepository: myPlantsRepository, authorId: authorIdFake);

      final myPlantToUpdate = MyPlant(
          name: 'Updated Plant', authorId: authorIdFake, id: '000-000-001');
      final updatedList = <MyPlant>[myPlantToUpdate];
      updatedList.addAll(myPlantsData.sublist(1));

      expectLater(
        myPlantsBloc,
        emitsInOrder(<MyPlantsState>[
          MyPlantsLoading(),
          MyPlantSavedFailed(myPlantToUpdate),
        ]),
      );

      myPlantsBloc.add(UpdateMyPlant(myPlantToUpdate));
    });
  });
}
