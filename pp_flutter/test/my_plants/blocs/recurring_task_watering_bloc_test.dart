import 'package:mockito/mockito.dart';
import 'package:pp_flutter/my_plants/blocs/recurring_task_watering/bloc.dart';
import 'package:pp_flutter/my_plants/repository/interface.dart';
import 'package:pp_flutter/my_plants/repository/mock.dart';
import 'package:pp_flutter/my_plants/repository/model.dart';
import 'package:test/test.dart';

class MockErrorMyPlantsRepository extends Mock implements MyPlantsRepository {}

void main() {
  group('RecurringTaskWateringBloc', () {
    MockMyPlantsRepository myPlantsRepository;
    RecurringTaskWateringBloc recurringTaskWateringBloc;

    setUp(() {
      myPlantsRepository = MockMyPlantsRepository();
      myPlantsRepository.data = myPlantsData;
      recurringTaskWateringBloc = RecurringTaskWateringBloc(
          myPlantsRepository: myPlantsRepository,
          authorId: authorIdFake,
          plantId: myPlantsData[0].id);
    });

    test('should listen to recurring task watering when loading', () {
      expectLater(
        recurringTaskWateringBloc,
        emitsInOrder(<RecurringTaskWateringState>[
          RecurringTaskWateringLoading(),
          RecurringTaskWateringSavedSuccess(recurringTaskWateringTestData[0]),
          RecurringTaskWateringLoaded(null),
          RecurringTaskWateringLoaded(recurringTaskWateringTestData[0]),
        ]),
      );

      recurringTaskWateringBloc.add(LoadRecurringTaskWatering());
      recurringTaskWateringBloc
          .add(AddRecurringTaskWatering(recurringTaskWateringTestData[0]));
      recurringTaskWateringBloc.add(LoadRecurringTaskWatering());
    });

    test('should add new recurring task watering successfully', () {
      final myRecurringTaskWateringToAdd = RecurringTaskWatering(
          datetime: DateTime(2035, 1, 1, 0, 0),
          repeat: repeatChoicesEnum.no.displayLabel,
          repeatDayEvery: 1,
          repeatWeekEvery: 1,
          repeatWeekDay: [],
          repeatMonthEvery: 1,
          repeatMonthOption: '',
          repeatYearEvery: 1,
          id: '100-000-000');

      expectLater(
        recurringTaskWateringBloc,
        emitsInOrder(<RecurringTaskWateringState>[
          RecurringTaskWateringLoading(),
          RecurringTaskWateringSavedSuccess(myRecurringTaskWateringToAdd),
          RecurringTaskWateringLoaded(myRecurringTaskWateringToAdd),
        ]),
      );

      recurringTaskWateringBloc
          .add(AddRecurringTaskWatering(myRecurringTaskWateringToAdd));
      recurringTaskWateringBloc.add(LoadRecurringTaskWatering());
    });

    test('should update a recurring task watering', () {
      final myRecurringTaskWateringToUpdate = RecurringTaskWatering(
          datetime: DateTime(2036, 1, 1, 0, 0),
          repeat: repeatChoicesEnum.no.displayLabel,
          repeatDayEvery: 1,
          repeatWeekEvery: 1,
          repeatWeekDay: [],
          repeatMonthEvery: 1,
          repeatMonthOption: '',
          repeatYearEvery: 1,
          id: '100-000-001');

      expectLater(
        recurringTaskWateringBloc,
        emitsInOrder(<RecurringTaskWateringState>[
          RecurringTaskWateringLoading(),
          RecurringTaskWateringSavedSuccess(myRecurringTaskWateringToUpdate),
          RecurringTaskWateringLoaded(myRecurringTaskWateringToUpdate),
        ]),
      );

      recurringTaskWateringBloc
          .add(UpdateRecurringTaskWatering(myRecurringTaskWateringToUpdate));
      recurringTaskWateringBloc.add(LoadRecurringTaskWatering());
    });

    test('should delete a recurring task watering', () {
      expectLater(
        recurringTaskWateringBloc,
        emitsInOrder(<RecurringTaskWateringState>[
          RecurringTaskWateringLoading(),
          RecurringTaskWateringLoaded(null),
        ]),
      );

      recurringTaskWateringBloc.add(DeleteRecurringTaskWatering());
      recurringTaskWateringBloc.add(LoadRecurringTaskWatering());
    });

    test('should update recurring task watering state', () {
      final updatedRecurringTaskWatering = recurringTaskWateringTestData[0];

      expectLater(
        recurringTaskWateringBloc,
        emitsInOrder(<RecurringTaskWateringState>[
          RecurringTaskWateringLoading(),
          RecurringTaskWateringLoaded(updatedRecurringTaskWatering),
        ]),
      );

      recurringTaskWateringBloc
          .add(RecurringTaskWateringUpdated(updatedRecurringTaskWatering));
    });
  });

  group('MyPlantsBloc fail gracefully', () {
    MyPlantsRepository myPlantsRepository;
    RecurringTaskWateringBloc recurringTaskWateringBloc;

    test('should fail gracefully when new plants', () {
      myPlantsRepository = MockErrorMyPlantsRepository();
      when(myPlantsRepository.addNewRecurringTaskWatering(any, any))
          .thenAnswer((_) => throw Error());
      recurringTaskWateringBloc = RecurringTaskWateringBloc(
          myPlantsRepository: myPlantsRepository,
          authorId: authorIdFake,
          plantId: plantIdFake);

      final myRecurringTaskWateringToAdd = RecurringTaskWatering(
          datetime: DateTime(2035, 1, 1, 0, 0),
          repeat: repeatChoicesEnum.no.displayLabel,
          repeatDayEvery: 1,
          repeatWeekEvery: 1,
          repeatWeekDay: [],
          repeatMonthEvery: 1,
          repeatMonthOption: '',
          repeatYearEvery: 1,
          id: '100-000-000');

      expectLater(
        recurringTaskWateringBloc,
        emitsInOrder(<RecurringTaskWateringState>[
          RecurringTaskWateringLoading(),
          RecurringTaskWateringSavedFailed(myRecurringTaskWateringToAdd),
        ]),
      );

      recurringTaskWateringBloc
          .add(AddRecurringTaskWatering(myRecurringTaskWateringToAdd));
    });

    test('should fail gracefully when updating a plant', () {
      myPlantsRepository = MockErrorMyPlantsRepository();
      when(myPlantsRepository.updateRecurringTaskWatering(any, any))
          .thenAnswer((_) => throw Error());
      recurringTaskWateringBloc = RecurringTaskWateringBloc(
          myPlantsRepository: myPlantsRepository,
          authorId: authorIdFake,
          plantId: plantIdFake);

      final myRecurringTaskWateringToUpdate = RecurringTaskWatering(
          datetime: DateTime(2036, 1, 1, 0, 0),
          repeat: repeatChoicesEnum.no.displayLabel,
          repeatDayEvery: 1,
          repeatWeekEvery: 1,
          repeatWeekDay: [],
          repeatMonthEvery: 1,
          repeatMonthOption: '',
          repeatYearEvery: 1,
          id: '100-000-001');

      expectLater(
        recurringTaskWateringBloc,
        emitsInOrder(<RecurringTaskWateringState>[
          RecurringTaskWateringLoading(),
          RecurringTaskWateringSavedFailed(myRecurringTaskWateringToUpdate),
        ]),
      );

      recurringTaskWateringBloc
          .add(UpdateRecurringTaskWatering(myRecurringTaskWateringToUpdate));
    });
  });
}
