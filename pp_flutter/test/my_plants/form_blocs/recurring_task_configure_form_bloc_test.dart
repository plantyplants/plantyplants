import 'package:pp_flutter/my_plants/form_blocs/recurring_task_configure_form_bloc.dart';
import 'package:test/test.dart';

void main() {
  group('_nthDay', () {
    test('_nthDay()', () {
      final testcases = <DateTime, String>{
        DateTime(2020, 02, 07): 'first',
        DateTime(2020, 02, 08): 'second',
        DateTime(2020, 02, 21): 'third',
        DateTime(2020, 02, 28): 'last',
        DateTime(2020, 07, 01): 'first',
        DateTime(2020, 07, 07): 'first',
        DateTime(2020, 07, 08): 'second',
        DateTime(2020, 07, 14): 'second',
        DateTime(2020, 07, 17): 'third',
        DateTime(2020, 07, 18): 'third',
        DateTime(2020, 07, 21): 'third',
        DateTime(2020, 07, 26): 'last',
        DateTime(2020, 07, 31): 'last',
      };

      testcases.forEach((dt, value) {
        expect(RecurringTaskConfigureFormBloc.nthDay(dt), equals(value));
      });
    });
  });
}
