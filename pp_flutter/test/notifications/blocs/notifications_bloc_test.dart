import 'package:mockito/mockito.dart';
import 'package:pp_flutter/notifications/blocs/notification/bloc.dart';
import 'package:pp_flutter/notifications/repository/interface.dart';
import 'package:pp_flutter/notifications/repository/mock.dart';
import 'package:pp_flutter/notifications/repository/model.dart';
import 'package:test/test.dart';

class MockErrorNotificationsRepository extends Mock
    implements NotificationsRepository {}

void main() {
  group('NotificationsBloc', () {
    MockNotificationsRepository notificationsRepository;
    NotificationBloc notificationBloc;

    setUp(() {
      notificationsRepository = MockNotificationsRepository();
      notificationBloc = NotificationBloc(
          notificationsRepository: notificationsRepository,
          receiverUserId: receiverUserIdFake);
    });

    test('should listen to notifications when loading', () {
      var notificationsFiltering = notificationsData
          .where((notification) =>
              notification.receiverUserId == receiverUserIdFake)
          .toList();

      expectLater(
        notificationBloc,
        emitsInOrder(<NotificationState>[
          NotificationsLoading(),
          NotificationsLoaded(notificationsFiltering),
        ]),
      );

      notificationBloc.add(LoadNotifications());
    });

    test('should add new notifications successfully', () {
      final notificationToAdd = Notification(
          id: '000-001-001',
          receiverUserId: receiverUserIdFake,
          title: 'New notification');
      var notificationsLoaded = notificationsData
          .where((notification) =>
              notification.receiverUserId == receiverUserIdFake)
          .toList();
      notificationsLoaded.add(notificationToAdd);

      expectLater(
        notificationBloc,
        emitsInOrder(<NotificationState>[
          NotificationsLoading(),
          NotificationSavedSuccess(notificationToAdd),
          NotificationsLoaded(notificationsLoaded),
        ]),
      );

      notificationBloc.add(AddNotification(notificationToAdd));
      notificationBloc.add(LoadNotifications());
    });

    test('should update a notification', () {
      var notificationsUpdated = notificationsData
          .where((notification) =>
              notification.receiverUserId == receiverUserIdFake)
          .toList();
      final notificationToUpdate =
          notificationsData[0].copyWith(title: 'Updated title');
      notificationsUpdated[0] = notificationToUpdate;

      expectLater(
        notificationBloc,
        emitsInOrder(<NotificationState>[
          NotificationsLoading(),
          NotificationSavedSuccess(notificationToUpdate),
          NotificationsLoaded(notificationsData
              .where((notification) =>
                  notification.receiverUserId == receiverUserIdFake)
              .toList()),
          NotificationsLoaded(notificationsUpdated),
        ]),
      );

      notificationBloc.add(LoadNotifications());
      notificationBloc.add(UpdateNotification(notificationToUpdate));
      notificationBloc.add(LoadNotifications());
    });

    test('should delete a notification', () {
      final notificationToDelete = notificationsData[0];
      final notificationsAfterDeletion = notificationsData
          .sublist(1)
          .where((notification) =>
              notification.receiverUserId == receiverUserIdFake)
          .toList();

      expectLater(
        notificationBloc,
        emitsInOrder(<NotificationState>[
          NotificationsLoading(),
          NotificationsLoaded(notificationsData
              .where((notification) =>
                  notification.receiverUserId == receiverUserIdFake)
              .toList()),
          NotificationsLoaded(notificationsAfterDeletion),
        ]),
      );

      notificationBloc.add(LoadNotifications());
      notificationBloc.add(DeleteNotification(notificationToDelete));
      notificationBloc.add(LoadNotifications());
    });

    test('should update notification state', () {
      final updatedList = notificationsData
          .sublist(1)
          .where((notification) =>
              notification.receiverUserId == receiverUserIdFake)
          .toList();

      expectLater(
        notificationBloc,
        emitsInOrder(<NotificationState>[
          NotificationsLoading(),
          NotificationsLoaded(updatedList),
        ]),
      );

      notificationBloc.add(NotificationUpdated(updatedList));
    });
  });

  group('NotificationsBloc fail gracefully', () {
    NotificationsRepository notificationsRepository;
    NotificationBloc notificationBloc;

    test('should fail gracefully when adding new notifications', () {
      notificationsRepository = MockErrorNotificationsRepository();
      when(notificationsRepository.addNotification(any))
          .thenAnswer((_) => throw Error());
      notificationBloc = NotificationBloc(
          notificationsRepository: notificationsRepository,
          receiverUserId: receiverUserIdFake);

      final notificationToAdd = notificationsData[0].copyWith();

      expectLater(
        notificationBloc,
        emitsInOrder(<NotificationState>[
          NotificationsLoading(),
          NotificationSavedFailed(notificationToAdd),
        ]),
      );

      notificationBloc.add(AddNotification(notificationToAdd));
    });

    test('should fail gracefully when updating a plant', () {
      notificationsRepository = MockErrorNotificationsRepository();
      when(notificationsRepository.updateNotification(any))
          .thenAnswer((_) => throw Error());
      notificationBloc = NotificationBloc(
          notificationsRepository: notificationsRepository,
          receiverUserId: receiverUserIdFake);

      final notificationToUpdate = notificationsData[0].copyWith();
      final updatedList = <Notification>[notificationToUpdate];
      updatedList.addAll(notificationsData.sublist(1));

      expectLater(
        notificationBloc,
        emitsInOrder(<NotificationState>[
          NotificationsLoading(),
          NotificationSavedFailed(notificationToUpdate),
        ]),
      );

      notificationBloc.add(UpdateNotification(notificationToUpdate));
    });
  });
}
