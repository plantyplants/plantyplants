import 'package:bloc_test/bloc_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pp_flutter/users/blocs/register/bloc.dart';
import 'package:pp_flutter/users/repository/interface.dart';
import 'package:test/test.dart';

class MockUserRepository extends Mock implements UserRepository {}

void main() {
  group('RegisterBloc', () {
    UserRepository userRepository;
    RegisterBloc registerBloc;

    setUp(() {
      userRepository = MockUserRepository();
      registerBloc = RegisterBloc(userRepository: userRepository);
    });

    blocTest(
        'should emit RegisterState.failure() if repository throws on '
        'Submitted event',
        build: () async {
          when(userRepository.signUp('test@example.com', '123456'))
              .thenThrow(Exception('oops'));
          return RegisterBloc(userRepository: userRepository);
        },
        act: (bloc) =>
            bloc.add(Submitted(email: 'test@example.com', password: '123456')),
        wait: const Duration(milliseconds: 500),
        skip: 0,
        expect: <RegisterState>[
          RegisterState.empty(),
          RegisterState.loading(),
          RegisterState.failure(),
        ]);

    blocTest(
        'should validate email in response to an invalid EmailChanged'
        ' Event',
        build: () async => RegisterBloc(userRepository: userRepository),
        act: (bloc) => bloc.add(EmailChanged(email: 'test')),
        wait: const Duration(milliseconds: 500),
        skip: 0,
        expect: <RegisterState>[
          RegisterState.empty(),
          RegisterState(
            isEmailValid: false,
            isPasswordValid: true,
            isSubmitting: false,
            isSuccess: false,
            isFailure: false,
          ),
        ]);

    blocTest(
        'should validate email in response to a valid EmailChanged'
        ' Event',
        build: () async => RegisterBloc(userRepository: userRepository),
        act: (bloc) => bloc.add(EmailChanged(email: 'test@example.com')),
        wait: const Duration(milliseconds: 500),
        skip: 0,
        expect: <RegisterState>[RegisterState.empty()]);

    blocTest(
        'should validate email in response to an invalid PasswordChanged'
        ' Event',
        build: () async => RegisterBloc(userRepository: userRepository),
        act: (bloc) => bloc.add(PasswordChanged(password: '12345')),
        wait: const Duration(milliseconds: 500),
        skip: 0,
        expect: <RegisterState>[
          RegisterState.empty(),
          RegisterState(
            isEmailValid: true,
            isPasswordValid: false,
            isSubmitting: false,
            isSuccess: false,
            isFailure: false,
          ),
        ]);

    blocTest(
        'should validate email in response to an valid PasswordChanged'
        ' Event',
        build: () async => RegisterBloc(userRepository: userRepository),
        act: (bloc) => bloc.add(PasswordChanged(password: '123456')),
        wait: const Duration(milliseconds: 500),
        skip: 0,
        expect: <RegisterState>[RegisterState.empty()]);

    blocTest('should register successfully in response to a Submitted event',
        build: () async => RegisterBloc(userRepository: userRepository),
        act: (bloc) =>
            bloc.add(Submitted(email: 'test@example.com', password: '123456')),
        wait: const Duration(milliseconds: 500),
        skip: 0,
        expect: <RegisterState>[
          RegisterState.empty(),
          RegisterState.loading(),
          RegisterState.success(),
        ]);

    tearDown(() {
      registerBloc.close();
    });
  });
}
