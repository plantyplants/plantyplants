import 'package:bloc_test/bloc_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pp_flutter/users/blocs/login/bloc.dart';
import 'package:pp_flutter/users/repository/interface.dart';
import 'package:test/test.dart';

class MockUserRepository extends Mock implements UserRepository {}

void main() {
  group('LoginBloc', () {
    UserRepository userRepository;
    LoginBloc loginBloc;

    setUp(() {
      userRepository = MockUserRepository();
      loginBloc = LoginBloc(userRepository: userRepository);
    });

    blocTest(
      'should emit LoginState.failure() if repository throws on '
      'LoginWithGooglePressed event',
      build: () async {
        when(userRepository.signInWithGoogle()).thenThrow(Exception('oops'));
        return LoginBloc(userRepository: userRepository);
      },
      act: (bloc) => bloc.add(LoginWithGooglePressed()),
      wait: const Duration(milliseconds: 500),
      skip: 0,
      expect: [
        LoginState.empty(),
        LoginState.loading(),
        LoginState.failure(),
      ],
    );

    blocTest(
      'should emit LoginState.failure() if repository throws on '
      'LoginWithCredentialsPressed event',
      build: () async {
        when(userRepository.signInWithEmailAndPassword(
                'test@example.com', '123456'))
            .thenThrow(Exception('oops'));
        return LoginBloc(userRepository: userRepository);
      },
      act: (bloc) => bloc.add(LoginEvent.loginWithCredentialsPressed(
          email: 'test@example.com', password: '123456')),
      wait: const Duration(milliseconds: 500),
      skip: 0,
      expect: [
        LoginState.empty(),
        LoginState.loading(),
        LoginState.failure(),
      ],
    );

    blocTest(
      'should validate email in response to an invalid EmailChanged'
      ' Event',
      build: () async => LoginBloc(userRepository: userRepository),
      act: (bloc) => bloc.add(EmailChanged(email: 'test')),
      wait: const Duration(milliseconds: 500),
      skip: 0,
      expect: [
        LoginState.empty(),
        LoginState(
          isEmailValid: false,
          isPasswordValid: true,
          isSubmitting: false,
          isSuccess: false,
          isFailure: false,
        )
      ],
    );

    blocTest(
      'should validate email in response to a valid EmailChanged'
      ' Event',
      build: () async => LoginBloc(userRepository: userRepository),
      act: (bloc) => bloc.add(EmailChanged(email: 'test@example.com')),
      wait: const Duration(milliseconds: 500),
      skip: 0,
      expect: [LoginState.empty()],
    );

    blocTest(
        'should validate email in response to an invalid PasswordChanged'
        ' Event',
        build: () async => LoginBloc(userRepository: userRepository),
        act: (bloc) => bloc.add(PasswordChanged(password: '12345')),
        wait: const Duration(milliseconds: 500),
        skip: 0,
        expect: [
          LoginState.empty(),
          LoginState(
            isEmailValid: true,
            isPasswordValid: false,
            isSubmitting: false,
            isSuccess: false,
            isFailure: false,
          )
        ]);

    blocTest(
        'should validate email in response to an valid PasswordChanged'
        ' Event',
        build: () async => LoginBloc(userRepository: userRepository),
        act: (bloc) => bloc.add(PasswordChanged(password: '123456')),
        wait: const Duration(milliseconds: 500),
        skip: 0,
        expect: [LoginState.empty()]);

    tearDown(() {
      loginBloc.close();
    });
  });
}
