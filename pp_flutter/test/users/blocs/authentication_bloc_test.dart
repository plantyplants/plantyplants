import 'package:bloc_test/bloc_test.dart';
import 'package:mockito/mockito.dart';
import 'package:pp_flutter/users/blocs/authentication/bloc.dart';
import 'package:pp_flutter/users/repository/interface.dart';
import 'package:pp_flutter/users/repository/model.dart';
import 'package:test/test.dart';

class MockUserRepository extends Mock implements UserRepository {}

void main() {
  group('AuthenticationBloc', () {
    UserRepository userRepository;
    AuthenticationBloc authenticationBloc;

    setUp(() {
      userRepository = MockUserRepository();
      authenticationBloc = AuthenticationBloc(userRepository: userRepository);
    });

    test('should emit Unauthenticated if repository throws', () async {
      when(userRepository.isSignedIn()).thenThrow(Exception('oops'));

      authenticationBloc.add(AppStarted());

      await emitsExactly(
        authenticationBloc,
        <AuthenticationState>[
          Uninitialized(),
          Unauthenticated(),
        ],
        skip: 0,
      );
    });

    test('should authenticate a user in response to an AppStarted Event',
        () async {
      final user = User(
          uid: '000-000-000', email: 'test@test.com', displayName: 'Test User');

      when(userRepository.isSignedIn()).thenAnswer((_) => true);
      when(userRepository.getUser()).thenAnswer((_) => Future.value(user));

      authenticationBloc.add(AppStarted());

      emitsExactly(
        authenticationBloc,
        <AuthenticationState>[
          Uninitialized(),
          Authenticated(user),
        ],
        skip: 0,
      );
    });

    test('should authenticate a user in response to an LoggedIn Event',
        () async {
      final user = User(
          uid: '000-000-000', email: 'test@test.com', displayName: 'Test User');

      when(userRepository.getUser()).thenAnswer((_) => Future.value(user));

      authenticationBloc.add(LoggedIn());

      emitsExactly(
        authenticationBloc,
        <AuthenticationState>[
          Uninitialized(),
          Authenticated(user),
        ],
        skip: 0,
      );
    });

    test('should log user out in response to a LoggedOut Event', () {
      when(userRepository.signOut()).thenAnswer((_) => Future<void>.value());

      authenticationBloc.add(LoggedOut());

      emitsExactly(
        authenticationBloc,
        <AuthenticationState>[
          Uninitialized(),
          Unauthenticated(),
        ],
        skip: 0,
      );
    });

    tearDown(() {
      authenticationBloc.close();
    });
  });
}
