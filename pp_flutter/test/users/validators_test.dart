import 'package:pp_flutter/users/repository/validators.dart';
import 'package:test/test.dart';

void main() {
  group('users validators', () {
    test('invalid addresses shouldn\'t validate', () {
      expect(Validators.isValidEmail('foo'), false);
      expect(Validators.isValidEmail('bar@'), false);
    });

    test('valid addresses should validate', () {
      expect(Validators.isValidEmail('foo@bar.baz'), true);
      expect(Validators.isValidEmail('test@example.com'), true);
    });

    test('passwords shorter than 6 characters shouldn\'t validate', () {
      expect(Validators.isValidPassword('12345'), false);
      expect(Validators.isValidPassword('abc45'), false);
      expect(Validators.isValidPassword('abcde'), false);
    });

    test('passwords longer than 6 characters should validate', () {
      expect(Validators.isValidPassword('123456'), true);
      expect(Validators.isValidPassword('abc456'), true);
      expect(Validators.isValidPassword('abcdef'), true);
    });
  });
}
