# PlantyPlants flutter

Planty Plants Flutter app

## Getting Started

Generate freezed code:
``flutter pub run build_runner watch --delete-conflicting-outputs``

## Configuration

Create `.env` with the following content:

```
FIREBASE_API_KEY = 'Your firebase API key'
FIREBASE_AUTH_DOMAIN = 'Your firebase authentifcation domain'
FIREBASE_PROJECT_ID = 'Your project ID'
FIREBASE_DATABASE_URL = 'Your database URL'
FIREBASE_MESSAGING_SENDER_ID = 'Cloud messaging sender ID'
FIREBASE_STORAGE_BUCKET = 'Storage bucket domain'
FIREBASE_ANDROID_GOOGLE_APP_ID = 'Your Android Google App ID'
FIREBASE_IOS_GOOGLE_APP_ID = 'Your IOS Google App ID'
```

## Run tests

### Integration tests

```
$ flutter drive --target=test_driver/app.dart
```
