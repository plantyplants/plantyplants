import 'package:flutter_driver/flutter_driver.dart';
import 'package:pp_flutter/utils_keys.dart';
import 'package:test/test.dart';

import 'page_objects/page_objects.dart';

Future<void> _addMyPlant(FlutterDriver driver, ListTestScreen listTestScreen,
    String name, String description, bool picture) async {
  final addScreen = listTestScreen.tapAddButton();

  await driver
      .tap(find.byValueKey(keyStringFromEnum(Keys.formAddEditNameField)));
  await driver.enterText(name); // enter text
  await driver.waitFor(find.text(name)); // verify text appears on UI

  if (description != '') {
    await driver.tap(
        find.byValueKey(keyStringFromEnum(Keys.formAddEditDescriptionField)));
    await driver.enterText(description); // enter text
    await driver.waitFor(find.text(description)); // verify text appears on UI
  }

  if (picture) {
    await addScreen.tapCameraButton();
    await driver
        .waitFor(find.byValueKey(keyStringFromEnum(Keys.previewFrontPicture)));
  }

  await addScreen.tapSaveButton();
}

void main() {
  group('MyPlants Test', () {
    FlutterDriver driver;
    ListTestScreen listTestScreen;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
      listTestScreen = ListTestScreen(driver);
    });

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

//    test('should show a loading screen while the plants are fetched', ()
// async {
//      expect(await listTestScreen.isLoading(), isTrue);
//    });

    test('should show bottom tabs', () async {
      expect(await listTestScreen.isReady(), isTrue);

      await driver.waitFor(find.text('My plants'));
      await driver.waitFor(find.text('Notifications'));
    });

    test('should be able to add a plant without description #20', () async {
      await _addMyPlant(driver, listTestScreen, 'Name 1.1', '', false);

      await driver.waitFor(find.byType('GridMyPlantItem'));
      await driver.waitFor(find.text('Name 1.1'));
    });

    test(
        'should be able to click on a plant without description to see details'
        ' #20', () async {
      await driver.tap(find.text('Name 1.1'));
      await driver.waitFor(find.text('Name 1.1'));
    });

    test('should be able to delete a plant without description #20', () async {
      await driver
          .tap(find.byValueKey(keyStringFromEnum(Keys.deleteMyPlantButton)));

      await driver.waitForAbsent(find.text('Name 1.1'));
    });

    test('should be able to add a plant with description', () async {
      expect(await listTestScreen.isReady(), isTrue);

      _addMyPlant(driver, listTestScreen, 'Name 1.3', 'Description 1.3', false);

      await driver.waitFor(find.byType('GridMyPlantItem'));
      await driver.waitFor(find.text('Name 1.3'));
    });

    test('should be able to click on an item to see details', () async {
      await driver.tap(find.text('Name 1.3'));
      await driver.waitFor(find.text('Name 1.3'));
      await driver.waitFor(find.text('Description 1.3'));
    });

    test('should be able to return to list screen', () async {
      await driver.tap(find.byType('BackButtonIcon'));
      await driver.waitFor(find.text('Name 1.3'));
      await driver.waitForAbsent(find.text('Description 1.3'));
    });

    test('should be able to add a plant with photo', () async {
      expect(await listTestScreen.isReady(), isTrue);

      _addMyPlant(driver, listTestScreen, 'Name 1.2', 'Description 1.2', true);

      await driver.waitFor(find.byType('GridMyPlantItem'));
      await driver.waitFor(find.text('Name 1.2'));
    });

    test('should be able to click on an item with photo to see details',
        () async {
      await driver.tap(find.text('Name 1.2'));
      await driver.waitFor(find.text('Name 1.2'));
      await driver.waitFor(find.text('Description 1.2'));
    });

    test('should be able to edit a plant', () async {
      await driver.tap(find.byValueKey(keyStringFromEnum(Keys.editMyPlantFab)));

      await driver
          .tap(find.byValueKey(keyStringFromEnum(Keys.formAddEditNameField)));
      await driver.enterText('Edited Name'); // enter text
      await driver
          .waitFor(find.text('Edited Name')); // verify text appears on UI

      await driver.tap(
          find.byValueKey(keyStringFromEnum(Keys.formAddEditDescriptionField)));
      await driver.enterText('Edited Description'); // enter text
      await driver.waitFor(
          find.text('Edited Description')); // verify text appears on UI

      await driver.tap(find.byValueKey(keyStringFromEnum(Keys.saveMyPlantFab)));

      await driver.waitFor(find.text('Edited Name'));
    });

    test('should see Add Reminder label', () async {
      await driver.waitFor(find.text('Add a reminder'));
    });

    test('should be able to cancel adding a watering task', () async {
      await driver.tap(find.byValueKey(
          keyStringFromEnum(Keys.actionIconActivitiesNotifications)));

      await driver.tap(find.byValueKey(
          keyStringFromEnum(Keys.myPlantRecurringTaskConfigureDateTimeField)));

      await driver.waitFor(find.byTooltip('Switch to input'));
      await driver.tap(find.byTooltip('Switch to input'));
      await driver.enterText('11/11/2030');
      await driver.waitFor(find.text('11/11/2030'));
      await driver.tap(find.text('OK'));

      await driver.waitFor(find.byTooltip('Switch to text input mode'));
      await driver.tap(find.byTooltip('Switch to text input mode'));
      await driver.waitFor(find.byType('_HourTextField'));
      await driver.tap(find.byType('_HourTextField'));
      await driver.enterText('7');
      await driver.waitFor(find.byType('_MinuteTextField'));
      await driver.tap(find.byType('_MinuteTextField'));
      await driver.enterText('47');
      await driver.waitFor(find.text('AM'));
      await driver.tap(find.text('AM'));
      await driver.waitFor(find.text('OK'));
      await driver.tap(find.text('OK'));

      await driver.waitFor(find.text('Cancel'));
      await driver.tap(find.text('Cancel'));
    });

    test('should be able to add a watering task', () async {
      await driver.tap(find.text('Add a reminder'));

      await driver.tap(find.byValueKey(
          keyStringFromEnum(Keys.myPlantRecurringTaskConfigureDateTimeField)));

      await driver.waitFor(find.byTooltip('Switch to input'));
      await driver.tap(find.byTooltip('Switch to input'));
      await driver.enterText('12/01/2030');
      await driver.waitFor(find.text('12/01/2030'));
      await driver.tap(find.text('OK'));

      await driver.waitFor(find.byTooltip('Switch to text input mode'));
      await driver.tap(find.byTooltip('Switch to text input mode'));
      await driver.waitFor(find.byType('_HourTextField'));
      await driver.tap(find.byType('_HourTextField'));
      await driver.enterText('3');
      await driver.waitFor(find.byType('_MinuteTextField'));
      await driver.tap(find.byType('_MinuteTextField'));
      await driver.enterText('53');
      await driver.waitFor(find.text('PM'));
      await driver.tap(find.text('PM'));
      await driver.waitFor(find.text('OK'));
      await driver.tap(find.text('OK'));

      // FIXME: couldn't manage to get a way to click on repeat dropdown
      // that worked.
      // text Repeat
      // type DropdownFieldBlocBuilding
      // doesn't work: text Does not repeat
      // await driver.waitFor(find.text('Does not repeat'),
      //     timeout: Duration(seconds: 15));
      // await driver.tap(find.text('Does not repeat'),
      //     timeout: Duration(seconds: 15));
      // await driver.waitFor(find.text('Repeat'));
      // await driver.waitFor(find.byType('DropdownFieldBlocBuilding'));
      // // await driver.tap(find.byType('DropdownFieldBlocBuilding'));
      // await driver.waitFor(find.byValueKey(
      //     keyStringFromEnum(Keys.myPlantRecurringTaskConfigureRepeatField)));
      // // await driver.tap(find.byValueKey(
      // //     keyStringFromEnum(Keys.myPlantRecurringTaskConfigureRepeatField)));
      // await driver.waitFor(
      //     find.byValueKey(
      //         keyStringFromEnum(Keys.myPlantRecurringIconRepeatForm)),
      //     timeout: Duration(seconds: 50));
      // await driver.tap(
      //     find.byValueKey(
      //         keyStringFromEnum(Keys.myPlantRecurringIconRepeatForm)),
      //     timeout: Duration(seconds: 50));
      // // await driver.tap(find
      // //     .byValueKey(keyStringFromEnum(Keys.myPlantRecurringIconRepeatForm)));
      // await driver.waitFor(find.text('Weekly'));
      // await driver.tap(find.text('Weekly'));

      // var rendertree = await driver.getRenderTree();
      // var f = File('/tmp/flutter.json');
      // await f.writeAsString(rendertree.toJson().toString());

      // print(rendertree.toJson());
      // driver.getText(finder);
      // driver.getWidgetDiagnostics(finder)

      await driver.tap(find.byValueKey(
          keyStringFromEnum(Keys.myPlantRecurringTaskConfigureSave)));

      await driver.waitFor(
          find.byValueKey(keyStringFromEnum(Keys.myPlantRecurringIconAlarm)));
      await driver.waitFor(find.text('Sun, Dec 1, 15:53'));
      await driver.waitForAbsent(find.text('Add a reminder'));
    });

    test('should be able to edit a watering task', () async {
      await driver.tap(find.text('Sun, Dec 1, 15:53'));

      await driver.tap(find.byValueKey(
          keyStringFromEnum(Keys.myPlantRecurringTaskConfigureDateTimeField)));

      await driver.waitFor(find.byTooltip('Switch to input'));
      await driver.tap(find.byTooltip('Switch to input'));
      await driver.enterText('3/05/2030');
      await driver.waitFor(find.text('3/05/2030'));
      await driver.tap(find.text('OK'));

      await driver.waitFor(find.byTooltip('Switch to text input mode'));
      await driver.tap(find.byTooltip('Switch to text input mode'));
      await driver.waitFor(find.byType('_HourTextField'));
      await driver.tap(find.byType('_HourTextField'));
      await driver.enterText('11');
      await driver.waitFor(find.byType('_MinuteTextField'));
      await driver.tap(find.byType('_MinuteTextField'));
      await driver.enterText('44');
      await driver.waitFor(find.text('PM'));
      await driver.tap(find.text('PM'));
      await driver.waitFor(find.text('OK'));
      await driver.tap(find.text('OK'));

      await driver.tap(find.byValueKey(
          keyStringFromEnum(Keys.myPlantRecurringTaskConfigureSave)));

      await driver.waitFor(
          find.byValueKey(keyStringFromEnum(Keys.myPlantRecurringIconAlarm)));
      await driver.waitFor(find.text('Tue, Mar 5, 23:44'));
      await driver.waitForAbsent(find.text('Add a reminder'));
    });

    test('should be able to delete a watering task', () async {
      await driver.tap(find.text('Tue, Mar 5, 23:44'));

      await driver.tap(find.text('Delete'));

      await driver.waitForAbsent(find.text('Tue, Mar 5, 11:44 PM'));
      await driver.waitFor(find.text('Add a reminder'));
    });

    test('should be able to delete a plant on the details screen', () async {
      await driver
          .tap(find.byValueKey(keyStringFromEnum(Keys.deleteMyPlantButton)));

      await _addMyPlant(
          driver, listTestScreen, 'Name 2', 'Description 2', false);

      await driver.waitFor(find.text('Name 2'));
      await driver.waitForAbsent(find.text('Edited Name'));
    });
  });
}
