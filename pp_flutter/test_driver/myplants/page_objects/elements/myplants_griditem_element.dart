import 'package:flutter_driver/flutter_driver.dart';

import '../../../test_element.dart';

class PlantGridItemElement extends TestElement {
  final String id;

  PlantGridItemElement(this.id, FlutterDriver driver) : super(driver);
}
