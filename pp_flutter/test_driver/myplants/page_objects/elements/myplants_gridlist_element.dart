import 'dart:async';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:pp_flutter/utils_keys.dart';

import '../../../test_element.dart';
import '../../../utils.dart';

class MyPlantGridListElement extends TestElement {
  final SerializableFinder _myPlantGridListFinder =
      find.byValueKey(keyStringFromEnum(Keys.myPlantsList));
  final SerializableFinder _loadingFinder =
      find.byValueKey(keyStringFromEnum(Keys.myPlantsLoading));

  MyPlantGridListElement(FlutterDriver driver) : super(driver);

  // We need to run this command "unsynchronized". This means it immediately
  // checks if the loading widget is on screen, rather than waiting for any
  // pending animations to complete.
  //
  // Since the CircularProgressIndicator runs a continuous animation, if we
  // do not `runUnsynchronized`, this check will never work.
  Future<bool> get isLoading =>
      driver.runUnsynchronized(() => widgetExists(driver, _loadingFinder));

  Future<bool> get isReady => widgetExists(driver, _myPlantGridListFinder);
}
