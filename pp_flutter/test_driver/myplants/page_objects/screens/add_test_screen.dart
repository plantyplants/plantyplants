import 'dart:async';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:pp_flutter/utils_keys.dart';

import '../../../test_screen.dart';
import '../../../utils.dart';

class AddTestScreen extends TestScreen {
  final SerializableFinder _addScreenFinder =
      find.byValueKey(keyStringFromEnum(Keys.addMyPlantScreen));
  final SerializableFinder _saveButtonFinder =
      find.byValueKey(keyStringFromEnum(Keys.saveNewMyPlant));
  final SerializableFinder _cameraButtonFinder =
      find.byValueKey(keyStringFromEnum(Keys.changePictureCamera));

  AddTestScreen(FlutterDriver driver) : super(driver);

  @override
  Future<bool> isReady({Duration timeout}) =>
      widgetExists(driver, _addScreenFinder);

  Future<void> tapSaveButton() async {
    await driver.tap(_saveButtonFinder);

    return this;
  }

  Future<void> tapCameraButton() async {
    await driver.tap(_cameraButtonFinder);

    return this;
  }
}
