import 'dart:async';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:pp_flutter/utils_keys.dart';

import '../../../test_screen.dart';
import '../elements/myplants_gridlist_element.dart';
import '../screens/add_test_screen.dart';

class ListTestScreen extends TestScreen {
  final SerializableFinder _addButtonFinder =
      find.byValueKey(keyStringFromEnum(Keys.addMyPlantFab));

  ListTestScreen(FlutterDriver driver) : super(driver);

  @override
  Future<bool> isLoading({Duration timeout}) async =>
      MyPlantGridListElement(driver).isLoading;

  @override
  Future<bool> isReady({Duration timeout}) =>
      MyPlantGridListElement(driver).isReady;

  AddTestScreen tapAddButton() {
    driver.tap(_addButtonFinder);

    return AddTestScreen(driver);
  }
}
