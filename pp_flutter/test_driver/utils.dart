import 'dart:async';

import 'package:flutter_driver/flutter_driver.dart';

Future<bool> widgetExists(
  FlutterDriver driver,
  SerializableFinder finder, {
  Duration timeout,
}) async {
  try {
    await driver.waitFor(finder, timeout: timeout);

    return true;
    // ignore: avoid_catches_without_on_clauses
  } catch (_) {
    // TODO: implement logging
    return false;
  }
}

Future<bool> widgetAbsent(
  FlutterDriver driver,
  SerializableFinder finder, {
  Duration timeout,
}) async {
  try {
    await driver.waitForAbsent(finder, timeout: timeout);

    return true;
    // ignore: avoid_catches_without_on_clauses
  } catch (_) {
    // TODO: implement logging
    return false;
  }
}
