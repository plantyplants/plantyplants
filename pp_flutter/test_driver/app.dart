import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pp_flutter/main_local_storage.dart' as app;

void main() {
  enableFlutterDriverExtension();

  const channel = MethodChannel('plugins.flutter.io/image_picker');

  channel.setMockMethodCallHandler((methodCall) async {
    var data = await rootBundle.load('test_driver/images/400x400.png');
    var bytes = data.buffer.asUint8List();
    var tempDir = await getTemporaryDirectory();
    var file = await File(
      '${tempDir.path}/tmp.tmp',
    ).writeAsBytes(bytes);
    return file.path;
  });

  app.main();
}
