import 'myplants/myplants.dart' as plants_integration_tests;
import 'notifications/notifications.dart' as notifications_integration_tests;

void main() {
  plants_integration_tests.main();
  notifications_integration_tests.main();
}
