import 'package:flutter_driver/flutter_driver.dart';
import 'package:pp_flutter/utils_keys.dart';
import 'package:test/test.dart';

import '../myplants/page_objects/screens/list_test_screen.dart';

void main() {
  group('Notifications Test', () {
    FlutterDriver driver;
    ListTestScreen listTestScreen;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
      listTestScreen = ListTestScreen(driver);
    });

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('should be able to tap on Notifications', () async {
      expect(await listTestScreen.isReady(), isTrue);

      await driver.waitFor(find.text('My plants'));
      await driver.waitFor(find.text('Notifications'));
      await driver.tap(find.text('Notifications'));
    });

    test('should be able to see notifications', () async {
      await driver.waitFor(find.byType('NotificationsList'));

      await driver.waitFor(find.text('Time to water Tulip!'));
      await driver.waitFor(find.byValueKey(
          keyStringFromStringFunc('000-000-011', KeysFunc.notificationItem)));
      await driver.waitFor(find.byValueKey(keyStringFromStringFunc(
          '000-000-011', KeysFunc.notificationItemTitle)));

      await driver.waitFor(find.text('Time to water Magnolia!'));
      await driver.waitFor(find.byValueKey(
          keyStringFromStringFunc('000-000-012', KeysFunc.notificationItem)));
      await driver.waitFor(find.byValueKey(keyStringFromStringFunc(
          '000-000-012', KeysFunc.notificationItemTitle)));
    });

    test('should be redirected to plant screen when tapping on a notifications',
        () async {
      await driver.tap(find.byValueKey(
          keyStringFromStringFunc('000-000-011', KeysFunc.notificationItem)));

      await driver.waitFor(find.text('Tulip'));
      await driver.waitForAbsent(find.text('Magnolia'));
      await driver.waitFor(find.text('Add a reminder'));
      await driver.waitFor(
          find.byValueKey(keyStringFromEnum(Keys.deleteMyPlantButton)));
      await driver.waitFor(find.byValueKey(
          keyStringFromEnum(Keys.actionIconActivitiesNotifications)));
      await driver
          .waitFor(find.byValueKey(keyStringFromEnum(Keys.editMyPlantFab)));
    });
  });
}
