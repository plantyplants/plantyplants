// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$TabEventTearOff {
  const _$TabEventTearOff();

  TabUpdated updated({@required AppTab tab}) {
    return TabUpdated(
      tab: tab,
    );
  }
}

// ignore: unused_element
const $TabEvent = _$TabEventTearOff();

mixin _$TabEvent {
  AppTab get tab;

  $TabEventCopyWith<TabEvent> get copyWith;
}

abstract class $TabEventCopyWith<$Res> {
  factory $TabEventCopyWith(TabEvent value, $Res Function(TabEvent) then) =
      _$TabEventCopyWithImpl<$Res>;
  $Res call({AppTab tab});
}

class _$TabEventCopyWithImpl<$Res> implements $TabEventCopyWith<$Res> {
  _$TabEventCopyWithImpl(this._value, this._then);

  final TabEvent _value;
  // ignore: unused_field
  final $Res Function(TabEvent) _then;

  @override
  $Res call({
    Object tab = freezed,
  }) {
    return _then(_value.copyWith(
      tab: tab == freezed ? _value.tab : tab as AppTab,
    ));
  }
}

abstract class $TabUpdatedCopyWith<$Res> implements $TabEventCopyWith<$Res> {
  factory $TabUpdatedCopyWith(
          TabUpdated value, $Res Function(TabUpdated) then) =
      _$TabUpdatedCopyWithImpl<$Res>;
  @override
  $Res call({AppTab tab});
}

class _$TabUpdatedCopyWithImpl<$Res> extends _$TabEventCopyWithImpl<$Res>
    implements $TabUpdatedCopyWith<$Res> {
  _$TabUpdatedCopyWithImpl(TabUpdated _value, $Res Function(TabUpdated) _then)
      : super(_value, (v) => _then(v as TabUpdated));

  @override
  TabUpdated get _value => super._value as TabUpdated;

  @override
  $Res call({
    Object tab = freezed,
  }) {
    return _then(TabUpdated(
      tab: tab == freezed ? _value.tab : tab as AppTab,
    ));
  }
}

class _$TabUpdated implements TabUpdated {
  const _$TabUpdated({@required this.tab}) : assert(tab != null);

  @override
  final AppTab tab;

  @override
  String toString() {
    return 'TabEvent.updated(tab: $tab)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is TabUpdated &&
            (identical(other.tab, tab) ||
                const DeepCollectionEquality().equals(other.tab, tab)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(tab);

  @override
  $TabUpdatedCopyWith<TabUpdated> get copyWith =>
      _$TabUpdatedCopyWithImpl<TabUpdated>(this, _$identity);
}

abstract class TabUpdated implements TabEvent {
  const factory TabUpdated({@required AppTab tab}) = _$TabUpdated;

  @override
  AppTab get tab;
  @override
  $TabUpdatedCopyWith<TabUpdated> get copyWith;
}
