part of 'bloc.dart';

/// [TabEvent] is an [Event] related to the navigation tabs.
@freezed
abstract class TabEvent with _$TabEvent {
  /// App starts.
  const factory TabEvent.updated({@required AppTab tab}) = TabUpdated;
}
