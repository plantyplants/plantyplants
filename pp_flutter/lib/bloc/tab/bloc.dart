import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'bloc.freezed.dart';
part 'event.dart';
part 'state.dart';

/// [TabBloc] is a [Bloc] that handles navigation tabs.
class TabBloc extends Bloc<TabEvent, AppTab> {
  /// Initialize with default screen.
  TabBloc() : super();

  @override
  AppTab get initialState => AppTab.myPlants;

  @override
  Stream<AppTab> mapEventToState(TabEvent event) async* {
    if (event is TabUpdated) {
      yield event.tab;
    }
  }
}
