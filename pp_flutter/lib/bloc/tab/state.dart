part of 'bloc.dart';

/// Navigation tab of the app
enum AppTab {
  /// List of MyPlants
  myPlants,

  /// List of notifications
  notifications
}
