import 'package:bloc/bloc.dart';

/// SimpleBlocDelegate is a BlocDelegate that logs events, errors and
/// transitions.
class SimpleBlocDelegate extends BlocDelegate {
  @override
  // ignore: always_specify_types
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print('EVENT: $event');
  }

  @override
  // ignore: always_specify_types
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print('ERROR: $error');
  }

  @override
  // ignore: always_specify_types
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print('TRANSITION: $transition');
  }
}
