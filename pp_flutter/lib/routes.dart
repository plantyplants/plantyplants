// ignore: avoid_classes_with_only_static_members
/// Contains route paths.
class Routes {
  /// Home/root path.
  static const String home = '/';

  /// Path to add a new my plant.
  static const String addMyPlant = '/addMyPlant';

  /// Path to see details of a plant
  static const String detailsMyPlant = '/detailsMyPlant';

  /// Path to notifications screen
  static const String notifications = '/notifications';
}
