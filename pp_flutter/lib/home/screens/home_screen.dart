import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/tab/bloc.dart';
import '../../my_plants/blocs/my_plants/bloc.dart';
import '../../my_plants/repository/interface.dart';
import '../../my_plants/repository/model.dart';
import '../../my_plants/screens/myplant_details/add_edit_screen.dart';
import '../../my_plants/screens/myplant_grid/grid_screen.dart';
import '../../notifications/blocs/notification/bloc.dart';
import '../../notifications/repository/interface.dart';
import '../../notifications/screens/notifications_screen.dart';
import '../../routes.dart';
import '../../users/blocs/authentication/bloc.dart';
import '../../users/repository/interface.dart';
import '../../utils_keys.dart';
import '../../widgets/loading.dart';

/// Defines type of a function that takes a [BuildContext] and returns a
/// [Widget].
typedef BuilderFunction = Widget Function(BuildContext bc);

/// Landing screen.
class HomeScreen extends StatelessWidget {
  /// Initialize.
  HomeScreen() : super(key: Key(keyStringFromEnum(Keys.homeScreen)));

  @override
  Widget build(BuildContext context) => MultiBlocProvider(
        providers: <BlocProvider<Bloc<dynamic, dynamic>>>[
          BlocProvider<TabBloc>(
            create: (context) => TabBloc(),
          ),
          BlocProvider<AuthenticationBloc>(
              create: (context) => AuthenticationBloc(
                  userRepository: context.repository<UserRepository>())
                ..add(AppStarted())),
          BlocProvider<MyPlantsBloc>(
            create: (context) => MyPlantsBloc(
                myPlantsRepository: context.repository<MyPlantsRepository>(),
                authorId: null),
          ),
          BlocProvider<NotificationBloc>(
            create: (context) => NotificationBloc(
                notificationsRepository:
                    context.repository<NotificationsRepository>(),
                receiverUserId: null),
          ),
        ],
        child: MaterialApp(
            title: 'Planty Plants',
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            routes: <String, BuilderFunction>{
              Routes.home: (context) =>
                  BlocBuilder<AuthenticationBloc, AuthenticationState>(
                      builder: (context, state) {
                    if (state is Authenticated) {
                      context.bloc<MyPlantsBloc>().authorId = state.user.uid;
                      context.bloc<MyPlantsBloc>().add(LoadMyPlants());
                      return MyPlantListScreen();
                    }
                    if (state is Unauthenticated) {
                      // TODO: add logging
                      // XXX: Remove
                      print(state);
                      return Center(
                        child: Text('Could not authenticate with Firebase'),
                      );
                    }
                    return LoadingSpinner();
                  }),
              Routes.notifications: (context) =>
                  BlocBuilder<AuthenticationBloc, AuthenticationState>(
                      builder: (context, state) {
                    if (state is Authenticated) {
                      context.bloc<NotificationBloc>().receiverUserId =
                          state.user.uid;
                      context.bloc<MyPlantsBloc>().authorId = state.user.uid;
                      context.bloc<NotificationBloc>().add(LoadNotifications());
                      return NotificationsScreen();
                    }
                    if (state is Unauthenticated) {
                      // TODO: add logging
                      return Center(
                        child: Text('Could not authenticate with Firebase'),
                      );
                    }
                    return LoadingSpinner();
                  }),
              Routes.addMyPlant: (context) =>
                  BlocBuilder<AuthenticationBloc, AuthenticationState>(
                      builder: (context, authenticationState) {
                    if (authenticationState is Authenticated) {
                      return BlocBuilder<MyPlantsBloc, MyPlantsState>(
                          builder: (context, myPlantsState) {
                        if (myPlantsState is MyPlantsLoaded) {
                          return MyPlantAddEditScreen(
                            onSave: (name, description, {imageFiles}) {
                              context.bloc<MyPlantsBloc>().add(
                                    AddMyPlant(
                                      MyPlant(
                                          name: name,
                                          authorId:
                                              authenticationState.user.uid,
                                          description: description),
                                      frontPictureFormats: imageFiles,
                                    ),
                                  );
                            },
                            savingMessage: "Saving plant...",
                            popRoute: Routes.home,
                            isEditing: false,
                          );
                        }
                        // TODO: add logging
                        return LoadingSpinner();
                      });
                    }
                    // TODO: add logging
                    return LoadingSpinner();
                  })
            }),
      );
}
