import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/simple_bloc_delegate.dart';
import 'home/screens/home_screen.dart';
import 'my_plants/repository/interface.dart';
import 'my_plants/repository/mock.dart';
import 'notifications/repository/interface.dart';
import 'notifications/repository/mock.dart';
import 'users/repository/anonymous.dart';
import 'users/repository/interface.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(MultiRepositoryProvider(providers: <RepositoryProvider<dynamic>>[
    RepositoryProvider<MyPlantsRepository>(
      create: (context) => MockMyPlantsRepository(),
    ),
    RepositoryProvider<UserRepository>(
      create: (context) => AnonymousUserRepository(),
    ),
    RepositoryProvider<NotificationsRepository>(
      create: (context) => MockNotificationsRepository(),
    ),
  ], child: HomeScreen()));
}
