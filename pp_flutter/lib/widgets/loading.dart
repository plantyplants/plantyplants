import 'package:flutter/material.dart';

/// Loading spinner widget
class LoadingSpinner extends StatelessWidget {
  /// Initialize.
  LoadingSpinner({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Center(
        child: CircularProgressIndicator(),
      );
}
