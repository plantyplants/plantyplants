import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

/// Return a [Widget] containing the front picture.
Widget myPlantFrontPicture(String pictureFrontUrl) {
  if (pictureFrontUrl == null || pictureFrontUrl == '') {
    return Container(
      color: Colors.transparent,
    );
  }
  return CachedNetworkImage(
    imageUrl: pictureFrontUrl,
    progressIndicatorBuilder: (context, url, downloadProgress) =>
        CircularProgressIndicator(value: downloadProgress.progress),
    errorWidget: (context, url, error) => Icon(Icons.error),
  );
}
