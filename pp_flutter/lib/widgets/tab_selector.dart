import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../bloc/tab/bloc.dart';
import '../routes.dart';
import '../utils_keys.dart';

/// Widget for navigation tabs
class TabSelector extends StatelessWidget {
  /// Currently active tab
  final AppTab activeTab;

  /// Callback when a tab is selected
  final Function(AppTab) onTabSelected;

  /// Initializes widget with arguments.
  TabSelector({
    Key key,
    @required this.activeTab,
    @required this.onTabSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      key: ValueKey<String>(keyStringFromEnum(Keys.navigationTabs)),
      currentIndex: AppTab.values.indexOf(activeTab),
      onTap: (index) {
        var tab = AppTab.values[index];
        onTabSelected(tab);
        switch (tab) {
          case AppTab.myPlants:
            {
              Navigator.pushNamed(context, Routes.home);
            }
            break;

          case AppTab.notifications:
            {
              Navigator.pushNamed(context, Routes.notifications);
            }
            break;

          default:
            {
              Navigator.pushNamed(context, Routes.home);
            }
            break;
        }
      },
      items: AppTab.values.map((tab) {
        return BottomNavigationBarItem(
          icon: Icon(
            tab == AppTab.myPlants ? Icons.list : Icons.alarm,
            key: tab == AppTab.myPlants
                ? ValueKey<String>(keyStringFromEnum(Keys.myPlantsListScreen))
                : ValueKey<String>(keyStringFromEnum(Keys.notificationsScreen)),
          ),
          label: tab == AppTab.myPlants ? 'My plants' : 'Notifications',
        );
      }).toList(),
    );
  }
}
