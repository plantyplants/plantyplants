// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$FrontPictureFormatTearOff {
  const _$FrontPictureFormatTearOff();

  _FrontPictureFormat call({ImageConstraintTypes type, @required File file}) {
    return _FrontPictureFormat(
      type: type,
      file: file,
    );
  }
}

// ignore: unused_element
const $FrontPictureFormat = _$FrontPictureFormatTearOff();

mixin _$FrontPictureFormat {
  ImageConstraintTypes get type;
  File get file;

  $FrontPictureFormatCopyWith<FrontPictureFormat> get copyWith;
}

abstract class $FrontPictureFormatCopyWith<$Res> {
  factory $FrontPictureFormatCopyWith(
          FrontPictureFormat value, $Res Function(FrontPictureFormat) then) =
      _$FrontPictureFormatCopyWithImpl<$Res>;
  $Res call({ImageConstraintTypes type, File file});
}

class _$FrontPictureFormatCopyWithImpl<$Res>
    implements $FrontPictureFormatCopyWith<$Res> {
  _$FrontPictureFormatCopyWithImpl(this._value, this._then);

  final FrontPictureFormat _value;
  // ignore: unused_field
  final $Res Function(FrontPictureFormat) _then;

  @override
  $Res call({
    Object type = freezed,
    Object file = freezed,
  }) {
    return _then(_value.copyWith(
      type: type == freezed ? _value.type : type as ImageConstraintTypes,
      file: file == freezed ? _value.file : file as File,
    ));
  }
}

abstract class _$FrontPictureFormatCopyWith<$Res>
    implements $FrontPictureFormatCopyWith<$Res> {
  factory _$FrontPictureFormatCopyWith(
          _FrontPictureFormat value, $Res Function(_FrontPictureFormat) then) =
      __$FrontPictureFormatCopyWithImpl<$Res>;
  @override
  $Res call({ImageConstraintTypes type, File file});
}

class __$FrontPictureFormatCopyWithImpl<$Res>
    extends _$FrontPictureFormatCopyWithImpl<$Res>
    implements _$FrontPictureFormatCopyWith<$Res> {
  __$FrontPictureFormatCopyWithImpl(
      _FrontPictureFormat _value, $Res Function(_FrontPictureFormat) _then)
      : super(_value, (v) => _then(v as _FrontPictureFormat));

  @override
  _FrontPictureFormat get _value => super._value as _FrontPictureFormat;

  @override
  $Res call({
    Object type = freezed,
    Object file = freezed,
  }) {
    return _then(_FrontPictureFormat(
      type: type == freezed ? _value.type : type as ImageConstraintTypes,
      file: file == freezed ? _value.file : file as File,
    ));
  }
}

class _$_FrontPictureFormat extends _FrontPictureFormat {
  const _$_FrontPictureFormat({this.type, @required this.file})
      : assert(file != null),
        super._();

  @override
  final ImageConstraintTypes type;
  @override
  final File file;

  @override
  String toString() {
    return 'FrontPictureFormat(type: $type, file: $file)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FrontPictureFormat &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.file, file) ||
                const DeepCollectionEquality().equals(other.file, file)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(file);

  @override
  _$FrontPictureFormatCopyWith<_FrontPictureFormat> get copyWith =>
      __$FrontPictureFormatCopyWithImpl<_FrontPictureFormat>(this, _$identity);
}

abstract class _FrontPictureFormat extends FrontPictureFormat {
  const _FrontPictureFormat._() : super._();
  const factory _FrontPictureFormat(
      {ImageConstraintTypes type, @required File file}) = _$_FrontPictureFormat;

  @override
  ImageConstraintTypes get type;
  @override
  File get file;
  @override
  _$FrontPictureFormatCopyWith<_FrontPictureFormat> get copyWith;
}

MyPlant _$MyPlantFromJson(Map<String, dynamic> json) {
  return _MyPlant.fromJson(json);
}

class _$MyPlantTearOff {
  const _$MyPlantTearOff();

  _MyPlant call(
      {String id,
      @required String name,
      @required String authorId,
      String description,
      String pictureFrontGridUrl,
      String pictureFrontDetailsUrl,
      String pictureFrontFullUrl,
      RecurringTaskWatering recurringTaskWatering}) {
    return _MyPlant(
      id: id,
      name: name,
      authorId: authorId,
      description: description,
      pictureFrontGridUrl: pictureFrontGridUrl,
      pictureFrontDetailsUrl: pictureFrontDetailsUrl,
      pictureFrontFullUrl: pictureFrontFullUrl,
      recurringTaskWatering: recurringTaskWatering,
    );
  }
}

// ignore: unused_element
const $MyPlant = _$MyPlantTearOff();

mixin _$MyPlant {
  String get id;
  String get name;
  String get authorId;
  String get description;
  String get pictureFrontGridUrl;
  String get pictureFrontDetailsUrl;
  String get pictureFrontFullUrl;
  RecurringTaskWatering get recurringTaskWatering;

  Map<String, dynamic> toJson();
  $MyPlantCopyWith<MyPlant> get copyWith;
}

abstract class $MyPlantCopyWith<$Res> {
  factory $MyPlantCopyWith(MyPlant value, $Res Function(MyPlant) then) =
      _$MyPlantCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String name,
      String authorId,
      String description,
      String pictureFrontGridUrl,
      String pictureFrontDetailsUrl,
      String pictureFrontFullUrl,
      RecurringTaskWatering recurringTaskWatering});

  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering;
}

class _$MyPlantCopyWithImpl<$Res> implements $MyPlantCopyWith<$Res> {
  _$MyPlantCopyWithImpl(this._value, this._then);

  final MyPlant _value;
  // ignore: unused_field
  final $Res Function(MyPlant) _then;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object authorId = freezed,
    Object description = freezed,
    Object pictureFrontGridUrl = freezed,
    Object pictureFrontDetailsUrl = freezed,
    Object pictureFrontFullUrl = freezed,
    Object recurringTaskWatering = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      authorId: authorId == freezed ? _value.authorId : authorId as String,
      description:
          description == freezed ? _value.description : description as String,
      pictureFrontGridUrl: pictureFrontGridUrl == freezed
          ? _value.pictureFrontGridUrl
          : pictureFrontGridUrl as String,
      pictureFrontDetailsUrl: pictureFrontDetailsUrl == freezed
          ? _value.pictureFrontDetailsUrl
          : pictureFrontDetailsUrl as String,
      pictureFrontFullUrl: pictureFrontFullUrl == freezed
          ? _value.pictureFrontFullUrl
          : pictureFrontFullUrl as String,
      recurringTaskWatering: recurringTaskWatering == freezed
          ? _value.recurringTaskWatering
          : recurringTaskWatering as RecurringTaskWatering,
    ));
  }

  @override
  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering {
    if (_value.recurringTaskWatering == null) {
      return null;
    }
    return $RecurringTaskWateringCopyWith<$Res>(_value.recurringTaskWatering,
        (value) {
      return _then(_value.copyWith(recurringTaskWatering: value));
    });
  }
}

abstract class _$MyPlantCopyWith<$Res> implements $MyPlantCopyWith<$Res> {
  factory _$MyPlantCopyWith(_MyPlant value, $Res Function(_MyPlant) then) =
      __$MyPlantCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String name,
      String authorId,
      String description,
      String pictureFrontGridUrl,
      String pictureFrontDetailsUrl,
      String pictureFrontFullUrl,
      RecurringTaskWatering recurringTaskWatering});

  @override
  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering;
}

class __$MyPlantCopyWithImpl<$Res> extends _$MyPlantCopyWithImpl<$Res>
    implements _$MyPlantCopyWith<$Res> {
  __$MyPlantCopyWithImpl(_MyPlant _value, $Res Function(_MyPlant) _then)
      : super(_value, (v) => _then(v as _MyPlant));

  @override
  _MyPlant get _value => super._value as _MyPlant;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object authorId = freezed,
    Object description = freezed,
    Object pictureFrontGridUrl = freezed,
    Object pictureFrontDetailsUrl = freezed,
    Object pictureFrontFullUrl = freezed,
    Object recurringTaskWatering = freezed,
  }) {
    return _then(_MyPlant(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      authorId: authorId == freezed ? _value.authorId : authorId as String,
      description:
          description == freezed ? _value.description : description as String,
      pictureFrontGridUrl: pictureFrontGridUrl == freezed
          ? _value.pictureFrontGridUrl
          : pictureFrontGridUrl as String,
      pictureFrontDetailsUrl: pictureFrontDetailsUrl == freezed
          ? _value.pictureFrontDetailsUrl
          : pictureFrontDetailsUrl as String,
      pictureFrontFullUrl: pictureFrontFullUrl == freezed
          ? _value.pictureFrontFullUrl
          : pictureFrontFullUrl as String,
      recurringTaskWatering: recurringTaskWatering == freezed
          ? _value.recurringTaskWatering
          : recurringTaskWatering as RecurringTaskWatering,
    ));
  }
}

@JsonSerializable()
class _$_MyPlant extends _MyPlant {
  const _$_MyPlant(
      {this.id,
      @required this.name,
      @required this.authorId,
      this.description,
      this.pictureFrontGridUrl,
      this.pictureFrontDetailsUrl,
      this.pictureFrontFullUrl,
      this.recurringTaskWatering})
      : assert(name != null),
        assert(authorId != null),
        super._();

  factory _$_MyPlant.fromJson(Map<String, dynamic> json) =>
      _$_$_MyPlantFromJson(json);

  @override
  final String id;
  @override
  final String name;
  @override
  final String authorId;
  @override
  final String description;
  @override
  final String pictureFrontGridUrl;
  @override
  final String pictureFrontDetailsUrl;
  @override
  final String pictureFrontFullUrl;
  @override
  final RecurringTaskWatering recurringTaskWatering;

  @override
  String toString() {
    return 'MyPlant(id: $id, name: $name, authorId: $authorId, description: $description, pictureFrontGridUrl: $pictureFrontGridUrl, pictureFrontDetailsUrl: $pictureFrontDetailsUrl, pictureFrontFullUrl: $pictureFrontFullUrl, recurringTaskWatering: $recurringTaskWatering)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MyPlant &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.authorId, authorId) ||
                const DeepCollectionEquality()
                    .equals(other.authorId, authorId)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.pictureFrontGridUrl, pictureFrontGridUrl) ||
                const DeepCollectionEquality()
                    .equals(other.pictureFrontGridUrl, pictureFrontGridUrl)) &&
            (identical(other.pictureFrontDetailsUrl, pictureFrontDetailsUrl) ||
                const DeepCollectionEquality().equals(
                    other.pictureFrontDetailsUrl, pictureFrontDetailsUrl)) &&
            (identical(other.pictureFrontFullUrl, pictureFrontFullUrl) ||
                const DeepCollectionEquality()
                    .equals(other.pictureFrontFullUrl, pictureFrontFullUrl)) &&
            (identical(other.recurringTaskWatering, recurringTaskWatering) ||
                const DeepCollectionEquality().equals(
                    other.recurringTaskWatering, recurringTaskWatering)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(authorId) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(pictureFrontGridUrl) ^
      const DeepCollectionEquality().hash(pictureFrontDetailsUrl) ^
      const DeepCollectionEquality().hash(pictureFrontFullUrl) ^
      const DeepCollectionEquality().hash(recurringTaskWatering);

  @override
  _$MyPlantCopyWith<_MyPlant> get copyWith =>
      __$MyPlantCopyWithImpl<_MyPlant>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_MyPlantToJson(this);
  }
}

abstract class _MyPlant extends MyPlant {
  const _MyPlant._() : super._();
  const factory _MyPlant(
      {String id,
      @required String name,
      @required String authorId,
      String description,
      String pictureFrontGridUrl,
      String pictureFrontDetailsUrl,
      String pictureFrontFullUrl,
      RecurringTaskWatering recurringTaskWatering}) = _$_MyPlant;

  factory _MyPlant.fromJson(Map<String, dynamic> json) = _$_MyPlant.fromJson;

  @override
  String get id;
  @override
  String get name;
  @override
  String get authorId;
  @override
  String get description;
  @override
  String get pictureFrontGridUrl;
  @override
  String get pictureFrontDetailsUrl;
  @override
  String get pictureFrontFullUrl;
  @override
  RecurringTaskWatering get recurringTaskWatering;
  @override
  _$MyPlantCopyWith<_MyPlant> get copyWith;
}

RecurringTaskWatering _$RecurringTaskWateringFromJson(
    Map<String, dynamic> json) {
  return _RecurringTaskWatering.fromJson(json);
}

class _$RecurringTaskWateringTearOff {
  const _$RecurringTaskWateringTearOff();

  _RecurringTaskWatering call(
      {String id,
      @required DateTime datetime,
      @required String repeat,
      int repeatDayEvery,
      int repeatWeekEvery,
      List<int> repeatWeekDay,
      int repeatMonthEvery,
      String repeatMonthOption,
      int repeatYearEvery}) {
    return _RecurringTaskWatering(
      id: id,
      datetime: datetime,
      repeat: repeat,
      repeatDayEvery: repeatDayEvery,
      repeatWeekEvery: repeatWeekEvery,
      repeatWeekDay: repeatWeekDay,
      repeatMonthEvery: repeatMonthEvery,
      repeatMonthOption: repeatMonthOption,
      repeatYearEvery: repeatYearEvery,
    );
  }
}

// ignore: unused_element
const $RecurringTaskWatering = _$RecurringTaskWateringTearOff();

mixin _$RecurringTaskWatering {
  String get id;
  DateTime get datetime;
  String get repeat;
  int get repeatDayEvery;
  int get repeatWeekEvery;
  List<int> get repeatWeekDay;
  int get repeatMonthEvery;
  String get repeatMonthOption;
  int get repeatYearEvery;

  Map<String, dynamic> toJson();
  $RecurringTaskWateringCopyWith<RecurringTaskWatering> get copyWith;
}

abstract class $RecurringTaskWateringCopyWith<$Res> {
  factory $RecurringTaskWateringCopyWith(RecurringTaskWatering value,
          $Res Function(RecurringTaskWatering) then) =
      _$RecurringTaskWateringCopyWithImpl<$Res>;
  $Res call(
      {String id,
      DateTime datetime,
      String repeat,
      int repeatDayEvery,
      int repeatWeekEvery,
      List<int> repeatWeekDay,
      int repeatMonthEvery,
      String repeatMonthOption,
      int repeatYearEvery});
}

class _$RecurringTaskWateringCopyWithImpl<$Res>
    implements $RecurringTaskWateringCopyWith<$Res> {
  _$RecurringTaskWateringCopyWithImpl(this._value, this._then);

  final RecurringTaskWatering _value;
  // ignore: unused_field
  final $Res Function(RecurringTaskWatering) _then;

  @override
  $Res call({
    Object id = freezed,
    Object datetime = freezed,
    Object repeat = freezed,
    Object repeatDayEvery = freezed,
    Object repeatWeekEvery = freezed,
    Object repeatWeekDay = freezed,
    Object repeatMonthEvery = freezed,
    Object repeatMonthOption = freezed,
    Object repeatYearEvery = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      datetime: datetime == freezed ? _value.datetime : datetime as DateTime,
      repeat: repeat == freezed ? _value.repeat : repeat as String,
      repeatDayEvery: repeatDayEvery == freezed
          ? _value.repeatDayEvery
          : repeatDayEvery as int,
      repeatWeekEvery: repeatWeekEvery == freezed
          ? _value.repeatWeekEvery
          : repeatWeekEvery as int,
      repeatWeekDay: repeatWeekDay == freezed
          ? _value.repeatWeekDay
          : repeatWeekDay as List<int>,
      repeatMonthEvery: repeatMonthEvery == freezed
          ? _value.repeatMonthEvery
          : repeatMonthEvery as int,
      repeatMonthOption: repeatMonthOption == freezed
          ? _value.repeatMonthOption
          : repeatMonthOption as String,
      repeatYearEvery: repeatYearEvery == freezed
          ? _value.repeatYearEvery
          : repeatYearEvery as int,
    ));
  }
}

abstract class _$RecurringTaskWateringCopyWith<$Res>
    implements $RecurringTaskWateringCopyWith<$Res> {
  factory _$RecurringTaskWateringCopyWith(_RecurringTaskWatering value,
          $Res Function(_RecurringTaskWatering) then) =
      __$RecurringTaskWateringCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      DateTime datetime,
      String repeat,
      int repeatDayEvery,
      int repeatWeekEvery,
      List<int> repeatWeekDay,
      int repeatMonthEvery,
      String repeatMonthOption,
      int repeatYearEvery});
}

class __$RecurringTaskWateringCopyWithImpl<$Res>
    extends _$RecurringTaskWateringCopyWithImpl<$Res>
    implements _$RecurringTaskWateringCopyWith<$Res> {
  __$RecurringTaskWateringCopyWithImpl(_RecurringTaskWatering _value,
      $Res Function(_RecurringTaskWatering) _then)
      : super(_value, (v) => _then(v as _RecurringTaskWatering));

  @override
  _RecurringTaskWatering get _value => super._value as _RecurringTaskWatering;

  @override
  $Res call({
    Object id = freezed,
    Object datetime = freezed,
    Object repeat = freezed,
    Object repeatDayEvery = freezed,
    Object repeatWeekEvery = freezed,
    Object repeatWeekDay = freezed,
    Object repeatMonthEvery = freezed,
    Object repeatMonthOption = freezed,
    Object repeatYearEvery = freezed,
  }) {
    return _then(_RecurringTaskWatering(
      id: id == freezed ? _value.id : id as String,
      datetime: datetime == freezed ? _value.datetime : datetime as DateTime,
      repeat: repeat == freezed ? _value.repeat : repeat as String,
      repeatDayEvery: repeatDayEvery == freezed
          ? _value.repeatDayEvery
          : repeatDayEvery as int,
      repeatWeekEvery: repeatWeekEvery == freezed
          ? _value.repeatWeekEvery
          : repeatWeekEvery as int,
      repeatWeekDay: repeatWeekDay == freezed
          ? _value.repeatWeekDay
          : repeatWeekDay as List<int>,
      repeatMonthEvery: repeatMonthEvery == freezed
          ? _value.repeatMonthEvery
          : repeatMonthEvery as int,
      repeatMonthOption: repeatMonthOption == freezed
          ? _value.repeatMonthOption
          : repeatMonthOption as String,
      repeatYearEvery: repeatYearEvery == freezed
          ? _value.repeatYearEvery
          : repeatYearEvery as int,
    ));
  }
}

@JsonSerializable()
class _$_RecurringTaskWatering extends _RecurringTaskWatering {
  const _$_RecurringTaskWatering(
      {this.id,
      @required this.datetime,
      @required this.repeat,
      this.repeatDayEvery,
      this.repeatWeekEvery,
      this.repeatWeekDay,
      this.repeatMonthEvery,
      this.repeatMonthOption,
      this.repeatYearEvery})
      : assert(datetime != null),
        assert(repeat != null),
        super._();

  factory _$_RecurringTaskWatering.fromJson(Map<String, dynamic> json) =>
      _$_$_RecurringTaskWateringFromJson(json);

  @override
  final String id;
  @override
  final DateTime datetime;
  @override
  final String repeat;
  @override
  final int repeatDayEvery;
  @override
  final int repeatWeekEvery;
  @override
  final List<int> repeatWeekDay;
  @override
  final int repeatMonthEvery;
  @override
  final String repeatMonthOption;
  @override
  final int repeatYearEvery;

  @override
  String toString() {
    return 'RecurringTaskWatering(id: $id, datetime: $datetime, repeat: $repeat, repeatDayEvery: $repeatDayEvery, repeatWeekEvery: $repeatWeekEvery, repeatWeekDay: $repeatWeekDay, repeatMonthEvery: $repeatMonthEvery, repeatMonthOption: $repeatMonthOption, repeatYearEvery: $repeatYearEvery)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RecurringTaskWatering &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.datetime, datetime) ||
                const DeepCollectionEquality()
                    .equals(other.datetime, datetime)) &&
            (identical(other.repeat, repeat) ||
                const DeepCollectionEquality().equals(other.repeat, repeat)) &&
            (identical(other.repeatDayEvery, repeatDayEvery) ||
                const DeepCollectionEquality()
                    .equals(other.repeatDayEvery, repeatDayEvery)) &&
            (identical(other.repeatWeekEvery, repeatWeekEvery) ||
                const DeepCollectionEquality()
                    .equals(other.repeatWeekEvery, repeatWeekEvery)) &&
            (identical(other.repeatWeekDay, repeatWeekDay) ||
                const DeepCollectionEquality()
                    .equals(other.repeatWeekDay, repeatWeekDay)) &&
            (identical(other.repeatMonthEvery, repeatMonthEvery) ||
                const DeepCollectionEquality()
                    .equals(other.repeatMonthEvery, repeatMonthEvery)) &&
            (identical(other.repeatMonthOption, repeatMonthOption) ||
                const DeepCollectionEquality()
                    .equals(other.repeatMonthOption, repeatMonthOption)) &&
            (identical(other.repeatYearEvery, repeatYearEvery) ||
                const DeepCollectionEquality()
                    .equals(other.repeatYearEvery, repeatYearEvery)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(datetime) ^
      const DeepCollectionEquality().hash(repeat) ^
      const DeepCollectionEquality().hash(repeatDayEvery) ^
      const DeepCollectionEquality().hash(repeatWeekEvery) ^
      const DeepCollectionEquality().hash(repeatWeekDay) ^
      const DeepCollectionEquality().hash(repeatMonthEvery) ^
      const DeepCollectionEquality().hash(repeatMonthOption) ^
      const DeepCollectionEquality().hash(repeatYearEvery);

  @override
  _$RecurringTaskWateringCopyWith<_RecurringTaskWatering> get copyWith =>
      __$RecurringTaskWateringCopyWithImpl<_RecurringTaskWatering>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_RecurringTaskWateringToJson(this);
  }
}

abstract class _RecurringTaskWatering extends RecurringTaskWatering {
  const _RecurringTaskWatering._() : super._();
  const factory _RecurringTaskWatering(
      {String id,
      @required DateTime datetime,
      @required String repeat,
      int repeatDayEvery,
      int repeatWeekEvery,
      List<int> repeatWeekDay,
      int repeatMonthEvery,
      String repeatMonthOption,
      int repeatYearEvery}) = _$_RecurringTaskWatering;

  factory _RecurringTaskWatering.fromJson(Map<String, dynamic> json) =
      _$_RecurringTaskWatering.fromJson;

  @override
  String get id;
  @override
  DateTime get datetime;
  @override
  String get repeat;
  @override
  int get repeatDayEvery;
  @override
  int get repeatWeekEvery;
  @override
  List<int> get repeatWeekDay;
  @override
  int get repeatMonthEvery;
  @override
  String get repeatMonthOption;
  @override
  int get repeatYearEvery;
  @override
  _$RecurringTaskWateringCopyWith<_RecurringTaskWatering> get copyWith;
}
