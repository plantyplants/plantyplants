// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_MyPlant _$_$_MyPlantFromJson(Map<String, dynamic> json) {
  return _$_MyPlant(
    id: json['id'] as String,
    name: json['name'] as String,
    authorId: json['authorId'] as String,
    description: json['description'] as String,
    pictureFrontGridUrl: json['pictureFrontGridUrl'] as String,
    pictureFrontDetailsUrl: json['pictureFrontDetailsUrl'] as String,
    pictureFrontFullUrl: json['pictureFrontFullUrl'] as String,
    recurringTaskWatering: json['recurringTaskWatering'] == null
        ? null
        : RecurringTaskWatering.fromJson(
            json['recurringTaskWatering'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$_$_MyPlantToJson(_$_MyPlant instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'authorId': instance.authorId,
      'description': instance.description,
      'pictureFrontGridUrl': instance.pictureFrontGridUrl,
      'pictureFrontDetailsUrl': instance.pictureFrontDetailsUrl,
      'pictureFrontFullUrl': instance.pictureFrontFullUrl,
      'recurringTaskWatering': instance.recurringTaskWatering,
    };

_$_RecurringTaskWatering _$_$_RecurringTaskWateringFromJson(
    Map<String, dynamic> json) {
  return _$_RecurringTaskWatering(
    id: json['id'] as String,
    datetime: json['datetime'] == null
        ? null
        : DateTime.parse(json['datetime'] as String),
    repeat: json['repeat'] as String,
    repeatDayEvery: json['repeatDayEvery'] as int,
    repeatWeekEvery: json['repeatWeekEvery'] as int,
    repeatWeekDay:
        (json['repeatWeekDay'] as List)?.map((e) => e as int)?.toList(),
    repeatMonthEvery: json['repeatMonthEvery'] as int,
    repeatMonthOption: json['repeatMonthOption'] as String,
    repeatYearEvery: json['repeatYearEvery'] as int,
  );
}

Map<String, dynamic> _$_$_RecurringTaskWateringToJson(
        _$_RecurringTaskWatering instance) =>
    <String, dynamic>{
      'id': instance.id,
      'datetime': instance.datetime?.toIso8601String(),
      'repeat': instance.repeat,
      'repeatDayEvery': instance.repeatDayEvery,
      'repeatWeekEvery': instance.repeatWeekEvery,
      'repeatWeekDay': instance.repeatWeekDay,
      'repeatMonthEvery': instance.repeatMonthEvery,
      'repeatMonthOption': instance.repeatMonthOption,
      'repeatYearEvery': instance.repeatYearEvery,
    };
