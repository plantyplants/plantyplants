import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:mime/mime.dart';
import 'package:path/path.dart' as path;

import '../../utils_uuid.dart';
import 'entities.dart';
import 'interface.dart';
import 'model.dart';

/// [Map] for [ImageConstraintTypes]: [String] where [String] is the firestore
/// field name.
const Map<ImageConstraintTypes, String> imageMappingField = {
  ImageConstraintTypes.frontGrid: 'pictureFrontGridUrl',
  ImageConstraintTypes.frontDetails: 'pictureFrontDetailsUrl',
  ImageConstraintTypes.frontFull: 'pictureFrontFullUrl',
};

/// [FirebaseMyPlantsRepository] is a [MyPlantsRepository] to persist the
/// [MyPlant] objects.
class FirebaseMyPlantsRepository extends MyPlantsRepository {
  /// Firestore path prefix for [MyPlant].
  static const String firestorePathPrefix = 'my_plants';

  /// Firebase Storage path prefix for [MyPlant] front picture.
  static String storagePathPrefixFrontPicture =
      path.join('my_plants', 'picture_front');

  /// Get the [CollectionReference] for all [MyPlant] of the repository.
  CollectionReference get myPlantCollection =>
      FirebaseFirestore.instance.collection(firestorePathPrefix);

  /// Get reference path for [RecurringTaskWatering] with a given plant.
  String storagePathRefRecurringTaskWatering(String taskId) =>
      path.join('recurringTasks', taskId);

  /// Get the [CollectionReference] for [RecurringTaskWatering] of a given
  /// plant.
  CollectionReference recurringTaskWateringCollection(String plantId) =>
      FirebaseFirestore.instance
          .collection('$firestorePathPrefix/$plantId/recurringTasks');

  Future<MyPlant> _uploadFrontPicture(
      MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats) async {
    var myPlantMap = myPlant.toEntity().toMapWithoutId();
    var pictureUuid = Uuid().generateV4();
    // Update the images and collect download URLs in plant map.
    for (var format in frontPictureFormats) {
      myPlantMap[imageMappingField[format.type]] =
          await _uploadFrontPictureOneFormat(myPlantMap, pictureUuid, format);
    }
    return MyPlant.fromJson(myPlantMap);
  }

  /// Add a new [MyPlant] to the repository.
  @override
  Future<void> addNewMyPlant(MyPlant myPlant,
      {List<FrontPictureFormat> frontPictureFormats}) async {
    var myPlantWithPicture;
    if (frontPictureFormats != null && frontPictureFormats.isNotEmpty) {
      myPlantWithPicture =
          await _uploadFrontPicture(myPlant, frontPictureFormats);
    } else {
      myPlantWithPicture = myPlant;
    }
    // Save the plant in DB.
    await myPlantCollection.add(myPlantWithPicture.toEntity().toMapWithoutId());
  }

  /// Update a [MyPlant] in the repository, takes the updated MyPlant and
  /// File of the new front picture (if any).
  /// It updates attributes for the [MyPlant] with that ID.
  @override
  Future<void> updateMyPlant(MyPlant update,
      {List<FrontPictureFormat> frontPictureFormats}) async {
    MyPlant myPlantWithPicture;
    if (frontPictureFormats != null && frontPictureFormats.isNotEmpty) {
      myPlantWithPicture =
          await _uploadFrontPicture(update, frontPictureFormats);
    } else {
      myPlantWithPicture = update;
    }
    // Save the plant in DB.
    myPlantCollection
        .doc(update.id)
        .update(myPlantWithPicture.toEntity().toMapWithoutId());
  }

  /// Delete a [MyPlant] from the repository.
  @override
  Future<void> deleteMyPlant(MyPlant myPlant) async =>
      myPlantCollection.doc(myPlant.id).delete();

  MyPlant _docToPlant(DocumentSnapshot doc) =>
      MyPlant.fromEntity(MyPlantEntity.fromDocument(doc));

  /// Returns a list of all [MyPlant] from the repository, as a stream of lists.
  @override
  Stream<List<MyPlant>> myPlants(String authorId) => myPlantCollection
      .where('authorId', isEqualTo: authorId)
      .snapshots()
      .map((snapshot) => snapshot.docs.map(_docToPlant).toList());

  /// Get a [MyPlant] from the repository, as a future, for the specific
  /// [MyPlant] ID and author [User] ID.
  @override
  Future<MyPlant> myPlant(String plantId, String authorId) async =>
      _docToPlant(await myPlantCollection.doc(plantId).get());

  /// Upload the front picture for one format
  /// Takes a [MyPlant] map and a [FrontPictureFormat] as input.
  /// Returns the download URL for the uploaded image.
  Future<String> _uploadFrontPictureOneFormat(Map<String, Object> myPlantMap,
      String pictureUuid, FrontPictureFormat format) async {
    var pictureFilename = '${pictureUuid}_${format.toString()}.jpg';
    var pictureFbPath = path.join('/', storagePathPrefixFrontPicture,
        myPlantMap['authorId'], pictureFilename);
    var pictureFbUrl =
        'gs://${DotEnv().env['FIREBASE_STORAGE_BUCKET']}/$pictureFbPath';
    var contentType = lookupMimeType(format.file.path);
    var ref = await FirebaseStorage.instance.refFromURL(pictureFbUrl);

    await ref.putFile(format.file, SettableMetadata(contentType: contentType));
    return await ref.getDownloadURL();
  }

  /// Add a new [RecurringTaskWatering] in the repository, takes the Plant ID
  /// and the [RecurringTaskWatering].
  @override
  Future<void> addNewRecurringTaskWatering(
      String plantId, RecurringTaskWatering recurringTaskWatering) async {
    final recurringTasksCollection = recurringTaskWateringCollection(plantId);
    final myPlantDocRef = await myPlantCollection.doc(plantId);

    final taskDocRef = await recurringTasksCollection
        .add(recurringTaskWatering.toEntity().toMapWithoutId());

    await myPlantDocRef.update({
      'recurringTasksWatering':
          storagePathRefRecurringTaskWatering(taskDocRef.id)
    });
  }

  /// Delete a [RecurringTaskWatering] from the repository.
  @override
  Future<void> deleteRecurringTaskWatering(String plantId) async {
    final recurringTasksCollection = recurringTaskWateringCollection(plantId);
    final myPlantDocRef = await myPlantCollection.doc(plantId);

    var documents = await recurringTasksCollection.get();
    documents.docs.first.reference.delete();
    myPlantDocRef.update({'recurringTasksWatering': null});
  }

  RecurringTaskWatering _docToRecurringTask(DocumentSnapshot doc) =>
      RecurringTaskWatering.fromEntity(
          RecurringTaskWateringEntity.fromDocument(doc));

  /// Get the [RecurringTaskWatering] for a given plant from the
  /// repository, as a stream.
  /// Takes plant ID as input.
  @override
  Stream<RecurringTaskWatering> recurringTaskWateringStream(String plantId) =>
      recurringTaskWateringCollection(plantId).snapshots().map((snapshot) {
        var tasks = snapshot.docs.map(_docToRecurringTask);
        return tasks.isEmpty ? null : tasks.first;
      });

  /// Get the [RecurringTaskWatering] for a given plant from the
  /// repository.
  /// Takes plant ID as input.
  Future<RecurringTaskWatering> recurringTaskWatering(String plantId) async {
    final recurringTasks = await recurringTaskWateringCollection(plantId).get();
    if (recurringTasks.docs.isEmpty) {
      return null;
    }
    return RecurringTaskWatering.fromEntity(
        RecurringTaskWateringEntity.fromDocument(recurringTasks.docs.first));
  }

  /// Update a [RecurringTaskWatering] in the repository, takes the plant ID
  /// and updated [RecurringTaskWatering].
  @override
  Future<void> updateRecurringTaskWatering(
      String plantId, RecurringTaskWatering recurringTaskWatering) async {
    final recurringTasksCollection =
        await recurringTaskWateringCollection(plantId);
    final recurringTaskDocuments = await recurringTasksCollection.get();
    final recurringTaskDocRef = recurringTaskDocuments.docs.first;
    await recurringTaskDocRef.reference
        .update(recurringTaskWatering.toEntity().toMapWithoutId());
  }
}
