import 'model.dart';

/// [MyPlantsRepository] is a [Repository] for [MyPlant] objects
abstract class MyPlantsRepository {
  /// Add a new [MyPlant] to the repository, takes the new [MyPlant] and
  /// Files for the front picture (if any).
  Future<void> addNewMyPlant(MyPlant myPlant,
      {List<FrontPictureFormat> frontPictureFormats});

  /// Delete a [MyPlant] from the repository.
  Future<void> deleteMyPlant(MyPlant myPlant);

  /// Returns a list of all [MyPlant] from the repository, as a stream of lists.
  Stream<List<MyPlant>> myPlants(String authorId);

  /// Get a [MyPlant] from the repository, as a future, for the specific
  /// [MyPlant] ID and author [User] ID.
  Future<MyPlant> myPlant(String plantId, String authorId);

  /// Update a [MyPlant] in the repository, takes the updated [MyPlant] and
  /// Files for the front picture (if any).
  /// It updates attributes for the [MyPlant] with that ID.
  Future<void> updateMyPlant(MyPlant myPlant,
      {List<FrontPictureFormat> frontPictureFormats});

  /// Add a new [RecurringTaskWatering] in the repository, takes the Plant ID
  /// and the [RecurringTaskWatering].
  Future<void> addNewRecurringTaskWatering(
      String plantId, RecurringTaskWatering recurringTaskWatering);

  /// Delete a [RecurringTaskWatering] from the repository.
  Future<void> deleteRecurringTaskWatering(String plantId);

  /// Get the [RecurringTaskWatering] for a given plant from the
  /// repository, as a stream.
  /// Takes plant ID as input.
  Stream<RecurringTaskWatering> recurringTaskWateringStream(String plantId);

  /// Get the [RecurringTaskWatering] for a given plant from the
  /// repository.
  /// Takes plant ID as input.
  Future<RecurringTaskWatering> recurringTaskWatering(String plantId);

  /// Update a [RecurringTaskWatering] in the repository, takes the plant ID
  /// and updated [RecurringTaskWatering].
  Future<void> updateRecurringTaskWatering(
      String plantId, RecurringTaskWatering recurringTaskWatering);
}
