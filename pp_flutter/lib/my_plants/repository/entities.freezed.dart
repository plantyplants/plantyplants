// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'entities.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$MyPlantEntityTearOff {
  const _$MyPlantEntityTearOff();

  _MyPlantEntity call(
      {String id,
      @required String name,
      @required String authorId,
      @required String description,
      String pictureFrontGridUrl,
      String pictureFrontDetailsUrl,
      String pictureFrontFullUrl,
      RecurringTaskWateringEntity recurringTaskWatering}) {
    return _MyPlantEntity(
      id: id,
      name: name,
      authorId: authorId,
      description: description,
      pictureFrontGridUrl: pictureFrontGridUrl,
      pictureFrontDetailsUrl: pictureFrontDetailsUrl,
      pictureFrontFullUrl: pictureFrontFullUrl,
      recurringTaskWatering: recurringTaskWatering,
    );
  }
}

// ignore: unused_element
const $MyPlantEntity = _$MyPlantEntityTearOff();

mixin _$MyPlantEntity {
  String get id;
  String get name;
  String get authorId;
  String get description;
  String get pictureFrontGridUrl;
  String get pictureFrontDetailsUrl;
  String get pictureFrontFullUrl;
  RecurringTaskWateringEntity get recurringTaskWatering;

  $MyPlantEntityCopyWith<MyPlantEntity> get copyWith;
}

abstract class $MyPlantEntityCopyWith<$Res> {
  factory $MyPlantEntityCopyWith(
          MyPlantEntity value, $Res Function(MyPlantEntity) then) =
      _$MyPlantEntityCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String name,
      String authorId,
      String description,
      String pictureFrontGridUrl,
      String pictureFrontDetailsUrl,
      String pictureFrontFullUrl,
      RecurringTaskWateringEntity recurringTaskWatering});

  $RecurringTaskWateringEntityCopyWith<$Res> get recurringTaskWatering;
}

class _$MyPlantEntityCopyWithImpl<$Res>
    implements $MyPlantEntityCopyWith<$Res> {
  _$MyPlantEntityCopyWithImpl(this._value, this._then);

  final MyPlantEntity _value;
  // ignore: unused_field
  final $Res Function(MyPlantEntity) _then;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object authorId = freezed,
    Object description = freezed,
    Object pictureFrontGridUrl = freezed,
    Object pictureFrontDetailsUrl = freezed,
    Object pictureFrontFullUrl = freezed,
    Object recurringTaskWatering = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      authorId: authorId == freezed ? _value.authorId : authorId as String,
      description:
          description == freezed ? _value.description : description as String,
      pictureFrontGridUrl: pictureFrontGridUrl == freezed
          ? _value.pictureFrontGridUrl
          : pictureFrontGridUrl as String,
      pictureFrontDetailsUrl: pictureFrontDetailsUrl == freezed
          ? _value.pictureFrontDetailsUrl
          : pictureFrontDetailsUrl as String,
      pictureFrontFullUrl: pictureFrontFullUrl == freezed
          ? _value.pictureFrontFullUrl
          : pictureFrontFullUrl as String,
      recurringTaskWatering: recurringTaskWatering == freezed
          ? _value.recurringTaskWatering
          : recurringTaskWatering as RecurringTaskWateringEntity,
    ));
  }

  @override
  $RecurringTaskWateringEntityCopyWith<$Res> get recurringTaskWatering {
    if (_value.recurringTaskWatering == null) {
      return null;
    }
    return $RecurringTaskWateringEntityCopyWith<$Res>(
        _value.recurringTaskWatering, (value) {
      return _then(_value.copyWith(recurringTaskWatering: value));
    });
  }
}

abstract class _$MyPlantEntityCopyWith<$Res>
    implements $MyPlantEntityCopyWith<$Res> {
  factory _$MyPlantEntityCopyWith(
          _MyPlantEntity value, $Res Function(_MyPlantEntity) then) =
      __$MyPlantEntityCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String name,
      String authorId,
      String description,
      String pictureFrontGridUrl,
      String pictureFrontDetailsUrl,
      String pictureFrontFullUrl,
      RecurringTaskWateringEntity recurringTaskWatering});

  @override
  $RecurringTaskWateringEntityCopyWith<$Res> get recurringTaskWatering;
}

class __$MyPlantEntityCopyWithImpl<$Res>
    extends _$MyPlantEntityCopyWithImpl<$Res>
    implements _$MyPlantEntityCopyWith<$Res> {
  __$MyPlantEntityCopyWithImpl(
      _MyPlantEntity _value, $Res Function(_MyPlantEntity) _then)
      : super(_value, (v) => _then(v as _MyPlantEntity));

  @override
  _MyPlantEntity get _value => super._value as _MyPlantEntity;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object authorId = freezed,
    Object description = freezed,
    Object pictureFrontGridUrl = freezed,
    Object pictureFrontDetailsUrl = freezed,
    Object pictureFrontFullUrl = freezed,
    Object recurringTaskWatering = freezed,
  }) {
    return _then(_MyPlantEntity(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      authorId: authorId == freezed ? _value.authorId : authorId as String,
      description:
          description == freezed ? _value.description : description as String,
      pictureFrontGridUrl: pictureFrontGridUrl == freezed
          ? _value.pictureFrontGridUrl
          : pictureFrontGridUrl as String,
      pictureFrontDetailsUrl: pictureFrontDetailsUrl == freezed
          ? _value.pictureFrontDetailsUrl
          : pictureFrontDetailsUrl as String,
      pictureFrontFullUrl: pictureFrontFullUrl == freezed
          ? _value.pictureFrontFullUrl
          : pictureFrontFullUrl as String,
      recurringTaskWatering: recurringTaskWatering == freezed
          ? _value.recurringTaskWatering
          : recurringTaskWatering as RecurringTaskWateringEntity,
    ));
  }
}

class _$_MyPlantEntity extends _MyPlantEntity with DiagnosticableTreeMixin {
  const _$_MyPlantEntity(
      {this.id,
      @required this.name,
      @required this.authorId,
      @required this.description,
      this.pictureFrontGridUrl,
      this.pictureFrontDetailsUrl,
      this.pictureFrontFullUrl,
      this.recurringTaskWatering})
      : assert(name != null),
        assert(authorId != null),
        assert(description != null),
        super._();

  @override
  final String id;
  @override
  final String name;
  @override
  final String authorId;
  @override
  final String description;
  @override
  final String pictureFrontGridUrl;
  @override
  final String pictureFrontDetailsUrl;
  @override
  final String pictureFrontFullUrl;
  @override
  final RecurringTaskWateringEntity recurringTaskWatering;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'MyPlantEntity(id: $id, name: $name, authorId: $authorId, description: $description, pictureFrontGridUrl: $pictureFrontGridUrl, pictureFrontDetailsUrl: $pictureFrontDetailsUrl, pictureFrontFullUrl: $pictureFrontFullUrl, recurringTaskWatering: $recurringTaskWatering)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'MyPlantEntity'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('name', name))
      ..add(DiagnosticsProperty('authorId', authorId))
      ..add(DiagnosticsProperty('description', description))
      ..add(DiagnosticsProperty('pictureFrontGridUrl', pictureFrontGridUrl))
      ..add(
          DiagnosticsProperty('pictureFrontDetailsUrl', pictureFrontDetailsUrl))
      ..add(DiagnosticsProperty('pictureFrontFullUrl', pictureFrontFullUrl))
      ..add(
          DiagnosticsProperty('recurringTaskWatering', recurringTaskWatering));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MyPlantEntity &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.authorId, authorId) ||
                const DeepCollectionEquality()
                    .equals(other.authorId, authorId)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.pictureFrontGridUrl, pictureFrontGridUrl) ||
                const DeepCollectionEquality()
                    .equals(other.pictureFrontGridUrl, pictureFrontGridUrl)) &&
            (identical(other.pictureFrontDetailsUrl, pictureFrontDetailsUrl) ||
                const DeepCollectionEquality().equals(
                    other.pictureFrontDetailsUrl, pictureFrontDetailsUrl)) &&
            (identical(other.pictureFrontFullUrl, pictureFrontFullUrl) ||
                const DeepCollectionEquality()
                    .equals(other.pictureFrontFullUrl, pictureFrontFullUrl)) &&
            (identical(other.recurringTaskWatering, recurringTaskWatering) ||
                const DeepCollectionEquality().equals(
                    other.recurringTaskWatering, recurringTaskWatering)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(authorId) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(pictureFrontGridUrl) ^
      const DeepCollectionEquality().hash(pictureFrontDetailsUrl) ^
      const DeepCollectionEquality().hash(pictureFrontFullUrl) ^
      const DeepCollectionEquality().hash(recurringTaskWatering);

  @override
  _$MyPlantEntityCopyWith<_MyPlantEntity> get copyWith =>
      __$MyPlantEntityCopyWithImpl<_MyPlantEntity>(this, _$identity);
}

abstract class _MyPlantEntity extends MyPlantEntity {
  const _MyPlantEntity._() : super._();
  const factory _MyPlantEntity(
      {String id,
      @required String name,
      @required String authorId,
      @required String description,
      String pictureFrontGridUrl,
      String pictureFrontDetailsUrl,
      String pictureFrontFullUrl,
      RecurringTaskWateringEntity recurringTaskWatering}) = _$_MyPlantEntity;

  @override
  String get id;
  @override
  String get name;
  @override
  String get authorId;
  @override
  String get description;
  @override
  String get pictureFrontGridUrl;
  @override
  String get pictureFrontDetailsUrl;
  @override
  String get pictureFrontFullUrl;
  @override
  RecurringTaskWateringEntity get recurringTaskWatering;
  @override
  _$MyPlantEntityCopyWith<_MyPlantEntity> get copyWith;
}

class _$RecurringTaskWateringEntityTearOff {
  const _$RecurringTaskWateringEntityTearOff();

  _RecurringTaskWateringEntity call(
      {String id,
      @required DateTime datetime,
      @required String repeat,
      int repeatDayEvery,
      int repeatWeekEvery,
      List<int> repeatWeekDay,
      int repeatMonthEvery,
      String repeatMonthOption,
      int repeatYearEvery}) {
    return _RecurringTaskWateringEntity(
      id: id,
      datetime: datetime,
      repeat: repeat,
      repeatDayEvery: repeatDayEvery,
      repeatWeekEvery: repeatWeekEvery,
      repeatWeekDay: repeatWeekDay,
      repeatMonthEvery: repeatMonthEvery,
      repeatMonthOption: repeatMonthOption,
      repeatYearEvery: repeatYearEvery,
    );
  }
}

// ignore: unused_element
const $RecurringTaskWateringEntity = _$RecurringTaskWateringEntityTearOff();

mixin _$RecurringTaskWateringEntity {
  String get id;
  DateTime get datetime;
  String get repeat;
  int get repeatDayEvery;
  int get repeatWeekEvery;
  List<int> get repeatWeekDay;
  int get repeatMonthEvery;
  String get repeatMonthOption;
  int get repeatYearEvery;

  $RecurringTaskWateringEntityCopyWith<RecurringTaskWateringEntity>
      get copyWith;
}

abstract class $RecurringTaskWateringEntityCopyWith<$Res> {
  factory $RecurringTaskWateringEntityCopyWith(
          RecurringTaskWateringEntity value,
          $Res Function(RecurringTaskWateringEntity) then) =
      _$RecurringTaskWateringEntityCopyWithImpl<$Res>;
  $Res call(
      {String id,
      DateTime datetime,
      String repeat,
      int repeatDayEvery,
      int repeatWeekEvery,
      List<int> repeatWeekDay,
      int repeatMonthEvery,
      String repeatMonthOption,
      int repeatYearEvery});
}

class _$RecurringTaskWateringEntityCopyWithImpl<$Res>
    implements $RecurringTaskWateringEntityCopyWith<$Res> {
  _$RecurringTaskWateringEntityCopyWithImpl(this._value, this._then);

  final RecurringTaskWateringEntity _value;
  // ignore: unused_field
  final $Res Function(RecurringTaskWateringEntity) _then;

  @override
  $Res call({
    Object id = freezed,
    Object datetime = freezed,
    Object repeat = freezed,
    Object repeatDayEvery = freezed,
    Object repeatWeekEvery = freezed,
    Object repeatWeekDay = freezed,
    Object repeatMonthEvery = freezed,
    Object repeatMonthOption = freezed,
    Object repeatYearEvery = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      datetime: datetime == freezed ? _value.datetime : datetime as DateTime,
      repeat: repeat == freezed ? _value.repeat : repeat as String,
      repeatDayEvery: repeatDayEvery == freezed
          ? _value.repeatDayEvery
          : repeatDayEvery as int,
      repeatWeekEvery: repeatWeekEvery == freezed
          ? _value.repeatWeekEvery
          : repeatWeekEvery as int,
      repeatWeekDay: repeatWeekDay == freezed
          ? _value.repeatWeekDay
          : repeatWeekDay as List<int>,
      repeatMonthEvery: repeatMonthEvery == freezed
          ? _value.repeatMonthEvery
          : repeatMonthEvery as int,
      repeatMonthOption: repeatMonthOption == freezed
          ? _value.repeatMonthOption
          : repeatMonthOption as String,
      repeatYearEvery: repeatYearEvery == freezed
          ? _value.repeatYearEvery
          : repeatYearEvery as int,
    ));
  }
}

abstract class _$RecurringTaskWateringEntityCopyWith<$Res>
    implements $RecurringTaskWateringEntityCopyWith<$Res> {
  factory _$RecurringTaskWateringEntityCopyWith(
          _RecurringTaskWateringEntity value,
          $Res Function(_RecurringTaskWateringEntity) then) =
      __$RecurringTaskWateringEntityCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      DateTime datetime,
      String repeat,
      int repeatDayEvery,
      int repeatWeekEvery,
      List<int> repeatWeekDay,
      int repeatMonthEvery,
      String repeatMonthOption,
      int repeatYearEvery});
}

class __$RecurringTaskWateringEntityCopyWithImpl<$Res>
    extends _$RecurringTaskWateringEntityCopyWithImpl<$Res>
    implements _$RecurringTaskWateringEntityCopyWith<$Res> {
  __$RecurringTaskWateringEntityCopyWithImpl(
      _RecurringTaskWateringEntity _value,
      $Res Function(_RecurringTaskWateringEntity) _then)
      : super(_value, (v) => _then(v as _RecurringTaskWateringEntity));

  @override
  _RecurringTaskWateringEntity get _value =>
      super._value as _RecurringTaskWateringEntity;

  @override
  $Res call({
    Object id = freezed,
    Object datetime = freezed,
    Object repeat = freezed,
    Object repeatDayEvery = freezed,
    Object repeatWeekEvery = freezed,
    Object repeatWeekDay = freezed,
    Object repeatMonthEvery = freezed,
    Object repeatMonthOption = freezed,
    Object repeatYearEvery = freezed,
  }) {
    return _then(_RecurringTaskWateringEntity(
      id: id == freezed ? _value.id : id as String,
      datetime: datetime == freezed ? _value.datetime : datetime as DateTime,
      repeat: repeat == freezed ? _value.repeat : repeat as String,
      repeatDayEvery: repeatDayEvery == freezed
          ? _value.repeatDayEvery
          : repeatDayEvery as int,
      repeatWeekEvery: repeatWeekEvery == freezed
          ? _value.repeatWeekEvery
          : repeatWeekEvery as int,
      repeatWeekDay: repeatWeekDay == freezed
          ? _value.repeatWeekDay
          : repeatWeekDay as List<int>,
      repeatMonthEvery: repeatMonthEvery == freezed
          ? _value.repeatMonthEvery
          : repeatMonthEvery as int,
      repeatMonthOption: repeatMonthOption == freezed
          ? _value.repeatMonthOption
          : repeatMonthOption as String,
      repeatYearEvery: repeatYearEvery == freezed
          ? _value.repeatYearEvery
          : repeatYearEvery as int,
    ));
  }
}

class _$_RecurringTaskWateringEntity extends _RecurringTaskWateringEntity
    with DiagnosticableTreeMixin {
  const _$_RecurringTaskWateringEntity(
      {this.id,
      @required this.datetime,
      @required this.repeat,
      this.repeatDayEvery,
      this.repeatWeekEvery,
      this.repeatWeekDay,
      this.repeatMonthEvery,
      this.repeatMonthOption,
      this.repeatYearEvery})
      : assert(datetime != null),
        assert(repeat != null),
        super._();

  @override
  final String id;
  @override
  final DateTime datetime;
  @override
  final String repeat;
  @override
  final int repeatDayEvery;
  @override
  final int repeatWeekEvery;
  @override
  final List<int> repeatWeekDay;
  @override
  final int repeatMonthEvery;
  @override
  final String repeatMonthOption;
  @override
  final int repeatYearEvery;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'RecurringTaskWateringEntity(id: $id, datetime: $datetime, repeat: $repeat, repeatDayEvery: $repeatDayEvery, repeatWeekEvery: $repeatWeekEvery, repeatWeekDay: $repeatWeekDay, repeatMonthEvery: $repeatMonthEvery, repeatMonthOption: $repeatMonthOption, repeatYearEvery: $repeatYearEvery)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'RecurringTaskWateringEntity'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('datetime', datetime))
      ..add(DiagnosticsProperty('repeat', repeat))
      ..add(DiagnosticsProperty('repeatDayEvery', repeatDayEvery))
      ..add(DiagnosticsProperty('repeatWeekEvery', repeatWeekEvery))
      ..add(DiagnosticsProperty('repeatWeekDay', repeatWeekDay))
      ..add(DiagnosticsProperty('repeatMonthEvery', repeatMonthEvery))
      ..add(DiagnosticsProperty('repeatMonthOption', repeatMonthOption))
      ..add(DiagnosticsProperty('repeatYearEvery', repeatYearEvery));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RecurringTaskWateringEntity &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.datetime, datetime) ||
                const DeepCollectionEquality()
                    .equals(other.datetime, datetime)) &&
            (identical(other.repeat, repeat) ||
                const DeepCollectionEquality().equals(other.repeat, repeat)) &&
            (identical(other.repeatDayEvery, repeatDayEvery) ||
                const DeepCollectionEquality()
                    .equals(other.repeatDayEvery, repeatDayEvery)) &&
            (identical(other.repeatWeekEvery, repeatWeekEvery) ||
                const DeepCollectionEquality()
                    .equals(other.repeatWeekEvery, repeatWeekEvery)) &&
            (identical(other.repeatWeekDay, repeatWeekDay) ||
                const DeepCollectionEquality()
                    .equals(other.repeatWeekDay, repeatWeekDay)) &&
            (identical(other.repeatMonthEvery, repeatMonthEvery) ||
                const DeepCollectionEquality()
                    .equals(other.repeatMonthEvery, repeatMonthEvery)) &&
            (identical(other.repeatMonthOption, repeatMonthOption) ||
                const DeepCollectionEquality()
                    .equals(other.repeatMonthOption, repeatMonthOption)) &&
            (identical(other.repeatYearEvery, repeatYearEvery) ||
                const DeepCollectionEquality()
                    .equals(other.repeatYearEvery, repeatYearEvery)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(datetime) ^
      const DeepCollectionEquality().hash(repeat) ^
      const DeepCollectionEquality().hash(repeatDayEvery) ^
      const DeepCollectionEquality().hash(repeatWeekEvery) ^
      const DeepCollectionEquality().hash(repeatWeekDay) ^
      const DeepCollectionEquality().hash(repeatMonthEvery) ^
      const DeepCollectionEquality().hash(repeatMonthOption) ^
      const DeepCollectionEquality().hash(repeatYearEvery);

  @override
  _$RecurringTaskWateringEntityCopyWith<_RecurringTaskWateringEntity>
      get copyWith => __$RecurringTaskWateringEntityCopyWithImpl<
          _RecurringTaskWateringEntity>(this, _$identity);
}

abstract class _RecurringTaskWateringEntity
    extends RecurringTaskWateringEntity {
  const _RecurringTaskWateringEntity._() : super._();
  const factory _RecurringTaskWateringEntity(
      {String id,
      @required DateTime datetime,
      @required String repeat,
      int repeatDayEvery,
      int repeatWeekEvery,
      List<int> repeatWeekDay,
      int repeatMonthEvery,
      String repeatMonthOption,
      int repeatYearEvery}) = _$_RecurringTaskWateringEntity;

  @override
  String get id;
  @override
  DateTime get datetime;
  @override
  String get repeat;
  @override
  int get repeatDayEvery;
  @override
  int get repeatWeekEvery;
  @override
  List<int> get repeatWeekDay;
  @override
  int get repeatMonthEvery;
  @override
  String get repeatMonthOption;
  @override
  int get repeatYearEvery;
  @override
  _$RecurringTaskWateringEntityCopyWith<_RecurringTaskWateringEntity>
      get copyWith;
}
