import 'package:file/memory.dart';
import 'package:uuid/uuid.dart';

import '../form_blocs/recurring_task_configure_form_bloc.dart';
import 'interface.dart';
import 'model.dart';

/// Fake Author ID, used in tests.
const String authorIdFake = '000-000-000';

/// Fake additional Author ID, used in tests.
const String otherAuthorIdFake = '000-000-001';

/// Fake Plant ID, used in tests.
const String plantIdFake = '000-000-010';

/// Fake list of [MyPlant], used in tests.
List<MyPlant> myPlantsData = <MyPlant>[
  MyPlant(
      name: 'Tulip',
      authorId: authorIdFake,
      pictureFrontGridUrl: '',
      pictureFrontDetailsUrl: '',
      pictureFrontFullUrl: '',
      description: '',
      id: '000-000-001'),
  MyPlant(
      name: 'Magnolia',
      authorId: authorIdFake,
      pictureFrontGridUrl: '',
      pictureFrontDetailsUrl: '',
      pictureFrontFullUrl: '',
      description: 'Magnificent Magnolia tree',
      id: '000-000-002'),
  MyPlant(
      name: 'Rose',
      authorId: authorIdFake,
      pictureFrontGridUrl: '',
      pictureFrontDetailsUrl: '',
      pictureFrontFullUrl: '',
      description: 'super pretty',
      id: '000-000-003'),
  MyPlant(
      name: 'Apple tree',
      authorId: otherAuthorIdFake,
      pictureFrontGridUrl: '',
      pictureFrontDetailsUrl: '',
      pictureFrontFullUrl: '',
      description: 'yummy apples',
      id: '000-000-004'),
];

/// Fake list of FrontPictureFormat
List<FrontPictureFormat> frontPictureFormatsFake = <FrontPictureFormat>[
  FrontPictureFormat(
    type: ImageConstraintTypes.frontGrid,
    file: MemoryFileSystem().file('test_grid'),
  ),
  FrontPictureFormat(
    type: ImageConstraintTypes.frontDetails,
    file: MemoryFileSystem().file('test_details'),
  ),
  FrontPictureFormat(
    type: ImageConstraintTypes.frontFull,
    file: MemoryFileSystem().file('test_full'),
  ),
];

/// Fake list of [RecurringTaskWatering], used in tests.
List<RecurringTaskWatering> recurringTaskWateringTestData =
    <RecurringTaskWatering>[
  RecurringTaskWatering(
      datetime: DateTime(2031, 1, 1, 0, 0),
      repeat: repeatChoicesEnum.no.displayLabel,
      repeatDayEvery: 1,
      repeatWeekEvery: 1,
      repeatWeekDay: [],
      repeatMonthEvery: 1,
      repeatMonthOption: '',
      repeatYearEvery: 1,
      id: '000-000-001'),
  RecurringTaskWatering(
      datetime: DateTime(2032, 2, 2, 0, 2),
      repeat: repeatChoicesEnum.weekly.displayLabel,
      repeatDayEvery: 1,
      repeatWeekEvery: 1,
      repeatWeekDay: [DateTime.friday],
      repeatMonthEvery: 1,
      repeatMonthOption: '',
      repeatYearEvery: 1,
      id: '000-000-002'),
  RecurringTaskWatering(
      datetime: DateTime(2033, 3, 3, 0, 3),
      repeat: repeatChoicesEnum.monthly.displayLabel,
      repeatDayEvery: 1,
      repeatWeekEvery: 1,
      repeatWeekDay: [],
      repeatMonthEvery: 1,
      repeatMonthOption: sameDayEachMonth,
      repeatYearEvery: 1,
      id: '000-000-003'),
  RecurringTaskWatering(
      datetime: DateTime(2033, 3, 3, 0, 3),
      repeat: repeatChoicesEnum.monthly.displayLabel,
      repeatDayEvery: 1,
      repeatWeekEvery: 1,
      repeatWeekDay: [],
      repeatMonthEvery: 1,
      repeatMonthOption: RecurringTaskConfigureFormBloc.repeatMonthOptionLabels(
          DateTime(2033, 3, 3, 0, 3),
          humanReadable: false)[1],
      repeatYearEvery: 1,
      id: '000-000-004'),
];

/// Mock implementation of [MyPlantsRepository] used for testing, filled with
/// sample data, works in memory.
class MockMyPlantsRepository extends MyPlantsRepository {
  /// Contains the list of [MyPlant] in the repository.
  List<MyPlant> data;

  /// Initialize data attribute with [myPlantsData].
  MockMyPlantsRepository() {
    data = myPlantsData;
  }

  /// Returns a list of all [MyPlant] from the repository, as a stream of lists.
  @override
  Stream<List<MyPlant>> myPlants(String authorId) =>
      Stream<List<MyPlant>>.fromIterable(<List<MyPlant>>[
        data.where((myPlant) => myPlant.authorId == authorId).toList()
      ]);

  /// Get a [MyPlant] from the repository, as a future, for the specific
  /// [MyPlant] ID and author [User] ID.
  @override
  Future<MyPlant> myPlant(String plantId, String authorId) =>
      Future<MyPlant>.value(data.firstWhere(
          (myPlant) => myPlant.id == plantId && myPlant.authorId == authorId));

  /// Add a new [MyPlant] to the repository.
  @override
  Future<void> addNewMyPlant(MyPlant myPlant,
      {List<FrontPictureFormat> frontPictureFormats}) async {
    final plantId = myPlant.id == null ? Uuid().v4() : myPlant.id;
    data..add(myPlant.copyWith(id: plantId));
  }

  /// Update a [MyPlant] in the repository, takes the updated MyPlant and
  /// updates attributes for the [MyPlant] with that ID.
  @override
  Future<void> updateMyPlant(MyPlant update,
          {List<FrontPictureFormat> frontPictureFormats}) async =>
      data = data.fold(
        <MyPlant>[],
        (prev, plant) => prev..add(plant.id == update.id ? update : plant),
      );

  /// Delete a [MyPlant] from the repository.
  @override
  Future<void> deleteMyPlant(MyPlant toDelete) async => data = data.fold(
        <MyPlant>[],
        (prev, plant) => plant.id == toDelete.id ? prev : (prev..add(plant)),
      );

  /// Add a new [RecurringTaskWatering] in the repository, takes the Plant ID
  /// and the [RecurringTaskWatering].
  @override
  Future<void> addNewRecurringTaskWatering(
      String plantId, RecurringTaskWatering recurringTaskWatering) async {
    var plant = await myPlant(plantId, authorIdFake);
    await updateMyPlant(
        plant.copyWith(recurringTaskWatering: recurringTaskWatering));
  }

  /// Delete a [RecurringTaskWatering] from the repository.
  @override
  Future<void> deleteRecurringTaskWatering(String plantId) async {
    var plant = await myPlant(plantId, authorIdFake);
    await updateMyPlant(plant.copyWith(recurringTaskWatering: null));
  }

  /// Get the [RecurringTaskWatering] for a given plant from the
  /// repository, as a stream of list.
  /// Takes plant ID as input.
  @override
  Stream<RecurringTaskWatering> recurringTaskWateringStream(
      String plantId) async* {
    var plant = await myPlant(plantId, authorIdFake);
    yield plant.recurringTaskWatering;
  }

  /// Get the [RecurringTaskWatering] for a given plant from the
  /// repository.
  /// Takes plant ID as input.
  Future<RecurringTaskWatering> recurringTaskWatering(String plantId) async {
    var plant = await myPlant(plantId, authorIdFake);
    return plant.recurringTaskWatering;
  }

  /// Update a [RecurringTaskWatering] in the repository, takes the plant ID
  /// and updated [RecurringTaskWatering].
  @override
  Future<void> updateRecurringTaskWatering(
          String plantId, RecurringTaskWatering recurringTaskWatering) async =>
      await addNewRecurringTaskWatering(plantId, recurringTaskWatering);
}
