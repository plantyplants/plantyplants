import 'dart:io';

import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:meta/meta.dart';
import 'package:path_provider/path_provider.dart';

import '../repository/model.dart';

typedef OnSaveCallback = Function(String name, String description,
    {List<FrontPictureFormat> imageFiles});

/// Form to add or edit a my plant.
class AddEditFormBloc extends FormBloc<String, String> {
  /// Initial value for name field.
  final String initialName;

  /// Initial value for description field.
  final String initialDescription;

  /// Callback to be executed to save the my plant.
  final OnSaveCallback onSave;

  ///
  /// Image
  ///

  File imageFile;

  /// Specific name plant field.
  final TextFieldBloc name = TextFieldBloc(
    validators: [
      FieldBlocValidators.required,
    ],
  );

  /// Specific description plant field.
  final TextFieldBloc description = TextFieldBloc();

  /// Constructor.
  AddEditFormBloc({
    this.initialName = '',
    this.initialDescription = '',
    @required this.onSave,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      name,
      description,
    ]);
  }

  @override
  void onLoading() async {
    name.updateInitialValue(initialName);
    description.updateInitialValue(initialDescription);
    emitLoaded();
  }

  @override
  void onSubmitting() async {
    final appTempDirectory = await getTemporaryDirectory();
    // ignore: omit_local_variable_types
    List<FrontPictureFormat> imageFiles = [];
    var futures = <Future>[];

    if (imageFile != null) {
      // Compress images to the different formats
      imagesFrontPictureConstraints.forEach((imageType, constraint) async {
        try {
          var targetFilePath = '${appTempDirectory.absolute.path}'
              '/'
              '${imageType.toString()}'
              '.jpg';
          var compressedFile = FlutterImageCompress.compressAndGetFile(
            imageFile.absolute.path,
            targetFilePath,
            minWidth: imagesFrontPictureConstraints[imageType].maxWidth.floor(),
            minHeight:
                imagesFrontPictureConstraints[imageType].maxHeight.floor(),
            quality: imagesFrontPictureConstraints[imageType].quality,
            keepExif: true,
          ).then((finishedFile) {
            imageFiles.add(FrontPictureFormat(
              type: imageType,
              file: finishedFile,
            ));
          }, onError: print); // XXX: TODO: implement logging
          futures.add(compressedFile);
          // ignore: avoid_catches_without_on_clauses
        } catch (e) {
          // XXX: TODO: implement logging
          print(e);
          emitFailure(failureResponse: 'Error while saving the plant.');
        }
      });
    }

    Future.wait(futures).then((_unused) {
      emitSuccess(
        canSubmitAgain: true,
        successResponse: 'Plant successfully saved',
      );
      onSave(name.value, description.value, imageFiles: imageFiles);
    });
  }
}
