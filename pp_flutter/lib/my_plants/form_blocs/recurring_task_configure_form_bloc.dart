import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

import '../blocs/recurring_task_watering/bloc.dart';
import '../repository/interface.dart';
import '../repository/model.dart';

typedef OnCustomCallback = Function();

// ignore: public_member_api_docs
const String higherThanZero = 'Must be higher than 0';
// ignore: public_member_api_docs
const String sameDayEachMonth = 'on the same day each month';

/// Validator to make sure a value is higher than 0
String higherThanZeroValidator(String value) {
  if (int.parse(value) > 0) {
    return null;
  }
  return higherThanZero;
}

/// Form to add or edit a my plant.
class RecurringTaskConfigureFormBloc extends FormBloc<String, String> {
  /// my plant ID
  final String plantId;

  /// MyPlants repository.
  final MyPlantsRepository myPlantsRepository;

  /// [RecurringTaskWateringBloc] bloc
  final RecurringTaskWateringBloc recurringTaskWateringBloc;

  /// Initial value for datetime field
  DateTime initialDatetime;

  /// Initial value for repeat field.
  String initialRepeat = repeatChoicesEnum.no.displayLabel;

  /// Initial value for repeat day every field.
  String initialRepeatDayEvery = '1';

  /// Initial value for repeat week every field.
  String initialRepeatWeekEvery = '1';

  /// Initial value for repeat week day field
  List<String> initialRepeatWeekDays;

  /// Initial value for repeat month every field.
  String initialRepeatMonthEvery = '1';

  /// Initial value for repeat month option field.
  String initialRepeatMonthOption;

  /// Initial value for repeat year every field.
  String initialRepeatYearEvery = '1';

  /// Date and time field
  final InputFieldBloc datetime = InputFieldBloc<DateTime, Object>(validators: [
    FieldBlocValidators.required,
  ]);

  /// Repeat field
  final SelectFieldBloc<String, Object> repeat = SelectFieldBloc(
    validators: [
      FieldBlocValidators.required,
    ],
  );

  /// Repeat day every field. Daily > Every [_] day(s).
  final TextFieldBloc repeatDayEvery =
      TextFieldBloc(initialValue: '1', validators: [
    FieldBlocValidators.required,
    higherThanZeroValidator,
  ]);

  /// Repeat week every field. Weekly > Every [_] week(s).
  final TextFieldBloc repeatWeekEvery =
      TextFieldBloc(initialValue: '1', validators: [
    FieldBlocValidators.required,
    higherThanZeroValidator,
  ]);

  /// Repeat week day field. Weekly > Day(s) of the week.
  final MultiSelectFieldBloc repeatWeekDay =
      MultiSelectFieldBloc<String, dynamic>(
    items: repeatDaysLabels.values.toList(),
    initialValue: [DateTime.now().weekday.toString()],
    validators: [
      FieldBlocValidators.required,
    ],
  );

  /// Repeat week every field. Monthly > Every [_] month(s).
  final TextFieldBloc repeatMonthEvery =
      TextFieldBloc(initialValue: '1', validators: [
    FieldBlocValidators.required,
    higherThanZeroValidator,
  ]);

  /// Repeat month option field. Monthly >
  final SelectFieldBloc<String, Object> repeatMonthOption =
      SelectFieldBloc(items: [], validators: [
    FieldBlocValidators.required,
  ]);

  /// Repeat week every field. Yearly > Every [_] year(s).
  final TextFieldBloc repeatYearEvery =
      TextFieldBloc(initialValue: '1', validators: [
    FieldBlocValidators.required,
    higherThanZeroValidator,
  ]);

  _showRepeatOptions(String repeatValue) {
    if (repeatValue == repeatChoicesEnum.daily.displayLabel) {
      addFieldBlocs(fieldBlocs: [
        repeatDayEvery,
      ]);
    } else if (repeatValue == repeatChoicesEnum.weekly.displayLabel) {
      addFieldBlocs(fieldBlocs: [
        repeatWeekEvery,
        repeatWeekDay,
      ]);

      if (datetime.value != initialDatetime ||
          (repeat.value != initialRepeat &&
              repeat.value == repeatChoicesEnum.weekly.displayLabel)) {
        repeatWeekDay.clear();
        DateTime dt = datetime.value;
        repeatWeekDay.select(repeatDaysLabels[dt.weekday]);
      }
    } else if (repeatValue == repeatChoicesEnum.monthly.displayLabel) {
      addFieldBlocs(fieldBlocs: [
        repeatMonthEvery,
        repeatMonthOption,
      ]);

      if (datetime.value != initialDatetime) {
        repeatMonthOption.clear();
      }
      repeatMonthOption..updateItems(repeatMonthOptionLabels(datetime.value));
    } else if (repeatValue == repeatChoicesEnum.yearly.displayLabel) {
      addFieldBlocs(fieldBlocs: [
        repeatYearEvery,
      ]);
    }
  }

  /// Constructor.
  RecurringTaskConfigureFormBloc({
    @required this.plantId,
    @required this.myPlantsRepository,
    @required this.recurringTaskWateringBloc,
  }) : super(isLoading: true) {
    addFieldBlocs(fieldBlocs: [
      datetime,
    ]);

    datetime.onValueChanges(onData: (previous, current) async* {
      removeFieldBlocs(fieldBlocs: [
        repeat,
        repeatDayEvery,
        repeatWeekEvery,
        repeatWeekDay,
        repeatMonthEvery,
        repeatMonthOption,
        repeatYearEvery,
      ]);

      if (current.value != '' && current.value != null) {
        addFieldBlocs(fieldBlocs: [
          repeat,
        ]);

        if (repeat.value != repeatChoicesEnum.no.displayLabel) {
          _showRepeatOptions(repeat.value);
        }
      }
    });

    repeat.onValueChanges(
      onData: (previous, current) async* {
        removeFieldBlocs(
          fieldBlocs: [
            repeatDayEvery,
            repeatWeekEvery,
            repeatWeekDay,
            repeatMonthEvery,
            repeatMonthOption,
            repeatYearEvery,
          ],
        );

        _showRepeatOptions(current.value);
      },
    );
  }

  /// Returns the week number during the month for a day. If it's on the last
  /// week, returns 'last'.
  /// Output: 'last', 'first', 'second', 'third', 'fourth' when humanReadable.
  /// Output: 'L', '1', '2', '3', '4' when not humanReadable.
  static String nthDay(DateTime dt, {bool humanReadable = true}) {
    if (dt.day >= 25) {
      return humanReadable ? 'last' : 'L';
    } else {
      switch ((dt.day / 7).ceil()) {
        case 1:
          {
            return humanReadable ? 'first' : '1';
          }
          break;

        case 2:
          {
            return humanReadable ? 'second' : '2';
          }
          break;

        case 3:
          {
            return humanReadable ? 'third' : '3';
          }
          break;

        case 4:
          {
            return humanReadable ? 'fourth' : '4';
          }
          break;
      }
    }
    return '';
  }

  /// Return labels for the repeat monthly option
  static List<String> repeatMonthOptionLabels(DateTime dt,
          {bool humanReadable = true}) =>
      [
        sameDayEachMonth,
        humanReadable
            ? 'on every ${nthDay(dt)} '
                '${repeatDaysLabels[dt.weekday]}'
            : '${nthDay(dt, humanReadable: humanReadable)} ${dt.weekday}',
      ];

  @override
  void onLoading() async {
    final recurringTaskWatering =
        await myPlantsRepository.recurringTaskWatering(plantId);
    if (recurringTaskWatering != null) {
      initialDatetime = recurringTaskWatering.datetime;
      initialRepeat = recurringTaskWatering.repeat;
      initialRepeatDayEvery = '${recurringTaskWatering.repeatDayEvery}';
      initialRepeatWeekEvery = '${recurringTaskWatering.repeatWeekEvery}';
      initialRepeatWeekDays = List<String>.from(recurringTaskWatering
          .repeatWeekDay
          .map((intDay) => repeatDaysLabels[intDay]));
      initialRepeatMonthEvery = '${recurringTaskWatering.repeatMonthEvery}';
      if (initialDatetime != null) {
        repeatMonthOption
          ..updateItems(repeatMonthOptionLabels(initialDatetime));
      }
      initialRepeatMonthOption =
          recurringTaskWatering.repeatMonthOption == sameDayEachMonth
              ? repeatMonthOptionLabels(initialDatetime)[0]
              : repeatMonthOptionLabels(initialDatetime)[1];
      initialRepeatYearEvery = '${recurringTaskWatering.repeatYearEvery}';
    }

    datetime.updateInitialValue(initialDatetime);
    repeat
      ..updateItems(repeatChoicesEnum.no.listLabels.values.toList())
      ..updateInitialValue(initialRepeat);
    repeatDayEvery.updateInitialValue(initialRepeatDayEvery);
    repeatWeekEvery.updateInitialValue(initialRepeatWeekEvery);
    repeatWeekDay.updateInitialValue(initialRepeatWeekDays);
    repeatMonthEvery.updateInitialValue(initialRepeatMonthEvery);
    repeatMonthOption.updateInitialValue(initialRepeatMonthOption);
    repeatYearEvery.updateInitialValue(initialRepeatYearEvery);

    emitLoaded();
  }

  @override
  void onSubmitting() async {
    var repeatMonthOptionsLabelsHumanReadable =
        repeatMonthOptionLabels(datetime.value, humanReadable: true);
    var repeatMonthOptionsLabelsMachineReadable =
        repeatMonthOptionLabels(datetime.value, humanReadable: false);

    var recurringTaskWatering = RecurringTaskWatering(
      datetime: datetime.value,
      repeat: repeat.value,
      repeatDayEvery: repeatDayEvery.valueToInt,
      repeatWeekEvery: repeatWeekEvery.valueToInt,
      repeatWeekDay: (repeatWeekDay.value as List<String>)
          .map((dayStr) => reversedRepeatDaysLabels[dayStr])
          .toList(),
      repeatMonthEvery: repeatMonthEvery.valueToInt,
      repeatMonthOption:
          repeatMonthOption.value == repeatMonthOptionsLabelsHumanReadable[1]
              ? repeatMonthOptionsLabelsMachineReadable[1]
              : sameDayEachMonth,
      repeatYearEvery: repeatYearEvery.valueToInt,
    );

    if (initialDatetime != null) {
      recurringTaskWateringBloc
          .add(UpdateRecurringTaskWatering(recurringTaskWatering));
    } else {
      recurringTaskWateringBloc
          .add(AddRecurringTaskWatering(recurringTaskWatering));
    }

    emitSuccess(
      canSubmitAgain: false,
      successResponse: 'Watering reminder successfully saved',
    );
  }
}
