part of 'bloc.dart';

/// State of the interface for the screens related to [RecurringTaskWatering].
@freezed
abstract class RecurringTaskWateringState with _$RecurringTaskWateringState {
  /// the state while our application is fetching recurringTaskWatering from the
  /// repository.
  const factory RecurringTaskWateringState.recurringTaskWateringLoading() =
      RecurringTaskWateringLoading;

  /// the state of our application after the recurringTaskWatering have
  /// successfully been loaded.
  const factory RecurringTaskWateringState.recurringTaskWateringLoaded(
          @nullable RecurringTaskWatering recurringTaskWatering) =
      RecurringTaskWateringLoaded;

  /// the state of our application if the recurringTaskWatering was not
  /// successfully loaded.
  const factory RecurringTaskWateringState.recurringTaskWateringNotLoaded() =
      RecurringTaskWateringNotLoaded;

  /// the state of our application if a [RecurringTaskWatering] was
  /// successfully saved.
  const factory RecurringTaskWateringState.recurringTaskWateringSuccess(
          RecurringTaskWatering recurringTaskWatering) =
      RecurringTaskWateringSavedSuccess;

  /// the state of our application if a [RecurringTaskWatering] was not
  /// successfully saved.
  const factory RecurringTaskWateringState.recurringTaskWateringSavedFailed(
          RecurringTaskWatering recurringTaskWatering) =
      RecurringTaskWateringSavedFailed;
}
