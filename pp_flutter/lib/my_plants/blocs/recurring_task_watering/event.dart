part of 'bloc.dart';

/// [Event] for [RecurringTaskWatering] model (load, add, update, etc.).
@freezed
abstract class RecurringTaskWateringEvent with _$RecurringTaskWateringEvent {
  /// Loading the [RecurringTaskWatering] from the repository into the
  /// interface.
  const factory RecurringTaskWateringEvent.loadRecurringTaskWatering() =
      LoadRecurringTaskWatering;

  /// Add a [RecurringTaskWatering] to the interface and repository.
  const factory RecurringTaskWateringEvent.addRecurringTaskWatering(
      RecurringTaskWatering recurringTaskWatering) = AddRecurringTaskWatering;

  /// Update a [RecurringTaskWatering] for a given plant on the interface and
  /// repository.
  const factory RecurringTaskWateringEvent.updateRecurringTaskWatering(
          RecurringTaskWatering recurringTaskWatering) =
      UpdateRecurringTaskWatering;

  /// Delete [RecurringTaskWatering] for a given plant to the interface and
  /// repository.
  const factory RecurringTaskWateringEvent.deleteRecurringTaskWatering() =
      DeleteRecurringTaskWatering;

  /// [RecurringTaskWatering] for a given plant was updated and need to be
  /// loaded.
  const factory RecurringTaskWateringEvent.recurringTaskWateringUpdated(
          @nullable RecurringTaskWatering recurringTaskWatering) =
      RecurringTaskWateringUpdated;
}
