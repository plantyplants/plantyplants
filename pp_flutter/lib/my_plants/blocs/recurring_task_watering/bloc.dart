import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

import '../../repository/interface.dart';
import '../../repository/model.dart';

part 'bloc.freezed.dart';
part 'event.dart';
part 'state.dart';

/// [RecurringTaskWateringBloc] manages [RecurringTaskWatering] models to add
/// watering schedule a [MyPlant].
class RecurringTaskWateringBloc
    extends Bloc<RecurringTaskWateringEvent, RecurringTaskWateringState> {
  final MyPlantsRepository _myPlantsRepository;
  StreamSubscription<RecurringTaskWatering> _recurringTaskWateringSubscription;

  /// User ID, generated by Firebase, of the author of the plant.
  String authorId;

  /// ID of the associated [MyPlant].
  String plantId;

  /// [myPlantsRepository] is the repository to use for MyPlant.
  /// [plantId] is the ID of the associated [MyPlant].
  RecurringTaskWateringBloc({
    @required MyPlantsRepository myPlantsRepository,
    @required String authorId,
    @required String plantId,
  })  : assert(myPlantsRepository != null),
        assert(authorId != null),
        assert(plantId != null),
        _myPlantsRepository = myPlantsRepository,
        authorId = authorId, // ignore: prefer_initializing_formals
        plantId = plantId; // ignore: prefer_initializing_formals

  @override
  RecurringTaskWateringState get initialState => RecurringTaskWateringLoading();

  @override
  Stream<RecurringTaskWateringState> mapEventToState(
      RecurringTaskWateringEvent event) async* {
    yield* event.map(
      loadRecurringTaskWatering: (_event) async* {
        yield* _mapLoadRecurringTaskWateringToState();
      },
      addRecurringTaskWatering: (event) async* {
        yield* _mapAddRecurringTaskWateringToState(event);
      },
      updateRecurringTaskWatering: (event) async* {
        yield* _mapUpdateRecurringTaskWateringToState(event);
      },
      deleteRecurringTaskWatering: (event) async* {
        yield* _mapDeleteRecurringTaskWateringToState(event);
      },
      recurringTaskWateringUpdated: (event) async* {
        yield* _mapRecurringTaskWateringUpdateToState(event);
      },
    );
  }

  Stream<RecurringTaskWateringState>
      _mapLoadRecurringTaskWateringToState() async* {
    _recurringTaskWateringSubscription?.cancel();
    _recurringTaskWateringSubscription =
        _myPlantsRepository.recurringTaskWateringStream(plantId).listen(
      (recurringTaskWatering) {
        add(RecurringTaskWateringUpdated(recurringTaskWatering));
      },
    );
  }

  Stream<RecurringTaskWateringState> _mapAddRecurringTaskWateringToState(
      AddRecurringTaskWatering event) async* {
    // TODO: Make error handling more precise and log
    try {
      await _myPlantsRepository.addNewRecurringTaskWatering(
          plantId, event.recurringTaskWatering);
      yield RecurringTaskWateringSavedSuccess(event.recurringTaskWatering);
    }
    // ignore: avoid_catches_without_on_clauses
    catch (e) {
      yield RecurringTaskWateringSavedFailed(event.recurringTaskWatering);
    }
  }

  Stream<RecurringTaskWateringState> _mapUpdateRecurringTaskWateringToState(
      UpdateRecurringTaskWatering event) async* {
    // TODO: Make error handling more precise and log
    try {
      await _myPlantsRepository.updateRecurringTaskWatering(
          plantId, event.recurringTaskWatering);
      yield RecurringTaskWateringSavedSuccess(event.recurringTaskWatering);
    }
    // ignore: avoid_catches_without_on_clauses
    catch (e) {
      yield RecurringTaskWateringSavedFailed(event.recurringTaskWatering);
    }
  }

  Stream<RecurringTaskWateringState> _mapDeleteRecurringTaskWateringToState(
      DeleteRecurringTaskWatering event) async* {
    await _myPlantsRepository.deleteRecurringTaskWatering(plantId);
  }

  Stream<RecurringTaskWateringState> _mapRecurringTaskWateringUpdateToState(
      RecurringTaskWateringUpdated event) async* {
    yield RecurringTaskWateringLoaded(event.recurringTaskWatering);
  }

  @override
  Future<void> close() async {
    _recurringTaskWateringSubscription?.cancel();
    await super.close();
  }
}
