// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$RecurringTaskWateringEventTearOff {
  const _$RecurringTaskWateringEventTearOff();

  LoadRecurringTaskWatering loadRecurringTaskWatering() {
    return const LoadRecurringTaskWatering();
  }

  AddRecurringTaskWatering addRecurringTaskWatering(
      RecurringTaskWatering recurringTaskWatering) {
    return AddRecurringTaskWatering(
      recurringTaskWatering,
    );
  }

  UpdateRecurringTaskWatering updateRecurringTaskWatering(
      RecurringTaskWatering recurringTaskWatering) {
    return UpdateRecurringTaskWatering(
      recurringTaskWatering,
    );
  }

  DeleteRecurringTaskWatering deleteRecurringTaskWatering() {
    return const DeleteRecurringTaskWatering();
  }

  RecurringTaskWateringUpdated recurringTaskWateringUpdated(
      @nullable RecurringTaskWatering recurringTaskWatering) {
    return RecurringTaskWateringUpdated(
      recurringTaskWatering,
    );
  }
}

// ignore: unused_element
const $RecurringTaskWateringEvent = _$RecurringTaskWateringEventTearOff();

mixin _$RecurringTaskWateringEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadRecurringTaskWatering(),
    @required
        Result addRecurringTaskWatering(
            RecurringTaskWatering recurringTaskWatering),
    @required
        Result updateRecurringTaskWatering(
            RecurringTaskWatering recurringTaskWatering),
    @required Result deleteRecurringTaskWatering(),
    @required
        Result recurringTaskWateringUpdated(
            @nullable RecurringTaskWatering recurringTaskWatering),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadRecurringTaskWatering(),
    Result addRecurringTaskWatering(
        RecurringTaskWatering recurringTaskWatering),
    Result updateRecurringTaskWatering(
        RecurringTaskWatering recurringTaskWatering),
    Result deleteRecurringTaskWatering(),
    Result recurringTaskWateringUpdated(
        @nullable RecurringTaskWatering recurringTaskWatering),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadRecurringTaskWatering(LoadRecurringTaskWatering value),
    @required Result addRecurringTaskWatering(AddRecurringTaskWatering value),
    @required
        Result updateRecurringTaskWatering(UpdateRecurringTaskWatering value),
    @required
        Result deleteRecurringTaskWatering(DeleteRecurringTaskWatering value),
    @required
        Result recurringTaskWateringUpdated(RecurringTaskWateringUpdated value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadRecurringTaskWatering(LoadRecurringTaskWatering value),
    Result addRecurringTaskWatering(AddRecurringTaskWatering value),
    Result updateRecurringTaskWatering(UpdateRecurringTaskWatering value),
    Result deleteRecurringTaskWatering(DeleteRecurringTaskWatering value),
    Result recurringTaskWateringUpdated(RecurringTaskWateringUpdated value),
    @required Result orElse(),
  });
}

abstract class $RecurringTaskWateringEventCopyWith<$Res> {
  factory $RecurringTaskWateringEventCopyWith(RecurringTaskWateringEvent value,
          $Res Function(RecurringTaskWateringEvent) then) =
      _$RecurringTaskWateringEventCopyWithImpl<$Res>;
}

class _$RecurringTaskWateringEventCopyWithImpl<$Res>
    implements $RecurringTaskWateringEventCopyWith<$Res> {
  _$RecurringTaskWateringEventCopyWithImpl(this._value, this._then);

  final RecurringTaskWateringEvent _value;
  // ignore: unused_field
  final $Res Function(RecurringTaskWateringEvent) _then;
}

abstract class $LoadRecurringTaskWateringCopyWith<$Res> {
  factory $LoadRecurringTaskWateringCopyWith(LoadRecurringTaskWatering value,
          $Res Function(LoadRecurringTaskWatering) then) =
      _$LoadRecurringTaskWateringCopyWithImpl<$Res>;
}

class _$LoadRecurringTaskWateringCopyWithImpl<$Res>
    extends _$RecurringTaskWateringEventCopyWithImpl<$Res>
    implements $LoadRecurringTaskWateringCopyWith<$Res> {
  _$LoadRecurringTaskWateringCopyWithImpl(LoadRecurringTaskWatering _value,
      $Res Function(LoadRecurringTaskWatering) _then)
      : super(_value, (v) => _then(v as LoadRecurringTaskWatering));

  @override
  LoadRecurringTaskWatering get _value =>
      super._value as LoadRecurringTaskWatering;
}

class _$LoadRecurringTaskWatering implements LoadRecurringTaskWatering {
  const _$LoadRecurringTaskWatering();

  @override
  String toString() {
    return 'RecurringTaskWateringEvent.loadRecurringTaskWatering()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadRecurringTaskWatering);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadRecurringTaskWatering(),
    @required
        Result addRecurringTaskWatering(
            RecurringTaskWatering recurringTaskWatering),
    @required
        Result updateRecurringTaskWatering(
            RecurringTaskWatering recurringTaskWatering),
    @required Result deleteRecurringTaskWatering(),
    @required
        Result recurringTaskWateringUpdated(
            @nullable RecurringTaskWatering recurringTaskWatering),
  }) {
    assert(loadRecurringTaskWatering != null);
    assert(addRecurringTaskWatering != null);
    assert(updateRecurringTaskWatering != null);
    assert(deleteRecurringTaskWatering != null);
    assert(recurringTaskWateringUpdated != null);
    return loadRecurringTaskWatering();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadRecurringTaskWatering(),
    Result addRecurringTaskWatering(
        RecurringTaskWatering recurringTaskWatering),
    Result updateRecurringTaskWatering(
        RecurringTaskWatering recurringTaskWatering),
    Result deleteRecurringTaskWatering(),
    Result recurringTaskWateringUpdated(
        @nullable RecurringTaskWatering recurringTaskWatering),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loadRecurringTaskWatering != null) {
      return loadRecurringTaskWatering();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadRecurringTaskWatering(LoadRecurringTaskWatering value),
    @required Result addRecurringTaskWatering(AddRecurringTaskWatering value),
    @required
        Result updateRecurringTaskWatering(UpdateRecurringTaskWatering value),
    @required
        Result deleteRecurringTaskWatering(DeleteRecurringTaskWatering value),
    @required
        Result recurringTaskWateringUpdated(RecurringTaskWateringUpdated value),
  }) {
    assert(loadRecurringTaskWatering != null);
    assert(addRecurringTaskWatering != null);
    assert(updateRecurringTaskWatering != null);
    assert(deleteRecurringTaskWatering != null);
    assert(recurringTaskWateringUpdated != null);
    return loadRecurringTaskWatering(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadRecurringTaskWatering(LoadRecurringTaskWatering value),
    Result addRecurringTaskWatering(AddRecurringTaskWatering value),
    Result updateRecurringTaskWatering(UpdateRecurringTaskWatering value),
    Result deleteRecurringTaskWatering(DeleteRecurringTaskWatering value),
    Result recurringTaskWateringUpdated(RecurringTaskWateringUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loadRecurringTaskWatering != null) {
      return loadRecurringTaskWatering(this);
    }
    return orElse();
  }
}

abstract class LoadRecurringTaskWatering implements RecurringTaskWateringEvent {
  const factory LoadRecurringTaskWatering() = _$LoadRecurringTaskWatering;
}

abstract class $AddRecurringTaskWateringCopyWith<$Res> {
  factory $AddRecurringTaskWateringCopyWith(AddRecurringTaskWatering value,
          $Res Function(AddRecurringTaskWatering) then) =
      _$AddRecurringTaskWateringCopyWithImpl<$Res>;
  $Res call({RecurringTaskWatering recurringTaskWatering});

  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering;
}

class _$AddRecurringTaskWateringCopyWithImpl<$Res>
    extends _$RecurringTaskWateringEventCopyWithImpl<$Res>
    implements $AddRecurringTaskWateringCopyWith<$Res> {
  _$AddRecurringTaskWateringCopyWithImpl(AddRecurringTaskWatering _value,
      $Res Function(AddRecurringTaskWatering) _then)
      : super(_value, (v) => _then(v as AddRecurringTaskWatering));

  @override
  AddRecurringTaskWatering get _value =>
      super._value as AddRecurringTaskWatering;

  @override
  $Res call({
    Object recurringTaskWatering = freezed,
  }) {
    return _then(AddRecurringTaskWatering(
      recurringTaskWatering == freezed
          ? _value.recurringTaskWatering
          : recurringTaskWatering as RecurringTaskWatering,
    ));
  }

  @override
  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering {
    if (_value.recurringTaskWatering == null) {
      return null;
    }
    return $RecurringTaskWateringCopyWith<$Res>(_value.recurringTaskWatering,
        (value) {
      return _then(_value.copyWith(recurringTaskWatering: value));
    });
  }
}

class _$AddRecurringTaskWatering implements AddRecurringTaskWatering {
  const _$AddRecurringTaskWatering(this.recurringTaskWatering)
      : assert(recurringTaskWatering != null);

  @override
  final RecurringTaskWatering recurringTaskWatering;

  @override
  String toString() {
    return 'RecurringTaskWateringEvent.addRecurringTaskWatering(recurringTaskWatering: $recurringTaskWatering)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddRecurringTaskWatering &&
            (identical(other.recurringTaskWatering, recurringTaskWatering) ||
                const DeepCollectionEquality().equals(
                    other.recurringTaskWatering, recurringTaskWatering)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(recurringTaskWatering);

  @override
  $AddRecurringTaskWateringCopyWith<AddRecurringTaskWatering> get copyWith =>
      _$AddRecurringTaskWateringCopyWithImpl<AddRecurringTaskWatering>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadRecurringTaskWatering(),
    @required
        Result addRecurringTaskWatering(
            RecurringTaskWatering recurringTaskWatering),
    @required
        Result updateRecurringTaskWatering(
            RecurringTaskWatering recurringTaskWatering),
    @required Result deleteRecurringTaskWatering(),
    @required
        Result recurringTaskWateringUpdated(
            @nullable RecurringTaskWatering recurringTaskWatering),
  }) {
    assert(loadRecurringTaskWatering != null);
    assert(addRecurringTaskWatering != null);
    assert(updateRecurringTaskWatering != null);
    assert(deleteRecurringTaskWatering != null);
    assert(recurringTaskWateringUpdated != null);
    return addRecurringTaskWatering(recurringTaskWatering);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadRecurringTaskWatering(),
    Result addRecurringTaskWatering(
        RecurringTaskWatering recurringTaskWatering),
    Result updateRecurringTaskWatering(
        RecurringTaskWatering recurringTaskWatering),
    Result deleteRecurringTaskWatering(),
    Result recurringTaskWateringUpdated(
        @nullable RecurringTaskWatering recurringTaskWatering),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (addRecurringTaskWatering != null) {
      return addRecurringTaskWatering(recurringTaskWatering);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadRecurringTaskWatering(LoadRecurringTaskWatering value),
    @required Result addRecurringTaskWatering(AddRecurringTaskWatering value),
    @required
        Result updateRecurringTaskWatering(UpdateRecurringTaskWatering value),
    @required
        Result deleteRecurringTaskWatering(DeleteRecurringTaskWatering value),
    @required
        Result recurringTaskWateringUpdated(RecurringTaskWateringUpdated value),
  }) {
    assert(loadRecurringTaskWatering != null);
    assert(addRecurringTaskWatering != null);
    assert(updateRecurringTaskWatering != null);
    assert(deleteRecurringTaskWatering != null);
    assert(recurringTaskWateringUpdated != null);
    return addRecurringTaskWatering(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadRecurringTaskWatering(LoadRecurringTaskWatering value),
    Result addRecurringTaskWatering(AddRecurringTaskWatering value),
    Result updateRecurringTaskWatering(UpdateRecurringTaskWatering value),
    Result deleteRecurringTaskWatering(DeleteRecurringTaskWatering value),
    Result recurringTaskWateringUpdated(RecurringTaskWateringUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (addRecurringTaskWatering != null) {
      return addRecurringTaskWatering(this);
    }
    return orElse();
  }
}

abstract class AddRecurringTaskWatering implements RecurringTaskWateringEvent {
  const factory AddRecurringTaskWatering(
      RecurringTaskWatering recurringTaskWatering) = _$AddRecurringTaskWatering;

  RecurringTaskWatering get recurringTaskWatering;
  $AddRecurringTaskWateringCopyWith<AddRecurringTaskWatering> get copyWith;
}

abstract class $UpdateRecurringTaskWateringCopyWith<$Res> {
  factory $UpdateRecurringTaskWateringCopyWith(
          UpdateRecurringTaskWatering value,
          $Res Function(UpdateRecurringTaskWatering) then) =
      _$UpdateRecurringTaskWateringCopyWithImpl<$Res>;
  $Res call({RecurringTaskWatering recurringTaskWatering});

  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering;
}

class _$UpdateRecurringTaskWateringCopyWithImpl<$Res>
    extends _$RecurringTaskWateringEventCopyWithImpl<$Res>
    implements $UpdateRecurringTaskWateringCopyWith<$Res> {
  _$UpdateRecurringTaskWateringCopyWithImpl(UpdateRecurringTaskWatering _value,
      $Res Function(UpdateRecurringTaskWatering) _then)
      : super(_value, (v) => _then(v as UpdateRecurringTaskWatering));

  @override
  UpdateRecurringTaskWatering get _value =>
      super._value as UpdateRecurringTaskWatering;

  @override
  $Res call({
    Object recurringTaskWatering = freezed,
  }) {
    return _then(UpdateRecurringTaskWatering(
      recurringTaskWatering == freezed
          ? _value.recurringTaskWatering
          : recurringTaskWatering as RecurringTaskWatering,
    ));
  }

  @override
  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering {
    if (_value.recurringTaskWatering == null) {
      return null;
    }
    return $RecurringTaskWateringCopyWith<$Res>(_value.recurringTaskWatering,
        (value) {
      return _then(_value.copyWith(recurringTaskWatering: value));
    });
  }
}

class _$UpdateRecurringTaskWatering implements UpdateRecurringTaskWatering {
  const _$UpdateRecurringTaskWatering(this.recurringTaskWatering)
      : assert(recurringTaskWatering != null);

  @override
  final RecurringTaskWatering recurringTaskWatering;

  @override
  String toString() {
    return 'RecurringTaskWateringEvent.updateRecurringTaskWatering(recurringTaskWatering: $recurringTaskWatering)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdateRecurringTaskWatering &&
            (identical(other.recurringTaskWatering, recurringTaskWatering) ||
                const DeepCollectionEquality().equals(
                    other.recurringTaskWatering, recurringTaskWatering)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(recurringTaskWatering);

  @override
  $UpdateRecurringTaskWateringCopyWith<UpdateRecurringTaskWatering>
      get copyWith => _$UpdateRecurringTaskWateringCopyWithImpl<
          UpdateRecurringTaskWatering>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadRecurringTaskWatering(),
    @required
        Result addRecurringTaskWatering(
            RecurringTaskWatering recurringTaskWatering),
    @required
        Result updateRecurringTaskWatering(
            RecurringTaskWatering recurringTaskWatering),
    @required Result deleteRecurringTaskWatering(),
    @required
        Result recurringTaskWateringUpdated(
            @nullable RecurringTaskWatering recurringTaskWatering),
  }) {
    assert(loadRecurringTaskWatering != null);
    assert(addRecurringTaskWatering != null);
    assert(updateRecurringTaskWatering != null);
    assert(deleteRecurringTaskWatering != null);
    assert(recurringTaskWateringUpdated != null);
    return updateRecurringTaskWatering(recurringTaskWatering);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadRecurringTaskWatering(),
    Result addRecurringTaskWatering(
        RecurringTaskWatering recurringTaskWatering),
    Result updateRecurringTaskWatering(
        RecurringTaskWatering recurringTaskWatering),
    Result deleteRecurringTaskWatering(),
    Result recurringTaskWateringUpdated(
        @nullable RecurringTaskWatering recurringTaskWatering),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (updateRecurringTaskWatering != null) {
      return updateRecurringTaskWatering(recurringTaskWatering);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadRecurringTaskWatering(LoadRecurringTaskWatering value),
    @required Result addRecurringTaskWatering(AddRecurringTaskWatering value),
    @required
        Result updateRecurringTaskWatering(UpdateRecurringTaskWatering value),
    @required
        Result deleteRecurringTaskWatering(DeleteRecurringTaskWatering value),
    @required
        Result recurringTaskWateringUpdated(RecurringTaskWateringUpdated value),
  }) {
    assert(loadRecurringTaskWatering != null);
    assert(addRecurringTaskWatering != null);
    assert(updateRecurringTaskWatering != null);
    assert(deleteRecurringTaskWatering != null);
    assert(recurringTaskWateringUpdated != null);
    return updateRecurringTaskWatering(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadRecurringTaskWatering(LoadRecurringTaskWatering value),
    Result addRecurringTaskWatering(AddRecurringTaskWatering value),
    Result updateRecurringTaskWatering(UpdateRecurringTaskWatering value),
    Result deleteRecurringTaskWatering(DeleteRecurringTaskWatering value),
    Result recurringTaskWateringUpdated(RecurringTaskWateringUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (updateRecurringTaskWatering != null) {
      return updateRecurringTaskWatering(this);
    }
    return orElse();
  }
}

abstract class UpdateRecurringTaskWatering
    implements RecurringTaskWateringEvent {
  const factory UpdateRecurringTaskWatering(
          RecurringTaskWatering recurringTaskWatering) =
      _$UpdateRecurringTaskWatering;

  RecurringTaskWatering get recurringTaskWatering;
  $UpdateRecurringTaskWateringCopyWith<UpdateRecurringTaskWatering>
      get copyWith;
}

abstract class $DeleteRecurringTaskWateringCopyWith<$Res> {
  factory $DeleteRecurringTaskWateringCopyWith(
          DeleteRecurringTaskWatering value,
          $Res Function(DeleteRecurringTaskWatering) then) =
      _$DeleteRecurringTaskWateringCopyWithImpl<$Res>;
}

class _$DeleteRecurringTaskWateringCopyWithImpl<$Res>
    extends _$RecurringTaskWateringEventCopyWithImpl<$Res>
    implements $DeleteRecurringTaskWateringCopyWith<$Res> {
  _$DeleteRecurringTaskWateringCopyWithImpl(DeleteRecurringTaskWatering _value,
      $Res Function(DeleteRecurringTaskWatering) _then)
      : super(_value, (v) => _then(v as DeleteRecurringTaskWatering));

  @override
  DeleteRecurringTaskWatering get _value =>
      super._value as DeleteRecurringTaskWatering;
}

class _$DeleteRecurringTaskWatering implements DeleteRecurringTaskWatering {
  const _$DeleteRecurringTaskWatering();

  @override
  String toString() {
    return 'RecurringTaskWateringEvent.deleteRecurringTaskWatering()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is DeleteRecurringTaskWatering);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadRecurringTaskWatering(),
    @required
        Result addRecurringTaskWatering(
            RecurringTaskWatering recurringTaskWatering),
    @required
        Result updateRecurringTaskWatering(
            RecurringTaskWatering recurringTaskWatering),
    @required Result deleteRecurringTaskWatering(),
    @required
        Result recurringTaskWateringUpdated(
            @nullable RecurringTaskWatering recurringTaskWatering),
  }) {
    assert(loadRecurringTaskWatering != null);
    assert(addRecurringTaskWatering != null);
    assert(updateRecurringTaskWatering != null);
    assert(deleteRecurringTaskWatering != null);
    assert(recurringTaskWateringUpdated != null);
    return deleteRecurringTaskWatering();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadRecurringTaskWatering(),
    Result addRecurringTaskWatering(
        RecurringTaskWatering recurringTaskWatering),
    Result updateRecurringTaskWatering(
        RecurringTaskWatering recurringTaskWatering),
    Result deleteRecurringTaskWatering(),
    Result recurringTaskWateringUpdated(
        @nullable RecurringTaskWatering recurringTaskWatering),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (deleteRecurringTaskWatering != null) {
      return deleteRecurringTaskWatering();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadRecurringTaskWatering(LoadRecurringTaskWatering value),
    @required Result addRecurringTaskWatering(AddRecurringTaskWatering value),
    @required
        Result updateRecurringTaskWatering(UpdateRecurringTaskWatering value),
    @required
        Result deleteRecurringTaskWatering(DeleteRecurringTaskWatering value),
    @required
        Result recurringTaskWateringUpdated(RecurringTaskWateringUpdated value),
  }) {
    assert(loadRecurringTaskWatering != null);
    assert(addRecurringTaskWatering != null);
    assert(updateRecurringTaskWatering != null);
    assert(deleteRecurringTaskWatering != null);
    assert(recurringTaskWateringUpdated != null);
    return deleteRecurringTaskWatering(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadRecurringTaskWatering(LoadRecurringTaskWatering value),
    Result addRecurringTaskWatering(AddRecurringTaskWatering value),
    Result updateRecurringTaskWatering(UpdateRecurringTaskWatering value),
    Result deleteRecurringTaskWatering(DeleteRecurringTaskWatering value),
    Result recurringTaskWateringUpdated(RecurringTaskWateringUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (deleteRecurringTaskWatering != null) {
      return deleteRecurringTaskWatering(this);
    }
    return orElse();
  }
}

abstract class DeleteRecurringTaskWatering
    implements RecurringTaskWateringEvent {
  const factory DeleteRecurringTaskWatering() = _$DeleteRecurringTaskWatering;
}

abstract class $RecurringTaskWateringUpdatedCopyWith<$Res> {
  factory $RecurringTaskWateringUpdatedCopyWith(
          RecurringTaskWateringUpdated value,
          $Res Function(RecurringTaskWateringUpdated) then) =
      _$RecurringTaskWateringUpdatedCopyWithImpl<$Res>;
  $Res call({@nullable RecurringTaskWatering recurringTaskWatering});

  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering;
}

class _$RecurringTaskWateringUpdatedCopyWithImpl<$Res>
    extends _$RecurringTaskWateringEventCopyWithImpl<$Res>
    implements $RecurringTaskWateringUpdatedCopyWith<$Res> {
  _$RecurringTaskWateringUpdatedCopyWithImpl(
      RecurringTaskWateringUpdated _value,
      $Res Function(RecurringTaskWateringUpdated) _then)
      : super(_value, (v) => _then(v as RecurringTaskWateringUpdated));

  @override
  RecurringTaskWateringUpdated get _value =>
      super._value as RecurringTaskWateringUpdated;

  @override
  $Res call({
    Object recurringTaskWatering = freezed,
  }) {
    return _then(RecurringTaskWateringUpdated(
      recurringTaskWatering == freezed
          ? _value.recurringTaskWatering
          : recurringTaskWatering as RecurringTaskWatering,
    ));
  }

  @override
  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering {
    if (_value.recurringTaskWatering == null) {
      return null;
    }
    return $RecurringTaskWateringCopyWith<$Res>(_value.recurringTaskWatering,
        (value) {
      return _then(_value.copyWith(recurringTaskWatering: value));
    });
  }
}

class _$RecurringTaskWateringUpdated implements RecurringTaskWateringUpdated {
  const _$RecurringTaskWateringUpdated(@nullable this.recurringTaskWatering);

  @override
  @nullable
  final RecurringTaskWatering recurringTaskWatering;

  @override
  String toString() {
    return 'RecurringTaskWateringEvent.recurringTaskWateringUpdated(recurringTaskWatering: $recurringTaskWatering)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is RecurringTaskWateringUpdated &&
            (identical(other.recurringTaskWatering, recurringTaskWatering) ||
                const DeepCollectionEquality().equals(
                    other.recurringTaskWatering, recurringTaskWatering)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(recurringTaskWatering);

  @override
  $RecurringTaskWateringUpdatedCopyWith<RecurringTaskWateringUpdated>
      get copyWith => _$RecurringTaskWateringUpdatedCopyWithImpl<
          RecurringTaskWateringUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadRecurringTaskWatering(),
    @required
        Result addRecurringTaskWatering(
            RecurringTaskWatering recurringTaskWatering),
    @required
        Result updateRecurringTaskWatering(
            RecurringTaskWatering recurringTaskWatering),
    @required Result deleteRecurringTaskWatering(),
    @required
        Result recurringTaskWateringUpdated(
            @nullable RecurringTaskWatering recurringTaskWatering),
  }) {
    assert(loadRecurringTaskWatering != null);
    assert(addRecurringTaskWatering != null);
    assert(updateRecurringTaskWatering != null);
    assert(deleteRecurringTaskWatering != null);
    assert(recurringTaskWateringUpdated != null);
    return recurringTaskWateringUpdated(recurringTaskWatering);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadRecurringTaskWatering(),
    Result addRecurringTaskWatering(
        RecurringTaskWatering recurringTaskWatering),
    Result updateRecurringTaskWatering(
        RecurringTaskWatering recurringTaskWatering),
    Result deleteRecurringTaskWatering(),
    Result recurringTaskWateringUpdated(
        @nullable RecurringTaskWatering recurringTaskWatering),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (recurringTaskWateringUpdated != null) {
      return recurringTaskWateringUpdated(recurringTaskWatering);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadRecurringTaskWatering(LoadRecurringTaskWatering value),
    @required Result addRecurringTaskWatering(AddRecurringTaskWatering value),
    @required
        Result updateRecurringTaskWatering(UpdateRecurringTaskWatering value),
    @required
        Result deleteRecurringTaskWatering(DeleteRecurringTaskWatering value),
    @required
        Result recurringTaskWateringUpdated(RecurringTaskWateringUpdated value),
  }) {
    assert(loadRecurringTaskWatering != null);
    assert(addRecurringTaskWatering != null);
    assert(updateRecurringTaskWatering != null);
    assert(deleteRecurringTaskWatering != null);
    assert(recurringTaskWateringUpdated != null);
    return recurringTaskWateringUpdated(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadRecurringTaskWatering(LoadRecurringTaskWatering value),
    Result addRecurringTaskWatering(AddRecurringTaskWatering value),
    Result updateRecurringTaskWatering(UpdateRecurringTaskWatering value),
    Result deleteRecurringTaskWatering(DeleteRecurringTaskWatering value),
    Result recurringTaskWateringUpdated(RecurringTaskWateringUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (recurringTaskWateringUpdated != null) {
      return recurringTaskWateringUpdated(this);
    }
    return orElse();
  }
}

abstract class RecurringTaskWateringUpdated
    implements RecurringTaskWateringEvent {
  const factory RecurringTaskWateringUpdated(
          @nullable RecurringTaskWatering recurringTaskWatering) =
      _$RecurringTaskWateringUpdated;

  @nullable
  RecurringTaskWatering get recurringTaskWatering;
  $RecurringTaskWateringUpdatedCopyWith<RecurringTaskWateringUpdated>
      get copyWith;
}

class _$RecurringTaskWateringStateTearOff {
  const _$RecurringTaskWateringStateTearOff();

  RecurringTaskWateringLoading recurringTaskWateringLoading() {
    return const RecurringTaskWateringLoading();
  }

  RecurringTaskWateringLoaded recurringTaskWateringLoaded(
      @nullable RecurringTaskWatering recurringTaskWatering) {
    return RecurringTaskWateringLoaded(
      recurringTaskWatering,
    );
  }

  RecurringTaskWateringNotLoaded recurringTaskWateringNotLoaded() {
    return const RecurringTaskWateringNotLoaded();
  }

  RecurringTaskWateringSavedSuccess recurringTaskWateringSuccess(
      RecurringTaskWatering recurringTaskWatering) {
    return RecurringTaskWateringSavedSuccess(
      recurringTaskWatering,
    );
  }

  RecurringTaskWateringSavedFailed recurringTaskWateringSavedFailed(
      RecurringTaskWatering recurringTaskWatering) {
    return RecurringTaskWateringSavedFailed(
      recurringTaskWatering,
    );
  }
}

// ignore: unused_element
const $RecurringTaskWateringState = _$RecurringTaskWateringStateTearOff();

mixin _$RecurringTaskWateringState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result recurringTaskWateringLoading(),
    @required
        Result recurringTaskWateringLoaded(
            @nullable RecurringTaskWatering recurringTaskWatering),
    @required Result recurringTaskWateringNotLoaded(),
    @required
        Result recurringTaskWateringSuccess(
            RecurringTaskWatering recurringTaskWatering),
    @required
        Result recurringTaskWateringSavedFailed(
            RecurringTaskWatering recurringTaskWatering),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result recurringTaskWateringLoading(),
    Result recurringTaskWateringLoaded(
        @nullable RecurringTaskWatering recurringTaskWatering),
    Result recurringTaskWateringNotLoaded(),
    Result recurringTaskWateringSuccess(
        RecurringTaskWatering recurringTaskWatering),
    Result recurringTaskWateringSavedFailed(
        RecurringTaskWatering recurringTaskWatering),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required
        Result recurringTaskWateringLoading(RecurringTaskWateringLoading value),
    @required
        Result recurringTaskWateringLoaded(RecurringTaskWateringLoaded value),
    @required
        Result recurringTaskWateringNotLoaded(
            RecurringTaskWateringNotLoaded value),
    @required
        Result recurringTaskWateringSuccess(
            RecurringTaskWateringSavedSuccess value),
    @required
        Result recurringTaskWateringSavedFailed(
            RecurringTaskWateringSavedFailed value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result recurringTaskWateringLoading(RecurringTaskWateringLoading value),
    Result recurringTaskWateringLoaded(RecurringTaskWateringLoaded value),
    Result recurringTaskWateringNotLoaded(RecurringTaskWateringNotLoaded value),
    Result recurringTaskWateringSuccess(
        RecurringTaskWateringSavedSuccess value),
    Result recurringTaskWateringSavedFailed(
        RecurringTaskWateringSavedFailed value),
    @required Result orElse(),
  });
}

abstract class $RecurringTaskWateringStateCopyWith<$Res> {
  factory $RecurringTaskWateringStateCopyWith(RecurringTaskWateringState value,
          $Res Function(RecurringTaskWateringState) then) =
      _$RecurringTaskWateringStateCopyWithImpl<$Res>;
}

class _$RecurringTaskWateringStateCopyWithImpl<$Res>
    implements $RecurringTaskWateringStateCopyWith<$Res> {
  _$RecurringTaskWateringStateCopyWithImpl(this._value, this._then);

  final RecurringTaskWateringState _value;
  // ignore: unused_field
  final $Res Function(RecurringTaskWateringState) _then;
}

abstract class $RecurringTaskWateringLoadingCopyWith<$Res> {
  factory $RecurringTaskWateringLoadingCopyWith(
          RecurringTaskWateringLoading value,
          $Res Function(RecurringTaskWateringLoading) then) =
      _$RecurringTaskWateringLoadingCopyWithImpl<$Res>;
}

class _$RecurringTaskWateringLoadingCopyWithImpl<$Res>
    extends _$RecurringTaskWateringStateCopyWithImpl<$Res>
    implements $RecurringTaskWateringLoadingCopyWith<$Res> {
  _$RecurringTaskWateringLoadingCopyWithImpl(
      RecurringTaskWateringLoading _value,
      $Res Function(RecurringTaskWateringLoading) _then)
      : super(_value, (v) => _then(v as RecurringTaskWateringLoading));

  @override
  RecurringTaskWateringLoading get _value =>
      super._value as RecurringTaskWateringLoading;
}

class _$RecurringTaskWateringLoading implements RecurringTaskWateringLoading {
  const _$RecurringTaskWateringLoading();

  @override
  String toString() {
    return 'RecurringTaskWateringState.recurringTaskWateringLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is RecurringTaskWateringLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result recurringTaskWateringLoading(),
    @required
        Result recurringTaskWateringLoaded(
            @nullable RecurringTaskWatering recurringTaskWatering),
    @required Result recurringTaskWateringNotLoaded(),
    @required
        Result recurringTaskWateringSuccess(
            RecurringTaskWatering recurringTaskWatering),
    @required
        Result recurringTaskWateringSavedFailed(
            RecurringTaskWatering recurringTaskWatering),
  }) {
    assert(recurringTaskWateringLoading != null);
    assert(recurringTaskWateringLoaded != null);
    assert(recurringTaskWateringNotLoaded != null);
    assert(recurringTaskWateringSuccess != null);
    assert(recurringTaskWateringSavedFailed != null);
    return recurringTaskWateringLoading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result recurringTaskWateringLoading(),
    Result recurringTaskWateringLoaded(
        @nullable RecurringTaskWatering recurringTaskWatering),
    Result recurringTaskWateringNotLoaded(),
    Result recurringTaskWateringSuccess(
        RecurringTaskWatering recurringTaskWatering),
    Result recurringTaskWateringSavedFailed(
        RecurringTaskWatering recurringTaskWatering),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (recurringTaskWateringLoading != null) {
      return recurringTaskWateringLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required
        Result recurringTaskWateringLoading(RecurringTaskWateringLoading value),
    @required
        Result recurringTaskWateringLoaded(RecurringTaskWateringLoaded value),
    @required
        Result recurringTaskWateringNotLoaded(
            RecurringTaskWateringNotLoaded value),
    @required
        Result recurringTaskWateringSuccess(
            RecurringTaskWateringSavedSuccess value),
    @required
        Result recurringTaskWateringSavedFailed(
            RecurringTaskWateringSavedFailed value),
  }) {
    assert(recurringTaskWateringLoading != null);
    assert(recurringTaskWateringLoaded != null);
    assert(recurringTaskWateringNotLoaded != null);
    assert(recurringTaskWateringSuccess != null);
    assert(recurringTaskWateringSavedFailed != null);
    return recurringTaskWateringLoading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result recurringTaskWateringLoading(RecurringTaskWateringLoading value),
    Result recurringTaskWateringLoaded(RecurringTaskWateringLoaded value),
    Result recurringTaskWateringNotLoaded(RecurringTaskWateringNotLoaded value),
    Result recurringTaskWateringSuccess(
        RecurringTaskWateringSavedSuccess value),
    Result recurringTaskWateringSavedFailed(
        RecurringTaskWateringSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (recurringTaskWateringLoading != null) {
      return recurringTaskWateringLoading(this);
    }
    return orElse();
  }
}

abstract class RecurringTaskWateringLoading
    implements RecurringTaskWateringState {
  const factory RecurringTaskWateringLoading() = _$RecurringTaskWateringLoading;
}

abstract class $RecurringTaskWateringLoadedCopyWith<$Res> {
  factory $RecurringTaskWateringLoadedCopyWith(
          RecurringTaskWateringLoaded value,
          $Res Function(RecurringTaskWateringLoaded) then) =
      _$RecurringTaskWateringLoadedCopyWithImpl<$Res>;
  $Res call({@nullable RecurringTaskWatering recurringTaskWatering});

  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering;
}

class _$RecurringTaskWateringLoadedCopyWithImpl<$Res>
    extends _$RecurringTaskWateringStateCopyWithImpl<$Res>
    implements $RecurringTaskWateringLoadedCopyWith<$Res> {
  _$RecurringTaskWateringLoadedCopyWithImpl(RecurringTaskWateringLoaded _value,
      $Res Function(RecurringTaskWateringLoaded) _then)
      : super(_value, (v) => _then(v as RecurringTaskWateringLoaded));

  @override
  RecurringTaskWateringLoaded get _value =>
      super._value as RecurringTaskWateringLoaded;

  @override
  $Res call({
    Object recurringTaskWatering = freezed,
  }) {
    return _then(RecurringTaskWateringLoaded(
      recurringTaskWatering == freezed
          ? _value.recurringTaskWatering
          : recurringTaskWatering as RecurringTaskWatering,
    ));
  }

  @override
  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering {
    if (_value.recurringTaskWatering == null) {
      return null;
    }
    return $RecurringTaskWateringCopyWith<$Res>(_value.recurringTaskWatering,
        (value) {
      return _then(_value.copyWith(recurringTaskWatering: value));
    });
  }
}

class _$RecurringTaskWateringLoaded implements RecurringTaskWateringLoaded {
  const _$RecurringTaskWateringLoaded(@nullable this.recurringTaskWatering);

  @override
  @nullable
  final RecurringTaskWatering recurringTaskWatering;

  @override
  String toString() {
    return 'RecurringTaskWateringState.recurringTaskWateringLoaded(recurringTaskWatering: $recurringTaskWatering)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is RecurringTaskWateringLoaded &&
            (identical(other.recurringTaskWatering, recurringTaskWatering) ||
                const DeepCollectionEquality().equals(
                    other.recurringTaskWatering, recurringTaskWatering)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(recurringTaskWatering);

  @override
  $RecurringTaskWateringLoadedCopyWith<RecurringTaskWateringLoaded>
      get copyWith => _$RecurringTaskWateringLoadedCopyWithImpl<
          RecurringTaskWateringLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result recurringTaskWateringLoading(),
    @required
        Result recurringTaskWateringLoaded(
            @nullable RecurringTaskWatering recurringTaskWatering),
    @required Result recurringTaskWateringNotLoaded(),
    @required
        Result recurringTaskWateringSuccess(
            RecurringTaskWatering recurringTaskWatering),
    @required
        Result recurringTaskWateringSavedFailed(
            RecurringTaskWatering recurringTaskWatering),
  }) {
    assert(recurringTaskWateringLoading != null);
    assert(recurringTaskWateringLoaded != null);
    assert(recurringTaskWateringNotLoaded != null);
    assert(recurringTaskWateringSuccess != null);
    assert(recurringTaskWateringSavedFailed != null);
    return recurringTaskWateringLoaded(recurringTaskWatering);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result recurringTaskWateringLoading(),
    Result recurringTaskWateringLoaded(
        @nullable RecurringTaskWatering recurringTaskWatering),
    Result recurringTaskWateringNotLoaded(),
    Result recurringTaskWateringSuccess(
        RecurringTaskWatering recurringTaskWatering),
    Result recurringTaskWateringSavedFailed(
        RecurringTaskWatering recurringTaskWatering),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (recurringTaskWateringLoaded != null) {
      return recurringTaskWateringLoaded(recurringTaskWatering);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required
        Result recurringTaskWateringLoading(RecurringTaskWateringLoading value),
    @required
        Result recurringTaskWateringLoaded(RecurringTaskWateringLoaded value),
    @required
        Result recurringTaskWateringNotLoaded(
            RecurringTaskWateringNotLoaded value),
    @required
        Result recurringTaskWateringSuccess(
            RecurringTaskWateringSavedSuccess value),
    @required
        Result recurringTaskWateringSavedFailed(
            RecurringTaskWateringSavedFailed value),
  }) {
    assert(recurringTaskWateringLoading != null);
    assert(recurringTaskWateringLoaded != null);
    assert(recurringTaskWateringNotLoaded != null);
    assert(recurringTaskWateringSuccess != null);
    assert(recurringTaskWateringSavedFailed != null);
    return recurringTaskWateringLoaded(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result recurringTaskWateringLoading(RecurringTaskWateringLoading value),
    Result recurringTaskWateringLoaded(RecurringTaskWateringLoaded value),
    Result recurringTaskWateringNotLoaded(RecurringTaskWateringNotLoaded value),
    Result recurringTaskWateringSuccess(
        RecurringTaskWateringSavedSuccess value),
    Result recurringTaskWateringSavedFailed(
        RecurringTaskWateringSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (recurringTaskWateringLoaded != null) {
      return recurringTaskWateringLoaded(this);
    }
    return orElse();
  }
}

abstract class RecurringTaskWateringLoaded
    implements RecurringTaskWateringState {
  const factory RecurringTaskWateringLoaded(
          @nullable RecurringTaskWatering recurringTaskWatering) =
      _$RecurringTaskWateringLoaded;

  @nullable
  RecurringTaskWatering get recurringTaskWatering;
  $RecurringTaskWateringLoadedCopyWith<RecurringTaskWateringLoaded>
      get copyWith;
}

abstract class $RecurringTaskWateringNotLoadedCopyWith<$Res> {
  factory $RecurringTaskWateringNotLoadedCopyWith(
          RecurringTaskWateringNotLoaded value,
          $Res Function(RecurringTaskWateringNotLoaded) then) =
      _$RecurringTaskWateringNotLoadedCopyWithImpl<$Res>;
}

class _$RecurringTaskWateringNotLoadedCopyWithImpl<$Res>
    extends _$RecurringTaskWateringStateCopyWithImpl<$Res>
    implements $RecurringTaskWateringNotLoadedCopyWith<$Res> {
  _$RecurringTaskWateringNotLoadedCopyWithImpl(
      RecurringTaskWateringNotLoaded _value,
      $Res Function(RecurringTaskWateringNotLoaded) _then)
      : super(_value, (v) => _then(v as RecurringTaskWateringNotLoaded));

  @override
  RecurringTaskWateringNotLoaded get _value =>
      super._value as RecurringTaskWateringNotLoaded;
}

class _$RecurringTaskWateringNotLoaded
    implements RecurringTaskWateringNotLoaded {
  const _$RecurringTaskWateringNotLoaded();

  @override
  String toString() {
    return 'RecurringTaskWateringState.recurringTaskWateringNotLoaded()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is RecurringTaskWateringNotLoaded);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result recurringTaskWateringLoading(),
    @required
        Result recurringTaskWateringLoaded(
            @nullable RecurringTaskWatering recurringTaskWatering),
    @required Result recurringTaskWateringNotLoaded(),
    @required
        Result recurringTaskWateringSuccess(
            RecurringTaskWatering recurringTaskWatering),
    @required
        Result recurringTaskWateringSavedFailed(
            RecurringTaskWatering recurringTaskWatering),
  }) {
    assert(recurringTaskWateringLoading != null);
    assert(recurringTaskWateringLoaded != null);
    assert(recurringTaskWateringNotLoaded != null);
    assert(recurringTaskWateringSuccess != null);
    assert(recurringTaskWateringSavedFailed != null);
    return recurringTaskWateringNotLoaded();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result recurringTaskWateringLoading(),
    Result recurringTaskWateringLoaded(
        @nullable RecurringTaskWatering recurringTaskWatering),
    Result recurringTaskWateringNotLoaded(),
    Result recurringTaskWateringSuccess(
        RecurringTaskWatering recurringTaskWatering),
    Result recurringTaskWateringSavedFailed(
        RecurringTaskWatering recurringTaskWatering),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (recurringTaskWateringNotLoaded != null) {
      return recurringTaskWateringNotLoaded();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required
        Result recurringTaskWateringLoading(RecurringTaskWateringLoading value),
    @required
        Result recurringTaskWateringLoaded(RecurringTaskWateringLoaded value),
    @required
        Result recurringTaskWateringNotLoaded(
            RecurringTaskWateringNotLoaded value),
    @required
        Result recurringTaskWateringSuccess(
            RecurringTaskWateringSavedSuccess value),
    @required
        Result recurringTaskWateringSavedFailed(
            RecurringTaskWateringSavedFailed value),
  }) {
    assert(recurringTaskWateringLoading != null);
    assert(recurringTaskWateringLoaded != null);
    assert(recurringTaskWateringNotLoaded != null);
    assert(recurringTaskWateringSuccess != null);
    assert(recurringTaskWateringSavedFailed != null);
    return recurringTaskWateringNotLoaded(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result recurringTaskWateringLoading(RecurringTaskWateringLoading value),
    Result recurringTaskWateringLoaded(RecurringTaskWateringLoaded value),
    Result recurringTaskWateringNotLoaded(RecurringTaskWateringNotLoaded value),
    Result recurringTaskWateringSuccess(
        RecurringTaskWateringSavedSuccess value),
    Result recurringTaskWateringSavedFailed(
        RecurringTaskWateringSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (recurringTaskWateringNotLoaded != null) {
      return recurringTaskWateringNotLoaded(this);
    }
    return orElse();
  }
}

abstract class RecurringTaskWateringNotLoaded
    implements RecurringTaskWateringState {
  const factory RecurringTaskWateringNotLoaded() =
      _$RecurringTaskWateringNotLoaded;
}

abstract class $RecurringTaskWateringSavedSuccessCopyWith<$Res> {
  factory $RecurringTaskWateringSavedSuccessCopyWith(
          RecurringTaskWateringSavedSuccess value,
          $Res Function(RecurringTaskWateringSavedSuccess) then) =
      _$RecurringTaskWateringSavedSuccessCopyWithImpl<$Res>;
  $Res call({RecurringTaskWatering recurringTaskWatering});

  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering;
}

class _$RecurringTaskWateringSavedSuccessCopyWithImpl<$Res>
    extends _$RecurringTaskWateringStateCopyWithImpl<$Res>
    implements $RecurringTaskWateringSavedSuccessCopyWith<$Res> {
  _$RecurringTaskWateringSavedSuccessCopyWithImpl(
      RecurringTaskWateringSavedSuccess _value,
      $Res Function(RecurringTaskWateringSavedSuccess) _then)
      : super(_value, (v) => _then(v as RecurringTaskWateringSavedSuccess));

  @override
  RecurringTaskWateringSavedSuccess get _value =>
      super._value as RecurringTaskWateringSavedSuccess;

  @override
  $Res call({
    Object recurringTaskWatering = freezed,
  }) {
    return _then(RecurringTaskWateringSavedSuccess(
      recurringTaskWatering == freezed
          ? _value.recurringTaskWatering
          : recurringTaskWatering as RecurringTaskWatering,
    ));
  }

  @override
  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering {
    if (_value.recurringTaskWatering == null) {
      return null;
    }
    return $RecurringTaskWateringCopyWith<$Res>(_value.recurringTaskWatering,
        (value) {
      return _then(_value.copyWith(recurringTaskWatering: value));
    });
  }
}

class _$RecurringTaskWateringSavedSuccess
    implements RecurringTaskWateringSavedSuccess {
  const _$RecurringTaskWateringSavedSuccess(this.recurringTaskWatering)
      : assert(recurringTaskWatering != null);

  @override
  final RecurringTaskWatering recurringTaskWatering;

  @override
  String toString() {
    return 'RecurringTaskWateringState.recurringTaskWateringSuccess(recurringTaskWatering: $recurringTaskWatering)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is RecurringTaskWateringSavedSuccess &&
            (identical(other.recurringTaskWatering, recurringTaskWatering) ||
                const DeepCollectionEquality().equals(
                    other.recurringTaskWatering, recurringTaskWatering)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(recurringTaskWatering);

  @override
  $RecurringTaskWateringSavedSuccessCopyWith<RecurringTaskWateringSavedSuccess>
      get copyWith => _$RecurringTaskWateringSavedSuccessCopyWithImpl<
          RecurringTaskWateringSavedSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result recurringTaskWateringLoading(),
    @required
        Result recurringTaskWateringLoaded(
            @nullable RecurringTaskWatering recurringTaskWatering),
    @required Result recurringTaskWateringNotLoaded(),
    @required
        Result recurringTaskWateringSuccess(
            RecurringTaskWatering recurringTaskWatering),
    @required
        Result recurringTaskWateringSavedFailed(
            RecurringTaskWatering recurringTaskWatering),
  }) {
    assert(recurringTaskWateringLoading != null);
    assert(recurringTaskWateringLoaded != null);
    assert(recurringTaskWateringNotLoaded != null);
    assert(recurringTaskWateringSuccess != null);
    assert(recurringTaskWateringSavedFailed != null);
    return recurringTaskWateringSuccess(recurringTaskWatering);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result recurringTaskWateringLoading(),
    Result recurringTaskWateringLoaded(
        @nullable RecurringTaskWatering recurringTaskWatering),
    Result recurringTaskWateringNotLoaded(),
    Result recurringTaskWateringSuccess(
        RecurringTaskWatering recurringTaskWatering),
    Result recurringTaskWateringSavedFailed(
        RecurringTaskWatering recurringTaskWatering),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (recurringTaskWateringSuccess != null) {
      return recurringTaskWateringSuccess(recurringTaskWatering);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required
        Result recurringTaskWateringLoading(RecurringTaskWateringLoading value),
    @required
        Result recurringTaskWateringLoaded(RecurringTaskWateringLoaded value),
    @required
        Result recurringTaskWateringNotLoaded(
            RecurringTaskWateringNotLoaded value),
    @required
        Result recurringTaskWateringSuccess(
            RecurringTaskWateringSavedSuccess value),
    @required
        Result recurringTaskWateringSavedFailed(
            RecurringTaskWateringSavedFailed value),
  }) {
    assert(recurringTaskWateringLoading != null);
    assert(recurringTaskWateringLoaded != null);
    assert(recurringTaskWateringNotLoaded != null);
    assert(recurringTaskWateringSuccess != null);
    assert(recurringTaskWateringSavedFailed != null);
    return recurringTaskWateringSuccess(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result recurringTaskWateringLoading(RecurringTaskWateringLoading value),
    Result recurringTaskWateringLoaded(RecurringTaskWateringLoaded value),
    Result recurringTaskWateringNotLoaded(RecurringTaskWateringNotLoaded value),
    Result recurringTaskWateringSuccess(
        RecurringTaskWateringSavedSuccess value),
    Result recurringTaskWateringSavedFailed(
        RecurringTaskWateringSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (recurringTaskWateringSuccess != null) {
      return recurringTaskWateringSuccess(this);
    }
    return orElse();
  }
}

abstract class RecurringTaskWateringSavedSuccess
    implements RecurringTaskWateringState {
  const factory RecurringTaskWateringSavedSuccess(
          RecurringTaskWatering recurringTaskWatering) =
      _$RecurringTaskWateringSavedSuccess;

  RecurringTaskWatering get recurringTaskWatering;
  $RecurringTaskWateringSavedSuccessCopyWith<RecurringTaskWateringSavedSuccess>
      get copyWith;
}

abstract class $RecurringTaskWateringSavedFailedCopyWith<$Res> {
  factory $RecurringTaskWateringSavedFailedCopyWith(
          RecurringTaskWateringSavedFailed value,
          $Res Function(RecurringTaskWateringSavedFailed) then) =
      _$RecurringTaskWateringSavedFailedCopyWithImpl<$Res>;
  $Res call({RecurringTaskWatering recurringTaskWatering});

  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering;
}

class _$RecurringTaskWateringSavedFailedCopyWithImpl<$Res>
    extends _$RecurringTaskWateringStateCopyWithImpl<$Res>
    implements $RecurringTaskWateringSavedFailedCopyWith<$Res> {
  _$RecurringTaskWateringSavedFailedCopyWithImpl(
      RecurringTaskWateringSavedFailed _value,
      $Res Function(RecurringTaskWateringSavedFailed) _then)
      : super(_value, (v) => _then(v as RecurringTaskWateringSavedFailed));

  @override
  RecurringTaskWateringSavedFailed get _value =>
      super._value as RecurringTaskWateringSavedFailed;

  @override
  $Res call({
    Object recurringTaskWatering = freezed,
  }) {
    return _then(RecurringTaskWateringSavedFailed(
      recurringTaskWatering == freezed
          ? _value.recurringTaskWatering
          : recurringTaskWatering as RecurringTaskWatering,
    ));
  }

  @override
  $RecurringTaskWateringCopyWith<$Res> get recurringTaskWatering {
    if (_value.recurringTaskWatering == null) {
      return null;
    }
    return $RecurringTaskWateringCopyWith<$Res>(_value.recurringTaskWatering,
        (value) {
      return _then(_value.copyWith(recurringTaskWatering: value));
    });
  }
}

class _$RecurringTaskWateringSavedFailed
    implements RecurringTaskWateringSavedFailed {
  const _$RecurringTaskWateringSavedFailed(this.recurringTaskWatering)
      : assert(recurringTaskWatering != null);

  @override
  final RecurringTaskWatering recurringTaskWatering;

  @override
  String toString() {
    return 'RecurringTaskWateringState.recurringTaskWateringSavedFailed(recurringTaskWatering: $recurringTaskWatering)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is RecurringTaskWateringSavedFailed &&
            (identical(other.recurringTaskWatering, recurringTaskWatering) ||
                const DeepCollectionEquality().equals(
                    other.recurringTaskWatering, recurringTaskWatering)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(recurringTaskWatering);

  @override
  $RecurringTaskWateringSavedFailedCopyWith<RecurringTaskWateringSavedFailed>
      get copyWith => _$RecurringTaskWateringSavedFailedCopyWithImpl<
          RecurringTaskWateringSavedFailed>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result recurringTaskWateringLoading(),
    @required
        Result recurringTaskWateringLoaded(
            @nullable RecurringTaskWatering recurringTaskWatering),
    @required Result recurringTaskWateringNotLoaded(),
    @required
        Result recurringTaskWateringSuccess(
            RecurringTaskWatering recurringTaskWatering),
    @required
        Result recurringTaskWateringSavedFailed(
            RecurringTaskWatering recurringTaskWatering),
  }) {
    assert(recurringTaskWateringLoading != null);
    assert(recurringTaskWateringLoaded != null);
    assert(recurringTaskWateringNotLoaded != null);
    assert(recurringTaskWateringSuccess != null);
    assert(recurringTaskWateringSavedFailed != null);
    return recurringTaskWateringSavedFailed(recurringTaskWatering);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result recurringTaskWateringLoading(),
    Result recurringTaskWateringLoaded(
        @nullable RecurringTaskWatering recurringTaskWatering),
    Result recurringTaskWateringNotLoaded(),
    Result recurringTaskWateringSuccess(
        RecurringTaskWatering recurringTaskWatering),
    Result recurringTaskWateringSavedFailed(
        RecurringTaskWatering recurringTaskWatering),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (recurringTaskWateringSavedFailed != null) {
      return recurringTaskWateringSavedFailed(recurringTaskWatering);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required
        Result recurringTaskWateringLoading(RecurringTaskWateringLoading value),
    @required
        Result recurringTaskWateringLoaded(RecurringTaskWateringLoaded value),
    @required
        Result recurringTaskWateringNotLoaded(
            RecurringTaskWateringNotLoaded value),
    @required
        Result recurringTaskWateringSuccess(
            RecurringTaskWateringSavedSuccess value),
    @required
        Result recurringTaskWateringSavedFailed(
            RecurringTaskWateringSavedFailed value),
  }) {
    assert(recurringTaskWateringLoading != null);
    assert(recurringTaskWateringLoaded != null);
    assert(recurringTaskWateringNotLoaded != null);
    assert(recurringTaskWateringSuccess != null);
    assert(recurringTaskWateringSavedFailed != null);
    return recurringTaskWateringSavedFailed(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result recurringTaskWateringLoading(RecurringTaskWateringLoading value),
    Result recurringTaskWateringLoaded(RecurringTaskWateringLoaded value),
    Result recurringTaskWateringNotLoaded(RecurringTaskWateringNotLoaded value),
    Result recurringTaskWateringSuccess(
        RecurringTaskWateringSavedSuccess value),
    Result recurringTaskWateringSavedFailed(
        RecurringTaskWateringSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (recurringTaskWateringSavedFailed != null) {
      return recurringTaskWateringSavedFailed(this);
    }
    return orElse();
  }
}

abstract class RecurringTaskWateringSavedFailed
    implements RecurringTaskWateringState {
  const factory RecurringTaskWateringSavedFailed(
          RecurringTaskWatering recurringTaskWatering) =
      _$RecurringTaskWateringSavedFailed;

  RecurringTaskWatering get recurringTaskWatering;
  $RecurringTaskWateringSavedFailedCopyWith<RecurringTaskWateringSavedFailed>
      get copyWith;
}
