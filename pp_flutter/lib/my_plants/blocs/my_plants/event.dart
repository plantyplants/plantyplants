part of 'bloc.dart';

/// [Event] for [MyPlant] model (load, add, update, etc.).
@freezed
abstract class MyPlantsEvent with _$MyPlantsEvent {
  /// Loading the list of [MyPlant] from the repository into the interface.
  const factory MyPlantsEvent.loadMyPlants() = LoadMyPlants;
  /// Add a [MyPlant] to the interface and repository.
  const factory MyPlantsEvent.addMyPlant(MyPlant myPlant,
      {List<FrontPictureFormat> frontPictureFormats}) = AddMyPlant;
  /// Update a [MyPlant] on the interface and repository.
  const factory MyPlantsEvent.updateMyPlant(MyPlant updatedMyPlant,
      {List<FrontPictureFormat> frontPictureFormats}) = UpdateMyPlant;
  /// Delete a [MyPlant] to the interface and repository.
  const factory MyPlantsEvent.deleteMyPlant(MyPlant myPlant) = DeleteMyPlant;
  /// The list of [MyPlant] were updated and need to be loaded.
  const factory MyPlantsEvent.myPlantsUpdated(List<MyPlant> myPlants) =
      MyPlantsUpdated;
}
