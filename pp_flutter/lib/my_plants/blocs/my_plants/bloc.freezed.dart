// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$MyPlantsEventTearOff {
  const _$MyPlantsEventTearOff();

  LoadMyPlants loadMyPlants() {
    return const LoadMyPlants();
  }

  AddMyPlant addMyPlant(MyPlant myPlant,
      {List<FrontPictureFormat> frontPictureFormats}) {
    return AddMyPlant(
      myPlant,
      frontPictureFormats: frontPictureFormats,
    );
  }

  UpdateMyPlant updateMyPlant(MyPlant updatedMyPlant,
      {List<FrontPictureFormat> frontPictureFormats}) {
    return UpdateMyPlant(
      updatedMyPlant,
      frontPictureFormats: frontPictureFormats,
    );
  }

  DeleteMyPlant deleteMyPlant(MyPlant myPlant) {
    return DeleteMyPlant(
      myPlant,
    );
  }

  MyPlantsUpdated myPlantsUpdated(List<MyPlant> myPlants) {
    return MyPlantsUpdated(
      myPlants,
    );
  }
}

// ignore: unused_element
const $MyPlantsEvent = _$MyPlantsEventTearOff();

mixin _$MyPlantsEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadMyPlants(),
    @required
        Result addMyPlant(
            MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats),
    @required
        Result updateMyPlant(MyPlant updatedMyPlant,
            List<FrontPictureFormat> frontPictureFormats),
    @required Result deleteMyPlant(MyPlant myPlant),
    @required Result myPlantsUpdated(List<MyPlant> myPlants),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadMyPlants(),
    Result addMyPlant(
        MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats),
    Result updateMyPlant(
        MyPlant updatedMyPlant, List<FrontPictureFormat> frontPictureFormats),
    Result deleteMyPlant(MyPlant myPlant),
    Result myPlantsUpdated(List<MyPlant> myPlants),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadMyPlants(LoadMyPlants value),
    @required Result addMyPlant(AddMyPlant value),
    @required Result updateMyPlant(UpdateMyPlant value),
    @required Result deleteMyPlant(DeleteMyPlant value),
    @required Result myPlantsUpdated(MyPlantsUpdated value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadMyPlants(LoadMyPlants value),
    Result addMyPlant(AddMyPlant value),
    Result updateMyPlant(UpdateMyPlant value),
    Result deleteMyPlant(DeleteMyPlant value),
    Result myPlantsUpdated(MyPlantsUpdated value),
    @required Result orElse(),
  });
}

abstract class $MyPlantsEventCopyWith<$Res> {
  factory $MyPlantsEventCopyWith(
          MyPlantsEvent value, $Res Function(MyPlantsEvent) then) =
      _$MyPlantsEventCopyWithImpl<$Res>;
}

class _$MyPlantsEventCopyWithImpl<$Res>
    implements $MyPlantsEventCopyWith<$Res> {
  _$MyPlantsEventCopyWithImpl(this._value, this._then);

  final MyPlantsEvent _value;
  // ignore: unused_field
  final $Res Function(MyPlantsEvent) _then;
}

abstract class $LoadMyPlantsCopyWith<$Res> {
  factory $LoadMyPlantsCopyWith(
          LoadMyPlants value, $Res Function(LoadMyPlants) then) =
      _$LoadMyPlantsCopyWithImpl<$Res>;
}

class _$LoadMyPlantsCopyWithImpl<$Res> extends _$MyPlantsEventCopyWithImpl<$Res>
    implements $LoadMyPlantsCopyWith<$Res> {
  _$LoadMyPlantsCopyWithImpl(
      LoadMyPlants _value, $Res Function(LoadMyPlants) _then)
      : super(_value, (v) => _then(v as LoadMyPlants));

  @override
  LoadMyPlants get _value => super._value as LoadMyPlants;
}

class _$LoadMyPlants implements LoadMyPlants {
  const _$LoadMyPlants();

  @override
  String toString() {
    return 'MyPlantsEvent.loadMyPlants()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadMyPlants);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadMyPlants(),
    @required
        Result addMyPlant(
            MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats),
    @required
        Result updateMyPlant(MyPlant updatedMyPlant,
            List<FrontPictureFormat> frontPictureFormats),
    @required Result deleteMyPlant(MyPlant myPlant),
    @required Result myPlantsUpdated(List<MyPlant> myPlants),
  }) {
    assert(loadMyPlants != null);
    assert(addMyPlant != null);
    assert(updateMyPlant != null);
    assert(deleteMyPlant != null);
    assert(myPlantsUpdated != null);
    return loadMyPlants();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadMyPlants(),
    Result addMyPlant(
        MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats),
    Result updateMyPlant(
        MyPlant updatedMyPlant, List<FrontPictureFormat> frontPictureFormats),
    Result deleteMyPlant(MyPlant myPlant),
    Result myPlantsUpdated(List<MyPlant> myPlants),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loadMyPlants != null) {
      return loadMyPlants();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadMyPlants(LoadMyPlants value),
    @required Result addMyPlant(AddMyPlant value),
    @required Result updateMyPlant(UpdateMyPlant value),
    @required Result deleteMyPlant(DeleteMyPlant value),
    @required Result myPlantsUpdated(MyPlantsUpdated value),
  }) {
    assert(loadMyPlants != null);
    assert(addMyPlant != null);
    assert(updateMyPlant != null);
    assert(deleteMyPlant != null);
    assert(myPlantsUpdated != null);
    return loadMyPlants(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadMyPlants(LoadMyPlants value),
    Result addMyPlant(AddMyPlant value),
    Result updateMyPlant(UpdateMyPlant value),
    Result deleteMyPlant(DeleteMyPlant value),
    Result myPlantsUpdated(MyPlantsUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loadMyPlants != null) {
      return loadMyPlants(this);
    }
    return orElse();
  }
}

abstract class LoadMyPlants implements MyPlantsEvent {
  const factory LoadMyPlants() = _$LoadMyPlants;
}

abstract class $AddMyPlantCopyWith<$Res> {
  factory $AddMyPlantCopyWith(
          AddMyPlant value, $Res Function(AddMyPlant) then) =
      _$AddMyPlantCopyWithImpl<$Res>;
  $Res call({MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats});

  $MyPlantCopyWith<$Res> get myPlant;
}

class _$AddMyPlantCopyWithImpl<$Res> extends _$MyPlantsEventCopyWithImpl<$Res>
    implements $AddMyPlantCopyWith<$Res> {
  _$AddMyPlantCopyWithImpl(AddMyPlant _value, $Res Function(AddMyPlant) _then)
      : super(_value, (v) => _then(v as AddMyPlant));

  @override
  AddMyPlant get _value => super._value as AddMyPlant;

  @override
  $Res call({
    Object myPlant = freezed,
    Object frontPictureFormats = freezed,
  }) {
    return _then(AddMyPlant(
      myPlant == freezed ? _value.myPlant : myPlant as MyPlant,
      frontPictureFormats: frontPictureFormats == freezed
          ? _value.frontPictureFormats
          : frontPictureFormats as List<FrontPictureFormat>,
    ));
  }

  @override
  $MyPlantCopyWith<$Res> get myPlant {
    if (_value.myPlant == null) {
      return null;
    }
    return $MyPlantCopyWith<$Res>(_value.myPlant, (value) {
      return _then(_value.copyWith(myPlant: value));
    });
  }
}

class _$AddMyPlant implements AddMyPlant {
  const _$AddMyPlant(this.myPlant, {this.frontPictureFormats})
      : assert(myPlant != null);

  @override
  final MyPlant myPlant;
  @override
  final List<FrontPictureFormat> frontPictureFormats;

  @override
  String toString() {
    return 'MyPlantsEvent.addMyPlant(myPlant: $myPlant, frontPictureFormats: $frontPictureFormats)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddMyPlant &&
            (identical(other.myPlant, myPlant) ||
                const DeepCollectionEquality()
                    .equals(other.myPlant, myPlant)) &&
            (identical(other.frontPictureFormats, frontPictureFormats) ||
                const DeepCollectionEquality()
                    .equals(other.frontPictureFormats, frontPictureFormats)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(myPlant) ^
      const DeepCollectionEquality().hash(frontPictureFormats);

  @override
  $AddMyPlantCopyWith<AddMyPlant> get copyWith =>
      _$AddMyPlantCopyWithImpl<AddMyPlant>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadMyPlants(),
    @required
        Result addMyPlant(
            MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats),
    @required
        Result updateMyPlant(MyPlant updatedMyPlant,
            List<FrontPictureFormat> frontPictureFormats),
    @required Result deleteMyPlant(MyPlant myPlant),
    @required Result myPlantsUpdated(List<MyPlant> myPlants),
  }) {
    assert(loadMyPlants != null);
    assert(addMyPlant != null);
    assert(updateMyPlant != null);
    assert(deleteMyPlant != null);
    assert(myPlantsUpdated != null);
    return addMyPlant(myPlant, frontPictureFormats);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadMyPlants(),
    Result addMyPlant(
        MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats),
    Result updateMyPlant(
        MyPlant updatedMyPlant, List<FrontPictureFormat> frontPictureFormats),
    Result deleteMyPlant(MyPlant myPlant),
    Result myPlantsUpdated(List<MyPlant> myPlants),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (addMyPlant != null) {
      return addMyPlant(myPlant, frontPictureFormats);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadMyPlants(LoadMyPlants value),
    @required Result addMyPlant(AddMyPlant value),
    @required Result updateMyPlant(UpdateMyPlant value),
    @required Result deleteMyPlant(DeleteMyPlant value),
    @required Result myPlantsUpdated(MyPlantsUpdated value),
  }) {
    assert(loadMyPlants != null);
    assert(addMyPlant != null);
    assert(updateMyPlant != null);
    assert(deleteMyPlant != null);
    assert(myPlantsUpdated != null);
    return addMyPlant(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadMyPlants(LoadMyPlants value),
    Result addMyPlant(AddMyPlant value),
    Result updateMyPlant(UpdateMyPlant value),
    Result deleteMyPlant(DeleteMyPlant value),
    Result myPlantsUpdated(MyPlantsUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (addMyPlant != null) {
      return addMyPlant(this);
    }
    return orElse();
  }
}

abstract class AddMyPlant implements MyPlantsEvent {
  const factory AddMyPlant(MyPlant myPlant,
      {List<FrontPictureFormat> frontPictureFormats}) = _$AddMyPlant;

  MyPlant get myPlant;
  List<FrontPictureFormat> get frontPictureFormats;
  $AddMyPlantCopyWith<AddMyPlant> get copyWith;
}

abstract class $UpdateMyPlantCopyWith<$Res> {
  factory $UpdateMyPlantCopyWith(
          UpdateMyPlant value, $Res Function(UpdateMyPlant) then) =
      _$UpdateMyPlantCopyWithImpl<$Res>;
  $Res call(
      {MyPlant updatedMyPlant, List<FrontPictureFormat> frontPictureFormats});

  $MyPlantCopyWith<$Res> get updatedMyPlant;
}

class _$UpdateMyPlantCopyWithImpl<$Res>
    extends _$MyPlantsEventCopyWithImpl<$Res>
    implements $UpdateMyPlantCopyWith<$Res> {
  _$UpdateMyPlantCopyWithImpl(
      UpdateMyPlant _value, $Res Function(UpdateMyPlant) _then)
      : super(_value, (v) => _then(v as UpdateMyPlant));

  @override
  UpdateMyPlant get _value => super._value as UpdateMyPlant;

  @override
  $Res call({
    Object updatedMyPlant = freezed,
    Object frontPictureFormats = freezed,
  }) {
    return _then(UpdateMyPlant(
      updatedMyPlant == freezed
          ? _value.updatedMyPlant
          : updatedMyPlant as MyPlant,
      frontPictureFormats: frontPictureFormats == freezed
          ? _value.frontPictureFormats
          : frontPictureFormats as List<FrontPictureFormat>,
    ));
  }

  @override
  $MyPlantCopyWith<$Res> get updatedMyPlant {
    if (_value.updatedMyPlant == null) {
      return null;
    }
    return $MyPlantCopyWith<$Res>(_value.updatedMyPlant, (value) {
      return _then(_value.copyWith(updatedMyPlant: value));
    });
  }
}

class _$UpdateMyPlant implements UpdateMyPlant {
  const _$UpdateMyPlant(this.updatedMyPlant, {this.frontPictureFormats})
      : assert(updatedMyPlant != null);

  @override
  final MyPlant updatedMyPlant;
  @override
  final List<FrontPictureFormat> frontPictureFormats;

  @override
  String toString() {
    return 'MyPlantsEvent.updateMyPlant(updatedMyPlant: $updatedMyPlant, frontPictureFormats: $frontPictureFormats)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdateMyPlant &&
            (identical(other.updatedMyPlant, updatedMyPlant) ||
                const DeepCollectionEquality()
                    .equals(other.updatedMyPlant, updatedMyPlant)) &&
            (identical(other.frontPictureFormats, frontPictureFormats) ||
                const DeepCollectionEquality()
                    .equals(other.frontPictureFormats, frontPictureFormats)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(updatedMyPlant) ^
      const DeepCollectionEquality().hash(frontPictureFormats);

  @override
  $UpdateMyPlantCopyWith<UpdateMyPlant> get copyWith =>
      _$UpdateMyPlantCopyWithImpl<UpdateMyPlant>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadMyPlants(),
    @required
        Result addMyPlant(
            MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats),
    @required
        Result updateMyPlant(MyPlant updatedMyPlant,
            List<FrontPictureFormat> frontPictureFormats),
    @required Result deleteMyPlant(MyPlant myPlant),
    @required Result myPlantsUpdated(List<MyPlant> myPlants),
  }) {
    assert(loadMyPlants != null);
    assert(addMyPlant != null);
    assert(updateMyPlant != null);
    assert(deleteMyPlant != null);
    assert(myPlantsUpdated != null);
    return updateMyPlant(updatedMyPlant, frontPictureFormats);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadMyPlants(),
    Result addMyPlant(
        MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats),
    Result updateMyPlant(
        MyPlant updatedMyPlant, List<FrontPictureFormat> frontPictureFormats),
    Result deleteMyPlant(MyPlant myPlant),
    Result myPlantsUpdated(List<MyPlant> myPlants),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (updateMyPlant != null) {
      return updateMyPlant(updatedMyPlant, frontPictureFormats);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadMyPlants(LoadMyPlants value),
    @required Result addMyPlant(AddMyPlant value),
    @required Result updateMyPlant(UpdateMyPlant value),
    @required Result deleteMyPlant(DeleteMyPlant value),
    @required Result myPlantsUpdated(MyPlantsUpdated value),
  }) {
    assert(loadMyPlants != null);
    assert(addMyPlant != null);
    assert(updateMyPlant != null);
    assert(deleteMyPlant != null);
    assert(myPlantsUpdated != null);
    return updateMyPlant(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadMyPlants(LoadMyPlants value),
    Result addMyPlant(AddMyPlant value),
    Result updateMyPlant(UpdateMyPlant value),
    Result deleteMyPlant(DeleteMyPlant value),
    Result myPlantsUpdated(MyPlantsUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (updateMyPlant != null) {
      return updateMyPlant(this);
    }
    return orElse();
  }
}

abstract class UpdateMyPlant implements MyPlantsEvent {
  const factory UpdateMyPlant(MyPlant updatedMyPlant,
      {List<FrontPictureFormat> frontPictureFormats}) = _$UpdateMyPlant;

  MyPlant get updatedMyPlant;
  List<FrontPictureFormat> get frontPictureFormats;
  $UpdateMyPlantCopyWith<UpdateMyPlant> get copyWith;
}

abstract class $DeleteMyPlantCopyWith<$Res> {
  factory $DeleteMyPlantCopyWith(
          DeleteMyPlant value, $Res Function(DeleteMyPlant) then) =
      _$DeleteMyPlantCopyWithImpl<$Res>;
  $Res call({MyPlant myPlant});

  $MyPlantCopyWith<$Res> get myPlant;
}

class _$DeleteMyPlantCopyWithImpl<$Res>
    extends _$MyPlantsEventCopyWithImpl<$Res>
    implements $DeleteMyPlantCopyWith<$Res> {
  _$DeleteMyPlantCopyWithImpl(
      DeleteMyPlant _value, $Res Function(DeleteMyPlant) _then)
      : super(_value, (v) => _then(v as DeleteMyPlant));

  @override
  DeleteMyPlant get _value => super._value as DeleteMyPlant;

  @override
  $Res call({
    Object myPlant = freezed,
  }) {
    return _then(DeleteMyPlant(
      myPlant == freezed ? _value.myPlant : myPlant as MyPlant,
    ));
  }

  @override
  $MyPlantCopyWith<$Res> get myPlant {
    if (_value.myPlant == null) {
      return null;
    }
    return $MyPlantCopyWith<$Res>(_value.myPlant, (value) {
      return _then(_value.copyWith(myPlant: value));
    });
  }
}

class _$DeleteMyPlant implements DeleteMyPlant {
  const _$DeleteMyPlant(this.myPlant) : assert(myPlant != null);

  @override
  final MyPlant myPlant;

  @override
  String toString() {
    return 'MyPlantsEvent.deleteMyPlant(myPlant: $myPlant)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is DeleteMyPlant &&
            (identical(other.myPlant, myPlant) ||
                const DeepCollectionEquality().equals(other.myPlant, myPlant)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(myPlant);

  @override
  $DeleteMyPlantCopyWith<DeleteMyPlant> get copyWith =>
      _$DeleteMyPlantCopyWithImpl<DeleteMyPlant>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadMyPlants(),
    @required
        Result addMyPlant(
            MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats),
    @required
        Result updateMyPlant(MyPlant updatedMyPlant,
            List<FrontPictureFormat> frontPictureFormats),
    @required Result deleteMyPlant(MyPlant myPlant),
    @required Result myPlantsUpdated(List<MyPlant> myPlants),
  }) {
    assert(loadMyPlants != null);
    assert(addMyPlant != null);
    assert(updateMyPlant != null);
    assert(deleteMyPlant != null);
    assert(myPlantsUpdated != null);
    return deleteMyPlant(myPlant);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadMyPlants(),
    Result addMyPlant(
        MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats),
    Result updateMyPlant(
        MyPlant updatedMyPlant, List<FrontPictureFormat> frontPictureFormats),
    Result deleteMyPlant(MyPlant myPlant),
    Result myPlantsUpdated(List<MyPlant> myPlants),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (deleteMyPlant != null) {
      return deleteMyPlant(myPlant);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadMyPlants(LoadMyPlants value),
    @required Result addMyPlant(AddMyPlant value),
    @required Result updateMyPlant(UpdateMyPlant value),
    @required Result deleteMyPlant(DeleteMyPlant value),
    @required Result myPlantsUpdated(MyPlantsUpdated value),
  }) {
    assert(loadMyPlants != null);
    assert(addMyPlant != null);
    assert(updateMyPlant != null);
    assert(deleteMyPlant != null);
    assert(myPlantsUpdated != null);
    return deleteMyPlant(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadMyPlants(LoadMyPlants value),
    Result addMyPlant(AddMyPlant value),
    Result updateMyPlant(UpdateMyPlant value),
    Result deleteMyPlant(DeleteMyPlant value),
    Result myPlantsUpdated(MyPlantsUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (deleteMyPlant != null) {
      return deleteMyPlant(this);
    }
    return orElse();
  }
}

abstract class DeleteMyPlant implements MyPlantsEvent {
  const factory DeleteMyPlant(MyPlant myPlant) = _$DeleteMyPlant;

  MyPlant get myPlant;
  $DeleteMyPlantCopyWith<DeleteMyPlant> get copyWith;
}

abstract class $MyPlantsUpdatedCopyWith<$Res> {
  factory $MyPlantsUpdatedCopyWith(
          MyPlantsUpdated value, $Res Function(MyPlantsUpdated) then) =
      _$MyPlantsUpdatedCopyWithImpl<$Res>;
  $Res call({List<MyPlant> myPlants});
}

class _$MyPlantsUpdatedCopyWithImpl<$Res>
    extends _$MyPlantsEventCopyWithImpl<$Res>
    implements $MyPlantsUpdatedCopyWith<$Res> {
  _$MyPlantsUpdatedCopyWithImpl(
      MyPlantsUpdated _value, $Res Function(MyPlantsUpdated) _then)
      : super(_value, (v) => _then(v as MyPlantsUpdated));

  @override
  MyPlantsUpdated get _value => super._value as MyPlantsUpdated;

  @override
  $Res call({
    Object myPlants = freezed,
  }) {
    return _then(MyPlantsUpdated(
      myPlants == freezed ? _value.myPlants : myPlants as List<MyPlant>,
    ));
  }
}

class _$MyPlantsUpdated implements MyPlantsUpdated {
  const _$MyPlantsUpdated(this.myPlants) : assert(myPlants != null);

  @override
  final List<MyPlant> myPlants;

  @override
  String toString() {
    return 'MyPlantsEvent.myPlantsUpdated(myPlants: $myPlants)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is MyPlantsUpdated &&
            (identical(other.myPlants, myPlants) ||
                const DeepCollectionEquality()
                    .equals(other.myPlants, myPlants)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(myPlants);

  @override
  $MyPlantsUpdatedCopyWith<MyPlantsUpdated> get copyWith =>
      _$MyPlantsUpdatedCopyWithImpl<MyPlantsUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadMyPlants(),
    @required
        Result addMyPlant(
            MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats),
    @required
        Result updateMyPlant(MyPlant updatedMyPlant,
            List<FrontPictureFormat> frontPictureFormats),
    @required Result deleteMyPlant(MyPlant myPlant),
    @required Result myPlantsUpdated(List<MyPlant> myPlants),
  }) {
    assert(loadMyPlants != null);
    assert(addMyPlant != null);
    assert(updateMyPlant != null);
    assert(deleteMyPlant != null);
    assert(myPlantsUpdated != null);
    return myPlantsUpdated(myPlants);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadMyPlants(),
    Result addMyPlant(
        MyPlant myPlant, List<FrontPictureFormat> frontPictureFormats),
    Result updateMyPlant(
        MyPlant updatedMyPlant, List<FrontPictureFormat> frontPictureFormats),
    Result deleteMyPlant(MyPlant myPlant),
    Result myPlantsUpdated(List<MyPlant> myPlants),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (myPlantsUpdated != null) {
      return myPlantsUpdated(myPlants);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadMyPlants(LoadMyPlants value),
    @required Result addMyPlant(AddMyPlant value),
    @required Result updateMyPlant(UpdateMyPlant value),
    @required Result deleteMyPlant(DeleteMyPlant value),
    @required Result myPlantsUpdated(MyPlantsUpdated value),
  }) {
    assert(loadMyPlants != null);
    assert(addMyPlant != null);
    assert(updateMyPlant != null);
    assert(deleteMyPlant != null);
    assert(myPlantsUpdated != null);
    return myPlantsUpdated(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadMyPlants(LoadMyPlants value),
    Result addMyPlant(AddMyPlant value),
    Result updateMyPlant(UpdateMyPlant value),
    Result deleteMyPlant(DeleteMyPlant value),
    Result myPlantsUpdated(MyPlantsUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (myPlantsUpdated != null) {
      return myPlantsUpdated(this);
    }
    return orElse();
  }
}

abstract class MyPlantsUpdated implements MyPlantsEvent {
  const factory MyPlantsUpdated(List<MyPlant> myPlants) = _$MyPlantsUpdated;

  List<MyPlant> get myPlants;
  $MyPlantsUpdatedCopyWith<MyPlantsUpdated> get copyWith;
}

class _$MyPlantsStateTearOff {
  const _$MyPlantsStateTearOff();

  MyPlantsLoading myPlantsLoading() {
    return const MyPlantsLoading();
  }

  MyPlantsLoaded myPlantsLoaded([List<MyPlant> myPlants = const <MyPlant>[]]) {
    return MyPlantsLoaded(
      myPlants,
    );
  }

  MyPlantsNotLoaded myPlantsNotLoaded() {
    return const MyPlantsNotLoaded();
  }

  MyPlantSavedSuccess myPlantSavedSuccess(MyPlant myPlant) {
    return MyPlantSavedSuccess(
      myPlant,
    );
  }

  MyPlantSavedFailed myPlantSavedFailed(MyPlant myPlant) {
    return MyPlantSavedFailed(
      myPlant,
    );
  }
}

// ignore: unused_element
const $MyPlantsState = _$MyPlantsStateTearOff();

mixin _$MyPlantsState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result myPlantsLoading(),
    @required Result myPlantsLoaded(List<MyPlant> myPlants),
    @required Result myPlantsNotLoaded(),
    @required Result myPlantSavedSuccess(MyPlant myPlant),
    @required Result myPlantSavedFailed(MyPlant myPlant),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result myPlantsLoading(),
    Result myPlantsLoaded(List<MyPlant> myPlants),
    Result myPlantsNotLoaded(),
    Result myPlantSavedSuccess(MyPlant myPlant),
    Result myPlantSavedFailed(MyPlant myPlant),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result myPlantsLoading(MyPlantsLoading value),
    @required Result myPlantsLoaded(MyPlantsLoaded value),
    @required Result myPlantsNotLoaded(MyPlantsNotLoaded value),
    @required Result myPlantSavedSuccess(MyPlantSavedSuccess value),
    @required Result myPlantSavedFailed(MyPlantSavedFailed value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result myPlantsLoading(MyPlantsLoading value),
    Result myPlantsLoaded(MyPlantsLoaded value),
    Result myPlantsNotLoaded(MyPlantsNotLoaded value),
    Result myPlantSavedSuccess(MyPlantSavedSuccess value),
    Result myPlantSavedFailed(MyPlantSavedFailed value),
    @required Result orElse(),
  });
}

abstract class $MyPlantsStateCopyWith<$Res> {
  factory $MyPlantsStateCopyWith(
          MyPlantsState value, $Res Function(MyPlantsState) then) =
      _$MyPlantsStateCopyWithImpl<$Res>;
}

class _$MyPlantsStateCopyWithImpl<$Res>
    implements $MyPlantsStateCopyWith<$Res> {
  _$MyPlantsStateCopyWithImpl(this._value, this._then);

  final MyPlantsState _value;
  // ignore: unused_field
  final $Res Function(MyPlantsState) _then;
}

abstract class $MyPlantsLoadingCopyWith<$Res> {
  factory $MyPlantsLoadingCopyWith(
          MyPlantsLoading value, $Res Function(MyPlantsLoading) then) =
      _$MyPlantsLoadingCopyWithImpl<$Res>;
}

class _$MyPlantsLoadingCopyWithImpl<$Res>
    extends _$MyPlantsStateCopyWithImpl<$Res>
    implements $MyPlantsLoadingCopyWith<$Res> {
  _$MyPlantsLoadingCopyWithImpl(
      MyPlantsLoading _value, $Res Function(MyPlantsLoading) _then)
      : super(_value, (v) => _then(v as MyPlantsLoading));

  @override
  MyPlantsLoading get _value => super._value as MyPlantsLoading;
}

class _$MyPlantsLoading implements MyPlantsLoading {
  const _$MyPlantsLoading();

  @override
  String toString() {
    return 'MyPlantsState.myPlantsLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is MyPlantsLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result myPlantsLoading(),
    @required Result myPlantsLoaded(List<MyPlant> myPlants),
    @required Result myPlantsNotLoaded(),
    @required Result myPlantSavedSuccess(MyPlant myPlant),
    @required Result myPlantSavedFailed(MyPlant myPlant),
  }) {
    assert(myPlantsLoading != null);
    assert(myPlantsLoaded != null);
    assert(myPlantsNotLoaded != null);
    assert(myPlantSavedSuccess != null);
    assert(myPlantSavedFailed != null);
    return myPlantsLoading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result myPlantsLoading(),
    Result myPlantsLoaded(List<MyPlant> myPlants),
    Result myPlantsNotLoaded(),
    Result myPlantSavedSuccess(MyPlant myPlant),
    Result myPlantSavedFailed(MyPlant myPlant),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (myPlantsLoading != null) {
      return myPlantsLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result myPlantsLoading(MyPlantsLoading value),
    @required Result myPlantsLoaded(MyPlantsLoaded value),
    @required Result myPlantsNotLoaded(MyPlantsNotLoaded value),
    @required Result myPlantSavedSuccess(MyPlantSavedSuccess value),
    @required Result myPlantSavedFailed(MyPlantSavedFailed value),
  }) {
    assert(myPlantsLoading != null);
    assert(myPlantsLoaded != null);
    assert(myPlantsNotLoaded != null);
    assert(myPlantSavedSuccess != null);
    assert(myPlantSavedFailed != null);
    return myPlantsLoading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result myPlantsLoading(MyPlantsLoading value),
    Result myPlantsLoaded(MyPlantsLoaded value),
    Result myPlantsNotLoaded(MyPlantsNotLoaded value),
    Result myPlantSavedSuccess(MyPlantSavedSuccess value),
    Result myPlantSavedFailed(MyPlantSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (myPlantsLoading != null) {
      return myPlantsLoading(this);
    }
    return orElse();
  }
}

abstract class MyPlantsLoading implements MyPlantsState {
  const factory MyPlantsLoading() = _$MyPlantsLoading;
}

abstract class $MyPlantsLoadedCopyWith<$Res> {
  factory $MyPlantsLoadedCopyWith(
          MyPlantsLoaded value, $Res Function(MyPlantsLoaded) then) =
      _$MyPlantsLoadedCopyWithImpl<$Res>;
  $Res call({List<MyPlant> myPlants});
}

class _$MyPlantsLoadedCopyWithImpl<$Res>
    extends _$MyPlantsStateCopyWithImpl<$Res>
    implements $MyPlantsLoadedCopyWith<$Res> {
  _$MyPlantsLoadedCopyWithImpl(
      MyPlantsLoaded _value, $Res Function(MyPlantsLoaded) _then)
      : super(_value, (v) => _then(v as MyPlantsLoaded));

  @override
  MyPlantsLoaded get _value => super._value as MyPlantsLoaded;

  @override
  $Res call({
    Object myPlants = freezed,
  }) {
    return _then(MyPlantsLoaded(
      myPlants == freezed ? _value.myPlants : myPlants as List<MyPlant>,
    ));
  }
}

class _$MyPlantsLoaded implements MyPlantsLoaded {
  const _$MyPlantsLoaded([this.myPlants = const <MyPlant>[]])
      : assert(myPlants != null);

  @JsonKey(defaultValue: const <MyPlant>[])
  @override
  final List<MyPlant> myPlants;

  @override
  String toString() {
    return 'MyPlantsState.myPlantsLoaded(myPlants: $myPlants)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is MyPlantsLoaded &&
            (identical(other.myPlants, myPlants) ||
                const DeepCollectionEquality()
                    .equals(other.myPlants, myPlants)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(myPlants);

  @override
  $MyPlantsLoadedCopyWith<MyPlantsLoaded> get copyWith =>
      _$MyPlantsLoadedCopyWithImpl<MyPlantsLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result myPlantsLoading(),
    @required Result myPlantsLoaded(List<MyPlant> myPlants),
    @required Result myPlantsNotLoaded(),
    @required Result myPlantSavedSuccess(MyPlant myPlant),
    @required Result myPlantSavedFailed(MyPlant myPlant),
  }) {
    assert(myPlantsLoading != null);
    assert(myPlantsLoaded != null);
    assert(myPlantsNotLoaded != null);
    assert(myPlantSavedSuccess != null);
    assert(myPlantSavedFailed != null);
    return myPlantsLoaded(myPlants);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result myPlantsLoading(),
    Result myPlantsLoaded(List<MyPlant> myPlants),
    Result myPlantsNotLoaded(),
    Result myPlantSavedSuccess(MyPlant myPlant),
    Result myPlantSavedFailed(MyPlant myPlant),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (myPlantsLoaded != null) {
      return myPlantsLoaded(myPlants);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result myPlantsLoading(MyPlantsLoading value),
    @required Result myPlantsLoaded(MyPlantsLoaded value),
    @required Result myPlantsNotLoaded(MyPlantsNotLoaded value),
    @required Result myPlantSavedSuccess(MyPlantSavedSuccess value),
    @required Result myPlantSavedFailed(MyPlantSavedFailed value),
  }) {
    assert(myPlantsLoading != null);
    assert(myPlantsLoaded != null);
    assert(myPlantsNotLoaded != null);
    assert(myPlantSavedSuccess != null);
    assert(myPlantSavedFailed != null);
    return myPlantsLoaded(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result myPlantsLoading(MyPlantsLoading value),
    Result myPlantsLoaded(MyPlantsLoaded value),
    Result myPlantsNotLoaded(MyPlantsNotLoaded value),
    Result myPlantSavedSuccess(MyPlantSavedSuccess value),
    Result myPlantSavedFailed(MyPlantSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (myPlantsLoaded != null) {
      return myPlantsLoaded(this);
    }
    return orElse();
  }
}

abstract class MyPlantsLoaded implements MyPlantsState {
  const factory MyPlantsLoaded([List<MyPlant> myPlants]) = _$MyPlantsLoaded;

  List<MyPlant> get myPlants;
  $MyPlantsLoadedCopyWith<MyPlantsLoaded> get copyWith;
}

abstract class $MyPlantsNotLoadedCopyWith<$Res> {
  factory $MyPlantsNotLoadedCopyWith(
          MyPlantsNotLoaded value, $Res Function(MyPlantsNotLoaded) then) =
      _$MyPlantsNotLoadedCopyWithImpl<$Res>;
}

class _$MyPlantsNotLoadedCopyWithImpl<$Res>
    extends _$MyPlantsStateCopyWithImpl<$Res>
    implements $MyPlantsNotLoadedCopyWith<$Res> {
  _$MyPlantsNotLoadedCopyWithImpl(
      MyPlantsNotLoaded _value, $Res Function(MyPlantsNotLoaded) _then)
      : super(_value, (v) => _then(v as MyPlantsNotLoaded));

  @override
  MyPlantsNotLoaded get _value => super._value as MyPlantsNotLoaded;
}

class _$MyPlantsNotLoaded implements MyPlantsNotLoaded {
  const _$MyPlantsNotLoaded();

  @override
  String toString() {
    return 'MyPlantsState.myPlantsNotLoaded()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is MyPlantsNotLoaded);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result myPlantsLoading(),
    @required Result myPlantsLoaded(List<MyPlant> myPlants),
    @required Result myPlantsNotLoaded(),
    @required Result myPlantSavedSuccess(MyPlant myPlant),
    @required Result myPlantSavedFailed(MyPlant myPlant),
  }) {
    assert(myPlantsLoading != null);
    assert(myPlantsLoaded != null);
    assert(myPlantsNotLoaded != null);
    assert(myPlantSavedSuccess != null);
    assert(myPlantSavedFailed != null);
    return myPlantsNotLoaded();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result myPlantsLoading(),
    Result myPlantsLoaded(List<MyPlant> myPlants),
    Result myPlantsNotLoaded(),
    Result myPlantSavedSuccess(MyPlant myPlant),
    Result myPlantSavedFailed(MyPlant myPlant),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (myPlantsNotLoaded != null) {
      return myPlantsNotLoaded();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result myPlantsLoading(MyPlantsLoading value),
    @required Result myPlantsLoaded(MyPlantsLoaded value),
    @required Result myPlantsNotLoaded(MyPlantsNotLoaded value),
    @required Result myPlantSavedSuccess(MyPlantSavedSuccess value),
    @required Result myPlantSavedFailed(MyPlantSavedFailed value),
  }) {
    assert(myPlantsLoading != null);
    assert(myPlantsLoaded != null);
    assert(myPlantsNotLoaded != null);
    assert(myPlantSavedSuccess != null);
    assert(myPlantSavedFailed != null);
    return myPlantsNotLoaded(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result myPlantsLoading(MyPlantsLoading value),
    Result myPlantsLoaded(MyPlantsLoaded value),
    Result myPlantsNotLoaded(MyPlantsNotLoaded value),
    Result myPlantSavedSuccess(MyPlantSavedSuccess value),
    Result myPlantSavedFailed(MyPlantSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (myPlantsNotLoaded != null) {
      return myPlantsNotLoaded(this);
    }
    return orElse();
  }
}

abstract class MyPlantsNotLoaded implements MyPlantsState {
  const factory MyPlantsNotLoaded() = _$MyPlantsNotLoaded;
}

abstract class $MyPlantSavedSuccessCopyWith<$Res> {
  factory $MyPlantSavedSuccessCopyWith(
          MyPlantSavedSuccess value, $Res Function(MyPlantSavedSuccess) then) =
      _$MyPlantSavedSuccessCopyWithImpl<$Res>;
  $Res call({MyPlant myPlant});

  $MyPlantCopyWith<$Res> get myPlant;
}

class _$MyPlantSavedSuccessCopyWithImpl<$Res>
    extends _$MyPlantsStateCopyWithImpl<$Res>
    implements $MyPlantSavedSuccessCopyWith<$Res> {
  _$MyPlantSavedSuccessCopyWithImpl(
      MyPlantSavedSuccess _value, $Res Function(MyPlantSavedSuccess) _then)
      : super(_value, (v) => _then(v as MyPlantSavedSuccess));

  @override
  MyPlantSavedSuccess get _value => super._value as MyPlantSavedSuccess;

  @override
  $Res call({
    Object myPlant = freezed,
  }) {
    return _then(MyPlantSavedSuccess(
      myPlant == freezed ? _value.myPlant : myPlant as MyPlant,
    ));
  }

  @override
  $MyPlantCopyWith<$Res> get myPlant {
    if (_value.myPlant == null) {
      return null;
    }
    return $MyPlantCopyWith<$Res>(_value.myPlant, (value) {
      return _then(_value.copyWith(myPlant: value));
    });
  }
}

class _$MyPlantSavedSuccess implements MyPlantSavedSuccess {
  const _$MyPlantSavedSuccess(this.myPlant) : assert(myPlant != null);

  @override
  final MyPlant myPlant;

  @override
  String toString() {
    return 'MyPlantsState.myPlantSavedSuccess(myPlant: $myPlant)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is MyPlantSavedSuccess &&
            (identical(other.myPlant, myPlant) ||
                const DeepCollectionEquality().equals(other.myPlant, myPlant)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(myPlant);

  @override
  $MyPlantSavedSuccessCopyWith<MyPlantSavedSuccess> get copyWith =>
      _$MyPlantSavedSuccessCopyWithImpl<MyPlantSavedSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result myPlantsLoading(),
    @required Result myPlantsLoaded(List<MyPlant> myPlants),
    @required Result myPlantsNotLoaded(),
    @required Result myPlantSavedSuccess(MyPlant myPlant),
    @required Result myPlantSavedFailed(MyPlant myPlant),
  }) {
    assert(myPlantsLoading != null);
    assert(myPlantsLoaded != null);
    assert(myPlantsNotLoaded != null);
    assert(myPlantSavedSuccess != null);
    assert(myPlantSavedFailed != null);
    return myPlantSavedSuccess(myPlant);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result myPlantsLoading(),
    Result myPlantsLoaded(List<MyPlant> myPlants),
    Result myPlantsNotLoaded(),
    Result myPlantSavedSuccess(MyPlant myPlant),
    Result myPlantSavedFailed(MyPlant myPlant),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (myPlantSavedSuccess != null) {
      return myPlantSavedSuccess(myPlant);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result myPlantsLoading(MyPlantsLoading value),
    @required Result myPlantsLoaded(MyPlantsLoaded value),
    @required Result myPlantsNotLoaded(MyPlantsNotLoaded value),
    @required Result myPlantSavedSuccess(MyPlantSavedSuccess value),
    @required Result myPlantSavedFailed(MyPlantSavedFailed value),
  }) {
    assert(myPlantsLoading != null);
    assert(myPlantsLoaded != null);
    assert(myPlantsNotLoaded != null);
    assert(myPlantSavedSuccess != null);
    assert(myPlantSavedFailed != null);
    return myPlantSavedSuccess(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result myPlantsLoading(MyPlantsLoading value),
    Result myPlantsLoaded(MyPlantsLoaded value),
    Result myPlantsNotLoaded(MyPlantsNotLoaded value),
    Result myPlantSavedSuccess(MyPlantSavedSuccess value),
    Result myPlantSavedFailed(MyPlantSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (myPlantSavedSuccess != null) {
      return myPlantSavedSuccess(this);
    }
    return orElse();
  }
}

abstract class MyPlantSavedSuccess implements MyPlantsState {
  const factory MyPlantSavedSuccess(MyPlant myPlant) = _$MyPlantSavedSuccess;

  MyPlant get myPlant;
  $MyPlantSavedSuccessCopyWith<MyPlantSavedSuccess> get copyWith;
}

abstract class $MyPlantSavedFailedCopyWith<$Res> {
  factory $MyPlantSavedFailedCopyWith(
          MyPlantSavedFailed value, $Res Function(MyPlantSavedFailed) then) =
      _$MyPlantSavedFailedCopyWithImpl<$Res>;
  $Res call({MyPlant myPlant});

  $MyPlantCopyWith<$Res> get myPlant;
}

class _$MyPlantSavedFailedCopyWithImpl<$Res>
    extends _$MyPlantsStateCopyWithImpl<$Res>
    implements $MyPlantSavedFailedCopyWith<$Res> {
  _$MyPlantSavedFailedCopyWithImpl(
      MyPlantSavedFailed _value, $Res Function(MyPlantSavedFailed) _then)
      : super(_value, (v) => _then(v as MyPlantSavedFailed));

  @override
  MyPlantSavedFailed get _value => super._value as MyPlantSavedFailed;

  @override
  $Res call({
    Object myPlant = freezed,
  }) {
    return _then(MyPlantSavedFailed(
      myPlant == freezed ? _value.myPlant : myPlant as MyPlant,
    ));
  }

  @override
  $MyPlantCopyWith<$Res> get myPlant {
    if (_value.myPlant == null) {
      return null;
    }
    return $MyPlantCopyWith<$Res>(_value.myPlant, (value) {
      return _then(_value.copyWith(myPlant: value));
    });
  }
}

class _$MyPlantSavedFailed implements MyPlantSavedFailed {
  const _$MyPlantSavedFailed(this.myPlant) : assert(myPlant != null);

  @override
  final MyPlant myPlant;

  @override
  String toString() {
    return 'MyPlantsState.myPlantSavedFailed(myPlant: $myPlant)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is MyPlantSavedFailed &&
            (identical(other.myPlant, myPlant) ||
                const DeepCollectionEquality().equals(other.myPlant, myPlant)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(myPlant);

  @override
  $MyPlantSavedFailedCopyWith<MyPlantSavedFailed> get copyWith =>
      _$MyPlantSavedFailedCopyWithImpl<MyPlantSavedFailed>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result myPlantsLoading(),
    @required Result myPlantsLoaded(List<MyPlant> myPlants),
    @required Result myPlantsNotLoaded(),
    @required Result myPlantSavedSuccess(MyPlant myPlant),
    @required Result myPlantSavedFailed(MyPlant myPlant),
  }) {
    assert(myPlantsLoading != null);
    assert(myPlantsLoaded != null);
    assert(myPlantsNotLoaded != null);
    assert(myPlantSavedSuccess != null);
    assert(myPlantSavedFailed != null);
    return myPlantSavedFailed(myPlant);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result myPlantsLoading(),
    Result myPlantsLoaded(List<MyPlant> myPlants),
    Result myPlantsNotLoaded(),
    Result myPlantSavedSuccess(MyPlant myPlant),
    Result myPlantSavedFailed(MyPlant myPlant),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (myPlantSavedFailed != null) {
      return myPlantSavedFailed(myPlant);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result myPlantsLoading(MyPlantsLoading value),
    @required Result myPlantsLoaded(MyPlantsLoaded value),
    @required Result myPlantsNotLoaded(MyPlantsNotLoaded value),
    @required Result myPlantSavedSuccess(MyPlantSavedSuccess value),
    @required Result myPlantSavedFailed(MyPlantSavedFailed value),
  }) {
    assert(myPlantsLoading != null);
    assert(myPlantsLoaded != null);
    assert(myPlantsNotLoaded != null);
    assert(myPlantSavedSuccess != null);
    assert(myPlantSavedFailed != null);
    return myPlantSavedFailed(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result myPlantsLoading(MyPlantsLoading value),
    Result myPlantsLoaded(MyPlantsLoaded value),
    Result myPlantsNotLoaded(MyPlantsNotLoaded value),
    Result myPlantSavedSuccess(MyPlantSavedSuccess value),
    Result myPlantSavedFailed(MyPlantSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (myPlantSavedFailed != null) {
      return myPlantSavedFailed(this);
    }
    return orElse();
  }
}

abstract class MyPlantSavedFailed implements MyPlantsState {
  const factory MyPlantSavedFailed(MyPlant myPlant) = _$MyPlantSavedFailed;

  MyPlant get myPlant;
  $MyPlantSavedFailedCopyWith<MyPlantSavedFailed> get copyWith;
}
