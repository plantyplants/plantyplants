part of 'bloc.dart';

/// State of the interface for the screens related to [MyPlant].
@freezed
abstract class MyPlantsState with _$MyPlantsState {
  /// the state while our application is fetching myPlants from the repository.
  const factory MyPlantsState.myPlantsLoading() = MyPlantsLoading;

  /// the state of our application after the myPlants have successfully been
  /// loaded.
  const factory MyPlantsState.myPlantsLoaded(
      [@Default(<MyPlant>[]) List<MyPlant> myPlants]) = MyPlantsLoaded;

  /// the state of our application if the myPlants were not successfully loaded.
  const factory MyPlantsState.myPlantsNotLoaded() = MyPlantsNotLoaded;

  /// the state of our application if a [MyPlant] was successfully saved.
  const factory MyPlantsState.myPlantSavedSuccess(MyPlant myPlant) =
      MyPlantSavedSuccess;

  /// the state of our application if a [MyPlant] was not successfully saved.
  const factory MyPlantsState.myPlantSavedFailed(MyPlant myPlant) =
      MyPlantSavedFailed;
}
