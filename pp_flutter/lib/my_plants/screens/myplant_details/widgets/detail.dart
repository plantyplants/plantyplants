import 'package:date_time_format/date_time_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

import '../../../../utils_keys.dart';
import '../../../blocs/recurring_task_watering/bloc.dart';
import '../../../repository/model.dart';
import 'detail_front_picture.dart';
import 'recurring_task_configure.dart';

/// [MyPlantDetail] is the body of [MyPlantDetailScreen].
class MyPlantDetail extends StatelessWidget {
  /// Initialize [MyPlantDetail].
  const MyPlantDetail({
    Key key,
    @required this.myPlant,
    @required this.recurringTaskWateringBlocParent,
  }) : super(key: key);

  /// [MyPlant] object related to this detail.
  final MyPlant myPlant;

  /// RecurringTaskWateringBloc of the parent context
  final RecurringTaskWateringBloc recurringTaskWateringBlocParent;

  /// Builds a widget to show a summary of the watering recurring task
  Widget summaryRecurringWateringTask(
      BuildContext context, RecurringTaskWatering recurringTaskWatering) {
    final dt = recurringTaskWatering.datetime.toLocal();
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.only(left: 0, top: 8, bottom: 8),
      child: Material(
          child: ActionChip(
        shape: StadiumBorder(
            side: BorderSide(
          width: 1.0,
          color: Colors.grey,
        )),
        backgroundColor: Colors.white,
        avatar: CircleAvatar(
          child: recurringTaskWatering.repeat ==
                  repeatChoicesEnum.no.displayLabel
              ? Icon(Icons.alarm,
                  key: Key(keyStringFromEnum(Keys.myPlantRecurringIconAlarm)))
              : Icon(Icons.repeat,
                  key: Key(keyStringFromEnum(Keys.myPlantRecurringIconRepeat))),
        ),
        label: Text(DateTimeFormat.format(dt, format: 'D, M j, H:i')),
        onPressed: () => showDialog(
            context: context,
            builder: (contextDialog) => MyPlantRecurringTaskConfigure(
                  plantId: myPlant.id,
                  authorId: myPlant.authorId,
                  recurringTaskWateringBlocParent:
                      recurringTaskWateringBlocParent,
                )),
      )),
    );
  }

  /// Builds a widget to show an watering recurring task
  Widget emptyRecurringWateringTask(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.only(left: 0, top: 8, bottom: 8),
      child: Material(
          child: ActionChip(
        shape: StadiumBorder(
            side: BorderSide(
          width: 1.0,
          color: Colors.grey,
        )),
        backgroundColor: Colors.white,
        avatar: CircleAvatar(
          child: Icon(Icons.alarm),
        ),
        label: Text('Add a reminder'),
        onPressed: () => showDialog(
            context: context,
            builder: (contextDialog) => MyPlantRecurringTaskConfigure(
                  plantId: myPlant.id,
                  authorId: myPlant.authorId,
                  recurringTaskWateringBlocParent:
                      recurringTaskWateringBlocParent,
                )),
      )),
    );
  }

  Widget _body(
          BuildContext context, RecurringTaskWatering recurringTaskWatering) =>
      Hero(
          tag: myPlant.id,
          child: ListView(
            children: <Widget>[
              if (myPlant.pictureFrontDetailsUrl != null &&
                  myPlant.pictureFrontDetailsUrl != '')
                MyPlantDetailFrontPicture(myPlant: myPlant),
              Padding(
                  padding: EdgeInsets.only(
                    top: 8.0,
                    left: 16.0,
                    right: 16.0,
                    bottom: 16.0,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text('Watering reminder:'),
                      (recurringTaskWatering != null &&
                              recurringTaskWatering.datetime != null &&
                              recurringTaskWatering.datetime != '' &&
                              recurringTaskWatering.repeat != null &&
                              recurringTaskWatering.repeat != '')
                          ? summaryRecurringWateringTask(
                              context, recurringTaskWatering)
                          : emptyRecurringWateringTask(context),
                      Divider(
                        color: Colors.black,
                      ),
                      SelectableText(
                        myPlant.description,
                        key: Key(keyStringFromEnum(
                            Keys.detailsMyPlantItemDescription)),
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                    ],
                  ))
            ],
          ));

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<RecurringTaskWateringBloc, RecurringTaskWateringState>(
        builder: (context, state) {
          if (state is RecurringTaskWateringLoaded) {
            return _body(context, state.recurringTaskWatering);
          }
          if (state is RecurringTaskWateringSavedSuccess) {
            return _body(context, state.recurringTaskWatering);
          }
          return Center(child: CircularProgressIndicator());
        },
      );
}
