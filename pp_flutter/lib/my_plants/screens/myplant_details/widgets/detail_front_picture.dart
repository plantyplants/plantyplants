import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

import '../../../../widgets/myplant_front_picture.dart';
import '../../../repository/model.dart';

/// [Widget] containing both the front details picture and tap gesture for full
/// sized picture if any is defined.
class MyPlantDetailFrontPicture extends StatelessWidget {
  /// Initialize.
  const MyPlantDetailFrontPicture({
    Key key,
    @required this.myPlant,
  }) : super(key: key);

  /// [MyPlant] object associated with the picture.
  final MyPlant myPlant;

  @override
  Widget build(BuildContext context) {
    if (myPlant.pictureFrontFullUrl == '') {
      return MyPlantDetailFrontPictureTop(myPlant);
    }
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => HeroPhotoViewWrapper(
                imageProvider:
                    CachedNetworkImageProvider(myPlant.pictureFrontFullUrl),
                loadingBuilder: (context, event) => Center(
                  child: Container(
                    width: 20.0,
                    height: 20.0,
                    child: CircularProgressIndicator(
                      value: event == null
                          ? 0
                          : event.cumulativeBytesLoaded /
                              event.expectedTotalBytes,
                    ),
                  ),
                ),
                tag: myPlant.id,
              ),
            ));
      },
      child: MyPlantDetailFrontPictureTop(myPlant),
    );
  }
}

/// [Widget] containing the front details picture only.
class MyPlantDetailFrontPictureTop extends StatelessWidget {
  /// [MyPlant] object associated with the picture.
  final MyPlant myPlant;

  /// Initialize.
  const MyPlantDetailFrontPictureTop(this.myPlant);

  @override
  Widget build(BuildContext context) => Container(
      height: MediaQuery.of(context).size.height / 3.2,
      width: MediaQuery.of(context).size.width,
      child: myPlantFrontPicture(myPlant.pictureFrontDetailsUrl));
}

/// [Widget] using the `photo_view` module to be able to zoom on full-sized
/// front picture.
class HeroPhotoViewWrapper extends StatelessWidget {
  /// Initialize.
  const HeroPhotoViewWrapper({
    this.imageProvider,
    this.loadingBuilder,
    this.backgroundDecoration,
    this.minScale,
    this.maxScale,
    this.tag,
  });

  /// Image provider.
  final ImageProvider imageProvider;

  /// Loading builder.
  final LoadingBuilder loadingBuilder;

  /// Decoration.
  final Decoration backgroundDecoration;

  /// Minimum scale in ??
  final dynamic minScale;

  /// Maximum scale in ??
  final dynamic maxScale;

  /// tag for the [Hero] widget
  final String tag;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(
        height: MediaQuery.of(context).size.height,
      ),
      child: PhotoView(
        imageProvider: imageProvider,
        loadingBuilder: loadingBuilder,
        backgroundDecoration: backgroundDecoration,
        minScale: minScale,
        maxScale: maxScale,
        heroAttributes: PhotoViewHeroAttributes(tag: tag),
      ),
    );
  }
}
