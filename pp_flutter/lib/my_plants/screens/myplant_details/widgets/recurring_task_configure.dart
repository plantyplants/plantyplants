import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../../../../routes.dart';
import '../../../../utils_keys.dart';
import '../../../blocs/recurring_task_watering/bloc.dart';
import '../../../form_blocs/recurring_task_configure_form_bloc.dart';
import '../../../repository/interface.dart';

/// Screen to configure a recurring task for a specific my plant.
class MyPlantRecurringTaskConfigure extends StatefulWidget {
  /// ID of the my plant to show details for.
  final String plantId;

  /// Author ID of the plant
  final String authorId;

  /// RecurringTaskWateringBloc of the parent context
  final RecurringTaskWateringBloc recurringTaskWateringBlocParent;

  /// Initialize with the my plant ID.
  MyPlantRecurringTaskConfigure({
    @required this.plantId,
    @required this.authorId,
    @required this.recurringTaskWateringBlocParent,
  }) : super(key: Key(keyStringFromEnum(Keys.myPlantRecurringTaskConfigure)));

  @override
  _MyPlantRecurringTaskConfigureState createState() =>
      _MyPlantRecurringTaskConfigureState();
}

class _MyPlantRecurringTaskConfigureState
    extends State<MyPlantRecurringTaskConfigure> {
  ProgressDialog _pr;

  Widget _dialog(BuildContext context,
          RecurringTaskConfigureFormBloc recurringTaskConfigureFormBloc) =>
      AlertDialog(
        scrollable: true,
        title: Text('Set a watering reminder'),
        content: Container(
          width: double.maxFinite,
          height: 700,
          child: SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            child: Column(
              children: <Widget>[
                DateTimeFieldBlocBuilder(
                  key: Key(keyStringFromEnum(
                      Keys.myPlantRecurringTaskConfigureDateTimeField)),
                  dateTimeFieldBloc: recurringTaskConfigureFormBloc.datetime,
                  canSelectTime: true,
                  format: DateFormat('yyyy-MM-dd hh:mm a'),
                  initialDate: DateTime.now().add(Duration(days: 1)),
                  selectableDayPredicate: (datetime) =>
                      datetime.isAfter(DateTime.now()),
                  firstDate: DateTime.now(),
                  lastDate: DateTime(2100),
                  decoration: InputDecoration(
                    labelText: 'Date & Time',
                    prefixIcon: Icon(Icons.calendar_today),
                  ),
                ),
                DropdownFieldBlocBuilder<String>(
                  key: Key(keyStringFromEnum(
                      Keys.myPlantRecurringTaskConfigureRepeatField)),
                  selectFieldBloc: recurringTaskConfigureFormBloc.repeat,
                  showEmptyItem: false,
                  decoration: InputDecoration(
                    labelText: 'Repeat',
                    prefixIcon: Icon(Icons.repeat,
                        key: Key(keyStringFromEnum(
                            Keys.myPlantRecurringIconRepeatForm))),
                  ),
                  itemBuilder: (context, value) => value,
                ),
                TextFieldBlocBuilder(
                  key: Key(keyStringFromEnum(
                      Keys.myPlantRecurringTaskConfigureDayEveryField)),
                  textFieldBloc: recurringTaskConfigureFormBloc.repeatDayEvery,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    prefixText: 'Every',
                    suffixText: 'day(s)',
                  ),
                  textAlign: TextAlign.center,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                ),
                TextFieldBlocBuilder(
                  key: Key(keyStringFromEnum(
                      Keys.myPlantRecurringTaskConfigureWeekEveryField)),
                  textFieldBloc: recurringTaskConfigureFormBloc.repeatWeekEvery,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    prefixText: 'Every',
                    suffixText: 'week(s)',
                  ),
                  textAlign: TextAlign.center,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                ),
                CheckboxGroupFieldBlocBuilder<String>(
                  key: Key(keyStringFromEnum(
                      Keys.myPlantRecurringTaskConfigureWeekDayField)),
                  multiSelectFieldBloc:
                      recurringTaskConfigureFormBloc.repeatWeekDay,
                  itemBuilder: (context, item) => item,
                  decoration: InputDecoration(
                    labelText: 'Day(s) of the week',
                    prefixIcon: Icon(Icons.calendar_view_day),
                  ),
                ),
                TextFieldBlocBuilder(
                  key: Key(keyStringFromEnum(
                      Keys.myPlantRecurringTaskConfigureMonthEveryField)),
                  textFieldBloc:
                      recurringTaskConfigureFormBloc.repeatMonthEvery,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    prefixText: 'Every',
                    suffixText: 'month(s)',
                  ),
                  textAlign: TextAlign.center,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                ),
                RadioButtonGroupFieldBlocBuilder<String>(
                  key: Key(keyStringFromEnum(
                      Keys.myPlantRecurringTaskConfigureMonthOptionField)),
                  selectFieldBloc:
                      recurringTaskConfigureFormBloc.repeatMonthOption,
                  itemBuilder: (context, item) => item,
                ),
                TextFieldBlocBuilder(
                  key: Key(keyStringFromEnum(
                      Keys.myPlantRecurringTaskConfigureYearEveryField)),
                  textFieldBloc: recurringTaskConfigureFormBloc.repeatYearEvery,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    prefixText: 'Every',
                    suffixText: 'year(s)',
                  ),
                  textAlign: TextAlign.center,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                ),
              ],
            ),
          ),
        ),
        actions: [
          if (recurringTaskConfigureFormBloc.initialDatetime != null &&
              recurringTaskConfigureFormBloc.initialDatetime != '')
            FlatButton(
              key: Key(
                  keyStringFromEnum(Keys.myPlantRecurringTaskConfigureDelete)),
              child: Text('Delete'),
              onPressed: () {
                widget.recurringTaskWateringBlocParent
                    .add(DeleteRecurringTaskWatering());
                widget.recurringTaskWateringBlocParent
                    .add(LoadRecurringTaskWatering());
                Navigator.of(context).pop();
              },
            ),
          FlatButton(
            key: Key(
                keyStringFromEnum(Keys.myPlantRecurringTaskConfigureCancel)),
            child: Text('Cancel'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          RaisedButton(
            key: Key(keyStringFromEnum(Keys.myPlantRecurringTaskConfigureSave)),
            child: Text('Save'),
            onPressed: recurringTaskConfigureFormBloc.submit,
            color: Colors.blue,
          )
        ],
        elevation: 24.0,
      );

  @override
  Widget build(BuildContext contextTop) => BlocProvider(
      create: (context) => RecurringTaskWateringBloc(
          myPlantsRepository:
              RepositoryProvider.of<MyPlantsRepository>(context),
          authorId: widget.authorId,
          plantId: widget.plantId),
      child: BlocProvider(
        create: (context) => RecurringTaskConfigureFormBloc(
            myPlantsRepository:
                RepositoryProvider.of<MyPlantsRepository>(context),
            recurringTaskWateringBloc:
                BlocProvider.of<RecurringTaskWateringBloc>(context),
            plantId: widget.plantId),
        child:
            BlocBuilder<RecurringTaskWateringBloc, RecurringTaskWateringState>(
          builder: (contextWateringBloc, stateWatering) => BlocBuilder<
                  RecurringTaskConfigureFormBloc, FormBlocState>(
              condition: (previous, current) =>
                  previous.runtimeType != current.runtimeType ||
                  previous is FormBlocLoading && current is FormBlocLoading,
              builder: (context, state) {
                _pr = ProgressDialog(
                  contextTop,
                  type: ProgressDialogType.Normal,
                  isDismissible: false,
                );
                final recurringTaskConfigureFormBloc =
                    BlocProvider.of<RecurringTaskConfigureFormBloc>(context);
                if (state is FormBlocLoading) {
                  return Center(child: CircularProgressIndicator());
                }
                return FormBlocListener<RecurringTaskConfigureFormBloc, String,
                    String>(
                  onSubmitting: (contextListener, state) async {
                    _pr.style(message: 'Saving reminder...');
                    await _pr.show();
                  },
                  onSuccess: (contextListener, state) async {
                    if (_pr.isShowing()) {
                      // FIXME: Looking up a deactivated widget's ancestor is
                      //  unsafe.
                      await _pr.hide();
                    }
                    widget.recurringTaskWateringBlocParent
                        .add(LoadRecurringTaskWatering());
                    Navigator.of(contextListener)
                        .popUntil(ModalRoute.withName(Routes.detailsMyPlant));
                  },
                  // onFailure: , // TODO: add logging
                  child: _dialog(context, recurringTaskConfigureFormBloc),
                );
              }),
        ),
      ));
}
