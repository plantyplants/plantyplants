import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../../../users/blocs/authentication/bloc.dart';
import '../../../utils_keys.dart';
import '../../blocs/my_plants/bloc.dart';
import '../../form_blocs/add_edit_form_bloc.dart';
import '../../repository/model.dart';
import 'widgets/detail_front_picture.dart';

typedef OnSaveCallback = Function(String name, String description,
    {List<FrontPictureFormat> imageFiles});

/// Screen to add and edit my plants.
class MyPlantAddEditScreen extends StatefulWidget {
  /// True if we're in editing mode (as opposed to adding a new plant).
  final bool isEditing;

  /// Callback to be executed to save the my plant.
  final OnSaveCallback onSave;

  /// Message to print when saving the my plant.
  final String savingMessage;

  /// Pop route after validating save.
  final String popRoute;

  /// my plant to edit.
  final MyPlant myPlant;

  /// Initialize. with callbacks, plant and state of the form.
  MyPlantAddEditScreen({
    Key key,
    @required this.onSave,
    @required this.popRoute,
    @required this.savingMessage,
    @required this.isEditing,
    this.myPlant,
  }) : super(key: key ?? Key(keyStringFromEnum(Keys.addMyPlantScreen)));

  @override
  _MyPlantAddEditScreenState createState() => _MyPlantAddEditScreenState();
}

class _MyPlantAddEditScreenState extends State<MyPlantAddEditScreen> {
  static final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  ProgressDialog _pr;

  AddEditFormBloc addEditFormBloc;

  ///
  /// Image
  ///

  File _imageFile;
  String _retrieveDataError;
  dynamic _pickImageError;
  final _picker = ImagePicker();

  bool get isEditing => widget.isEditing;

  Future<void> _retrieveLostData() async {
    final response = await ImagePicker.retrieveLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        _imageFile = response.file;
      });
    } else {
      _retrieveDataError = response.exception.code;
    }
  }

  void _onImageButtonPressed(ImageSource source, {BuildContext context}) async {
    try {
      final pickedFile = await _picker.getImage(source: source);
      if (pickedFile != null) {
        _imageFile = File(pickedFile.path);
      }
      setState(() {});
      // ignore: avoid_catches_without_on_clauses
    } catch (e) {
      _pickImageError = e;
      // XXX: TODO: Add logging here
      print(e);
    }
  }

  Widget _previewImage() {
    final retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    if (_pickImageError != null) {
      return Text(
        'Pick image error: $_pickImageError',
        textAlign: TextAlign.center,
      );
    }
    if (_imageFile != null) {
      return Container(
        key: Key(keyStringFromEnum(Keys.previewFrontPicture)),
        child: Image.file(_imageFile),
        height: MediaQuery.of(context).size.height / 3.2,
        width: MediaQuery.of(context).size.width,
      );
    }
    if (widget.myPlant != null &&
        widget.myPlant.pictureFrontDetailsUrl != null) {
      return MyPlantDetailFrontPictureTop(widget.myPlant);
    }
    return Container();
  }

  Widget _previewImageWithErrorHandling() => Platform.isAndroid
      ? FutureBuilder<void>(
          future: _retrieveLostData(),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
              case ConnectionState.done:
                return _previewImage();
              default:
                if (snapshot.hasError) {
                  return Text(
                    'Pick image error: ${snapshot.error}}',
                    textAlign: TextAlign.center,
                  );
                }
                return _previewImage();
            }
          })
      : _previewImage();

  Text _getRetrieveErrorWidget() {
    if (_retrieveDataError != null) {
      final result = Text(_retrieveDataError);
      _retrieveDataError = null;
      return result;
    }
    return null;
  }

  @override
  Widget build(BuildContext contextTop) {
    _pr = ProgressDialog(
      contextTop,
      type: ProgressDialogType.Normal,
      isDismissible: false,
    );
    return BlocProvider(
      create: (context) => AddEditFormBloc(
        initialName: widget.myPlant != null ? widget.myPlant.name : '',
        initialDescription:
            widget.myPlant != null ? widget.myPlant.description : '',
        onSave: widget.onSave,
      ),
      child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          builder: (contextBlocBuilder, authState) {
        addEditFormBloc = contextBlocBuilder.bloc<AddEditFormBloc>();
        if (authState is Authenticated) {
          return Scaffold(
              appBar: AppBar(
                title: Text(
                  isEditing ? 'Edit Plant' : 'Add Plant',
                ),
              ),
              body: FormBlocListener<AddEditFormBloc, String, String>(
                  onSubmitting: (contextListener, state) async {
                    _pr.style(message: widget.savingMessage);
                    await _pr.show();
                  },
                  onSuccess: (contextListener, state) async {
                    if (_pr.isShowing()) {
                      await _pr.hide();
                    }
                    contextListener.bloc<MyPlantsBloc>().add(LoadMyPlants());
                  },
                  onFailure: (contextListener, state) async {
                    if (_pr.isShowing()) {
                      await _pr.hide();
                    }
                    // TODO: Add logging

                    Scaffold.of(contextListener).showSnackBar(
                        SnackBar(content: Text(state.failureResponse)));
                  },
                  child: BlocListener<MyPlantsBloc, MyPlantsState>(
                      listener: (contextListener, myPlantState) {
                        if (myPlantState is MyPlantSavedFailed) {
                          if (_pr.isShowing()) {
                            _pr.hide();
                          }
                          // TODO: Add logging
                          final snackBar = SnackBar(
                            content:
                                Text('Error while saving the plant! Please try'
                                    'again.'),
                          );

                          // Find the Scaffold in the widget tree and use
                          // it to show a SnackBar.
                          // Scaffold.of(contextTop).showSnackBar(snackBar);
                          Scaffold.of(contextListener).showSnackBar(snackBar);
                        }
                        if (myPlantState is MyPlantSavedSuccess) {
                          if (_pr.isShowing()) {
                            _pr.hide();
                          }
                          contextListener
                              .bloc<MyPlantsBloc>()
                              .add(LoadMyPlants());
                          // https://stackoverflow.com/questions/51567885/flutter-bloc-pattern-how-to-navigate-to-another-screen-after-stream-event
//                SchedulerBinding.instance.addPostFrameCallback((_) {
                          Navigator.of(contextTop)
                              .popUntil(ModalRoute.withName(widget.popRoute));
//                  Navigator.of(context).pop();
//                });
                        }
                      },
                      child: Padding(
                        padding: EdgeInsets.all(16.0),
                        child: Form(
                          key: formKey,
                          autovalidateMode: AutovalidateMode.always,
                          onWillPop: () => Future<bool>(() => true),
                          child: ListView(
                            children: <Widget>[
                              if (_imageFile != null ||
                                  (widget.myPlant != null &&
                                      widget.myPlant.pictureFrontDetailsUrl !=
                                          ''))
                                Padding(
                                    padding: EdgeInsets.only(
                                      top: 8.0,
                                      bottom: 8.0,
                                    ),
                                    child: _previewImageWithErrorHandling()),
                              TextFieldBlocBuilder(
                                textFieldBloc: addEditFormBloc.name,
                                key: ValueKey<String>(keyStringFromEnum(
                                    Keys.formAddEditNameField)),
                                style: Theme.of(contextBlocBuilder)
                                    .textTheme
                                    .headline5,
                                decoration: InputDecoration(
                                  hintText: 'Plant name',
                                ),
                              ),
                              TextFieldBlocBuilder(
                                textFieldBloc: addEditFormBloc.description,
                                key: ValueKey<String>(keyStringFromEnum(
                                    Keys.formAddEditDescriptionField)),
                                maxLines: 10,
                                style: Theme.of(contextBlocBuilder)
                                    .textTheme
                                    .subtitle1,
                                decoration: InputDecoration(
                                  hintText: 'Description',
                                ),
                              )
                            ],
                          ),
                        ),
                      ))),
              floatingActionButton: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FloatingActionButton(
                    heroTag: null,
                    key: Key(keyStringFromEnum(Keys.changePictureGallery)),
                    onPressed: () {
                      _onImageButtonPressed(ImageSource.gallery,
                          context: contextBlocBuilder);
                    },
                    tooltip: 'Pick picture from gallery',
                    backgroundColor: Colors.pink,
                    child: const Icon(Icons.photo_library),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: FloatingActionButton(
                      heroTag: null,
                      key: Key(keyStringFromEnum(Keys.changePictureCamera)),
                      onPressed: () {
                        _onImageButtonPressed(ImageSource.camera,
                            context: contextBlocBuilder);
                      },
                      tooltip: 'Take a picture',
                      backgroundColor: Colors.pink,
                      child: const Icon(Icons.camera_alt),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: FloatingActionButton(
                      heroTag: null,
                      key: isEditing
                          ? Key(keyStringFromEnum(Keys.saveMyPlantFab))
                          : Key(keyStringFromEnum(Keys.saveNewMyPlant)),
                      tooltip: isEditing ? 'Save changes' : 'Add Plant',
                      child: Icon(Icons.check),
                      onPressed: () {
                        addEditFormBloc.imageFile = _imageFile;
                        return addEditFormBloc.submit();
                      },
                    ),
                  ),
                ],
              ));
        }
        // TODO: add some logging here
        return Center(
          child: Text('Could not authenticate with Firebase'),
        );
      }),
    );
  }
}
