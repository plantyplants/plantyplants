import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../routes.dart';
import '../../../utils_keys.dart';
import '../../../widgets/loading.dart';
import '../../blocs/my_plants/bloc.dart';
import '../../blocs/recurring_task_watering/bloc.dart';
import '../../repository/interface.dart';
import 'add_edit_screen.dart';
import 'widgets/detail.dart';
import 'widgets/recurring_task_configure.dart';

/// Screen with details about a specific my plant.
class MyPlantDetailScreen extends StatelessWidget {
  /// ID of the my plant to show details for.
  final String plantId;

  /// Author ID
  final String authorId;

  /// Initialize with the my plant ID and author ID.
  MyPlantDetailScreen({
    @required this.plantId,
    @required this.authorId,
  }) : super(key: Key(keyStringFromEnum(Keys.myPlantDetailsScreen)));

  @override
  Widget build(BuildContext contextTop) => BlocProvider<
          RecurringTaskWateringBloc>(
      create: (context) => RecurringTaskWateringBloc(
          authorId: authorId,
          plantId: plantId,
          myPlantsRepository:
              RepositoryProvider.of<MyPlantsRepository>(context))
        ..add(LoadRecurringTaskWatering()),
      child: BlocBuilder<MyPlantsBloc, MyPlantsState>(
        builder: (contextBlocBuilder, state) {
          var myPlant;
          if (state is MyPlantsLoaded) {
            myPlant = state.myPlants.firstWhere(
                (myPlant) => myPlant.id == plantId,
                orElse: () => null);
          }

          if (myPlant == null) {
            // TODO: add logging
            return CircularProgressIndicator();
          }

          return Scaffold(
            appBar: AppBar(
              title: SelectableText(myPlant.name),
              actions: <Widget>[
                IconButton(
                  key: Key(keyStringFromEnum(Keys.deleteMyPlantButton)),
                  tooltip: 'Delete Plant',
                  icon: Icon(Icons.delete),
                  onPressed: () {
                    contextBlocBuilder
                        .bloc<MyPlantsBloc>()
                        .add(DeleteMyPlant(myPlant));
                    contextBlocBuilder.bloc<MyPlantsBloc>().add(LoadMyPlants());
                    Navigator.pop(contextBlocBuilder, myPlant);
                  },
                ),
                IconButton(
                  key: Key(keyStringFromEnum(
                      Keys.actionIconActivitiesNotifications)),
                  // TODO: notifications_active if a recurring task is set.
                  icon: Icon(Icons.notifications),
                  onPressed: () {
                    showDialog(
                        context: contextBlocBuilder,
                        builder: (contextDialog) =>
                            MyPlantRecurringTaskConfigure(
                              authorId: myPlant.authorId,
                              plantId: myPlant.id,
                              recurringTaskWateringBlocParent:
                                  contextBlocBuilder
                                      .bloc<RecurringTaskWateringBloc>(),
                            ));
                  },
                )
              ],
            ),
            body: myPlant == null
                ? LoadingSpinner()
                : MyPlantDetail(
                    myPlant: myPlant,
                    recurringTaskWateringBlocParent:
                        contextBlocBuilder.bloc<RecurringTaskWateringBloc>(),
                  ),
            floatingActionButton: FloatingActionButton(
              tooltip: 'Edit Plant',
              child: Icon(Icons.edit),
              key: Key(keyStringFromEnum(Keys.editMyPlantFab)),
              onPressed: myPlant == null
                  ? null
                  : () {
                      Navigator.of(contextBlocBuilder).push(
                        MaterialPageRoute<Object>(
                          builder: (context) => MyPlantAddEditScreen(
                            onSave: (name, description, {imageFiles}) {
                              context.bloc<MyPlantsBloc>().add(UpdateMyPlant(
                                    myPlant.copyWith(
                                      name: name,
                                      description: description,
                                    ),
                                    frontPictureFormats: imageFiles,
                                  ));
                            },
                            savingMessage: "Updating plant...",
                            popRoute: Routes.detailsMyPlant,
                            isEditing: true,
                            myPlant: myPlant,
                          ),
                        ),
                      );
                    },
            ),
          );
        },
      ));
}
