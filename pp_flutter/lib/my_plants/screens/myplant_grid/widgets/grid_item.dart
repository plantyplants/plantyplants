import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../../../utils_keys.dart';
import '../../../../widgets/myplant_front_picture.dart';
import '../../../repository/model.dart';

/// Signature of a callback function when tapping the plant banner.
typedef BannerTapCallback = void Function(MyPlant plant);

class _GridNameText extends StatelessWidget {
  const _GridNameText(this.text);

  final String text;

  @override
  Widget build(BuildContext context) => FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Text(text),
      );
}

/// Element (plant) in the plants grid.
class GridMyPlantItem extends StatelessWidget {
  /// Plant.
  final MyPlant myPlant;

  /// Plant banner callback.
  final BannerTapCallback
      onBannerTap; // User taps on the photo's header or footer.

  /// Initialize with banner callback and plant.
  GridMyPlantItem({
    @required this.onBannerTap,
    @required this.myPlant,
  });

  @override
  Widget build(BuildContext context) {
    final Widget image = GestureDetector(
        onTap: () {
          onBannerTap(myPlant);
        },
        child: Hero(
            key: Key(
                keyStringFromStringFunc(myPlant.id, KeysFunc.pictureFrontGrid)),
            tag: myPlant.id,
            child: myPlantFrontPicture(myPlant.pictureFrontGridUrl)));

    return GridTile(
      footer: GestureDetector(
        key: Key(keyStringFromStringFunc(
            myPlant.id, KeysFunc.myPlantItemGestureDetector)),
        onTap: () {
          onBannerTap(myPlant);
        },
        child: GridTileBar(
          backgroundColor: Colors.black45,
          title: _GridNameText(myPlant.name),
        ),
      ),
      child: image,
    );
  }
}
