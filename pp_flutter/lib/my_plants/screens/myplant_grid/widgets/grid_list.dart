import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../routes.dart';
import '../../../../utils_keys.dart';
import '../../../../widgets/loading.dart';
import '../../../blocs/my_plants/bloc.dart';
import '../../../repository/model.dart';
import '../../myplant_details/detail_screen.dart';
import 'grid_item.dart';

/// Grid list widget of representing all the my plants for a user.
class MyPlantsGridList extends StatelessWidget {
  /// Initialize.
  MyPlantsGridList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;

    return BlocBuilder<MyPlantsBloc, MyPlantsState>(
        builder: (context, myPlantsState) {
      if (myPlantsState is MyPlantsLoading ||
          myPlantsState is MyPlantsNotLoaded) {
        return LoadingSpinner();
      } else if (myPlantsState is MyPlantsLoaded) {
        final myPlants = myPlantsState.myPlants;
        return Column(
          key: Key(keyStringFromEnum(Keys.myPlantsList)),
          children: <Widget>[
            Expanded(
              child: SafeArea(
                top: false,
                bottom: false,
                child: GridView.count(
                  crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3,
                  mainAxisSpacing: 4.0,
                  crossAxisSpacing: 4.0,
                  padding: const EdgeInsets.all(4.0),
                  childAspectRatio:
                      (orientation == Orientation.portrait) ? 1.0 : 1.3,
                  children: myPlants == null
                      ? <Widget>[LoadingSpinner()]
                      : myPlants
                          .map((myPlant) => GridMyPlantItem(
                                myPlant: myPlant,
                                onBannerTap: (myPlantBanner) {
                                  Navigator.of(context)
                                      .push(
                                    MaterialPageRoute<MyPlant>(
                                      settings: RouteSettings(
                                          name: Routes.detailsMyPlant),
                                      builder: (_) => MyPlantDetailScreen(
                                        plantId: myPlantBanner.id,
                                        authorId: myPlant.authorId,
                                      ),
                                    ),
                                  )
                                      .then((dynamic plant) {
                                    if (plant is MyPlant) {
                                      _showUndoSnackbar(context, plant);
                                    }
                                  });
                                },
                              ))
                          .toList(),
                ),
              ),
            ),
          ],
        );
      }
      return LoadingSpinner();
    });
  }

  void _showUndoSnackbar(BuildContext context, MyPlant myPlant) {
    final myPlantsBloc = context.bloc<MyPlantsBloc>();

    final snackBar = SnackBar(
      key: Key(keyStringFromEnum(Keys.snackbar)),
      duration: Duration(seconds: 2),
      backgroundColor: Theme.of(context).backgroundColor,
      content: Text(
        'Deleted \'${myPlant.name}\'',
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      action: SnackBarAction(
        key: Key(keyStringFromStringFunc(myPlant.id, KeysFunc.snackbarAction)),
        label: 'Undo',
        onPressed: () {
          myPlantsBloc.add(AddMyPlant(myPlant));
        },
      ),
    );

    Scaffold.of(context).showSnackBar(snackBar);
  }
}
