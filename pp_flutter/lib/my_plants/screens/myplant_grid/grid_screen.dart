import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/tab/bloc.dart';
import '../../../routes.dart';
import '../../../utils_keys.dart';
import '../../../widgets/tab_selector.dart';
import 'widgets/grid_list.dart';

/// Screen to see all of my plants for which the user is the author.
class MyPlantListScreen extends StatelessWidget {
  /// Initialize.
  MyPlantListScreen()
      : super(key: Key(keyStringFromEnum(Keys.myPlantsListScreen)));

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: Text('Planty Plants')),
        body: MyPlantsGridList(),
        floatingActionButton: FloatingActionButton(
          key: Key(keyStringFromEnum(Keys.addMyPlantFab)),
          onPressed: () {
            Navigator.pushNamed(context, Routes.addMyPlant);
          },
          child: Icon(Icons.add),
          tooltip: 'Add Plant',
        ),
        bottomNavigationBar: TabSelector(
          activeTab: AppTab.myPlants,
          onTabSelected: (tab) =>
              BlocProvider.of<TabBloc>(context).add(TabUpdated(tab: tab)),
        ),
      );
}
