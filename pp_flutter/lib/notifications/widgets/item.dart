import 'package:date_time_format/date_time_format.dart';
import 'package:flutter/material.dart' hide Notification;
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../my_plants/blocs/my_plants/bloc.dart';
import '../../my_plants/repository/model.dart';
import '../../notifications/repository/model.dart';
import '../../utils_keys.dart';
import '../../widgets/myplant_front_picture.dart';

/// Signature of a callback function when tapping the notification.
typedef TapCallback = void Function(Notification notification);

/// Item in the list of notifications
class NotificationsItem extends StatelessWidget {
  /// Notification.
  final Notification notification;

  /// Notification tap callback.
  final TapCallback onTap; // User taps

  /// Initialize with tap callback and notification.
  NotificationsItem({
    @required this.notification,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<MyPlantsBloc, MyPlantsState>(
          builder: (context, myPlantsState) {
        MyPlant myPlant;
        if (myPlantsState is MyPlantsLoaded) {
          myPlant = myPlantsState.myPlants.firstWhere(
              (myPlant) => myPlant.id == notification.myPlantId,
              orElse: () => null);

          if (myPlant != null) {
            var dtNow = DateTime.now();

            return ListTile(
              key: Key(keyStringFromStringFunc(
                  notification.id, KeysFunc.notificationItem)),
              leading: ExcludeSemantics(
                child: (myPlant.pictureFrontGridUrl != null &&
                        myPlant.pictureFrontGridUrl != '')
                    ? myPlantFrontPicture(myPlant.pictureFrontGridUrl)
                    : Container(width: 50),
              ),
              title: Text(
                'Time to water ${myPlant.name}!',
                key: Key(keyStringFromStringFunc(
                    notification.id, KeysFunc.notificationItemTitle)),
              ),
              subtitle: Text(
                  '${DateTimeFormat.relative(dtNow.subtract(dtNow.difference(notification.sentTime)))} ago'), // ignore: lines_longer_than_80_chars
              onTap: () {
                onTap(notification);
              },
            );
          }
          return Container();
        }

        return CircularProgressIndicator();
      });
}
