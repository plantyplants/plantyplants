import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../my_plants/repository/model.dart';
import '../../my_plants/screens/myplant_details/detail_screen.dart';
import '../../routes.dart';
import '../../utils_keys.dart';
import '../../widgets/loading.dart';
import '../blocs/notification/bloc.dart';
import 'item.dart';

/// List of notifications
class NotificationsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) =>
      BlocBuilder<NotificationBloc, NotificationState>(
          builder: (context, notificationsState) {
        if (notificationsState is NotificationsLoading ||
            notificationsState is NotificationsNotLoaded) {
          return LoadingSpinner();
        } else if (notificationsState is NotificationsLoaded) {
          final notifications = notificationsState.notifications;
          // final dtNow = DateTime.now();
          return Scrollbar(
            child: ListView(
                key: Key(keyStringFromEnum(Keys.notificationsList)),
                children: notifications == null
                    ? [LoadingSpinner()]
                    : notifications
                        // .where((notification) =>
                        //     notification.sentTime != null &&
                        //     dtNow.difference(notification.sentTime).inDays <= 7)
                        .where((notification) => notification.myPlantId != null)
                        .map((notification) => NotificationsItem(
                            notification: notification,
                            onTap: (notification) {
                              Navigator.of(context).push(
                                MaterialPageRoute<MyPlant>(
                                  settings: RouteSettings(
                                      name: Routes.detailsMyPlant),
                                  builder: (_) => MyPlantDetailScreen(
                                    plantId: notification.myPlantId,
                                    authorId: notification.receiverUserId,
                                  ),
                                ),
                              );
                            }))
                        .toList()),
          );
        }
        return LoadingSpinner();
      });
}
