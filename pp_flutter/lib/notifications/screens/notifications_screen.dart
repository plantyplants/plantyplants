import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/tab/bloc.dart';
import '../../utils_keys.dart';
import '../../widgets/tab_selector.dart';
import '../widgets/list.dart';

/// Screen to see the list of user's notifications
class NotificationsScreen extends StatelessWidget {
  /// Initialize.
  NotificationsScreen()
      : super(key: Key(keyStringFromEnum(Keys.notificationsScreen)));

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: Text('Planty Plants')),
        body: NotificationsList(),
        bottomNavigationBar: TabSelector(
          activeTab: AppTab.notifications,
          onTabSelected: (tab) =>
              BlocProvider.of<TabBloc>(context).add(TabUpdated(tab: tab)),
        ),
      );
}
