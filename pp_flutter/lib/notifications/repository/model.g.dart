// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Notification _$_$_NotificationFromJson(Map<String, dynamic> json) {
  return _$_Notification(
    id: json['id'] as String,
    receiverUserId: json['receiverUserId'] as String,
    title: json['title'] as String,
    body: json['body'] as String,
    myPlantId: json['myPlantId'] as String,
    openedApplication: json['openedApplication'] as bool,
    messageId: json['messageId'] as String,
    senderId: json['senderId'] as String,
    category: json['category'] as String,
    collapseKey: json['collapseKey'] as String,
    contentAvailable: json['contentAvailable'] as bool,
    data: (json['data'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as String),
    ),
    from: json['from'] as String,
    sentTime: json['sentTime'] == null
        ? null
        : DateTime.parse(json['sentTime'] as String),
    threadId: json['threadId'] as String,
    ttl: json['ttl'] as int,
    notificationAndroidChannelId:
        json['notificationAndroidChannelId'] as String,
    notificationAndroidClickAction:
        json['notificationAndroidClickAction'] as String,
    notificationAndroidColor: json['notificationAndroidColor'] as String,
    notificationAndroidCount: json['notificationAndroidCount'] as int,
    notificationAndroidImageUrl: json['notificationAndroidImageUrl'] as String,
    notificationAndroidLink: json['notificationAndroidLink'] as String,
    notificationAndroidPriority: json['notificationAndroidPriority'] as String,
    notificationAndroidSmallIcon:
        json['notificationAndroidSmallIcon'] as String,
    notificationAndroidSound: json['notificationAndroidSound'] as String,
    notificationAndroidTicker: json['notificationAndroidTicker'] as String,
    notificationAndroidVisibility:
        json['notificationAndroidVisibility'] as String,
  );
}

Map<String, dynamic> _$_$_NotificationToJson(_$_Notification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'receiverUserId': instance.receiverUserId,
      'title': instance.title,
      'body': instance.body,
      'myPlantId': instance.myPlantId,
      'openedApplication': instance.openedApplication,
      'messageId': instance.messageId,
      'senderId': instance.senderId,
      'category': instance.category,
      'collapseKey': instance.collapseKey,
      'contentAvailable': instance.contentAvailable,
      'data': instance.data,
      'from': instance.from,
      'sentTime': instance.sentTime?.toIso8601String(),
      'threadId': instance.threadId,
      'ttl': instance.ttl,
      'notificationAndroidChannelId': instance.notificationAndroidChannelId,
      'notificationAndroidClickAction': instance.notificationAndroidClickAction,
      'notificationAndroidColor': instance.notificationAndroidColor,
      'notificationAndroidCount': instance.notificationAndroidCount,
      'notificationAndroidImageUrl': instance.notificationAndroidImageUrl,
      'notificationAndroidLink': instance.notificationAndroidLink,
      'notificationAndroidPriority': instance.notificationAndroidPriority,
      'notificationAndroidSmallIcon': instance.notificationAndroidSmallIcon,
      'notificationAndroidSound': instance.notificationAndroidSound,
      'notificationAndroidTicker': instance.notificationAndroidTicker,
      'notificationAndroidVisibility': instance.notificationAndroidVisibility,
    };
