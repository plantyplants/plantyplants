import 'model.dart';

/// [NotificationsRepository] is a [Repository] for [Notification] objects
abstract class NotificationsRepository {
  /// Add a new [Notification] to the repository, takes the new [Notification].
  Future<void> addNotification(Notification notification);

  /// Delete a [Notification] from the repository.
  Future<void> deleteNotification(Notification notification);

  /// Returns a list of all [Notification] from the repository, as a stream of
  /// lists.
  Stream<List<Notification>> notifications(String receiverUserId);

  /// Get a [Notification] from the repository, as a future, for the specific
  /// [Notification] ID and receiver [User] ID.
  Future<Notification> notification(
      String notificationId, String receiverUserId);

  /// Update a [Notification] in the repository, takes the updated
  /// [Notification].
  /// It updates attributes for the [Notification] with that ID.
  Future<void> updateNotification(Notification notification);
}
