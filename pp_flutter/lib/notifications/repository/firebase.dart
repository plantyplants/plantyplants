import 'package:cloud_firestore/cloud_firestore.dart';

import 'entities.dart';
import 'interface.dart';
import 'model.dart';

/// [FirebaseNotificationsRepository] is a [NotificationsRepository] to persist
/// the [Notification] objects.
class FirebaseNotificationsRepository extends NotificationsRepository {
  /// Firestore path prefix for [Notification].
  static const String firestorePathPrefix = 'notifications';

  /// Get the [CollectionReference] for all [Notification] of the repository.
  CollectionReference get notificationsCollection =>
      FirebaseFirestore.instance.collection(firestorePathPrefix);

  /// Add a new [Notification] to the repository.
  @override
  Future<void> addNotification(Notification notification) async =>
      await notificationsCollection
          .add(notification.toEntity().toMapWithoutId());

  /// Update a [Notification] in the repository, takes the updated Notification.
  /// It updates attributes for the [Notification] with that ID.
  @override
  Future<void> updateNotification(Notification update) async =>
      notificationsCollection
          .doc(update.id)
          .update(update.toEntity().toMapWithoutId());

  /// Delete a [Notification] from the repository.
  @override
  Future<void> deleteNotification(Notification notification) async =>
      notificationsCollection.doc(notification.id).delete();

  Notification _docToNotification(DocumentSnapshot doc) =>
      Notification.fromEntity(NotificationEntity.fromDocument(doc));

  /// Returns a list of all [Notification] from the repository, as a stream of
  /// lists.
  @override
  Stream<List<Notification>> notifications(String receiverUserId) =>
      notificationsCollection
          .where('receiverUserId', isEqualTo: receiverUserId)
          .snapshots()
          .map((snapshot) => snapshot.docs.map(_docToNotification).toList());

  /// Get a [Notification] from the repository, as a future, for the specific
  /// [Notification] ID and receiver [User] ID.
  @override
  Future<Notification> notification(
          String notificationId, String receiverUserId) async =>
      _docToNotification(
          await notificationsCollection.doc(notificationId).get());
}
