// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
Notification _$NotificationFromJson(Map<String, dynamic> json) {
  return _Notification.fromJson(json);
}

class _$NotificationTearOff {
  const _$NotificationTearOff();

  _Notification call(
      {String id,
      @required String receiverUserId,
      @required String title,
      String body,
      String myPlantId,
      bool openedApplication,
      String messageId,
      String senderId,
      String category,
      String collapseKey,
      bool contentAvailable,
      Map<String, String> data,
      String from,
      DateTime sentTime,
      String threadId,
      int ttl,
      String notificationAndroidChannelId,
      String notificationAndroidClickAction,
      String notificationAndroidColor,
      int notificationAndroidCount,
      String notificationAndroidImageUrl,
      String notificationAndroidLink,
      String notificationAndroidPriority,
      String notificationAndroidSmallIcon,
      String notificationAndroidSound,
      String notificationAndroidTicker,
      String notificationAndroidVisibility}) {
    return _Notification(
      id: id,
      receiverUserId: receiverUserId,
      title: title,
      body: body,
      myPlantId: myPlantId,
      openedApplication: openedApplication,
      messageId: messageId,
      senderId: senderId,
      category: category,
      collapseKey: collapseKey,
      contentAvailable: contentAvailable,
      data: data,
      from: from,
      sentTime: sentTime,
      threadId: threadId,
      ttl: ttl,
      notificationAndroidChannelId: notificationAndroidChannelId,
      notificationAndroidClickAction: notificationAndroidClickAction,
      notificationAndroidColor: notificationAndroidColor,
      notificationAndroidCount: notificationAndroidCount,
      notificationAndroidImageUrl: notificationAndroidImageUrl,
      notificationAndroidLink: notificationAndroidLink,
      notificationAndroidPriority: notificationAndroidPriority,
      notificationAndroidSmallIcon: notificationAndroidSmallIcon,
      notificationAndroidSound: notificationAndroidSound,
      notificationAndroidTicker: notificationAndroidTicker,
      notificationAndroidVisibility: notificationAndroidVisibility,
    );
  }
}

// ignore: unused_element
const $Notification = _$NotificationTearOff();

mixin _$Notification {
  String get id;
  String get receiverUserId;
  String get title;
  String get body;
  String get myPlantId;
  bool get openedApplication;
  String get messageId;
  String get senderId;
  String get category;
  String get collapseKey;
  bool get contentAvailable;
  Map<String, String> get data;
  String get from;
  DateTime get sentTime;
  String get threadId;
  int get ttl;
  String get notificationAndroidChannelId;
  String get notificationAndroidClickAction;
  String get notificationAndroidColor;
  int get notificationAndroidCount;
  String get notificationAndroidImageUrl;
  String get notificationAndroidLink;
  String get notificationAndroidPriority;
  String get notificationAndroidSmallIcon;
  String get notificationAndroidSound;
  String get notificationAndroidTicker;
  String get notificationAndroidVisibility;

  Map<String, dynamic> toJson();
  $NotificationCopyWith<Notification> get copyWith;
}

abstract class $NotificationCopyWith<$Res> {
  factory $NotificationCopyWith(
          Notification value, $Res Function(Notification) then) =
      _$NotificationCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String receiverUserId,
      String title,
      String body,
      String myPlantId,
      bool openedApplication,
      String messageId,
      String senderId,
      String category,
      String collapseKey,
      bool contentAvailable,
      Map<String, String> data,
      String from,
      DateTime sentTime,
      String threadId,
      int ttl,
      String notificationAndroidChannelId,
      String notificationAndroidClickAction,
      String notificationAndroidColor,
      int notificationAndroidCount,
      String notificationAndroidImageUrl,
      String notificationAndroidLink,
      String notificationAndroidPriority,
      String notificationAndroidSmallIcon,
      String notificationAndroidSound,
      String notificationAndroidTicker,
      String notificationAndroidVisibility});
}

class _$NotificationCopyWithImpl<$Res> implements $NotificationCopyWith<$Res> {
  _$NotificationCopyWithImpl(this._value, this._then);

  final Notification _value;
  // ignore: unused_field
  final $Res Function(Notification) _then;

  @override
  $Res call({
    Object id = freezed,
    Object receiverUserId = freezed,
    Object title = freezed,
    Object body = freezed,
    Object myPlantId = freezed,
    Object openedApplication = freezed,
    Object messageId = freezed,
    Object senderId = freezed,
    Object category = freezed,
    Object collapseKey = freezed,
    Object contentAvailable = freezed,
    Object data = freezed,
    Object from = freezed,
    Object sentTime = freezed,
    Object threadId = freezed,
    Object ttl = freezed,
    Object notificationAndroidChannelId = freezed,
    Object notificationAndroidClickAction = freezed,
    Object notificationAndroidColor = freezed,
    Object notificationAndroidCount = freezed,
    Object notificationAndroidImageUrl = freezed,
    Object notificationAndroidLink = freezed,
    Object notificationAndroidPriority = freezed,
    Object notificationAndroidSmallIcon = freezed,
    Object notificationAndroidSound = freezed,
    Object notificationAndroidTicker = freezed,
    Object notificationAndroidVisibility = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      receiverUserId: receiverUserId == freezed
          ? _value.receiverUserId
          : receiverUserId as String,
      title: title == freezed ? _value.title : title as String,
      body: body == freezed ? _value.body : body as String,
      myPlantId: myPlantId == freezed ? _value.myPlantId : myPlantId as String,
      openedApplication: openedApplication == freezed
          ? _value.openedApplication
          : openedApplication as bool,
      messageId: messageId == freezed ? _value.messageId : messageId as String,
      senderId: senderId == freezed ? _value.senderId : senderId as String,
      category: category == freezed ? _value.category : category as String,
      collapseKey:
          collapseKey == freezed ? _value.collapseKey : collapseKey as String,
      contentAvailable: contentAvailable == freezed
          ? _value.contentAvailable
          : contentAvailable as bool,
      data: data == freezed ? _value.data : data as Map<String, String>,
      from: from == freezed ? _value.from : from as String,
      sentTime: sentTime == freezed ? _value.sentTime : sentTime as DateTime,
      threadId: threadId == freezed ? _value.threadId : threadId as String,
      ttl: ttl == freezed ? _value.ttl : ttl as int,
      notificationAndroidChannelId: notificationAndroidChannelId == freezed
          ? _value.notificationAndroidChannelId
          : notificationAndroidChannelId as String,
      notificationAndroidClickAction: notificationAndroidClickAction == freezed
          ? _value.notificationAndroidClickAction
          : notificationAndroidClickAction as String,
      notificationAndroidColor: notificationAndroidColor == freezed
          ? _value.notificationAndroidColor
          : notificationAndroidColor as String,
      notificationAndroidCount: notificationAndroidCount == freezed
          ? _value.notificationAndroidCount
          : notificationAndroidCount as int,
      notificationAndroidImageUrl: notificationAndroidImageUrl == freezed
          ? _value.notificationAndroidImageUrl
          : notificationAndroidImageUrl as String,
      notificationAndroidLink: notificationAndroidLink == freezed
          ? _value.notificationAndroidLink
          : notificationAndroidLink as String,
      notificationAndroidPriority: notificationAndroidPriority == freezed
          ? _value.notificationAndroidPriority
          : notificationAndroidPriority as String,
      notificationAndroidSmallIcon: notificationAndroidSmallIcon == freezed
          ? _value.notificationAndroidSmallIcon
          : notificationAndroidSmallIcon as String,
      notificationAndroidSound: notificationAndroidSound == freezed
          ? _value.notificationAndroidSound
          : notificationAndroidSound as String,
      notificationAndroidTicker: notificationAndroidTicker == freezed
          ? _value.notificationAndroidTicker
          : notificationAndroidTicker as String,
      notificationAndroidVisibility: notificationAndroidVisibility == freezed
          ? _value.notificationAndroidVisibility
          : notificationAndroidVisibility as String,
    ));
  }
}

abstract class _$NotificationCopyWith<$Res>
    implements $NotificationCopyWith<$Res> {
  factory _$NotificationCopyWith(
          _Notification value, $Res Function(_Notification) then) =
      __$NotificationCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String receiverUserId,
      String title,
      String body,
      String myPlantId,
      bool openedApplication,
      String messageId,
      String senderId,
      String category,
      String collapseKey,
      bool contentAvailable,
      Map<String, String> data,
      String from,
      DateTime sentTime,
      String threadId,
      int ttl,
      String notificationAndroidChannelId,
      String notificationAndroidClickAction,
      String notificationAndroidColor,
      int notificationAndroidCount,
      String notificationAndroidImageUrl,
      String notificationAndroidLink,
      String notificationAndroidPriority,
      String notificationAndroidSmallIcon,
      String notificationAndroidSound,
      String notificationAndroidTicker,
      String notificationAndroidVisibility});
}

class __$NotificationCopyWithImpl<$Res> extends _$NotificationCopyWithImpl<$Res>
    implements _$NotificationCopyWith<$Res> {
  __$NotificationCopyWithImpl(
      _Notification _value, $Res Function(_Notification) _then)
      : super(_value, (v) => _then(v as _Notification));

  @override
  _Notification get _value => super._value as _Notification;

  @override
  $Res call({
    Object id = freezed,
    Object receiverUserId = freezed,
    Object title = freezed,
    Object body = freezed,
    Object myPlantId = freezed,
    Object openedApplication = freezed,
    Object messageId = freezed,
    Object senderId = freezed,
    Object category = freezed,
    Object collapseKey = freezed,
    Object contentAvailable = freezed,
    Object data = freezed,
    Object from = freezed,
    Object sentTime = freezed,
    Object threadId = freezed,
    Object ttl = freezed,
    Object notificationAndroidChannelId = freezed,
    Object notificationAndroidClickAction = freezed,
    Object notificationAndroidColor = freezed,
    Object notificationAndroidCount = freezed,
    Object notificationAndroidImageUrl = freezed,
    Object notificationAndroidLink = freezed,
    Object notificationAndroidPriority = freezed,
    Object notificationAndroidSmallIcon = freezed,
    Object notificationAndroidSound = freezed,
    Object notificationAndroidTicker = freezed,
    Object notificationAndroidVisibility = freezed,
  }) {
    return _then(_Notification(
      id: id == freezed ? _value.id : id as String,
      receiverUserId: receiverUserId == freezed
          ? _value.receiverUserId
          : receiverUserId as String,
      title: title == freezed ? _value.title : title as String,
      body: body == freezed ? _value.body : body as String,
      myPlantId: myPlantId == freezed ? _value.myPlantId : myPlantId as String,
      openedApplication: openedApplication == freezed
          ? _value.openedApplication
          : openedApplication as bool,
      messageId: messageId == freezed ? _value.messageId : messageId as String,
      senderId: senderId == freezed ? _value.senderId : senderId as String,
      category: category == freezed ? _value.category : category as String,
      collapseKey:
          collapseKey == freezed ? _value.collapseKey : collapseKey as String,
      contentAvailable: contentAvailable == freezed
          ? _value.contentAvailable
          : contentAvailable as bool,
      data: data == freezed ? _value.data : data as Map<String, String>,
      from: from == freezed ? _value.from : from as String,
      sentTime: sentTime == freezed ? _value.sentTime : sentTime as DateTime,
      threadId: threadId == freezed ? _value.threadId : threadId as String,
      ttl: ttl == freezed ? _value.ttl : ttl as int,
      notificationAndroidChannelId: notificationAndroidChannelId == freezed
          ? _value.notificationAndroidChannelId
          : notificationAndroidChannelId as String,
      notificationAndroidClickAction: notificationAndroidClickAction == freezed
          ? _value.notificationAndroidClickAction
          : notificationAndroidClickAction as String,
      notificationAndroidColor: notificationAndroidColor == freezed
          ? _value.notificationAndroidColor
          : notificationAndroidColor as String,
      notificationAndroidCount: notificationAndroidCount == freezed
          ? _value.notificationAndroidCount
          : notificationAndroidCount as int,
      notificationAndroidImageUrl: notificationAndroidImageUrl == freezed
          ? _value.notificationAndroidImageUrl
          : notificationAndroidImageUrl as String,
      notificationAndroidLink: notificationAndroidLink == freezed
          ? _value.notificationAndroidLink
          : notificationAndroidLink as String,
      notificationAndroidPriority: notificationAndroidPriority == freezed
          ? _value.notificationAndroidPriority
          : notificationAndroidPriority as String,
      notificationAndroidSmallIcon: notificationAndroidSmallIcon == freezed
          ? _value.notificationAndroidSmallIcon
          : notificationAndroidSmallIcon as String,
      notificationAndroidSound: notificationAndroidSound == freezed
          ? _value.notificationAndroidSound
          : notificationAndroidSound as String,
      notificationAndroidTicker: notificationAndroidTicker == freezed
          ? _value.notificationAndroidTicker
          : notificationAndroidTicker as String,
      notificationAndroidVisibility: notificationAndroidVisibility == freezed
          ? _value.notificationAndroidVisibility
          : notificationAndroidVisibility as String,
    ));
  }
}

@JsonSerializable()
class _$_Notification extends _Notification {
  const _$_Notification(
      {this.id,
      @required this.receiverUserId,
      @required this.title,
      this.body,
      this.myPlantId,
      this.openedApplication,
      this.messageId,
      this.senderId,
      this.category,
      this.collapseKey,
      this.contentAvailable,
      this.data,
      this.from,
      this.sentTime,
      this.threadId,
      this.ttl,
      this.notificationAndroidChannelId,
      this.notificationAndroidClickAction,
      this.notificationAndroidColor,
      this.notificationAndroidCount,
      this.notificationAndroidImageUrl,
      this.notificationAndroidLink,
      this.notificationAndroidPriority,
      this.notificationAndroidSmallIcon,
      this.notificationAndroidSound,
      this.notificationAndroidTicker,
      this.notificationAndroidVisibility})
      : assert(receiverUserId != null),
        assert(title != null),
        super._();

  factory _$_Notification.fromJson(Map<String, dynamic> json) =>
      _$_$_NotificationFromJson(json);

  @override
  final String id;
  @override
  final String receiverUserId;
  @override
  final String title;
  @override
  final String body;
  @override
  final String myPlantId;
  @override
  final bool openedApplication;
  @override
  final String messageId;
  @override
  final String senderId;
  @override
  final String category;
  @override
  final String collapseKey;
  @override
  final bool contentAvailable;
  @override
  final Map<String, String> data;
  @override
  final String from;
  @override
  final DateTime sentTime;
  @override
  final String threadId;
  @override
  final int ttl;
  @override
  final String notificationAndroidChannelId;
  @override
  final String notificationAndroidClickAction;
  @override
  final String notificationAndroidColor;
  @override
  final int notificationAndroidCount;
  @override
  final String notificationAndroidImageUrl;
  @override
  final String notificationAndroidLink;
  @override
  final String notificationAndroidPriority;
  @override
  final String notificationAndroidSmallIcon;
  @override
  final String notificationAndroidSound;
  @override
  final String notificationAndroidTicker;
  @override
  final String notificationAndroidVisibility;

  @override
  String toString() {
    return 'Notification(id: $id, receiverUserId: $receiverUserId, title: $title, body: $body, myPlantId: $myPlantId, openedApplication: $openedApplication, messageId: $messageId, senderId: $senderId, category: $category, collapseKey: $collapseKey, contentAvailable: $contentAvailable, data: $data, from: $from, sentTime: $sentTime, threadId: $threadId, ttl: $ttl, notificationAndroidChannelId: $notificationAndroidChannelId, notificationAndroidClickAction: $notificationAndroidClickAction, notificationAndroidColor: $notificationAndroidColor, notificationAndroidCount: $notificationAndroidCount, notificationAndroidImageUrl: $notificationAndroidImageUrl, notificationAndroidLink: $notificationAndroidLink, notificationAndroidPriority: $notificationAndroidPriority, notificationAndroidSmallIcon: $notificationAndroidSmallIcon, notificationAndroidSound: $notificationAndroidSound, notificationAndroidTicker: $notificationAndroidTicker, notificationAndroidVisibility: $notificationAndroidVisibility)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Notification &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.receiverUserId, receiverUserId) ||
                const DeepCollectionEquality()
                    .equals(other.receiverUserId, receiverUserId)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.body, body) ||
                const DeepCollectionEquality().equals(other.body, body)) &&
            (identical(other.myPlantId, myPlantId) ||
                const DeepCollectionEquality()
                    .equals(other.myPlantId, myPlantId)) &&
            (identical(other.openedApplication, openedApplication) ||
                const DeepCollectionEquality()
                    .equals(other.openedApplication, openedApplication)) &&
            (identical(other.messageId, messageId) ||
                const DeepCollectionEquality()
                    .equals(other.messageId, messageId)) &&
            (identical(other.senderId, senderId) ||
                const DeepCollectionEquality()
                    .equals(other.senderId, senderId)) &&
            (identical(other.category, category) ||
                const DeepCollectionEquality()
                    .equals(other.category, category)) &&
            (identical(other.collapseKey, collapseKey) ||
                const DeepCollectionEquality()
                    .equals(other.collapseKey, collapseKey)) &&
            (identical(other.contentAvailable, contentAvailable) ||
                const DeepCollectionEquality()
                    .equals(other.contentAvailable, contentAvailable)) &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)) &&
            (identical(other.from, from) ||
                const DeepCollectionEquality().equals(other.from, from)) &&
            (identical(other.sentTime, sentTime) ||
                const DeepCollectionEquality()
                    .equals(other.sentTime, sentTime)) &&
            (identical(other.threadId, threadId) ||
                const DeepCollectionEquality()
                    .equals(other.threadId, threadId)) &&
            (identical(other.ttl, ttl) ||
                const DeepCollectionEquality().equals(other.ttl, ttl)) &&
            (identical(other.notificationAndroidChannelId, notificationAndroidChannelId) ||
                const DeepCollectionEquality().equals(
                    other.notificationAndroidChannelId,
                    notificationAndroidChannelId)) &&
            (identical(other.notificationAndroidClickAction, notificationAndroidClickAction) ||
                const DeepCollectionEquality().equals(
                    other.notificationAndroidClickAction,
                    notificationAndroidClickAction)) &&
            (identical(other.notificationAndroidColor, notificationAndroidColor) ||
                const DeepCollectionEquality().equals(
                    other.notificationAndroidColor,
                    notificationAndroidColor)) &&
            (identical(other.notificationAndroidCount, notificationAndroidCount) ||
                const DeepCollectionEquality().equals(
                    other.notificationAndroidCount,
                    notificationAndroidCount)) &&
            (identical(other.notificationAndroidImageUrl, notificationAndroidImageUrl) ||
                const DeepCollectionEquality().equals(other.notificationAndroidImageUrl, notificationAndroidImageUrl)) &&
            (identical(other.notificationAndroidLink, notificationAndroidLink) || const DeepCollectionEquality().equals(other.notificationAndroidLink, notificationAndroidLink)) &&
            (identical(other.notificationAndroidPriority, notificationAndroidPriority) || const DeepCollectionEquality().equals(other.notificationAndroidPriority, notificationAndroidPriority)) &&
            (identical(other.notificationAndroidSmallIcon, notificationAndroidSmallIcon) || const DeepCollectionEquality().equals(other.notificationAndroidSmallIcon, notificationAndroidSmallIcon)) &&
            (identical(other.notificationAndroidSound, notificationAndroidSound) || const DeepCollectionEquality().equals(other.notificationAndroidSound, notificationAndroidSound)) &&
            (identical(other.notificationAndroidTicker, notificationAndroidTicker) || const DeepCollectionEquality().equals(other.notificationAndroidTicker, notificationAndroidTicker)) &&
            (identical(other.notificationAndroidVisibility, notificationAndroidVisibility) || const DeepCollectionEquality().equals(other.notificationAndroidVisibility, notificationAndroidVisibility)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(receiverUserId) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(body) ^
      const DeepCollectionEquality().hash(myPlantId) ^
      const DeepCollectionEquality().hash(openedApplication) ^
      const DeepCollectionEquality().hash(messageId) ^
      const DeepCollectionEquality().hash(senderId) ^
      const DeepCollectionEquality().hash(category) ^
      const DeepCollectionEquality().hash(collapseKey) ^
      const DeepCollectionEquality().hash(contentAvailable) ^
      const DeepCollectionEquality().hash(data) ^
      const DeepCollectionEquality().hash(from) ^
      const DeepCollectionEquality().hash(sentTime) ^
      const DeepCollectionEquality().hash(threadId) ^
      const DeepCollectionEquality().hash(ttl) ^
      const DeepCollectionEquality().hash(notificationAndroidChannelId) ^
      const DeepCollectionEquality().hash(notificationAndroidClickAction) ^
      const DeepCollectionEquality().hash(notificationAndroidColor) ^
      const DeepCollectionEquality().hash(notificationAndroidCount) ^
      const DeepCollectionEquality().hash(notificationAndroidImageUrl) ^
      const DeepCollectionEquality().hash(notificationAndroidLink) ^
      const DeepCollectionEquality().hash(notificationAndroidPriority) ^
      const DeepCollectionEquality().hash(notificationAndroidSmallIcon) ^
      const DeepCollectionEquality().hash(notificationAndroidSound) ^
      const DeepCollectionEquality().hash(notificationAndroidTicker) ^
      const DeepCollectionEquality().hash(notificationAndroidVisibility);

  @override
  _$NotificationCopyWith<_Notification> get copyWith =>
      __$NotificationCopyWithImpl<_Notification>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_NotificationToJson(this);
  }
}

abstract class _Notification extends Notification {
  const _Notification._() : super._();
  const factory _Notification(
      {String id,
      @required String receiverUserId,
      @required String title,
      String body,
      String myPlantId,
      bool openedApplication,
      String messageId,
      String senderId,
      String category,
      String collapseKey,
      bool contentAvailable,
      Map<String, String> data,
      String from,
      DateTime sentTime,
      String threadId,
      int ttl,
      String notificationAndroidChannelId,
      String notificationAndroidClickAction,
      String notificationAndroidColor,
      int notificationAndroidCount,
      String notificationAndroidImageUrl,
      String notificationAndroidLink,
      String notificationAndroidPriority,
      String notificationAndroidSmallIcon,
      String notificationAndroidSound,
      String notificationAndroidTicker,
      String notificationAndroidVisibility}) = _$_Notification;

  factory _Notification.fromJson(Map<String, dynamic> json) =
      _$_Notification.fromJson;

  @override
  String get id;
  @override
  String get receiverUserId;
  @override
  String get title;
  @override
  String get body;
  @override
  String get myPlantId;
  @override
  bool get openedApplication;
  @override
  String get messageId;
  @override
  String get senderId;
  @override
  String get category;
  @override
  String get collapseKey;
  @override
  bool get contentAvailable;
  @override
  Map<String, String> get data;
  @override
  String get from;
  @override
  DateTime get sentTime;
  @override
  String get threadId;
  @override
  int get ttl;
  @override
  String get notificationAndroidChannelId;
  @override
  String get notificationAndroidClickAction;
  @override
  String get notificationAndroidColor;
  @override
  int get notificationAndroidCount;
  @override
  String get notificationAndroidImageUrl;
  @override
  String get notificationAndroidLink;
  @override
  String get notificationAndroidPriority;
  @override
  String get notificationAndroidSmallIcon;
  @override
  String get notificationAndroidSound;
  @override
  String get notificationAndroidTicker;
  @override
  String get notificationAndroidVisibility;
  @override
  _$NotificationCopyWith<_Notification> get copyWith;
}
