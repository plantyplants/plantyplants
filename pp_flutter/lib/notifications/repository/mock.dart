import 'package:uuid/uuid.dart';

import '../../my_plants/repository/mock.dart';
import 'interface.dart';
import 'model.dart';

/// Fake receiver User ID, used in tests.
const String receiverUserIdFake = '000-000-000';

/// Fake additional receiver User ID, used in tests.
const String otherReceiverUserIdFake = '000-000-001';

/// Fake Notification ID, used in tests.
const String notificationIdFake = '000-000-110';

/// Current datetime
DateTime dtNow = DateTime.now();

/// Fake list of [Notification], used in tests.
List<Notification> notificationsData = <Notification>[
  Notification(
      title: 'Water 11',
      receiverUserId: receiverUserIdFake,
      sentTime: dtNow.subtract(Duration(days: 1)),
      id: '000-000-011',
      myPlantId: myPlantsData[0].id),
  Notification(
      title: 'Water 12',
      receiverUserId: receiverUserIdFake,
      sentTime: dtNow.subtract(Duration(days: 2)),
      id: '000-000-012',
      myPlantId: myPlantsData[1].id),
  Notification(
    title: 'Water 13',
    receiverUserId: receiverUserIdFake,
    sentTime: dtNow.subtract(Duration(days: 4)),
    id: '000-000-013',
  ),
  Notification(
      title: 'Water 14',
      receiverUserId: otherReceiverUserIdFake,
      sentTime: dtNow.subtract(Duration(days: 4)),
      id: '000-000-014',
      myPlantId: myPlantsData[2].id),
];

/// Mock implementation of [MockNotificationsRepository] used for testing,
/// filled with sample data, works in memory.
class MockNotificationsRepository extends NotificationsRepository {
  /// Contains the list of [Notification] in the repository.
  List<Notification> data;

  /// Initialize data attribute.
  MockNotificationsRepository() {
    data = notificationsData;
  }

  /// Returns a list of all [Notification] from the repository, as a stream of
  /// lists.
  @override
  Stream<List<Notification>> notifications(String receiverUserId) =>
      Stream<List<Notification>>.fromIterable(<List<Notification>>[
        data
            .where(
                (notification) => notification.receiverUserId == receiverUserId)
            .toList()
      ]);

  /// Get a [Notification] from the repository, as a future, for the specific
  /// [Notification] ID and receiver [User] ID.
  @override
  Future<Notification> notification(
          String notificationId, String receiverUserId) =>
      Future<Notification>.value(data.firstWhere((notification) =>
          notification.id == notificationId &&
          notification.receiverUserId == receiverUserId));

  /// Add a new [Notification] to the repository.
  @override
  Future<void> addNotification(Notification notification) async {
    final notificationId =
        notification.id == null ? Uuid().v4() : notification.id;
    data..add(notification.copyWith(id: notificationId));
  }

  /// Update a [Notification] in the repository, takes the updated Notification
  /// and updates attributes for the [Notification] with that ID.
  @override
  Future<void> updateNotification(Notification update) async =>
      data = data.fold(
        <Notification>[],
        (prev, notification) =>
            prev..add(notification.id == update.id ? update : notification),
      );

  /// Delete a [Notification] from the repository.
  @override
  Future<void> deleteNotification(Notification toDelete) async =>
      data = data.fold(
        <Notification>[],
        (prev, notification) =>
            notification.id == toDelete.id ? prev : (prev..add(notification)),
      );
}
