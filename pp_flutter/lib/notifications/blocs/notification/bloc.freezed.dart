// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$NotificationEventTearOff {
  const _$NotificationEventTearOff();

  LoadNotifications loadNotifications() {
    return const LoadNotifications();
  }

  AddNotification addNotification(Notification notification) {
    return AddNotification(
      notification,
    );
  }

  UpdateNotification updateNotification(Notification updatedNotification) {
    return UpdateNotification(
      updatedNotification,
    );
  }

  DeleteNotification deleteNotification(Notification notification) {
    return DeleteNotification(
      notification,
    );
  }

  NotificationUpdated notificationsUpdated(List<Notification> notifications) {
    return NotificationUpdated(
      notifications,
    );
  }
}

// ignore: unused_element
const $NotificationEvent = _$NotificationEventTearOff();

mixin _$NotificationEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadNotifications(),
    @required Result addNotification(Notification notification),
    @required Result updateNotification(Notification updatedNotification),
    @required Result deleteNotification(Notification notification),
    @required Result notificationsUpdated(List<Notification> notifications),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadNotifications(),
    Result addNotification(Notification notification),
    Result updateNotification(Notification updatedNotification),
    Result deleteNotification(Notification notification),
    Result notificationsUpdated(List<Notification> notifications),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadNotifications(LoadNotifications value),
    @required Result addNotification(AddNotification value),
    @required Result updateNotification(UpdateNotification value),
    @required Result deleteNotification(DeleteNotification value),
    @required Result notificationsUpdated(NotificationUpdated value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadNotifications(LoadNotifications value),
    Result addNotification(AddNotification value),
    Result updateNotification(UpdateNotification value),
    Result deleteNotification(DeleteNotification value),
    Result notificationsUpdated(NotificationUpdated value),
    @required Result orElse(),
  });
}

abstract class $NotificationEventCopyWith<$Res> {
  factory $NotificationEventCopyWith(
          NotificationEvent value, $Res Function(NotificationEvent) then) =
      _$NotificationEventCopyWithImpl<$Res>;
}

class _$NotificationEventCopyWithImpl<$Res>
    implements $NotificationEventCopyWith<$Res> {
  _$NotificationEventCopyWithImpl(this._value, this._then);

  final NotificationEvent _value;
  // ignore: unused_field
  final $Res Function(NotificationEvent) _then;
}

abstract class $LoadNotificationsCopyWith<$Res> {
  factory $LoadNotificationsCopyWith(
          LoadNotifications value, $Res Function(LoadNotifications) then) =
      _$LoadNotificationsCopyWithImpl<$Res>;
}

class _$LoadNotificationsCopyWithImpl<$Res>
    extends _$NotificationEventCopyWithImpl<$Res>
    implements $LoadNotificationsCopyWith<$Res> {
  _$LoadNotificationsCopyWithImpl(
      LoadNotifications _value, $Res Function(LoadNotifications) _then)
      : super(_value, (v) => _then(v as LoadNotifications));

  @override
  LoadNotifications get _value => super._value as LoadNotifications;
}

class _$LoadNotifications implements LoadNotifications {
  const _$LoadNotifications();

  @override
  String toString() {
    return 'NotificationEvent.loadNotifications()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadNotifications);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadNotifications(),
    @required Result addNotification(Notification notification),
    @required Result updateNotification(Notification updatedNotification),
    @required Result deleteNotification(Notification notification),
    @required Result notificationsUpdated(List<Notification> notifications),
  }) {
    assert(loadNotifications != null);
    assert(addNotification != null);
    assert(updateNotification != null);
    assert(deleteNotification != null);
    assert(notificationsUpdated != null);
    return loadNotifications();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadNotifications(),
    Result addNotification(Notification notification),
    Result updateNotification(Notification updatedNotification),
    Result deleteNotification(Notification notification),
    Result notificationsUpdated(List<Notification> notifications),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loadNotifications != null) {
      return loadNotifications();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadNotifications(LoadNotifications value),
    @required Result addNotification(AddNotification value),
    @required Result updateNotification(UpdateNotification value),
    @required Result deleteNotification(DeleteNotification value),
    @required Result notificationsUpdated(NotificationUpdated value),
  }) {
    assert(loadNotifications != null);
    assert(addNotification != null);
    assert(updateNotification != null);
    assert(deleteNotification != null);
    assert(notificationsUpdated != null);
    return loadNotifications(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadNotifications(LoadNotifications value),
    Result addNotification(AddNotification value),
    Result updateNotification(UpdateNotification value),
    Result deleteNotification(DeleteNotification value),
    Result notificationsUpdated(NotificationUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loadNotifications != null) {
      return loadNotifications(this);
    }
    return orElse();
  }
}

abstract class LoadNotifications implements NotificationEvent {
  const factory LoadNotifications() = _$LoadNotifications;
}

abstract class $AddNotificationCopyWith<$Res> {
  factory $AddNotificationCopyWith(
          AddNotification value, $Res Function(AddNotification) then) =
      _$AddNotificationCopyWithImpl<$Res>;
  $Res call({Notification notification});

  $NotificationCopyWith<$Res> get notification;
}

class _$AddNotificationCopyWithImpl<$Res>
    extends _$NotificationEventCopyWithImpl<$Res>
    implements $AddNotificationCopyWith<$Res> {
  _$AddNotificationCopyWithImpl(
      AddNotification _value, $Res Function(AddNotification) _then)
      : super(_value, (v) => _then(v as AddNotification));

  @override
  AddNotification get _value => super._value as AddNotification;

  @override
  $Res call({
    Object notification = freezed,
  }) {
    return _then(AddNotification(
      notification == freezed
          ? _value.notification
          : notification as Notification,
    ));
  }

  @override
  $NotificationCopyWith<$Res> get notification {
    if (_value.notification == null) {
      return null;
    }
    return $NotificationCopyWith<$Res>(_value.notification, (value) {
      return _then(_value.copyWith(notification: value));
    });
  }
}

class _$AddNotification implements AddNotification {
  const _$AddNotification(this.notification) : assert(notification != null);

  @override
  final Notification notification;

  @override
  String toString() {
    return 'NotificationEvent.addNotification(notification: $notification)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AddNotification &&
            (identical(other.notification, notification) ||
                const DeepCollectionEquality()
                    .equals(other.notification, notification)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(notification);

  @override
  $AddNotificationCopyWith<AddNotification> get copyWith =>
      _$AddNotificationCopyWithImpl<AddNotification>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadNotifications(),
    @required Result addNotification(Notification notification),
    @required Result updateNotification(Notification updatedNotification),
    @required Result deleteNotification(Notification notification),
    @required Result notificationsUpdated(List<Notification> notifications),
  }) {
    assert(loadNotifications != null);
    assert(addNotification != null);
    assert(updateNotification != null);
    assert(deleteNotification != null);
    assert(notificationsUpdated != null);
    return addNotification(notification);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadNotifications(),
    Result addNotification(Notification notification),
    Result updateNotification(Notification updatedNotification),
    Result deleteNotification(Notification notification),
    Result notificationsUpdated(List<Notification> notifications),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (addNotification != null) {
      return addNotification(notification);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadNotifications(LoadNotifications value),
    @required Result addNotification(AddNotification value),
    @required Result updateNotification(UpdateNotification value),
    @required Result deleteNotification(DeleteNotification value),
    @required Result notificationsUpdated(NotificationUpdated value),
  }) {
    assert(loadNotifications != null);
    assert(addNotification != null);
    assert(updateNotification != null);
    assert(deleteNotification != null);
    assert(notificationsUpdated != null);
    return addNotification(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadNotifications(LoadNotifications value),
    Result addNotification(AddNotification value),
    Result updateNotification(UpdateNotification value),
    Result deleteNotification(DeleteNotification value),
    Result notificationsUpdated(NotificationUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (addNotification != null) {
      return addNotification(this);
    }
    return orElse();
  }
}

abstract class AddNotification implements NotificationEvent {
  const factory AddNotification(Notification notification) = _$AddNotification;

  Notification get notification;
  $AddNotificationCopyWith<AddNotification> get copyWith;
}

abstract class $UpdateNotificationCopyWith<$Res> {
  factory $UpdateNotificationCopyWith(
          UpdateNotification value, $Res Function(UpdateNotification) then) =
      _$UpdateNotificationCopyWithImpl<$Res>;
  $Res call({Notification updatedNotification});

  $NotificationCopyWith<$Res> get updatedNotification;
}

class _$UpdateNotificationCopyWithImpl<$Res>
    extends _$NotificationEventCopyWithImpl<$Res>
    implements $UpdateNotificationCopyWith<$Res> {
  _$UpdateNotificationCopyWithImpl(
      UpdateNotification _value, $Res Function(UpdateNotification) _then)
      : super(_value, (v) => _then(v as UpdateNotification));

  @override
  UpdateNotification get _value => super._value as UpdateNotification;

  @override
  $Res call({
    Object updatedNotification = freezed,
  }) {
    return _then(UpdateNotification(
      updatedNotification == freezed
          ? _value.updatedNotification
          : updatedNotification as Notification,
    ));
  }

  @override
  $NotificationCopyWith<$Res> get updatedNotification {
    if (_value.updatedNotification == null) {
      return null;
    }
    return $NotificationCopyWith<$Res>(_value.updatedNotification, (value) {
      return _then(_value.copyWith(updatedNotification: value));
    });
  }
}

class _$UpdateNotification implements UpdateNotification {
  const _$UpdateNotification(this.updatedNotification)
      : assert(updatedNotification != null);

  @override
  final Notification updatedNotification;

  @override
  String toString() {
    return 'NotificationEvent.updateNotification(updatedNotification: $updatedNotification)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UpdateNotification &&
            (identical(other.updatedNotification, updatedNotification) ||
                const DeepCollectionEquality()
                    .equals(other.updatedNotification, updatedNotification)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(updatedNotification);

  @override
  $UpdateNotificationCopyWith<UpdateNotification> get copyWith =>
      _$UpdateNotificationCopyWithImpl<UpdateNotification>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadNotifications(),
    @required Result addNotification(Notification notification),
    @required Result updateNotification(Notification updatedNotification),
    @required Result deleteNotification(Notification notification),
    @required Result notificationsUpdated(List<Notification> notifications),
  }) {
    assert(loadNotifications != null);
    assert(addNotification != null);
    assert(updateNotification != null);
    assert(deleteNotification != null);
    assert(notificationsUpdated != null);
    return updateNotification(updatedNotification);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadNotifications(),
    Result addNotification(Notification notification),
    Result updateNotification(Notification updatedNotification),
    Result deleteNotification(Notification notification),
    Result notificationsUpdated(List<Notification> notifications),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (updateNotification != null) {
      return updateNotification(updatedNotification);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadNotifications(LoadNotifications value),
    @required Result addNotification(AddNotification value),
    @required Result updateNotification(UpdateNotification value),
    @required Result deleteNotification(DeleteNotification value),
    @required Result notificationsUpdated(NotificationUpdated value),
  }) {
    assert(loadNotifications != null);
    assert(addNotification != null);
    assert(updateNotification != null);
    assert(deleteNotification != null);
    assert(notificationsUpdated != null);
    return updateNotification(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadNotifications(LoadNotifications value),
    Result addNotification(AddNotification value),
    Result updateNotification(UpdateNotification value),
    Result deleteNotification(DeleteNotification value),
    Result notificationsUpdated(NotificationUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (updateNotification != null) {
      return updateNotification(this);
    }
    return orElse();
  }
}

abstract class UpdateNotification implements NotificationEvent {
  const factory UpdateNotification(Notification updatedNotification) =
      _$UpdateNotification;

  Notification get updatedNotification;
  $UpdateNotificationCopyWith<UpdateNotification> get copyWith;
}

abstract class $DeleteNotificationCopyWith<$Res> {
  factory $DeleteNotificationCopyWith(
          DeleteNotification value, $Res Function(DeleteNotification) then) =
      _$DeleteNotificationCopyWithImpl<$Res>;
  $Res call({Notification notification});

  $NotificationCopyWith<$Res> get notification;
}

class _$DeleteNotificationCopyWithImpl<$Res>
    extends _$NotificationEventCopyWithImpl<$Res>
    implements $DeleteNotificationCopyWith<$Res> {
  _$DeleteNotificationCopyWithImpl(
      DeleteNotification _value, $Res Function(DeleteNotification) _then)
      : super(_value, (v) => _then(v as DeleteNotification));

  @override
  DeleteNotification get _value => super._value as DeleteNotification;

  @override
  $Res call({
    Object notification = freezed,
  }) {
    return _then(DeleteNotification(
      notification == freezed
          ? _value.notification
          : notification as Notification,
    ));
  }

  @override
  $NotificationCopyWith<$Res> get notification {
    if (_value.notification == null) {
      return null;
    }
    return $NotificationCopyWith<$Res>(_value.notification, (value) {
      return _then(_value.copyWith(notification: value));
    });
  }
}

class _$DeleteNotification implements DeleteNotification {
  const _$DeleteNotification(this.notification) : assert(notification != null);

  @override
  final Notification notification;

  @override
  String toString() {
    return 'NotificationEvent.deleteNotification(notification: $notification)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is DeleteNotification &&
            (identical(other.notification, notification) ||
                const DeepCollectionEquality()
                    .equals(other.notification, notification)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(notification);

  @override
  $DeleteNotificationCopyWith<DeleteNotification> get copyWith =>
      _$DeleteNotificationCopyWithImpl<DeleteNotification>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadNotifications(),
    @required Result addNotification(Notification notification),
    @required Result updateNotification(Notification updatedNotification),
    @required Result deleteNotification(Notification notification),
    @required Result notificationsUpdated(List<Notification> notifications),
  }) {
    assert(loadNotifications != null);
    assert(addNotification != null);
    assert(updateNotification != null);
    assert(deleteNotification != null);
    assert(notificationsUpdated != null);
    return deleteNotification(notification);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadNotifications(),
    Result addNotification(Notification notification),
    Result updateNotification(Notification updatedNotification),
    Result deleteNotification(Notification notification),
    Result notificationsUpdated(List<Notification> notifications),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (deleteNotification != null) {
      return deleteNotification(notification);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadNotifications(LoadNotifications value),
    @required Result addNotification(AddNotification value),
    @required Result updateNotification(UpdateNotification value),
    @required Result deleteNotification(DeleteNotification value),
    @required Result notificationsUpdated(NotificationUpdated value),
  }) {
    assert(loadNotifications != null);
    assert(addNotification != null);
    assert(updateNotification != null);
    assert(deleteNotification != null);
    assert(notificationsUpdated != null);
    return deleteNotification(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadNotifications(LoadNotifications value),
    Result addNotification(AddNotification value),
    Result updateNotification(UpdateNotification value),
    Result deleteNotification(DeleteNotification value),
    Result notificationsUpdated(NotificationUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (deleteNotification != null) {
      return deleteNotification(this);
    }
    return orElse();
  }
}

abstract class DeleteNotification implements NotificationEvent {
  const factory DeleteNotification(Notification notification) =
      _$DeleteNotification;

  Notification get notification;
  $DeleteNotificationCopyWith<DeleteNotification> get copyWith;
}

abstract class $NotificationUpdatedCopyWith<$Res> {
  factory $NotificationUpdatedCopyWith(
          NotificationUpdated value, $Res Function(NotificationUpdated) then) =
      _$NotificationUpdatedCopyWithImpl<$Res>;
  $Res call({List<Notification> notifications});
}

class _$NotificationUpdatedCopyWithImpl<$Res>
    extends _$NotificationEventCopyWithImpl<$Res>
    implements $NotificationUpdatedCopyWith<$Res> {
  _$NotificationUpdatedCopyWithImpl(
      NotificationUpdated _value, $Res Function(NotificationUpdated) _then)
      : super(_value, (v) => _then(v as NotificationUpdated));

  @override
  NotificationUpdated get _value => super._value as NotificationUpdated;

  @override
  $Res call({
    Object notifications = freezed,
  }) {
    return _then(NotificationUpdated(
      notifications == freezed
          ? _value.notifications
          : notifications as List<Notification>,
    ));
  }
}

class _$NotificationUpdated implements NotificationUpdated {
  const _$NotificationUpdated(this.notifications)
      : assert(notifications != null);

  @override
  final List<Notification> notifications;

  @override
  String toString() {
    return 'NotificationEvent.notificationsUpdated(notifications: $notifications)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is NotificationUpdated &&
            (identical(other.notifications, notifications) ||
                const DeepCollectionEquality()
                    .equals(other.notifications, notifications)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(notifications);

  @override
  $NotificationUpdatedCopyWith<NotificationUpdated> get copyWith =>
      _$NotificationUpdatedCopyWithImpl<NotificationUpdated>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadNotifications(),
    @required Result addNotification(Notification notification),
    @required Result updateNotification(Notification updatedNotification),
    @required Result deleteNotification(Notification notification),
    @required Result notificationsUpdated(List<Notification> notifications),
  }) {
    assert(loadNotifications != null);
    assert(addNotification != null);
    assert(updateNotification != null);
    assert(deleteNotification != null);
    assert(notificationsUpdated != null);
    return notificationsUpdated(notifications);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadNotifications(),
    Result addNotification(Notification notification),
    Result updateNotification(Notification updatedNotification),
    Result deleteNotification(Notification notification),
    Result notificationsUpdated(List<Notification> notifications),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (notificationsUpdated != null) {
      return notificationsUpdated(notifications);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadNotifications(LoadNotifications value),
    @required Result addNotification(AddNotification value),
    @required Result updateNotification(UpdateNotification value),
    @required Result deleteNotification(DeleteNotification value),
    @required Result notificationsUpdated(NotificationUpdated value),
  }) {
    assert(loadNotifications != null);
    assert(addNotification != null);
    assert(updateNotification != null);
    assert(deleteNotification != null);
    assert(notificationsUpdated != null);
    return notificationsUpdated(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadNotifications(LoadNotifications value),
    Result addNotification(AddNotification value),
    Result updateNotification(UpdateNotification value),
    Result deleteNotification(DeleteNotification value),
    Result notificationsUpdated(NotificationUpdated value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (notificationsUpdated != null) {
      return notificationsUpdated(this);
    }
    return orElse();
  }
}

abstract class NotificationUpdated implements NotificationEvent {
  const factory NotificationUpdated(List<Notification> notifications) =
      _$NotificationUpdated;

  List<Notification> get notifications;
  $NotificationUpdatedCopyWith<NotificationUpdated> get copyWith;
}

class _$NotificationStateTearOff {
  const _$NotificationStateTearOff();

  NotificationsLoading notificationsLoading() {
    return const NotificationsLoading();
  }

  NotificationsLoaded notificationsLoaded(
      [List<Notification> notifications = const <Notification>[]]) {
    return NotificationsLoaded(
      notifications,
    );
  }

  NotificationsNotLoaded notificationsNotLoaded() {
    return const NotificationsNotLoaded();
  }

  NotificationSavedSuccess notificationSavedSuccess(Notification notification) {
    return NotificationSavedSuccess(
      notification,
    );
  }

  NotificationSavedFailed notificationSavedFailed(Notification notification) {
    return NotificationSavedFailed(
      notification,
    );
  }
}

// ignore: unused_element
const $NotificationState = _$NotificationStateTearOff();

mixin _$NotificationState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result notificationsLoading(),
    @required Result notificationsLoaded(List<Notification> notifications),
    @required Result notificationsNotLoaded(),
    @required Result notificationSavedSuccess(Notification notification),
    @required Result notificationSavedFailed(Notification notification),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result notificationsLoading(),
    Result notificationsLoaded(List<Notification> notifications),
    Result notificationsNotLoaded(),
    Result notificationSavedSuccess(Notification notification),
    Result notificationSavedFailed(Notification notification),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result notificationsLoading(NotificationsLoading value),
    @required Result notificationsLoaded(NotificationsLoaded value),
    @required Result notificationsNotLoaded(NotificationsNotLoaded value),
    @required Result notificationSavedSuccess(NotificationSavedSuccess value),
    @required Result notificationSavedFailed(NotificationSavedFailed value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result notificationsLoading(NotificationsLoading value),
    Result notificationsLoaded(NotificationsLoaded value),
    Result notificationsNotLoaded(NotificationsNotLoaded value),
    Result notificationSavedSuccess(NotificationSavedSuccess value),
    Result notificationSavedFailed(NotificationSavedFailed value),
    @required Result orElse(),
  });
}

abstract class $NotificationStateCopyWith<$Res> {
  factory $NotificationStateCopyWith(
          NotificationState value, $Res Function(NotificationState) then) =
      _$NotificationStateCopyWithImpl<$Res>;
}

class _$NotificationStateCopyWithImpl<$Res>
    implements $NotificationStateCopyWith<$Res> {
  _$NotificationStateCopyWithImpl(this._value, this._then);

  final NotificationState _value;
  // ignore: unused_field
  final $Res Function(NotificationState) _then;
}

abstract class $NotificationsLoadingCopyWith<$Res> {
  factory $NotificationsLoadingCopyWith(NotificationsLoading value,
          $Res Function(NotificationsLoading) then) =
      _$NotificationsLoadingCopyWithImpl<$Res>;
}

class _$NotificationsLoadingCopyWithImpl<$Res>
    extends _$NotificationStateCopyWithImpl<$Res>
    implements $NotificationsLoadingCopyWith<$Res> {
  _$NotificationsLoadingCopyWithImpl(
      NotificationsLoading _value, $Res Function(NotificationsLoading) _then)
      : super(_value, (v) => _then(v as NotificationsLoading));

  @override
  NotificationsLoading get _value => super._value as NotificationsLoading;
}

class _$NotificationsLoading implements NotificationsLoading {
  const _$NotificationsLoading();

  @override
  String toString() {
    return 'NotificationState.notificationsLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is NotificationsLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result notificationsLoading(),
    @required Result notificationsLoaded(List<Notification> notifications),
    @required Result notificationsNotLoaded(),
    @required Result notificationSavedSuccess(Notification notification),
    @required Result notificationSavedFailed(Notification notification),
  }) {
    assert(notificationsLoading != null);
    assert(notificationsLoaded != null);
    assert(notificationsNotLoaded != null);
    assert(notificationSavedSuccess != null);
    assert(notificationSavedFailed != null);
    return notificationsLoading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result notificationsLoading(),
    Result notificationsLoaded(List<Notification> notifications),
    Result notificationsNotLoaded(),
    Result notificationSavedSuccess(Notification notification),
    Result notificationSavedFailed(Notification notification),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (notificationsLoading != null) {
      return notificationsLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result notificationsLoading(NotificationsLoading value),
    @required Result notificationsLoaded(NotificationsLoaded value),
    @required Result notificationsNotLoaded(NotificationsNotLoaded value),
    @required Result notificationSavedSuccess(NotificationSavedSuccess value),
    @required Result notificationSavedFailed(NotificationSavedFailed value),
  }) {
    assert(notificationsLoading != null);
    assert(notificationsLoaded != null);
    assert(notificationsNotLoaded != null);
    assert(notificationSavedSuccess != null);
    assert(notificationSavedFailed != null);
    return notificationsLoading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result notificationsLoading(NotificationsLoading value),
    Result notificationsLoaded(NotificationsLoaded value),
    Result notificationsNotLoaded(NotificationsNotLoaded value),
    Result notificationSavedSuccess(NotificationSavedSuccess value),
    Result notificationSavedFailed(NotificationSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (notificationsLoading != null) {
      return notificationsLoading(this);
    }
    return orElse();
  }
}

abstract class NotificationsLoading implements NotificationState {
  const factory NotificationsLoading() = _$NotificationsLoading;
}

abstract class $NotificationsLoadedCopyWith<$Res> {
  factory $NotificationsLoadedCopyWith(
          NotificationsLoaded value, $Res Function(NotificationsLoaded) then) =
      _$NotificationsLoadedCopyWithImpl<$Res>;
  $Res call({List<Notification> notifications});
}

class _$NotificationsLoadedCopyWithImpl<$Res>
    extends _$NotificationStateCopyWithImpl<$Res>
    implements $NotificationsLoadedCopyWith<$Res> {
  _$NotificationsLoadedCopyWithImpl(
      NotificationsLoaded _value, $Res Function(NotificationsLoaded) _then)
      : super(_value, (v) => _then(v as NotificationsLoaded));

  @override
  NotificationsLoaded get _value => super._value as NotificationsLoaded;

  @override
  $Res call({
    Object notifications = freezed,
  }) {
    return _then(NotificationsLoaded(
      notifications == freezed
          ? _value.notifications
          : notifications as List<Notification>,
    ));
  }
}

class _$NotificationsLoaded implements NotificationsLoaded {
  const _$NotificationsLoaded([this.notifications = const <Notification>[]])
      : assert(notifications != null);

  @JsonKey(defaultValue: const <Notification>[])
  @override
  final List<Notification> notifications;

  @override
  String toString() {
    return 'NotificationState.notificationsLoaded(notifications: $notifications)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is NotificationsLoaded &&
            (identical(other.notifications, notifications) ||
                const DeepCollectionEquality()
                    .equals(other.notifications, notifications)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(notifications);

  @override
  $NotificationsLoadedCopyWith<NotificationsLoaded> get copyWith =>
      _$NotificationsLoadedCopyWithImpl<NotificationsLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result notificationsLoading(),
    @required Result notificationsLoaded(List<Notification> notifications),
    @required Result notificationsNotLoaded(),
    @required Result notificationSavedSuccess(Notification notification),
    @required Result notificationSavedFailed(Notification notification),
  }) {
    assert(notificationsLoading != null);
    assert(notificationsLoaded != null);
    assert(notificationsNotLoaded != null);
    assert(notificationSavedSuccess != null);
    assert(notificationSavedFailed != null);
    return notificationsLoaded(notifications);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result notificationsLoading(),
    Result notificationsLoaded(List<Notification> notifications),
    Result notificationsNotLoaded(),
    Result notificationSavedSuccess(Notification notification),
    Result notificationSavedFailed(Notification notification),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (notificationsLoaded != null) {
      return notificationsLoaded(notifications);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result notificationsLoading(NotificationsLoading value),
    @required Result notificationsLoaded(NotificationsLoaded value),
    @required Result notificationsNotLoaded(NotificationsNotLoaded value),
    @required Result notificationSavedSuccess(NotificationSavedSuccess value),
    @required Result notificationSavedFailed(NotificationSavedFailed value),
  }) {
    assert(notificationsLoading != null);
    assert(notificationsLoaded != null);
    assert(notificationsNotLoaded != null);
    assert(notificationSavedSuccess != null);
    assert(notificationSavedFailed != null);
    return notificationsLoaded(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result notificationsLoading(NotificationsLoading value),
    Result notificationsLoaded(NotificationsLoaded value),
    Result notificationsNotLoaded(NotificationsNotLoaded value),
    Result notificationSavedSuccess(NotificationSavedSuccess value),
    Result notificationSavedFailed(NotificationSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (notificationsLoaded != null) {
      return notificationsLoaded(this);
    }
    return orElse();
  }
}

abstract class NotificationsLoaded implements NotificationState {
  const factory NotificationsLoaded([List<Notification> notifications]) =
      _$NotificationsLoaded;

  List<Notification> get notifications;
  $NotificationsLoadedCopyWith<NotificationsLoaded> get copyWith;
}

abstract class $NotificationsNotLoadedCopyWith<$Res> {
  factory $NotificationsNotLoadedCopyWith(NotificationsNotLoaded value,
          $Res Function(NotificationsNotLoaded) then) =
      _$NotificationsNotLoadedCopyWithImpl<$Res>;
}

class _$NotificationsNotLoadedCopyWithImpl<$Res>
    extends _$NotificationStateCopyWithImpl<$Res>
    implements $NotificationsNotLoadedCopyWith<$Res> {
  _$NotificationsNotLoadedCopyWithImpl(NotificationsNotLoaded _value,
      $Res Function(NotificationsNotLoaded) _then)
      : super(_value, (v) => _then(v as NotificationsNotLoaded));

  @override
  NotificationsNotLoaded get _value => super._value as NotificationsNotLoaded;
}

class _$NotificationsNotLoaded implements NotificationsNotLoaded {
  const _$NotificationsNotLoaded();

  @override
  String toString() {
    return 'NotificationState.notificationsNotLoaded()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is NotificationsNotLoaded);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result notificationsLoading(),
    @required Result notificationsLoaded(List<Notification> notifications),
    @required Result notificationsNotLoaded(),
    @required Result notificationSavedSuccess(Notification notification),
    @required Result notificationSavedFailed(Notification notification),
  }) {
    assert(notificationsLoading != null);
    assert(notificationsLoaded != null);
    assert(notificationsNotLoaded != null);
    assert(notificationSavedSuccess != null);
    assert(notificationSavedFailed != null);
    return notificationsNotLoaded();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result notificationsLoading(),
    Result notificationsLoaded(List<Notification> notifications),
    Result notificationsNotLoaded(),
    Result notificationSavedSuccess(Notification notification),
    Result notificationSavedFailed(Notification notification),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (notificationsNotLoaded != null) {
      return notificationsNotLoaded();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result notificationsLoading(NotificationsLoading value),
    @required Result notificationsLoaded(NotificationsLoaded value),
    @required Result notificationsNotLoaded(NotificationsNotLoaded value),
    @required Result notificationSavedSuccess(NotificationSavedSuccess value),
    @required Result notificationSavedFailed(NotificationSavedFailed value),
  }) {
    assert(notificationsLoading != null);
    assert(notificationsLoaded != null);
    assert(notificationsNotLoaded != null);
    assert(notificationSavedSuccess != null);
    assert(notificationSavedFailed != null);
    return notificationsNotLoaded(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result notificationsLoading(NotificationsLoading value),
    Result notificationsLoaded(NotificationsLoaded value),
    Result notificationsNotLoaded(NotificationsNotLoaded value),
    Result notificationSavedSuccess(NotificationSavedSuccess value),
    Result notificationSavedFailed(NotificationSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (notificationsNotLoaded != null) {
      return notificationsNotLoaded(this);
    }
    return orElse();
  }
}

abstract class NotificationsNotLoaded implements NotificationState {
  const factory NotificationsNotLoaded() = _$NotificationsNotLoaded;
}

abstract class $NotificationSavedSuccessCopyWith<$Res> {
  factory $NotificationSavedSuccessCopyWith(NotificationSavedSuccess value,
          $Res Function(NotificationSavedSuccess) then) =
      _$NotificationSavedSuccessCopyWithImpl<$Res>;
  $Res call({Notification notification});

  $NotificationCopyWith<$Res> get notification;
}

class _$NotificationSavedSuccessCopyWithImpl<$Res>
    extends _$NotificationStateCopyWithImpl<$Res>
    implements $NotificationSavedSuccessCopyWith<$Res> {
  _$NotificationSavedSuccessCopyWithImpl(NotificationSavedSuccess _value,
      $Res Function(NotificationSavedSuccess) _then)
      : super(_value, (v) => _then(v as NotificationSavedSuccess));

  @override
  NotificationSavedSuccess get _value =>
      super._value as NotificationSavedSuccess;

  @override
  $Res call({
    Object notification = freezed,
  }) {
    return _then(NotificationSavedSuccess(
      notification == freezed
          ? _value.notification
          : notification as Notification,
    ));
  }

  @override
  $NotificationCopyWith<$Res> get notification {
    if (_value.notification == null) {
      return null;
    }
    return $NotificationCopyWith<$Res>(_value.notification, (value) {
      return _then(_value.copyWith(notification: value));
    });
  }
}

class _$NotificationSavedSuccess implements NotificationSavedSuccess {
  const _$NotificationSavedSuccess(this.notification)
      : assert(notification != null);

  @override
  final Notification notification;

  @override
  String toString() {
    return 'NotificationState.notificationSavedSuccess(notification: $notification)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is NotificationSavedSuccess &&
            (identical(other.notification, notification) ||
                const DeepCollectionEquality()
                    .equals(other.notification, notification)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(notification);

  @override
  $NotificationSavedSuccessCopyWith<NotificationSavedSuccess> get copyWith =>
      _$NotificationSavedSuccessCopyWithImpl<NotificationSavedSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result notificationsLoading(),
    @required Result notificationsLoaded(List<Notification> notifications),
    @required Result notificationsNotLoaded(),
    @required Result notificationSavedSuccess(Notification notification),
    @required Result notificationSavedFailed(Notification notification),
  }) {
    assert(notificationsLoading != null);
    assert(notificationsLoaded != null);
    assert(notificationsNotLoaded != null);
    assert(notificationSavedSuccess != null);
    assert(notificationSavedFailed != null);
    return notificationSavedSuccess(notification);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result notificationsLoading(),
    Result notificationsLoaded(List<Notification> notifications),
    Result notificationsNotLoaded(),
    Result notificationSavedSuccess(Notification notification),
    Result notificationSavedFailed(Notification notification),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (notificationSavedSuccess != null) {
      return notificationSavedSuccess(notification);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result notificationsLoading(NotificationsLoading value),
    @required Result notificationsLoaded(NotificationsLoaded value),
    @required Result notificationsNotLoaded(NotificationsNotLoaded value),
    @required Result notificationSavedSuccess(NotificationSavedSuccess value),
    @required Result notificationSavedFailed(NotificationSavedFailed value),
  }) {
    assert(notificationsLoading != null);
    assert(notificationsLoaded != null);
    assert(notificationsNotLoaded != null);
    assert(notificationSavedSuccess != null);
    assert(notificationSavedFailed != null);
    return notificationSavedSuccess(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result notificationsLoading(NotificationsLoading value),
    Result notificationsLoaded(NotificationsLoaded value),
    Result notificationsNotLoaded(NotificationsNotLoaded value),
    Result notificationSavedSuccess(NotificationSavedSuccess value),
    Result notificationSavedFailed(NotificationSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (notificationSavedSuccess != null) {
      return notificationSavedSuccess(this);
    }
    return orElse();
  }
}

abstract class NotificationSavedSuccess implements NotificationState {
  const factory NotificationSavedSuccess(Notification notification) =
      _$NotificationSavedSuccess;

  Notification get notification;
  $NotificationSavedSuccessCopyWith<NotificationSavedSuccess> get copyWith;
}

abstract class $NotificationSavedFailedCopyWith<$Res> {
  factory $NotificationSavedFailedCopyWith(NotificationSavedFailed value,
          $Res Function(NotificationSavedFailed) then) =
      _$NotificationSavedFailedCopyWithImpl<$Res>;
  $Res call({Notification notification});

  $NotificationCopyWith<$Res> get notification;
}

class _$NotificationSavedFailedCopyWithImpl<$Res>
    extends _$NotificationStateCopyWithImpl<$Res>
    implements $NotificationSavedFailedCopyWith<$Res> {
  _$NotificationSavedFailedCopyWithImpl(NotificationSavedFailed _value,
      $Res Function(NotificationSavedFailed) _then)
      : super(_value, (v) => _then(v as NotificationSavedFailed));

  @override
  NotificationSavedFailed get _value => super._value as NotificationSavedFailed;

  @override
  $Res call({
    Object notification = freezed,
  }) {
    return _then(NotificationSavedFailed(
      notification == freezed
          ? _value.notification
          : notification as Notification,
    ));
  }

  @override
  $NotificationCopyWith<$Res> get notification {
    if (_value.notification == null) {
      return null;
    }
    return $NotificationCopyWith<$Res>(_value.notification, (value) {
      return _then(_value.copyWith(notification: value));
    });
  }
}

class _$NotificationSavedFailed implements NotificationSavedFailed {
  const _$NotificationSavedFailed(this.notification)
      : assert(notification != null);

  @override
  final Notification notification;

  @override
  String toString() {
    return 'NotificationState.notificationSavedFailed(notification: $notification)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is NotificationSavedFailed &&
            (identical(other.notification, notification) ||
                const DeepCollectionEquality()
                    .equals(other.notification, notification)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(notification);

  @override
  $NotificationSavedFailedCopyWith<NotificationSavedFailed> get copyWith =>
      _$NotificationSavedFailedCopyWithImpl<NotificationSavedFailed>(
          this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result notificationsLoading(),
    @required Result notificationsLoaded(List<Notification> notifications),
    @required Result notificationsNotLoaded(),
    @required Result notificationSavedSuccess(Notification notification),
    @required Result notificationSavedFailed(Notification notification),
  }) {
    assert(notificationsLoading != null);
    assert(notificationsLoaded != null);
    assert(notificationsNotLoaded != null);
    assert(notificationSavedSuccess != null);
    assert(notificationSavedFailed != null);
    return notificationSavedFailed(notification);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result notificationsLoading(),
    Result notificationsLoaded(List<Notification> notifications),
    Result notificationsNotLoaded(),
    Result notificationSavedSuccess(Notification notification),
    Result notificationSavedFailed(Notification notification),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (notificationSavedFailed != null) {
      return notificationSavedFailed(notification);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result notificationsLoading(NotificationsLoading value),
    @required Result notificationsLoaded(NotificationsLoaded value),
    @required Result notificationsNotLoaded(NotificationsNotLoaded value),
    @required Result notificationSavedSuccess(NotificationSavedSuccess value),
    @required Result notificationSavedFailed(NotificationSavedFailed value),
  }) {
    assert(notificationsLoading != null);
    assert(notificationsLoaded != null);
    assert(notificationsNotLoaded != null);
    assert(notificationSavedSuccess != null);
    assert(notificationSavedFailed != null);
    return notificationSavedFailed(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result notificationsLoading(NotificationsLoading value),
    Result notificationsLoaded(NotificationsLoaded value),
    Result notificationsNotLoaded(NotificationsNotLoaded value),
    Result notificationSavedSuccess(NotificationSavedSuccess value),
    Result notificationSavedFailed(NotificationSavedFailed value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (notificationSavedFailed != null) {
      return notificationSavedFailed(this);
    }
    return orElse();
  }
}

abstract class NotificationSavedFailed implements NotificationState {
  const factory NotificationSavedFailed(Notification notification) =
      _$NotificationSavedFailed;

  Notification get notification;
  $NotificationSavedFailedCopyWith<NotificationSavedFailed> get copyWith;
}
