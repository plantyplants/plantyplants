part of 'bloc.dart';

/// State of the interface for the screens related to [Notification].
@freezed
abstract class NotificationState with _$NotificationState {
  /// the state while our application is fetching myPlants from the repository.
  const factory NotificationState.notificationsLoading() = NotificationsLoading;

  /// the state of our application after the notifications have successfully
  /// been loaded.
  const factory NotificationState.notificationsLoaded(
          [@Default(<Notification>[]) List<Notification> notifications]) =
      NotificationsLoaded;

  /// the state of our application if the notifications were not successfully
  /// loaded.
  const factory NotificationState.notificationsNotLoaded() =
      NotificationsNotLoaded;

  /// the state of our application if a [Notification] was successfully saved.
  const factory NotificationState.notificationSavedSuccess(
      Notification notification) = NotificationSavedSuccess;

  /// the state of our application if a [Notification] was not successfully
  /// saved.
  const factory NotificationState.notificationSavedFailed(
      Notification notification) = NotificationSavedFailed;
}
