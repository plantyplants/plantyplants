part of 'bloc.dart';

/// [Event] for [Notification] model (load, add, update, etc.).
@freezed
abstract class NotificationEvent with _$NotificationEvent {
  /// Loading the list of [Notification] from the repository into the interface.
  const factory NotificationEvent.loadNotifications() = LoadNotifications;

  /// Add a [Notification] to the interface and repository.
  const factory NotificationEvent.addNotification(Notification notification) =
      AddNotification;

  /// Update a [Notification] on the interface and repository.
  const factory NotificationEvent.updateNotification(
      Notification updatedNotification) = UpdateNotification;

  /// Delete a [Notification] to the interface and repository.
  const factory NotificationEvent.deleteNotification(
      Notification notification) = DeleteNotification;

  /// The list of [Notification] were updated and need to be loaded.
  const factory NotificationEvent.notificationsUpdated(
      List<Notification> notifications) = NotificationUpdated;
}
