import 'package:bloc/bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'bloc/simple_bloc_delegate.dart';
import 'home/screens/home_screen.dart';
import 'my_plants/repository/firebase.dart';
import 'my_plants/repository/interface.dart';
import 'notifications/repository/firebase.dart';
import 'notifications/repository/interface.dart';
import 'routes.dart';
import 'users/repository/firebase.dart';
import 'users/repository/interface.dart';

/// Initializes firebase
Future<void> _initializeFirebase() async {
  await DotEnv().load('.env');
  final options = FirebaseOptions(
    appId: DotEnv().env['FIREBASE_IOS_GOOGLE_APP_ID'],
    androidClientId: DotEnv().env['FIREBASE_ANDROID_GOOGLE_APP_ID'],
    messagingSenderId: DotEnv().env['FIREBASE_MESSAGING_SENDER_ID'],
    apiKey: DotEnv().env['FIREBASE_API_KEY'],
    projectId: DotEnv().env['FIREBASE_PROJECT_ID'],
    storageBucket: DotEnv().env['FIREBASE_STORAGE_BUCKET'],
  );
  await Firebase.initializeApp(
    name: DotEnv().env['APP_NAME'],
    options: options,
  );
}

/// Define a top-level named handler which background/terminated messages will
/// call.
///
/// To verify things are working, check out the native platform logs.
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as
  // Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await _initializeFirebase();
  print("Handling a background message ${message.messageId}");

  // Since the handler runs in it's own isolate outside of your applications
  // context, it is not possible to update application state or execute any UI
  // impacting logic. You can however perform logic such as HTTP requests, IO
  // operations (updating local storage), communicate with other plugins etc.
  // Shouldn't run more than 30 seconds.
  // If notification, firebase SDKs will intercept and display a visible
  // notification to users.

  // TODO: Add logging
  // TODO: Update database to register message has arrived and at what time
}

/// Initialize the [FlutterLocalNotificationsPlugin] package.
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

class _FirebaseWrapperWidget extends StatefulWidget {
  @override
  _FirebaseWrapperWidgetState createState() => _FirebaseWrapperWidgetState();
}

/// Message route arguments.
class MessageArguments {
  /// The RemoteMessage
  final RemoteMessage message;

  /// Whether this message caused the application to open.
  final bool openedApplication;

  // ignore: public_member_api_docs
  MessageArguments(this.message, {@required this.openedApplication})
      : assert(message != null);
}

/// Create a [AndroidNotificationChannel] for heads up notifications
const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.high,
  enableVibration: true,
  playSound: true,
);

class _FirebaseWrapperWidgetState extends State<_FirebaseWrapperWidget> {
  @override
  Widget build(BuildContext context) {
    return HomeScreen();
  }

  @override
  void initState() {
    super.initState();

    // Get any messages which caused the application to open from
    // a terminated state.
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if (message != null) {
        Navigator.pushNamed(context, Routes.notifications,
            arguments: MessageArguments(message, openedApplication: true));
      }
    });

    // Get any messages while the app is in foreground
    FirebaseMessaging.onMessage.listen((message) {
      var notification = message.notification;
      var android = message.notification?.android;

      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                "description ${channel.description}",
                // TODO add a proper drawable resource to android, for now using
                //      one that already exists in example app.
                icon: 'launch_background',
                channelShowBadge: true,
              ),
            ));
      }
    });

    // Also handle any interaction when the app is in the background via a
    // Stream listener
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('A new onMessageOpenedApp event was published!');
      Navigator.pushNamed(context, Routes.notifications,
          arguments: MessageArguments(message, openedApplication: true));
    });
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await _initializeFirebase();

  // Set the background messaging handler early on, as a named top-level
  // function
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  /// Create an Android Notification Channel.
  ///
  /// We use this channel in the `AndroidManifest.xml` file to override the
  /// default FCM channel to enable heads up notifications.
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(MultiRepositoryProvider(providers: <RepositoryProvider<dynamic>>[
    RepositoryProvider<MyPlantsRepository>(
      create: (context) => FirebaseMyPlantsRepository(),
    ),
    RepositoryProvider<UserRepository>(
      create: (context) => FirebaseUserRepository(),
    ),
    RepositoryProvider<NotificationsRepository>(
      create: (context) => FirebaseNotificationsRepository(),
    ),
  ], child: _FirebaseWrapperWidget()));
}
