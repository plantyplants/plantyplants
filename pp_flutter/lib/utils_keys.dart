// ignore_for_file: public_member_api_docs
// ignore_for_file: prefer_function_declarations_over_variables
// ignore_for_file: avoid_types_on_closure_parameters

typedef StringFunc = String Function(String str);

/// Converts a key enum into a [Key]'s string.
String keyStringFromEnum(Keys key) => key.toString();

/// Converts a key StringFunc and its string arg into a [Key]'s string.
String keyStringFromStringFunc(String str, StringFunc f) => f(str);

/// Keys const literals identifying UI parts.
enum Keys {
  // List Screens
  homeScreen,
  addMyPlantFab,
  snackbar,

  // Plants
  myPlantsListScreen,
  myPlantsList,
  myPlantsLoading,

  // Details Screen
  editMyPlantFab,
  deleteMyPlantButton,
  myPlantDetailsScreen,
  detailsMyPlantItemName,
  detailsMyPlantItemPictureFrontGrid,
  detailsMyPlantItemDescription,

  // Add + Edit screen
  previewFrontPicture,
  actionIconActivitiesNotifications,
  formAddEditNameField,
  formAddEditDescriptionField,

  // Add Screen
  addMyPlantScreen,
  saveNewMyPlant,

  // Edit Screen
  editMyPlantScreen,
  saveMyPlantFab,
  changePictureGallery,
  changePictureCamera,

  // Front picture upload screen
  myPlantFrontPictureUploadScreen,

  // Recurring Task Configure
  myPlantRecurringIconAlarm,
  myPlantRecurringIconRepeat,
  myPlantRecurringIconRepeatForm,
  myPlantRecurringTaskConfigure,
  myPlantRecurringTaskConfigureDelete,
  myPlantRecurringTaskConfigureCancel,
  myPlantRecurringTaskConfigureSave,
  myPlantRecurringTaskConfigureDateTimeField,
  myPlantRecurringTaskConfigureRepeatField,
  myPlantRecurringTaskConfigureDayEveryField,
  myPlantRecurringTaskConfigureWeekEveryField,
  myPlantRecurringTaskConfigureWeekDayField,
  myPlantRecurringTaskConfigureMonthEveryField,
  myPlantRecurringTaskConfigureMonthOptionField,
  myPlantRecurringTaskConfigureYearEveryField,

  // Navigation Tabs
  navigationTabs,

  // Notifications screen
  notificationsScreen,
  notificationsList,
}

// ignore: avoid_classes_with_only_static_members
/// Keys functions identifying UI parts.
class KeysFunc {
  // List Screens
  static final StringFunc snackbarAction =
      (String id) => '__snackbar_action_${id}__';

  // Plants
  static final StringFunc myPlantItem = (String id) => 'MyPlantItem__$id';
  static final StringFunc myPlantItemGestureDetector =
      (String id) => 'MyPlantItem__${id}__GestureDetector';
  static final StringFunc myPlantItemName =
      (String id) => 'MyPlantItem__${id}__Name';
  static final StringFunc pictureFrontGrid =
      (String id) => 'MyPlantItem__${id}__pictureFrontGrid';

  // Notifications
  static final StringFunc notificationItem =
      (String id) => 'NotificationItem__$id';
  static final StringFunc notificationItemTitle =
      (String id) => 'NotificationItem__${id}__Title';
}
