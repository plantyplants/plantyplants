// ignore: avoid_classes_with_only_static_members
/// Class of static methods used to validate user input
class Validators {
  static final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );
  static final RegExp _passwordRegExp = RegExp(
    r'^[A-Za-z\d]{6,}$',
  );

  /// Returns true is the email address looks valid.
  static bool isValidEmail(String email) => _emailRegExp.hasMatch(email);

  /// Returns true if the password is longer than 6 characters.
  static bool isValidPassword(String password) =>
      _passwordRegExp.hasMatch(password);
}
