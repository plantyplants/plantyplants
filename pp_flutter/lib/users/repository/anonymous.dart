import '../../my_plants/repository/mock.dart';
import 'interface.dart';
import 'model.dart';

/// Actions that can be performed on an user, implementation for anonymous login
/// (used for integration testing)
class AnonymousUserRepository implements UserRepository {
  /// Returns a [User] after signing-in with Google.
  @override
  Future<User> signInWithGoogle() => Future<User>.value(dummyUser());

  /// Returns a [User] after signing-in with email and password.
  @override
  Future<User> signInWithEmailAndPassword(String email, String password) =>
      Future<User>.value(dummyUser());

  /// Returns a [User] after registering with email and password.
  @override
  Future<User> signUp(String email, String password) =>
      Future<User>.value(dummyUser());

  /// Implements a user signing out, no-op here.
  @override
  Future<Null> signOut() => null;

  /// Implements a user signing in, mocked here.
  @override
  bool isSignedIn() => true;

  /// Returns the signed-in user
  @override
  Future<User> getUser() => Future.value(dummyUser());
}

/// Sample User, used for tests.
User dummyUser() => User(
    uid: authorIdFake, email: 'test@example.com', displayName: 'Test User');
