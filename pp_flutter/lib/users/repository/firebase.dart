import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'interface.dart';
import 'model.dart';

/// Saves device FCM tokens in firestore.
Future<void> saveTokenToDatabase(String token, String userId) async {
  var userSnapQ = FirebaseFirestore.instance
      .collection('users')
      .where('userId', isEqualTo: userId)
      .limit(1);
  var userSnap = await userSnapQ.get();

  if (userSnap.docs.isEmpty) {
    await FirebaseFirestore.instance.collection('users').add({
      'tokens': FieldValue.arrayUnion([token]),
      'userId': userId,
    });
    return;
  }

  await userSnap.docs.first.reference.update({
    'tokens': FieldValue.arrayUnion([token]),
  });
}

/// Actions that can be performed on an user, implementation using Firebase.
class FirebaseUserRepository extends UserRepository {
  /// Global sign-in configuration settings for Google.
  final GoogleSignIn googleSignIn = GoogleSignIn();

  @override
  Future<User> signInWithGoogle() async {
    final googleUser = await googleSignIn.signIn();
    final googleAuth = await googleUser.authentication;
    final credential = firebase_auth.GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    await firebase_auth.FirebaseAuth.instance.signInWithCredential(credential);
    var user = firebase_auth.FirebaseAuth.instance.currentUser;

    var token = await FirebaseMessaging.instance.getToken();

    // Save the initial token to the database
    await saveTokenToDatabase(token, user.uid);

    // Any time the token refreshes, store this in the database too.
    FirebaseMessaging.instance.onTokenRefresh
        .listen((token) => saveTokenToDatabase(token, user.uid));

    return firebaseUserToUser(user);
  }

  @override
  Future<User> signInWithEmailAndPassword(String email, String password) async {
    var authResult = await firebase_auth.FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password);
    return firebaseUserToUser(authResult.user);
  }

  @override
  Future<User> signUp(String email, String password) async {
    var authResult = await firebase_auth.FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password);
    return firebaseUserToUser(authResult.user);
  }

  @override
  Future<Null> signOut() => firebase_auth.FirebaseAuth.instance.signOut();

  @override
  bool isSignedIn() => firebase_auth.FirebaseAuth.instance.currentUser != null;

  @override
  Future<User> getUser() async {
    final user = firebase_auth.FirebaseAuth.instance.currentUser;
    if (user == null) {
      return null;
    }

    var token = await FirebaseMessaging.instance.getToken();
    print('token $token');

    // Save the initial token to the database
    await saveTokenToDatabase(token, user.uid);

    // Any time the token refreshes, store this in the database too.
    FirebaseMessaging.instance.onTokenRefresh
        .listen((token) => saveTokenToDatabase(token, user.uid));

    return User(
        uid: user.uid, email: user.email, displayName: user.displayName);
  }
}

/// Converts a [firebase_auth.User] to a [User]
User firebaseUserToUser(firebase_auth.User user) {
  if (user == null) {
    return null;
  }
  return User(uid: user.uid, email: user.email, displayName: user.displayName);
}
