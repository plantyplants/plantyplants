import 'model.dart';

/// Actions that can be performed on an user.
abstract class UserRepository {
  /// Signing in with Google. Returns signed-in [User].
  Future<User> signInWithGoogle();

  /// Signing in with credentials. Returns signed-in [User].
  Future<User> signInWithEmailAndPassword(String email, String password);

  /// Registering with email and password (credentials).
  /// Returns registered [User].
  Future<User> signUp(String email, String password);

  /// Sign out.
  Future<Null> signOut();

  /// Returns true is the user is currently signed-in.
  bool isSignedIn();

  /// Returns the signed-in [User].
  Future<User> getUser();
}
