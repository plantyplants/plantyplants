part of 'bloc.dart';

/// [LoginEvent] is an [Event] related to the login form processing.
@freezed
abstract class LoginEvent with _$LoginEvent {
  /// Email field in the login form has been changed.
  const factory LoginEvent.emailChanged({@required String email}) =
      EmailChanged;
  /// Password field in the login form has been changed.
  const factory LoginEvent.passwordChanged({@required String password}) =
      PasswordChanged;
  /// Google authentication button in the login form has been clicked/pressed.
  const factory LoginEvent.loginWithGooglePressed() = LoginWithGooglePressed;
  /// Login with credentials button in the login form has been clicked/pressed.
  const factory LoginEvent.loginWithCredentialsPressed(
    {@required String email, @required String password}) =
        LoginWithCredentialsPressed;
}
