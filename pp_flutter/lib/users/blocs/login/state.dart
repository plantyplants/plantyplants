part of 'bloc.dart';

/// [LoginState] is the [State] of the login form processing.
@freezed
abstract class LoginState implements _$LoginState {
  LoginState._();

  /// Create a new LoginState.
  factory LoginState({
    /// True is the email input is passes validity checks.
    @required bool isEmailValid,

    /// True is the email input is passes validity checks.
    @required bool isPasswordValid,

    /// True if the form is being submitted.
    @required bool isSubmitting,

    /// True if the user successfully signed in.
    @required bool isSuccess,

    /// True if there was a failure when signing in.
    @required bool isFailure,
  }) = _LoginState;

  /// True if all the form fields pass validity checks.
  @late
  bool get isFormValid => isEmailValid && isPasswordValid;

  /// Default initial state.
  factory LoginState.empty() => LoginState(
        isEmailValid: true,
        isPasswordValid: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
      );

  /// The form is being processed.
  factory LoginState.loading() => LoginState(
        isEmailValid: true,
        isPasswordValid: true,
        isSubmitting: true,
        isSuccess: false,
        isFailure: false,
      );

  /// The login failed.
  factory LoginState.failure() => LoginState(
        isEmailValid: true,
        isPasswordValid: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: true,
      );

  /// The user successfully logged in.
  factory LoginState.success() => LoginState(
        isEmailValid: true,
        isPasswordValid: true,
        isSubmitting: false,
        isSuccess: true,
        isFailure: false,
      );
}
