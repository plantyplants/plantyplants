import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

import '../../repository/interface.dart';
import '../../repository/validators.dart';

part 'bloc.freezed.dart';
part 'event.dart';
part 'state.dart';

/// [LoginBloc] is a [Bloc] that handles the logging form.
class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository _userRepository;

  /// Initialize with the user repository that has methods to implement sign-in.
  LoginBloc({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  LoginState get initialState => LoginState.empty();

  @override
  Stream<Transition<LoginEvent, LoginState>> transformEvents(
    Stream<LoginEvent> events,
    TransitionFunction<LoginEvent, LoginState> transitionFn,
  ) {
    final nonDebounceStream = events.where(
        (event) => (event is! EmailChanged && event is! PasswordChanged));
    final debounceStream = events
        .where((event) => (event is EmailChanged || event is PasswordChanged))
        .debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
        nonDebounceStream.mergeWith([debounceStream]), transitionFn);
  }

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    yield* event.map(
      emailChanged: (event) async* {
        yield* _mapEmailChangedToState(event.email);
      },
      passwordChanged: (event) async* {
        yield* _mapPasswordChangedToState(event.password);
      },
      loginWithGooglePressed: (_event) async* {
        yield* _mapLoginWithGooglePressedToState();
      },
      loginWithCredentialsPressed: (event) async* {
        yield* _mapLoginWithCredentialsPressedToState(
          email: event.email,
          password: event.password,
        );
      },
    );
  }

  Stream<LoginState> _mapEmailChangedToState(String email) async* {
    yield state.copyWith(
      isEmailValid: Validators.isValidEmail(email),
    );
  }

  Stream<LoginState> _mapPasswordChangedToState(String password) async* {
    yield state.copyWith(
      isPasswordValid: Validators.isValidPassword(password),
    );
  }

  Stream<LoginState> _mapLoginWithGooglePressedToState() async* {
    yield LoginState.loading();
    try {
      await _userRepository.signInWithGoogle();
      yield LoginState.success();
      // ignore: avoid_catches_without_on_clauses
    } catch (_) {
      // TODO: Implement logging
      yield LoginState.failure();
    }
  }

  Stream<LoginState> _mapLoginWithCredentialsPressedToState({
    String email,
    String password,
  }) async* {
    yield LoginState.loading();
    try {
      await _userRepository.signInWithEmailAndPassword(email, password);
      yield LoginState.success();
      // ignore: avoid_catches_without_on_clauses
    } catch (_) {
      // TODO: Implement logging
      yield LoginState.failure();
    }
  }
}
