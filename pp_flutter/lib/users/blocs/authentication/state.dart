part of 'bloc.dart';

/// [AuthenticationState] is a [State] during the sign-in process.
@freezed
abstract class AuthenticationState with _$AuthenticationState {
  /// The authentication is uninitialized.
  const factory AuthenticationState.uninitialized() = Uninitialized;
  /// User is authenticated.
  const factory AuthenticationState.authenticated(User user) =
      Authenticated;
  /// User is unauthenticated.
  const factory AuthenticationState.unauthenticated() = Unauthenticated;
}
