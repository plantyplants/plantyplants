import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

import '../../repository/interface.dart';
import '../../repository/model.dart';

part 'bloc.freezed.dart';
part 'event.dart';
part 'state.dart';

/// [AuthenticationBloc] is a [Bloc] that handles sign-in.
class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository _userRepository;

  /// Initialize with a UserRepository that implements Google-based sign-in.
  AuthenticationBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  AuthenticationState get initialState => Uninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    yield* event.map(
      appStarted: (_event) async* {
        yield* _mapAppStartedToState();
      },
      loggedIn: (event) async* {
        yield* _mapLoggedInToState();
      },
      loggedOut: (event) async* {
        yield* _mapLoggedOutToState();
      },
    );
  }

  Stream<AuthenticationState> _mapAppStartedToState() async* {
    User user;
    try {
      final isSignedIn = _userRepository.isSignedIn();
      if (!isSignedIn) {
        user = await _userRepository.signInWithGoogle();
      } else {
        user = await _userRepository.getUser();
      }
      if (user == null) {
        yield Unauthenticated();
      } else {
        yield Authenticated(user);
      }
      // ignore: avoid_catches_without_on_clauses
    } catch (err) {
      // TODO: add logging
      yield Unauthenticated();
    }
  }

  Stream<AuthenticationState> _mapLoggedInToState() async* {
    yield Authenticated(await _userRepository.getUser());
  }

  Stream<AuthenticationState> _mapLoggedOutToState() async* {
    yield Unauthenticated();
    _userRepository.signOut();
  }
}
