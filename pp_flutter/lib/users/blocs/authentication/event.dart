part of 'bloc.dart';

/// [AuthenticationEvent] is an [Event] related to sign-in.
@freezed
abstract class AuthenticationEvent with _$AuthenticationEvent {
  /// App starts.
  const factory AuthenticationEvent.appStarted() = AppStarted;
  /// User logs in.
  const factory AuthenticationEvent.loggedIn() = LoggedIn;
  /// User logs out.
  const factory AuthenticationEvent.loggedOut() = LoggedOut;
}
