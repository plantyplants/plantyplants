part of 'bloc.dart';

/// [RegisterState] is the [State] of the registration form processing.
@freezed
abstract class RegisterState implements _$RegisterState {
  RegisterState._();

  /// Create a new RegisterState.
  factory RegisterState({
    /// True is the email input is passes validity checks.
    @required bool isEmailValid,

    /// True is the email input is passes validity checks.
    @required bool isPasswordValid,

    /// True if the form is being submitted.
    @required bool isSubmitting,

    /// True if the user successfully signed in.
    @required bool isSuccess,

    /// True if there was a failure when signing in.
    @required bool isFailure,
  }) = _RegisterState;

  /// True if all the form fields pass validity checks.
  @late
  bool get isFormValid => isEmailValid && isPasswordValid;

  /// Default initial state.
  factory RegisterState.empty() => RegisterState(
        isEmailValid: true,
        isPasswordValid: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
      );

  /// The form is being processed.
  factory RegisterState.loading() => RegisterState(
        isEmailValid: true,
        isPasswordValid: true,
        isSubmitting: true,
        isSuccess: false,
        isFailure: false,
      );

  /// The registration failed.
  factory RegisterState.failure() => RegisterState(
        isEmailValid: true,
        isPasswordValid: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: true,
      );

  /// The registration succeeded.
  factory RegisterState.success() => RegisterState(
        isEmailValid: true,
        isPasswordValid: true,
        isSubmitting: false,
        isSuccess: true,
        isFailure: false,
      );
}
