part of 'bloc.dart';

/// [RegisterEvent] is an [Event] related to the registration form.
@freezed
abstract class RegisterEvent with _$RegisterEvent {
  /// The email from the registration form was changed.
  const factory RegisterEvent.emailChanged({@required String email}) =
      EmailChanged;
  /// The password from the registration form was changed.
  const factory RegisterEvent.passwordChanged({@required String password}) =
      PasswordChanged;
  /// The registration form was submitted for processing.
  const factory RegisterEvent.submitted(
      {@required String email, @required String password}) = Submitted;
}
