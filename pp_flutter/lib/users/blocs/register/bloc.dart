import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

import '../../repository/interface.dart';
import '../../repository/validators.dart';

part 'bloc.freezed.dart';
part 'event.dart';
part 'state.dart';

/// [RegisterBloc] processes the registration form.
class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final UserRepository _userRepository;

  /// Initialize with the user repository that implements persisting the
  /// registration.
  RegisterBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  RegisterState get initialState => RegisterState.empty();

  @override
  Stream<Transition<RegisterEvent, RegisterState>> transformEvents(
    Stream<RegisterEvent> events,
    TransitionFunction<RegisterEvent, RegisterState> transitionFn,
  ) {
    final nonDebounceStream = events.where(
        (event) => (event is! EmailChanged && event is! PasswordChanged));
    final debounceStream = events
        .where((event) => (event is EmailChanged || event is PasswordChanged))
        .debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
        nonDebounceStream.mergeWith([debounceStream]), transitionFn);
  }

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    yield* event.map(
      emailChanged: (event) async* {
        yield* _mapEmailChangedToState(event.email);
      },
      passwordChanged: (event) async* {
        yield* _mapPasswordChangedToState(event.password);
      },
      submitted: (event) async* {
        yield* _mapFormSubmittedToState(event.email, event.password);
      },
    );
  }

  Stream<RegisterState> _mapEmailChangedToState(String email) async* {
    yield state.copyWith(
      isEmailValid: Validators.isValidEmail(email),
    );
  }

  Stream<RegisterState> _mapPasswordChangedToState(String password) async* {
    yield state.copyWith(
      isPasswordValid: Validators.isValidPassword(password),
    );
  }

  Stream<RegisterState> _mapFormSubmittedToState(
    String email,
    String password,
  ) async* {
    yield RegisterState.loading();
    try {
      await _userRepository.signUp(email, password);
      yield RegisterState.success();
      // ignore: avoid_catches_without_on_clauses
    } catch (_) {
      // TODO: implement logging
      yield RegisterState.failure();
    }
  }
}
